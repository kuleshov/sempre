package edu.stanford.nlp.sempre.regex;

import edu.stanford.nlp.sempre.*;

import dk.brics.automaton.Automaton;
import dk.brics.automaton.RegExp;

/**
 * Used to evaluate regular expressions.
 * In regular expressions, all final values are String values,
 * which are compared using DFA equivalence.
 *
 * @author Yushi Wang
 */

public class RegexValueEvaluator implements ValueEvaluator {

  public double getCompatibility(Value t, Value p) {

    // In regexes, both values should always be strings
    if (t instanceof StringValue && p instanceof StringValue) {
      StringValue target = (StringValue) t;
      StringValue pred = (StringValue) p;
      // TODO(alex): hack for checking for large numbers
      boolean number1 = pred.value.matches(".*\\{[0-9]{2}.*") || pred.value.matches(".*[0-9]{2}\\}.*");
      boolean number2 = target.value.matches(".*\\{[0-9]{2}.*") || target.value.matches(".*[0-9]{2}\\}.*");
      if (number1 ^ number2) return 0.0;
      RegExp predRegexp = new RegExp(pred.value);
      RegExp targetRegexp = new RegExp(target.value);
      Automaton predDfa = predRegexp.toAutomaton();
      Automaton targetDfa = targetRegexp.toAutomaton();
      return predDfa.equals(targetDfa) ? 1 : 0;
    } else {  // This should never happen
      throw new RuntimeException("Illegal value type in RegexValueEvaluator");
    }
  }
}
