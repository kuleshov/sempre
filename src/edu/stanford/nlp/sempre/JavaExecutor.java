package edu.stanford.nlp.sempre;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fig.basic.MapUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * JavaExecutor takes a Formula which is composed recursively of CallFormulas,
 * does reflection, and returns a Value.
 *
 * @author Percy Liang
 */
public class JavaExecutor extends Executor {
  private static JavaExecutor defaultExecutor = new JavaExecutor();

  // To simplify logical forms, define some shortcuts.
  private Map<String, String> shortcuts = Maps.newHashMap();

  public JavaExecutor() {
    String className = BasicFunctions.class.getName();

    shortcuts.put("+", className + ".plus");
    shortcuts.put("-", className + ".minus");
    shortcuts.put("*", className + ".times");
    shortcuts.put("/", className + ".divide");
    shortcuts.put("%", className + ".mod");
    shortcuts.put("!", className + ".not");

    shortcuts.put("<", className + ".lessThan");
    shortcuts.put("<=", className + ".lessThanEq");
    shortcuts.put("==", className + ".equals");
    shortcuts.put(">", className + ".greaterThan");
    shortcuts.put(">=", className + ".greaterThanEq");

    shortcuts.put("if", className + ".ifThenElse");
    shortcuts.put("map", className + ".map");
    shortcuts.put("reduce", className + ".reduce");
    shortcuts.put("select", className + ".select");
    shortcuts.put("range", className + ".range");

    // Unclear if these should go in this class
    shortcuts.put("sparql", className + ".sparql");
    shortcuts.put("www", className + ".www");
  }

  public static class BasicFunctions {
    public static double plus(double x, double y) { return x + y; }
    public static int plus(int x, int y) { return x + y; }
    public static int minus(int x, int y) { return x - y; }
    public static double minus(double x, double y) { return x - y; }
    public static int times(int x, int y) { return x * y; }
    public static double times(double x, double y) { return x * y; }
    public static int divide(int x, int y) { return x / y; }
    public static double divide(double x, double y) { return x / y; }
    public static int mod(int x, int y) { return x % y; }
    public static boolean not(boolean x) { return !x; }

    public static boolean lessThan(double x, double y) { return x < y; }
    public static boolean lessThanEq(double x, double y) { return x <= y; }
    public static boolean equals(double x, double y) { return x == y; }
    public static boolean greaterThan(double x, double y) { return x > y; }
    public static boolean greaterThanEq(double x, double y) { return x >= y; }

    public static Object ifThenElse(boolean b, Object x, Object y) { return b ? x : y; }

    // For very simple string concatenation
    public static StringValue plus(String a, String b) { return new StringValue(a + b); }
    public static StringValue plus(String a, String b, String c) {
      return new StringValue(a + b + c);
    }
    public static StringValue plus(String a, String b, String c, String d) {
      return new StringValue(a + b + c + d);
    }
    public static StringValue plus(String a, String b, String c, String d, String e) {
      return new StringValue(a + b + c + d + e);
    }
    private static String toString(Object x) {
      if (x instanceof String)
        return (String) x;
      else if (x instanceof Value)
        return (x instanceof NameValue) ? ((NameValue) x).id : ((StringValue) x).value;
      else
        return null;
    }

    // Regex functions - *accept regex as string*, outputs: 1/0 t/f, -1 = regex error
    public static int matches(String s, String rgx) {
      try {
        Pattern p = Pattern.compile(rgx);
        Matcher m = p.matcher(s);
        return m.matches() ? 1 : 0;
      } catch (Exception e) {
        return -1;
      }
    }
    public static int matches(Object s, Object rgx) {
      return matches(toString(s), toString(rgx));
    }

    // Apply func to each element of |list| and return the resulting list.
    public static List<Object> map(List<Object> list, LambdaFormula func) {
      List<Object> newList = new ArrayList<Object>();
      for (Object elem : list) {
        Object newElem = apply(func, elem);
        newList.add(newElem);
      }
      return newList;
    }

    public static List<Object> reduce(List<Object> list, LambdaFormula func) {
      // TODO(pliang): implement this (need to define a version of apply that takes two arguments)
      throw new RuntimeException("Not supported");
    }

    // Return elements x of |list| such that func(x) is true.
    public static List<Object> select(List<Object> list, LambdaFormula func) {
      List<Object> newList = new ArrayList<Object>();
      for (Object elem : list) {
        Object test = apply(func, elem);
        if ((Boolean) test)
          newList.add(elem);
      }
      return newList;
    }

    private static Object apply(LambdaFormula func, Object x) {
      // Apply the function func to x.  In order to do that, need to convert x into a value.
      Formula formula = Formulas.lambdaApply(func, new ValueFormula<Value>(toValue(x)));
      return defaultExecutor.processFormula(formula);
    }

    public static List<Integer> range(int start, int end) {
      List<Integer> result = new ArrayList<Integer>();
      for (int i = start; i < end; i++)
        result.add(i);
      return result;
    }
  }

  public Response execute(Formula formula, ContextValue context) {
    formula = Formulas.betaReduction(formula);
    try {
      return new Response(toValue(processFormula(formula)));
    } catch (Exception e) {
      e.printStackTrace();
      return new Response(ErrorValue.badJava(e.toString()));
    }
  }

  private Object processFormula(Formula formula) {
    if (formula instanceof ValueFormula)  // Unpack value and convert to object (e.g., for ints)
      return toObject(((ValueFormula) formula).value);

    if (formula instanceof CallFormula) {  // Invoke the function.
      // Recurse
      CallFormula call = (CallFormula) formula;
      Object func = processFormula(call.func);
      List<Object> args = Lists.newArrayList();
      for (Formula arg : call.args) {
        args.add(processFormula(arg));
      }

      if (!(func instanceof NameValue))
        throw new RuntimeException("Invalid func: " + call.func + " => " + func);

      String id = ((NameValue) func).id;
      id = MapUtils.get(shortcuts, id, id);

      if (id.startsWith("."))  // Instance method
        return invoke(id.substring(1), args.get(0), args.subList(1, args.size()).toArray(new Object[0]));
      else  // Static method
        return invoke(id, null, args.toArray(new Object[0]));
    }

    // Just pass it through...
    return formula;
  }

  // Convert the object back to a value
  private static Value toValue(Object obj) {
    if (obj instanceof Value) return (Value) obj;
    if (obj instanceof Boolean) return new BooleanValue((Boolean) obj);
    if (obj instanceof Integer) return new NumberValue(((Integer) obj).intValue());
    if (obj instanceof Double) return new NumberValue(((Double) obj).doubleValue());
    if (obj instanceof String) return new StringValue((String) obj);
    if (obj instanceof List) {
      List<Value> list = Lists.newArrayList();
      for (Object elem : (List) obj)
        list.add(toValue(elem));
      return new ListValue(list);
    }
    throw new RuntimeException("Invalid object: " + obj);
  }

  private static Object toObject(Value value) {
    if (value instanceof NumberValue) {
      // Unfortunately, we don't make a distinction between ints and doubles, so this is a hack.
      double x = ((NumberValue) value).value;
      if (x == (int) x) return new Integer((int) x);
      return new Double((int) x);
    } else if (value instanceof BooleanValue) {
      return ((BooleanValue) value).value;
    } else if (value instanceof StringValue) {
      return ((StringValue) value).value;
    } else if (value instanceof ListValue) {
      List<Object> list = Lists.newArrayList();
      for (Value elem : ((ListValue) value).values)
        list.add(toObject(elem));
      return list;
    } else if (value instanceof NameValue) {
      return value;  // Preserve
    } else {
      throw new RuntimeException("Unhandled: " + value);
    }
  }

  // Example: id = "Math.cos"
  private Object invoke(String id, Object thisObj, Object[] args) {
    Method[] methods;
    Class<?> cls;
    String methodName;
    boolean isStatic = thisObj == null;

    if (isStatic) {  // Static methods
      int i = id.lastIndexOf('.');
      if (i == -1) {
        throw new RuntimeException("Expected <class>.<method>, but got: " + id);
      }
      String className = id.substring(0, i);
      methodName = id.substring(i + 1);

      try {
        cls = Class.forName(className);
      } catch (ClassNotFoundException e) {
        throw new RuntimeException(e);
      }
      methods = cls.getMethods();
    } else {  // Instance methods
      cls = thisObj.getClass();
      methodName = id;
      methods = cls.getMethods();
    }

    // Find a suitable method
    List<Method> nameMatches = Lists.newArrayList();
    Method bestMethod = null;
    int bestCost = INVALID_TYPE_COST;
    for (Method m : methods) {
      if (!m.getName().equals(methodName)) continue;
      nameMatches.add(m);
      if (isStatic != Modifier.isStatic(m.getModifiers())) continue;
      int cost = typeCastCost(m.getParameterTypes(), args);
      if (cost < bestCost) {
        bestCost = cost;
        bestMethod = m;
      }
    }

    if (bestMethod != null) {
      try {
        return bestMethod.invoke(thisObj, args);
      } catch (InvocationTargetException e) {
        throw new RuntimeException(e.getCause());
      } catch (IllegalAccessException e) {
        throw new RuntimeException(e);
      }
    }
    List<String> types = Lists.newArrayList();
    for (Object arg : args)
      types.add(arg.getClass().toString());
    throw new RuntimeException("Method " + methodName + " not found in class " + cls + " with arguments " + Arrays.asList(args) + " having types " + types + "; candidates: " + nameMatches);
  }

  private int typeCastCost(Class[] types, Object[] args) {
    if (types.length != args.length) return INVALID_TYPE_COST;
    int cost = 0;
    for (int i = 0; i < types.length; i++) {
      cost += typeCastCost(types[i], args[i]);
      if (cost >= INVALID_TYPE_COST) {
        // LogInfo.dbgs("NOT COMPATIBLE: want %s, got %s with type %s", types[i], args[i], args[i].getClass());
        break;
      }
    }
    return cost;
  }

  // Return whether the object |arg| is compatible with |type|.
  // 0: perfect match
  // 1: don't match, but don't lose anything
  // 2: don't match, and can lose something
  // INVALID_TYPE_COST: impossible
  private int typeCastCost(Class<?> type, Object arg) {
    if (arg == null) return !type.isPrimitive() ? 0 : INVALID_TYPE_COST;
    if (type.isInstance(arg)) return 0;
    if (type == Boolean.TYPE) return arg instanceof Boolean ? 0 : INVALID_TYPE_COST;
    else if (type == Integer.TYPE) {
      if (arg instanceof Integer) return 0;
      if (arg instanceof Long) return 1;
      return INVALID_TYPE_COST;
    }
    if (type == Long.TYPE) {
      if (arg instanceof Integer) return 1;
      if (arg instanceof Long) return 0;
      return INVALID_TYPE_COST;
    }
    if (type == Float.TYPE) {
      if (arg instanceof Integer) return 1;
      if (arg instanceof Long) return 1;
      if (arg instanceof Float) return 0;
      if (arg instanceof Double) return 2;
      return INVALID_TYPE_COST;
    }
    if (type == Double.TYPE) {
      if (arg instanceof Integer) return 1;
      if (arg instanceof Long) return 1;
      if (arg instanceof Float) return 1;
      if (arg instanceof Double) return 0;
      return INVALID_TYPE_COST;
    }
    return INVALID_TYPE_COST;
  }

  private static final int INVALID_TYPE_COST = 1000;
}
