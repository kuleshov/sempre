package edu.stanford.nlp.sempre;

import java.util.Iterator;

/**
 * Represents a stream of Derivations which are constructed lazily for efficiency.
 * Use either SingleDerivationStream or MultipleDerivationStream.
 * Created by joberant on 3/14/14.
 */
public interface DerivationStream extends Iterator<Derivation> {
  Derivation peek();
  int estimatedSize();
}

// TODO(pliang): replace this with SingleDerivationStream
class SingleDerivIterator implements DerivationStream {

  private Derivation singleDeriv;
  private boolean hasNext = true;

  public SingleDerivIterator(Derivation deriv) {
    singleDeriv = deriv;
  }

  @Override
  public boolean hasNext() {
    return hasNext;
  }

  public Derivation peek() {
    return singleDeriv;
  }

  @Override
  public Derivation next() {
    if (hasNext) {
      hasNext = false;
      return singleDeriv;
    }
    return null;
  }

  @Override
  public void remove() {
    throw new RuntimeException("SingleDerivIterator does not support the remove operation");
  }

  @Override
  public int estimatedSize() {
    return 1;
  }
}
