package edu.stanford.nlp.sempre.tables;

import java.util.List;

import edu.stanford.nlp.sempre.*;
import fig.basic.LogInfo;
import fig.basic.Option;

/**
 * Return 1 if |pred| and |target| represent the same list and 0 otherwise.
 *
 * This is similar to FreebaseValueEvaluator, but does not give partial credits.
 *
 * @author ppasupat
 */
public class TableValueEvaluator implements ValueEvaluator {
  public static class Options {
    @Option(gloss = "Allow matching on normalized strings")
    public boolean allowNormalizedStringMatch = false;
    @Option(gloss = "Allow matching unitless numbers")
    public boolean allowUnitlessNumberMatch = false;
  }
  public static Options opts = new Options();

  public double getCompatibility(Value target, Value pred) {
    List<Value> targetList = ((ListValue) target).values;
    if (!(pred instanceof ListValue)) return 0;
    List<Value> predList = ((ListValue) pred).values;

    if (targetList.size() != predList.size()) return 0;

    for (Value targetValue : targetList) {
      boolean found = false;
      for (Value predValue : predList) {
        if (getItemCompatibility(targetValue, predValue)) {
          found = true;
          break;
        }
      }
      if (!found) return 0;
    }
    return 1;
  }

  // Compare one element of the list.
  public boolean getItemCompatibility(Value target, Value pred) {
    if (pred instanceof ErrorValue) return false;  // Never award points for error
    if (pred == null) {
      LogInfo.warning("Predicted value is null!");
      return false;
    }

    if (target instanceof DescriptionValue) {
      String trueText = ((DescriptionValue) target).value;
      // Just has to match the description
      if (pred instanceof NameValue) {
        String predText = ((NameValue) pred).description;
        if (predText == null) predText = "";
        if (opts.allowNormalizedStringMatch) {
          trueText = StringNormalizationUtils.aggressiveNormalize(trueText);
          predText = StringNormalizationUtils.aggressiveNormalize(predText);
        }
        return trueText.equals(predText);
      } else if (pred instanceof NumberValue && opts.allowUnitlessNumberMatch) {
        NumberValue trueNumber = StringNormalizationUtils.parseNumberLenient(trueText);
        return trueNumber != null && trueNumber.equals(pred);
      }
      return false;
    }

    if (target instanceof NumberValue) {
      // Try converting NameValue into NumberValue
      if (pred instanceof NameValue) {
        return target.equals(StringNormalizationUtils.nameValueToNumberValue((NameValue) pred));
      }
    }

    if (target.getClass() != pred.getClass()) return false;

    if (target instanceof DateValue) {
      DateValue targetDate = (DateValue) target;
      DateValue predDate = (DateValue) pred;
      // Only comparing the year right now!  This is crude.
      return (targetDate.year == predDate.year);
    }

    return target.equals(pred);
  }

}
