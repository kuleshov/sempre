package edu.stanford.nlp.sempre.tables;

import java.util.*;

import edu.stanford.nlp.sempre.*;
import fig.basic.*;

/**
 * Prune derivations from DPParser.
 *
 * @author ppasupat
 */

public class DPParserPruner {
  public static class Options {
    @Option public List<String> pruningStrategies = new ArrayList<>();
    @Option public int pruningVerbosity = 0;
  }
  public static Options opts = new Options();

  public static final Set<String> allowedPruningStrategies = new HashSet<String>(Arrays.asList(
      "emptyDenotation",
      "forwardBackward",
      "doubleNext"
      ));

  public final Parser parser;
  public final Example ex;

  public DPParserPruner(ParserState parserState) {
    // Validate the pruning strategies
    for (String strategy : opts.pruningStrategies) {
      if (!allowedPruningStrategies.contains(strategy)) {
        throw new RuntimeException("DPParserPruner: Unrecognized pruning strategy: " + strategy);
      }
    }
    this.parser = parserState.parser;
    this.ex = parserState.ex;
  }

  public boolean isPruned(Derivation deriv) {
    if (opts.pruningStrategies.isEmpty()) return false;
    return pruneJoins(deriv)
        || pruneEmpty(deriv);
  }

  // ============================================================
  // Formula-based Pruning
  // ============================================================

  /**
   * Consider strings of joins such as (relation1 (relation2 (...)))
   *
   * - forwardBackward: prune (!relation (relation (...)))
   * - doubleNext: prune (next (next (...))), (!next (next (...))), etc.
   */
  private boolean pruneJoins(Derivation deriv) {
    Formula formula = deriv.formula, current = formula;
    String rid1 = null, rid2 = null;
    while (current instanceof JoinFormula) {
      rid2 = rid1;
      rid1 = getRelationIdIfPossible(((JoinFormula) current).relation);
      if (rid1 != null && rid2 != null) {
        // Prune (!relation (relation (...)))
        if (opts.pruningStrategies.contains("forwardBackward")) {
          if (rid1.equals("!" + rid2) || rid2.equals("!" + rid1)) {
            if (opts.pruningVerbosity >= 2)
              LogInfo.logs("PRUNED [forwardBackward] %s", formula);
            return true;
          }
        }
        // Prune (next (next (...)))
        if (opts.pruningStrategies.contains("doubleNext")) {
          if ((rid1.equals("fb:row.row.next") && rid2.equals("fb:row.row.next")) ||
              (rid1.equals("!fb:row.row.next") && rid2.equals("!fb:row.row.next"))) {
            if (opts.pruningVerbosity >= 2)
              LogInfo.logs("PRUNED [doubleNext] %s", formula);
            return true;
          }
        }
      }
      current = ((JoinFormula) current).child;
    }
    return false;
  }

  /**
   * Convert formula to relation id if possible.
   */
  private String getRelationIdIfPossible(Formula formula) {
    if (formula instanceof ReverseFormula) {
      String childId = getRelationIdIfPossible(((ReverseFormula) formula).child);
      if (childId != null && !childId.isEmpty()) {
        return childId.charAt(0) == '!' ? childId.substring(1) : "!" + childId;
      }
    } else if (formula instanceof ValueFormula) {
      Value v = ((ValueFormula<?>) formula).value;
      if (v instanceof NameValue) {
        return ((NameValue) v).id;
      }
    }
    return null;
  }

  // ============================================================
  // Denotation-based Pruning
  // ============================================================

  /**
   * Prune empty denotations.
   *
   * Errors are not counted as empty.
   */
  private boolean pruneEmpty(Derivation deriv) {
    Formula formula = deriv.formula;
    if (opts.pruningStrategies.contains("emptyDenotation")) {
      deriv.ensureExecuted(parser.executor, ex.context);
      if (deriv.value instanceof ListValue) {
        if (((ListValue) deriv.value).values.isEmpty()) {
          if (opts.pruningVerbosity >= 3)
            LogInfo.logs("PRUNED [emptyDenotation] %s", formula);
          return true;
        }
      }
    }
    return false;
  }

}
