package edu.stanford.nlp.sempre.tables;

import java.util.*;

import edu.stanford.nlp.sempre.*;
import edu.stanford.nlp.sempre.tables.TableKnowledgeGraph.TableCell;
import edu.stanford.nlp.sempre.tables.TableKnowledgeGraph.TableColumn;
import fig.basic.MapUtils;
import fig.basic.Option;

/**
 * Perform fuzzy matching on the table knowledge graph.
 *
 * @author ppasupat
 */
public class FuzzyMatcher {
  public static class Options {
    // This would prevent "canada ?" from fuzzy matching: we already fuzzy match "canada"
    @Option(gloss = "Ignore query strings where a boundary word is a  punctuation (prevent overgeneration)")
    public boolean ignorePunctuationBoundedQueries = false;
  }
  public static Options opts = new Options();

  public final TableKnowledgeGraph graph;

  public FuzzyMatcher(TableKnowledgeGraph graph) {
    this.graph = graph;
    precomputeForFuzzyMatching();
  }

  // Map normalized strings to Values
  // ENTITIY --> ValueFormula fb:cell.___ or other primitive format
  //   UNARY --> JoinFormula (type fb:column.___)
  //  BINARY --> ValueFormula fb:row.row.___
  Set<Formula> allEntityFormulas, allUnaryFormulas, allBinaryFormulas;
  Map<String, Set<Formula>> phraseToEntityFormulas, phraseToUnaryFormulas, phraseToBinaryFormulas;

  protected void precomputeForFuzzyMatching() {
    allEntityFormulas = new HashSet<>();
    allUnaryFormulas = new HashSet<>();
    allBinaryFormulas = new HashSet<>();
    phraseToEntityFormulas = new HashMap<>();
    phraseToUnaryFormulas = new HashMap<>();
    phraseToBinaryFormulas = new HashMap<>();
    String collapsedOriginal, collapsedNormalized;
    for (TableColumn column : graph.columns) {
      collapsedOriginal = StringNormalizationUtils.collapseNormalize(column.originalString);
      collapsedNormalized = StringNormalizationUtils.collapseNormalize(StringNormalizationUtils.aggressiveNormalize(column.originalString));
      // unary
      Formula unary = new JoinFormula(
          new ValueFormula<>(KnowledgeGraph.getReversedPredicate(column.propertyNameValue)),
          new JoinFormula(new ValueFormula<>(new NameValue(CanonicalNames.TYPE)),
                          new ValueFormula<>(new NameValue(TableTypeSystem.ROW_TYPE)))
      );
      allUnaryFormulas.add(unary);
      MapUtils.addToSet(phraseToUnaryFormulas, collapsedOriginal, unary);
      MapUtils.addToSet(phraseToUnaryFormulas, collapsedNormalized, unary);
      // binary
      Formula binary = new ValueFormula<>(column.propertyNameValue);
      allBinaryFormulas.add(binary);
      MapUtils.addToSet(phraseToBinaryFormulas, collapsedOriginal, binary);
      MapUtils.addToSet(phraseToBinaryFormulas, collapsedNormalized, binary);
      // entity
      for (TableCell cell : column.children) {
        collapsedOriginal = StringNormalizationUtils.collapseNormalize(cell.properties.originalString);
        collapsedNormalized = StringNormalizationUtils.collapseNormalize(StringNormalizationUtils.aggressiveNormalize(cell.properties.originalString));
        Formula entity = new ValueFormula<>(cell.properties.entityNameValue);
        allEntityFormulas.add(entity);
        MapUtils.addToSet(phraseToEntityFormulas, collapsedOriginal, entity);
        MapUtils.addToSet(phraseToEntityFormulas, collapsedNormalized, entity);
      }
    }
  }

  boolean checkPunctuationBoundaries(String term) {
    String[] tokens = term.trim().split("\\s+");
    if (tokens.length == 0) return false;
    if (StringNormalizationUtils.collapseNormalize(tokens[0]).isEmpty()) return false;
    if (tokens.length == 1) return true;
    if (StringNormalizationUtils.collapseNormalize(tokens[tokens.length - 1]).isEmpty()) return false;
    return true;
  }

  // TODO(ice): Allow some edit distance and lexicon mismatch
  public Collection<Formula> getFuzzyMatchedFormulas(String term, FuzzyMatchFn.FuzzyMatchFnMode mode) {
    if (opts.ignorePunctuationBoundedQueries && !checkPunctuationBoundaries(term)) return Collections.emptySet();
    String normalized = StringNormalizationUtils.collapseNormalize(term);
    Set<Formula> answer;
    switch (mode) {
      case ENTITY: answer = phraseToEntityFormulas.get(normalized); break;
      case UNARY:  answer = phraseToUnaryFormulas.get(normalized);  break;
      case BINARY: answer = phraseToBinaryFormulas.get(normalized); break;
      default: throw new RuntimeException("Unknown FuzzyMatchMode " + mode);
    }
    return (answer == null) ? Collections.emptySet() : answer;
  }

  public Collection<Formula> getAllFormulas(FuzzyMatchFn.FuzzyMatchFnMode mode) {
    switch (mode) {
      case ENTITY: return allEntityFormulas;
      case UNARY:  return allUnaryFormulas;
      case BINARY: return allBinaryFormulas;
      default: throw new RuntimeException("Unknown FuzzyMatchMode " + mode);
    }
  }

}
