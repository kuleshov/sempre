package edu.stanford.nlp.sempre.tables.test;

import java.io.*;
import java.util.*;

import edu.stanford.nlp.sempre.*;
import edu.stanford.nlp.sempre.tables.TableKnowledgeGraph;
import edu.stanford.nlp.sempre.tables.test.CustomExample.ExampleProcessor;
import fig.basic.*;
import fig.exec.Execution;

public class AnnotatedFormulasChecker implements Runnable {

  public static void main(String[] args) {
    Execution.run(args, "AnnotatedFormulasChecker", new AnnotatedFormulasChecker(), Master.getOptionsParser());
  }

  @Override
  public void run() {
    AnnotatedFormulaProcessor processor = new AnnotatedFormulaProcessor();
    CustomExample.getDataset(Dataset.opts.inPaths, processor);
    processor.summarize();
  }

  static class AnnotatedFormulaProcessor implements ExampleProcessor {
    int n = 0, annotated = 0, oracle = 0;
    final Builder builder;

    public AnnotatedFormulaProcessor() {
      builder = new Builder();
      builder.build();
    }

    @Override
    public void run(CustomExample ex) {
      n++;
      if (ex.targetFormula == null) return;
      annotated++;
      LogInfo.begin_track("Testing %s", ex.id);
      StopWatch watch = new StopWatch();
      watch.start();
      LogInfo.logs("TRUE: %s", ex.targetValue);
      double result = 0;
      try {
        LogInfo.logs("Inferred Type: %s", TypeInference.inferType(ex.targetFormula));
        Value pred = builder.executor.execute(ex.targetFormula, ex.context).value;
        if (pred instanceof ListValue)
          pred = addOriginalStrings((ListValue) pred, (TableKnowledgeGraph) ex.context.graph);
        LogInfo.logs("Example %s: %s", ex.id, ex.getTokens());
        LogInfo.logs("  targetFormula: %s", ex.targetFormula);
        LogInfo.logs("TRUE: %s", ex.targetValue);
        LogInfo.logs("PRED: %s", pred);
        result = builder.valueEvaluator.getCompatibility(ex.targetValue, pred);
        if (result != 1) {
          LogInfo.warnings("TRUE != PRED. %s Either targetValue or targetFormula is wrong.", ex.id);
        }
      } catch (Exception e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        LogInfo.logs("Example %s: %s", ex.id, ex.getTokens());
        LogInfo.logs("  targetFormula: %s", ex.targetFormula);
        LogInfo.logs("TRUE: %s", ex.targetValue);
        LogInfo.logs("PRED: ERROR %s\n%s", e, sw);
        LogInfo.warnings("TRUE != PRED. %s Something was wrong during the execution.", ex.id);
      }
      watch.stop();
      LogInfo.logs("Parse Time: %s", watch);
      LogInfo.end_track();
      if (result == 1) oracle++;
    }

    // Add original strings to each NameValue in ListValue
    ListValue addOriginalStrings(ListValue answers, TableKnowledgeGraph graph) {
      List<Value> values = new ArrayList<>();
      for (Value value : answers.values) {
        if (value instanceof NameValue) {
          NameValue name = (NameValue) value;
          if (name.description == null)
            value = new NameValue(name.id, graph.getOriginalString(((NameValue) value).id));
        }
        values.add(value);
      }
      return new ListValue(values);
    }

    public void summarize() {
      LogInfo.logs("N = %d | Annotated = %d | Oracle = %d", n, annotated, oracle);
      Execution.putOutput("train.oracle.mean", oracle * 1.0 / n);
      Execution.putOutput("train.correct.count", n);
    }

  }

}
