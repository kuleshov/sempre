package edu.stanford.nlp.sempre.freebase.test;

import java.util.*;
import org.testng.annotations.Test;
import fig.basic.*;
import edu.stanford.nlp.sempre.*;
import edu.stanford.nlp.sempre.freebase.*;

/**
 * Test LambdaCalculusConverter using examples taken from geoquery training data
 *
 * @author Ziang Xie
 */

public class LambdaCalculusConverterTest extends SparqlExecutorTest {

  public static ValuesChecker contains(String expected) {
    final Value expectedValue = Value.fromString(expected);
    return new ValuesChecker() {
      public void checkValues(List<Value> values) {
        boolean passed = false;
        for (Value v : values) {
          if (v.equals(expectedValue)) {
            passed = true;
            break;
          }
        }
        if (!passed)
          throw new RuntimeException("Expected " + expectedValue + ", but got " + values);
      }
    };
  }

  SparqlExecutor executor = new SparqlExecutor();
  LambdaCalculusConverter converter = new LambdaCalculusConverter();

  public LambdaCalculusConverterTest() {
    super();

    String geoPath = "freebase/data/geo";
    LambdaCalculusConverter.opts.primPath = geoPath + "/geo.primitives";
    LambdaCalculusConverter.opts.varPath = geoPath + "/geo.vars";
    LambdaCalculusConverter.opts.replacePath = geoPath + "/geo.replace";

    converter.readPrereqs();
  }

  // NOTE formula here first needs to be converted
  protected void runFormula(String formula, ValuesChecker checker) {
    LispTree tree = LispTree.proto.parseFromString(formula);
    Formula form = converter.toFormula(tree, null, new ArrayList<String>());
    LogInfo.logs("RESULT FORMULA: %s", form);
    Executor.Response response = executor.execute(form, null);
    LogInfo.logs("RESULT VALUE: %s", response.value);
    checker.checkValues(((ListValue) response.value).values);
  }

  // TODO(pliang): these tests should really compare the formulas which are returned, not the SPARQL query
  @Test(groups = { "lcc", "sparql" }) public void lccSuperlatives() {
    runFormula("(argmax $0 (and (place:t $0) (loc:t $0 florida:s)) (elevation:i $0))", matches("(name fb:en.britton_hill)"));  // what is the highest point in florida
    runFormula("(argmin $0 (and (place:t $0) (loc:t $0 texas:s)) (elevation:i $0))", matches("(name fb:en.tackitt_mountain)")); // what is the lowest point in texas
    // TODO(pliang): times out on SPARQL right now
    // runFormula("(elevation:i (argmax $0 (and (place:t $0) (loc:t $0 (argmax $1 (state:t $1) (size:i $1)))) (elevation:i $0)))", matches("(number 6196 fb:en.meter)"));  // how high is the highest point in the largest state
  }

  @Test(groups = { "lcc", "sparql" }) public void lccPredicates() {
    runFormula("(capital:c maine:s)", matches("(name fb:en.augusta_maine)"));  // what is the of maine
    // TODO(pliang): Currently using latitude as proxy for population
    // runFormula("(population:i illinois:s)", matches("(number 12875255)"));  // what is the population of illinois
    runFormula("(size:i texas:s)", matches("(number 696241 fb:en.square_kilometer)"));  // what is the size of texas
    runFormula("(lambda $0 e (and (state:t $0) (next_to:t $0 kentucky:s)))$a", size(8));  // which state border kentucky (includes kentucky)
    runFormula("(elevation:i guadalupe_peak:m)", matches("(number 2667 fb:en.meter)"));  // how high is guadalupe peak
    runFormula("(len:i mississippi_river:r)", matches("(number 3734 fb:en.kilometer)"));  // how long is the mississippi river
    runFormula("(area:i south_carolina:s)", matches("(number 82931 fb:en.square_kilometer)"));  // what is the area of south carolina
    runFormula("(argmax $0 (state:t $0) (density:i $0))", size(1));  // what state has the largest population density
  }

  @Test(groups = { "lcc", "sparql" }) public void lccRivers() {
    // Because rivers suck in geoquery
    runFormula("(argmax $0 (and (river:t $0) (river_loc:t $0 texas:s)) (len:i $0))", matches("(name fb:en.rio_grande)"));  // what is the longest river in texas
  }

  @Test(groups = { "lcc", "sparql" }) public void lccPrimitives() {
    runFormula("(count $0 (and (state:t $0) (exists $1 (and (city:t $1) (named:t $1 rochester:n) (loc:t $1 $0)))))", matches("(number 18)"));  // how many states have a city called rochester
  }

  @Test(groups = { "lcc", "sparql" }) public void lccLambda() {
    runFormula("(lambda $0 e (loc:t new_orleans_la:c $0))", contains("(name fb:en.louisiana)"));  // where is new orleans
  }

  @Test(groups = {"lcc", "sparql" }) public void lccBasicExists() {
    runFormula("(lambda $0 e (and (capital:t $0) (exists $1 (and (state:t $1) (next_to:t $1 texas:s) (loc:t $0 $1)))))", size(5));  // what are the capitals in the states that border texas (include texas)
  }
}
