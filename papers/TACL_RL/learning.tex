\section{Reinforcement Learning}
\label{sec:rl}

The objective in \refeqn{old_obj} is based on only a distribution over root
derivations. Thus, by optimizing it,
%we only learn to distinguish ``good" root derivations from ``bad" root derivations,
we do not explicitly learn anything about partial derivations that never make
it to the root.
Consider the derivation in \reffig{parsechart} over the phrase \nl{lincoln}
with the logical form
\wl{USSLincoln}. If none of the $K$ root derivations contains this
partial derivation, \refeqn{old_obj} will not penalize it,
and we might repeatedly construct it even though it is useless. 
To discourage this, we need an objective function sensitive to the intermediate
stages of parsing.

%We would like learn a scoring function that generates as few partial derivations
%as possible,
% PL: want to generate less than this number
%and takes advantage of the $O(|x|^2 K)$ partial derivations in the parse chart, 
%rather than only the $K$ root derivations. 
%To this end, we cast our problem in the reinforcement learning framework.

\subsection{Reinforcement learning}

Our approach is based on standard policy gradient \cite{sutton1999policy}, but
we adapt it to agenda-based semantic parsing.
Recall that a \emph{parsing state} is $s = (\Chart, Q)$,
where $\Chart \subseteq \sD$ is the chart and $Q \subseteq \sD$ is the agenda.
%and let $\sS$ be the set of all parsing states. 
The available actions are exactly $Q$, and the successor state $s'$ is computed
via $\Call{executeAction}$ from \refalg{agenda}.
%a single action $a$, where the derivation is chosen and the
%successor state $s'$ is deterministically computed.
We model the \emph{policy} as a log-linear distribution over (partial) derivations $Q$:
$p_\theta(a \mid s) = p_\theta(d = a \mid Q)$,
according to \refeqn{basicModel}.
%log-linear model over all possible subsequent actions, using a feature vector
%$\phi(s,a)$:
%\begin{align*} 
%p_\theta(a|s) = \frac{\exp[\phi(s,a)^\top \theta]}{\sum_{a} \exp[\phi(s,a)^\top
%\theta]}.
%\end{align*} 
Note that the state $s$ only provides the support of the distribution;
the shape depends on only features $\phi(a)$ of the chosen action $a$,
not on other aspects of $s$.
%action depend only on the derivation chosen and not on other
%derivations in $\Chart$ and $Q$.
This simple parametrization allows us to follow a policy efficiently:
%Let $s(d) = \phi(d)^\top \theta$;
when we add a derivation $a$ to the agenda,
we insert it with priority equal to the score $s(a) = \theta^\top\phi(a)$.
Computing the best action $\arg\max_a p_\theta(a \mid s)$ simply involves popping
from the priority queue.
%and it is guaranteed that this score will remain constant throughout subsequent states. 

% PL: save space
%Moreover, $s(d)$ can be viewed as the standard priority of an agenda item
%in agenda-based parsing. Note that while the feature function $\phi(d)$ has
%identical form to fixed-order parsing, the model is different -- here $d$ is
%either a partial derivation or root derivation.

% History
A history $\bh=(s_1,a_1,\dots,a_T,s_{T+1})$ (see \reffig{learning}) is a sequence of states and
actions, such that $s_1$ has an empty chart and an initial agenda, and $s_{T+1}$
is a terminal state reached after performing the chart action in which we choose a root
derivation $a_T$ from $\ChartCell{0}{|x|}{\Root}$ 
(\refalg{agenda}).
We define the reward of a history as $R(\bh)=R(d_T)=R(a_T)$.

\FigTop{figures.slides/learning.pdf}{0.5}{learning}{A
history of states and actions. Each ellipse represents a state, and the red path
marks the actions chosen by the proposal distribution $q_\theta$.
Every action contributes $\delta_t$ to the overall gradient.}

A policy induces a distribution over histories
$p_\theta(\bh) = \prod_{t=1}^{T} p_\theta(a_t \mid s_t)$. 
%Given $p_\theta(\bh)$, we can define
We seek to maximize the expected reward under the policy:

\begin{align} \label{eqn:objective} 
\sO(\theta) = \BE_{p_\theta} \big[R(\bh) \big]=\sum_{\bh} p_\theta(\bh)R(\bh).
\end{align} 
Towards optimizing~\refeqn{objective},
let us write the gradient of $O(\theta)$ \cite{sutton1999policy}:
\begin{align}\label{eqn:derivative}
  \nabla \sO(\theta) &= \BE_{p_\theta}\left[R(\bh) \sum_{t=1}^T \delta_t(\bh)\right], \\
       \delta_t(\bh) &= \phi(a_t) - \BE_{p_\theta(a_t' \mid s_t)}[\phi(a_t')] \label{eqn:deltat}.
\end{align} 
Unlike the fixed-order formulation, the gradient
involves all partial derivations that were constructed during parsing.
Intuitively, we seek to favor
actions $a_t$ in histories leading to high
reward, and to disfavor all other available actions $a_t'$.
\reffig{learning}
illustrates the contribution to the gradient for a single action $a_t$.

Standard policy gradient is problematic for two reasons.
First, we cannot sum over all exponentially many histories to compute~\refeqn{derivative}.
We could estimate the gradient by sampling a history according to $p_\theta$,
but such an estimate would have huge variance.
Second, even the true gradient $\nabla\sO(\theta)$ is inadequate:
Indeed, the initial policy $p_\theta$ is bad, resulting in a tiny $\nabla\sO(\theta)$.
%However, a single sample provides a very crude estimate of the gradient.
% PL: conflates two things
%In fact, when parameters are untrained, sampling from $p_\theta$ almost always
%ends up with $R(\bh)=0$, and thus no update is made.
To address both problems,
we replace the sampling distribution $p_\theta(\bh)$ with a \emph{proposal
distribution} $q_\theta(\bh)$, yielding 
$\BE_{q_\theta}[R(\bh) \sum_{t=1}^T \delta_t(\bh)]$.
%\begin{align}\label{eqn:derivative}
%\theta \leftarrow \theta + \eta \BE_{q_\theta}\left[R(\bh) \sum_{t=1}^T \delta_t\right].
%\end{align} 
We do not include the importance weights
$\frac{p_\theta(\bh)}{q_\theta(\bh)}$
since the true gradient is not desirable.
%\footnote{We do not multiply each
%sample by $\frac{p_\theta(h)}{p^+_\theta(h)}$, as in importance sampling, since
%the gradient is inherently minuscule and this term suffers from the same problem.}

\subsection{Proposal distributions for semantic parsing} \label{subsec:proposal}

What should $q_\theta$ be?
Intuitively, it should be correlated with $R(\bh)$.
%and exploits the structure of agenda-based parsing.
To construct $q_\theta$,
suppose we have a root derivation $\doracle$ with high $R(\doracle)$;
call such a $\doracle$ a \emph{quasi-oracle}.
%We start with the policy $p_\theta$ and make two tranformations
The proposal is based on $\doracle$ in two ways:
local reweighting and history compression.
We present both as transformations from a base proposal $q_\theta$
to new proposals $q_\theta^{\Reweight}$ and $q_\theta^{\Compress}$.
%We suggest two methods for mapping a distribution $q_\theta(\bh)$
%to a distribution $q^+_\theta(\bh)$ based on $\doracle$: (i) local reweighting and
%(ii) history compression. 
 
\textbf{Local reweighting}
Let $\indoracle{a}$ be an indicator on whether an action $a$
is a sub-derivation of $\doracle$.
Given a history $\bh$, we reweight every action $a$ based on $\indoracle{a}$.
We define the locally reweighted policy 
$q_\theta^{\Reweight}(a \mid s) \propto q_\theta(a \mid s) \cdot \exp[\beta \, \indoracle{a}]$ for some $\beta > 0$.
If $\beta$ is large enough, 
we will always choose a derivation that is part of
$\doracle$ if there is one on the agenda. Otherwise, we will fall back to sampling
from $q_\theta$.
Then define $q_\theta^{\Reweight}(\bh) = \prod_{t=1}^T q_\theta^{\Reweight{}}(a_t \mid s_t)$.

\FigTop{figures.slides/compress.pdf}{0.65}{compress}{A example history of states
and actions, where actions that are part of the
quasi-oracle derivation $\doracle = a_4$ are in red.
The compressed history is
$c(\bh)=(s_1,a_1,s_3,a_3,s_4,a_4,s_5)$.}

\textbf{History compression}
A problem with local reweighting is that because the parser generates $K$ root
derivations, many of which are incorrect, we still end up updating towards incorrect
derivations.
E.g., in the sampled actions in~\reffig{compress}, we would
update towards $d_2$ although it is not part of $\doracle$.

Given $\doracle$, we can define for every history $\bh$ a sequence of indices $(t_1,
t_2, \dots)$ 
such that  
$\indoracle{a_{t_i}}=1$ for every $i$. Then, the \emph{compressed
history} $c(\bh) = (s_{t_1}, a_{t_1}, s_{t_2}, a_{t_2}, \dots)$ is
a sequence of states and actions such that all actions
choose sub-derivations of $\doracle$.
Note that $c(\bh)$ is not a ``real history''
in the sense that taking action $a_{t_i}$ does not necessarily result in state
$s_{t_{i+1}}$.
In~\reffig{compress}, the compressed history $c(\bh) =
(s_1,a_1,s_3,a_3,s_4,a_4,s_5)$.

We can now define the distribution over compressed histories,
$q^{\Compress{}}_\theta(\bh) = \sum_{\bh': c(\bh') = \bh} q_\theta(\bh)$, where we
marginalize over all histories that have the same compressed history.
To sample from $q^{\Compress{}}_\theta$, we simply sample $\bh' \sim q_\theta$
and return $\bh = c(\bh')$.
%This will
%provide histories containing only actions that chose a correct partial
%derivation.

History compression and local reweighting are complementary and can be combined
on top of $p_\theta$.
Let $p_\theta^{\Reweight{}}$ be the
locally reweighted distribution, $p_\theta^{\Compress{}}$ is a
distribution over compressed histories, and $p_\theta^{\CompressReweight}$ is
a locally reweighted distribution over compressed histories.
We use $p_\theta^{\CompressReweight{}}$ in our full model, 
and compare it to other proposal distributions in 
\refsec{experiments}.
 
% PL: already defined it in text, and described in pseudocode.
%For any proposal distribution $q_\theta(\bh)$, we approximate $\nabla O(\theta)$ 
%by sampling a history $\bh$ and performing the update:
%\begin{align*}\label{eqn:derivative}
%\delta(\bh) = R(\bh) \sum_{a_t \in \sA(\bh)} (\phi(s_t,a_t) - 
%\BE_{p_\theta(a_t|s_t)} \big[ \phi(s_t,a_t) \big]),
%\end{align*} 
%where $\sA(\bh)$ is the set of actions in $\bh$.

%Last, we assumed access to a quasi-oracle that
%guides our actions during training.
% PL: to save space
%In supervised learning an oracle can be
%derived from the gold structure, but when training from utterance-denotation pairs we
%do not have gold structures.
%Finally, to obtain a quasi-oracle derivation, we first parse every
%input by sampling from $p_\theta$, and then choosing from $C_{0,|x|}^\gr{\Root{}}$ the
%derivation with highest reward.

\refalg{learning} summarizes learning (for a single iteration). 
For each example, we first parse it by sampling a history
from $p_\theta$.
We then choose the derivation with highest reward in $\ChartCell{0}{|x|}{\Root}$
as the quasi-oracle derivation $\doracle$.
%(we assume the parser returns the state $s_T$).
The oracle derivation defines $p_\theta^{\CompressReweight{}}$, which we sample from
to compute gradients.
%While parsing, we can incrementally compute 
%$\delta(\bh)$, and store it in $s_T$ 
%to perform the final update.

\begin{algorithm}[t] 
\caption{Learning algorithm}
\label{alg:learning} 
\begin{algorithmic} 
{\footnotesize
\Procedure {Learn}{$\{x_i,y_i\}_{i=1}^n$}
\State $\theta \leftarrow 0$
\For{each iteration $\tau$ and example $i$}
%\State $s_T^o \leftarrow$ \Call{Parse$_{p_\theta}$}{$x_i$}
\State $\bh_0 \leftarrow$ \Call{Parse}{$p_\theta, x_i$}
\State $\doracle \leftarrow$ \Call{chooseOracle}{$\bh_0$}
\State $\bh \leftarrow$ \Call{Parse}{$p_\theta^{\CompressReweight}, x_i$}
\State $\theta \leftarrow \theta + \eta_{\tau,i} \sum_{t=1}^T \delta_t(\bh)$
\EndFor
\EndProcedure
}
\end{algorithmic}
\end{algorithm}

%We denote by $\textsc{Parse}_{q_\theta}$ a parser that samples sequences from
%$p_\theta$.
%We assume that the parser returns the entire
%parsing state $s_T$ and not just the derivation $d_T$. Last, when sampling
%from $q_\theta$, we compute 
%$\delta_{\doracle}(\bh)$ incrementally (as illustrated in
%~\reffig{learning}), and store it in the
%parsing state, so that at the end of parsing, $\delta_{\doracle}(\bh)$ is stored
%in $s_T$.  

%One important detail is computing the feature vector $\phi(s,a)$. We define
%features that depend on the derivation removed from the agenda only, and not on
%the chart or agenda. This results in a weaker
%representation but greatly simplifies computation: when we add a derivation $d$
%to the agenda, we can compute the derivation score $s(d) = \phi(s,a)^\top
%\theta$,
%and this score is guaranteed to stay fixed. If the score depended on the state
%of the chart
%and agenda, we would have to re-compute derivation scores throughout 
%parsing. Augmenting the feature represenation with information from the chart
%and agenda is an interesting future research direction.
%\pl{this comes across as too much of a side comment: this is somehow an
%approximation to some other model, but I think we should just state (earlier)
%that our features are just $\phi(s,a) = \phi(a)$,
%and note that this has nice computational properties;
%important: exactly the the same as normal agenda-based parser!}

% PL: are we saying this too much?
An important property of our formulation 
is that our state includes the entire chart and agenda,
and so it can entertain multiple candidates at once.
In contrast, prior work in imitation learning
\cite{daume09searn,ross2011reduction,goldberg2013training,chen2014nndep}
used states that were one single partial structure. In~\refsec{experiments}, we 
show that maintaining a single hypothesis fails
due to high lexical ambiguity present in semantic parsing.

%Next, we present a method for making parsing more efficient by introducing a
%lazy implementation of the agenda.
