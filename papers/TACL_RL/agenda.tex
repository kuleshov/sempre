\section{Agenda-based Parsing} \label{sec:agenda}

The idea of using an agenda has a long history in syntactic parsing
\cite{kay86algorithm,caraballo1998new,pauls2009kbest,auli2011efficient}.
An agenda-based parser controls the order in which derivations are constructed
using an agenda $Q$, which contains a set of derivations to be processed.
Our exposition is slightly unorthodox as it highlights the state/action structure,
setting the stage for \refsec{rl}.
%The derivations on the agenda can be constructed using the
%ones that have already been placed in the chart.
At each point in time, the \emph{state} of the parser consists of 
two sets of derivations, the chart $\Chart$ and the agenda $Q$.
Each \emph{parsing action}
chooses a derivation from the agenda, moves it to the chart, combines it
with other chart derivations, and adds new derivations to the agenda
(\reffig{agendaparsing}). Note that the set of derivations in $Q$ defines
the set of possible parsing actions at each point in time.

\FigTop{figures.slides/agendaParsing.pdf}{0.35}{agendaparsing}{A schematic
illustration of a executing a parsing action, specified by a derivation on the agenda.
First, we \emph{remove} it from the agenda and put it in the chart.
Then, combine it with other chart
derivations to produce new derivations, which are \emph{added} back to the agenda.
}

\begin{algorithm}[t] 
\caption{Agenda-based parsing}
\label{alg:agenda} 
\begin{algorithmic}[1]
{\footnotesize

\Procedure {Parse}{$x$}
\State \Call{initAgenda}{\null}
\While{$|Q|>0 \land |\ChartCell{0}{|x|}{\Root}|< K$}
\State $\Derivation \leftarrow \text{choose derivation from $Q$}$ \label{line:agenda} 
\State \Call{executeAction}{$\Derivation$}
\EndWhile
\State choose and return derivation from $\ChartCell{0}{|x|}{\Root}$ \label{line:chart}
\EndProcedure
\Function {executeAction}{$\Derivation$}
\State remove $\Derivation{}$ from $Q$
\If {$|\ChartCell{i}{j}{A}| < K$}
\State $\ChartCell{i}{j}{A}.\text{add}(\Derivation)$
\State \Call{combine}{$\Derivation$}
\EndIf
\EndFunction

\Function {combine}{$\Derivation$}
\For{$k>j$ and $r = \SpecifiedRule{B}{A}{C}{f} \in \sR$}
\For{$d_{j,k}^C \in \ChartCell{j}{k}{C}$}
\State $D_{i:k}^B \leftarrow f(\Derivation,d_{j:k}^C)$
\State $Q.\text{addAll}(D_{i:k}^B)$
\EndFor
\EndFor
\For{$k<i$ and $r = \SpecifiedRule{B}{C}{A}{f} \in \sR$}
\For{$d_{k,i}^C \in \ChartCell{k}{i}{C}$}
\State $D_{k,j}^B \leftarrow f(d_{k:i}^C,\Derivation)$
\State $Q.\text{addAll}(D_{k,j}^\gr{B})$
\EndFor
\EndFor
\EndFunction
\Function{initAgenda}{\null}
\For{$\SpecifiedUnary{A}{x_{i:j}}{f} \in \sR$}
\State $\Derivations{} \leftarrow f(x_{i:j})$
\State $Q.\text{addAll}(\Derivations)$
\EndFor
\EndFunction
}
\end{algorithmic}
\end{algorithm}

\refalg{agenda} describes agenda-based parsing. The algorithm shows binary
rules; unary rules are treated similarly. First, we initialize
the agenda by applying all rules whose RHS has only terminals, adding the
resulting derivations to the agenda. Then, we perform parsing actions until
either the agenda is empty or we obtain $K$ root derivations. On each action, we
first choose a derivation $\Derivation{}$ to remove from $Q$ and add it to 
$\ChartCell{i}{j}{A}$, unless $\ChartCell{i}{j}{A}$ already has $K$ derivations. Then, we combine 
$\Derivation{}$ with all derivations
$d_{j,k}$ to the right and $d_{k,i}$ to the left.
In the last step, we perform one action, in which we choose and return a single
derivation from all constructed root derivations.

% PL: not sure if this is really that necessary (if we set s this way, then
% we get a pretty sucky parser since s is by default used for reranking too)
%A good scoring function is crucial for improving efficiency;
%for instance, the naive scoring function $s(d) = i-j$ only yields the standard
%fixed-order parser from \refsec{fixedorder}.

The most natural way to choose an agenda derivation is by taking the highest
scoring derivation $d = \argmax_{d \in Q} s(d)$.
Prior work on agenda-based parsing generally assumed 
%A crucial distinction from prior work is that agenda-based parsing
%algorithms usually assume
that the scoring function $s$ is learned separately (e.g., from maximum likelihood
estimation of a generative PCFG).
Furthermore, they assumed that $s$ satisfies the decomposition
property (\refsec{fixedorder}),
which guarantees obtaining the highest scoring root derivation in the end.
%while reducing the number of constructed derivations. % PL: clear already
On the other hand, we make no assumptions on $s$,
and would like to \emph{learn} a scoring function that
is tightly coupled with agenda-based parsing
in order to explicitly reduce the number of constructed derivations.
% PL: we don't explicitly have a penalty on the number of derivations
% constructed (except with the compression quasi-oracle)
This is the topic of the next section.
%In the next section, we learn $\theta$ (which parametrizes $s$)
%in a way that is tailored for agenda-based parsing.
%that learns a good scoring function $s(d) = \phi(d)^\top \theta$.
% PL: not necessary
%We will demonstrate in~\refsec{experiments} that this learned scoring function is
%faster and more accurate than the one learned when optimizing~\refeqn{old_obj}.
