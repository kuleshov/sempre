// For node.js
require('./sfig/internal/sfig.js');
require('./sfig/internal/Graph.js');
require('./sfig/internal/RootedTree.js');
require('./sfig/internal/metapost.js');
require('./sfig/internal/seedrandom.js');
require('./utils.js');

Text.defaults.setProperty('font', 'Times New Roman');  // For the paper

prez = sfig.presentation();
prez.addSlide(chartParse().id('chartParse'));
prez.addSlide(parseExample().id('parseExample'));
prez.addSlide(semFunc().id('semFunc'));
prez.addSlide(gridedTriangle(10).id('chart'));
prez.addSlide(agendaParsing().id('agendaParsing'));
prez.addSlide(learning().id('learning'));
prez.addSlide(streams().id('streams'));
prez.addSlide(genlex().id('genlex'));
prez.addSlide(compress().id('compress'));
prez.addSlide(chartCount().id('chartCount'));
// For node.js: output PDF (you must have Metapost installed).
prez.writePdf({outPrefix: 'figures', combine: false});
