\section{Lazy agenda using derivation streams} \label{sec:streams}

As we saw in~\refsec{semfn}, a single semantic function (e.g., \gr{Lex}) can
create hundreds of derivations.
%e.g., the \gr{Lex} semantic function uses a lexicon to produce
%multiple predicates for a language phrase.
In this section, we assume semantic functions return a \emph{derivation stream},
i.e., an iterator that lazily computes derivations on demand.
We use a \emph{lazy agenda} $G$ containing derivation streams,
which implicitly defines the actual agenda $Q$ (containing derivations).

% Sorting
We would like the lazy agenda to closely approximate the original agenda
without explicitly constructing all the derivations.
To make the analysis possible, we make two assumptions:
% PL: use $u$ not to overload D too much
(i) for each derivation stream $g = [d_1, d_2, \dots]$, the derivations are sorted
by decreasing score ($s(d_1) \ge s(d_2) \ge \cdots$);
and (ii) we can compute an upper bound $M(g)$ on the number of derivations in $g$.
Importantly, these assumptions should hold without actually creating and scoring
all the derivations!
In practice, we make a best effort to satisfy this property (described later).
% PL: we should think not just saving on featurization, but on even constructing the derivations.
%The motivation is that scoring derivations is an expensive
%operation that involves extracting features and computing a dot product.
%By scoring derivations in a lazy fashion we can avoid featurizing downstream
%derivations that will never be put in the chart.

\paragraph{Unrolling the lazy agenda}

% PL: this might seem kind of obvious...
As a na\"{i}ve start,
assume we put each stream $g = [d_1, \dots]$ on the agenda with priority $l(g) = s(d_1)$,
which can be computed by scoring $d_1$.
At test time, we just need to choose the highest scoring derivation $d =
\arg\max_{d \in Q}$, which can be obtained by popping the $g$ with the largest
$l(g)$ and extracting the first derivation.
But during training, we need access to the full distribution \refeqn{deltat},
and the distribution induced by $l(g)$ can be inaccurate:
e.g., suppose the agenda contains two derivation streams:
$g_1$ contains one derivation with score $1$
and $g_2$ contains 50 derivations with score $0$.
Then we would assign $g_1$ probability $\frac{e^1}{e^1+e^0} = 0.73$
instead of the true model probability $\frac{e^1}{e^1 + 50 e^0} = 0.05$.
The issue is that the first derivation of $g$ is not indicative of the
actual probability mass in $g$.

%imagine an agenda with two derivation streams, $\sD_1$ and $\sD_2$. $\sD_1$ has a single derivation
%with score $0$, and $\sD_2$ has two derivations with score $1$. If we consider
%only the first derivation in each stream, and assume we are sampling from $Q$ with
%$p_\theta$, then we would sample from $\sD_1$ with probability
%$\frac{\exp[0]}{\exp[0]+\exp[1]}=0.27$, when according to the true distribution
%over agenda derivations we should sample from $\sD_1$ with probability
%$\frac{\exp[0]}{\exp[0] + 2 \cdot \exp[1]}=0.16$. The technical challenge is to store derivations in
%streams, while still sampling and computing gradients based on the true
%distribution over agenda derivations.

\FigTop{figures.slides/streams.pdf}{0.35}{streams}
{
Unrolling derivation streams. The streams in red and blue on the left
violate the condition \refeqn{stoppingCondition} and are unrolled
until all streams on $G$ satisfy the condition.
}

% Solution
Now we develop a solution.
Let $p_\theta(d)$ denote the desired distribution
\refeqn{basicModel}, omitting $Q$ for simplicity.
We want to construct an approximation
$\tilde p_\theta(d)$ so that
\begin{align}
  \label{eqn:lazyApprox}
\max_{d \in Q} |\tilde p_\theta(d) - p_\theta(d)| \le \epsilon.
\end{align}

% Define lower and upper bounds
For each stream $g \in G$,
let $w(g) = \sum_{d \in g} e^{s(d)}$ denote the unnormalized probability,
which we can't compute.
Instead, we form lower and upper bounds based on the first derivation $g[1]$:
$l(g) = e^{s(g[1])}$ and $u(g) = M(g) e^{s(g[1])}$.
By construction, $l(g) \le w(g) \le u(g)$.
We define $\tilde p_\theta$ to place probability mass only on the first
derivation of each $g \in G$ based on the lower bounds:
formally $\tilde p_\theta(d) \propto l(g)$ if $d$ is the first
derivation in some $g$, and $0$ otherwise.

% Conditions
Each time we modify the lazy agenda $G$,
we unroll derivation streams as necessary to satisfy
\refeqn{lazyApprox} (see \reffig{streams}).
But how do we guarantee \refeqn{lazyApprox} without actually computing $w(g)$?
First, define the normalization constants:
$L = \sum_{g \in G} l(g)$,
$W = \sum_{g \in G} w(g)$, and
$U = \sum_{g \in G} u(g)$.
Next, observe that the probabilities of initial derivations
$p_\theta(g[1]) = \frac{l(g)}{W}$ and
$\tilde p_\theta(g[1]) = \frac{l(g)}{L}$
are both contained in the interval $[\frac{l(g)}{U}, \frac{l(g)}{L}]$.
For non-initial derivations $g[j]$ where $j > 1$,
we have $p_\theta(g[j]) \le \frac{l(g)}{L}$ and
$\tilde p_\theta(g[j]) = 0$.
Thus, \refeqn{lazyApprox} is satisfied if
%for all $g \in G$, $\frac{l(g)}{L} - \frac{l(g)}{U} \le \epsilon$
%and if $M(g) > 1$, $\frac{l(g)}{L} \le \epsilon$.
\begin{align}
  \label{eqn:stoppingCondition}
  \forall g \in G: \frac{l(g)}{L} - \frac{l(g)}{U} \le \epsilon,
M(g) > 1 \Rightarrow \frac{l(g)}{L} \le \epsilon.
\end{align}

\refeqn{stoppingCondition}
can be readily computed and checked. % without instantiating entire streams.
To gain intuition, there are two cases:
(i) if $g$ contains one derivation, then $L$ and $U$ must be reasonably close
(we can't allow derivation streams with many potentially high scoring derivations);
and (ii) if $g$ contains multiple derivations, then the first derivation $g[1]$ (and thus
each derivation) must have small $l(g)$. \reffig{streams} illustrates an
agenda $G$ where derivations in blue and red are unrolled until
\refeqn{stoppingCondition} is satisfied.

% Proposal
We have so far approximated $p_\theta$ with $\tilde p_\theta$ based on scoring the first
derivation of each stream.  During training, we also sample from an approximation of
the proposal $q_\theta = p_\theta^{\Reweight}$ (\refsubsec{proposal}).
We enforce a condition analogous to \refeqn{stoppingCondition} based on
modified scores $s(d) + \beta \, \indoracle{d}$ on the same lazy agenda.

% Implementation
% PL: I think this is obvious, and we're low on space
%Two simple changes are required in~\refalg{agenda} to support a lazy agenda. 
%First, before we choose a derivation from $Q$ (represented by $G$),
%we enforce \refeqn{stoppingCondition} for both the model $p_\theta$ and proposal $q_\theta$.
%To choose a derivation, we choose a stream and return the first derivation on the stream.
% PL: already said this earlier.
%Note that this constraint is only maintained at training time, when we do sampling and compute
%gradients. At test time, we always choose the highest scoring derivation stream.

% PL: this would be nice to have, but don't have space
%The intuition is that when learning starts, all derivations on a stream have
%the same score, and we have to pop many derivations from the stream. As
%learning continues, scores decay quickly and many derivations are not
%featurized.

%We suggest the following solution.
%Let's assume that derivations on streams are sorted by score (although we do
%not explicitly score them). 
%Before performing a parsing action, 
%we go over the agenda, and check that the following constraint is satisfied: 
%For every derivation stream,
%either it contains a single derivation, or the sum of probabilities of all
%derivations on that stream is smaller than the \emph{tolerance} $\epsilon$. This guarantees
%that not computing scores for this stream when sampling and
%computing gradients is small. If a derivation stream does
%not satisfy this constraint, we pop the 
%highest scoring derivation from the stream (which is first if the stream is
%sorted), add it to the
%agenda as a stream with a single derivation, score the next derivation on the
%stream, and check the constraint again. 
% PL: need to be be more precise
%We want to compute an upper bound for the sum of probabilities on a derivation stream
%$D$. Given a
%state $s$ and derivation $d$, we can write for simplicity 
%$p_\theta(d) = p_\theta(a|s)$, where we omit $s$ and identify the action
%with the chosen agenda derivation. The total probability of a
%derivation stream is $p_\theta(D) = \sum_{d \in D} p_\theta(d)$. 
%When derivations on the stream are sorted and we have an upper bound 
%$|D|$ on the stream size, then $p_\theta(D) \leq |D| \cdot p_\theta(d_0)$, where $d_0$ is the first 
%derivation on $D$. Thus, if $p_\theta(d_0) \leq \frac{\epsilon}{|D|}$ we can
%avoid featurizing $D$.

\paragraph{Sorting derivation streams}

Let us revisit our assumption that derivations on streams $g \in G$
are sorted by decreasing score.  To do this without explicitly scoring each
derivation, we need to impose more structure on the derivation streams.
%The last problem is to maintain sorted streams without
%explicitly featurizing derivations. 
% PL: say that this isn't for all the semantic functions?
We assume that derivations returned by a semantic function $f$ are parametrized
by an auxiliary set $\sB$.
For example, for $f = \gr{Lex}$, $\sB$ is a list of phrase-predicate pairs (e.g., 
(\nl{born}, \wl{PlaceOfBirthOf})).  We equip each $b \in \sB$ with a feature vector
$\phi_{\sB}(b)$ (e.g., various alignment statistics),
giving rise to a score $s_{\sB}(b) = \phi_{\sB}(b)^\top \theta$.
Assume each derivation $d \in f(\cdot)$ is associated with a $b \in \sB$
and $\phi(d) = \phi_{\sB}(b)$.
%For example, the \gr{Lex}
%semantic function holds a list of mappings from text phrases (e.g., \nl{born}) to
%logical predicates (e.g., \wl{PlaceOfBirthOf}).
%Each entry is associated with a set of features $\phi(l_i)$ and we can sort the
%list based on the score $s(l_i) = \phi(l_i)^\top \theta$.
Importantly, $s_{\sB}(b)$ is not utterance-specific. Thus, 
we can sort $\sB$ by $s_\sB$ before parsing,
so that when the actual semantic function $f$ is called,
we do not need to instantiate the derivations,
whose scores are predictably equal to $s_\sB$.
Note that the parameters $\theta$ and thus $s_{\sB}$ change during learning,
so we re-sort $\sB$ after every training iteration,
yielding an approximation to the true ordering of $\sB$.

% PL: we're sorting $A$, not the derivation stream
%This yields an approximate sorting of $A$.
%The sorted list provides an approximation for the true sorting of derivations
%on the derivation stream.
