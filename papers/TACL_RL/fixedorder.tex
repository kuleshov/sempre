\section{Fixed-order Parsing} \label{sec:fixedorder}

We now describe fixed-order parsing with beam search,
which has been the common practice in past work
\cite{krishnamurthy2012weakly,kwiatkowski2013scaling,berant2013freebase}.

We call derivations $\SpecifiedDeriv{0}{|x|}{\gr{\Root}}$, 
spanning the utterance $x$ and with root category, 
\emph{root derivations}. We call all other derivations \emph{partial
derivations}.
Given a scoring function $s : \sD \to \R$, the goal is to
find the (approximately) highest scoring root derivation, or
more generally the top-$K$ scoring root derivations.  A fixed-order parser
computes $K$ derivations for every category and span $x_{i:j}$ by applying
grammar rules over the utterance in a fixed order, most commonly by going over
spans of length $n$ before spans of length $n+1$.
Formally, let $\Chart \subseteq \sD$ be the constructed derivations called a \emph{chart}.
Let $\ChartCell{i}{j}{A} \subseteq \Chart$ be those with category $A$ over the span $x_{i:j}$.
The $K$ derivations in $\ChartCell{i}{j}{A}$ are computed as follows:
\begin{align*} 
\ChartCell{i}{j}{A} = \text{Trunc}_K \big( \bigcup_{\GenericRule} \
\bigcup_{\begin{subarray}{1}{k:i<k<j}\\{d_1 \in \ChartCell{i}{k}{B}}\\{d_2 \in
\ChartCell{k}{j}{C}}\end{subarray}} f(d_1,d_2) \big) 
\end{align*} 
where
$\text{Trunc}_K$ is a function that keeps the $K$ derivations with highest score.
Note that we described binary rule application, but unary
rules are similar. 

A fixed-order parser is guaranteed to compute the $K$ highest scoring derivations,
if it satisfies the following \emph{decomposition} property:
(i) all semantic functions return exactly one derivation, and
% i.e, $f: \sD \times \sD \to \sD$ for binary rules and % PL: don't change the type of $f$ - confusing
(ii) the scoring function decomposes, i.e.,
there is a function $s_\text{rule}: \sR \to \R$ such that
for every rule $r = \GenericRule$,
the score of a derivation produced by the rule is
$s(d_{i:j}^A) = s(d_{i,k}^B) + s(d_{k,j}^C) + s_\text{rule}(r)$.
Unfortunately, the decomposition property generally does not hold in semantic
parsing. For example, the \gr{Intersect} function returns an
empty set when type-checking fails,
and in general, we want the flexibility of having the scoring function depend on at least
the logical forms and sub-derivations. % of the child as well as the generated derivations.
Consequently, semantic parsers use a beam of size $K>1$, which allows
partial derivations that are not necessarily locally optimal
to remain on the beam.
%even when they do not have the highest score.

\textbf{Model}
We focus on linear scoring functions:
$s(d) = \theta^\top \phi(d)$,
where $\theta \in \R^{F}$ is the parameter vector to be learned and $\phi(d) \in \R^F$
is the feature vector (we use standard features, adapted from
\newcite{berant2013freebase}).
Given any set of derivations $D \subseteq \sD$,
we can define the corresponding log-linear distribution:
\begin{align} 
  \label{eqn:basicModel}
  p_\theta(d \mid D) &= \frac{\exp\{\phi(d)^\top \theta\}}{\sum_{d' \in D}
\exp\{\phi(d')^\top \theta\}}.
\end{align} 

\textbf{Learning}
The training data consists of a set of utterance-denotation (question-answer)
pairs $\{(x_i,y_i)\}_{i=1}^n$.
%In practice $\sD(x)$ can not be enumerated and is approximated by the set
%$\sD_\theta(x)$ that contains the $K$ root derivations obtained by parsing $x$
%given $\theta$.
To learn $\theta$, we use an online learning algorithm, where on each $(x_i, y_i)$,
we use beam search based on the current parameters to construct a set of root derivations
$D_i = \ChartCell{0}{|x|}{\gr{\Root}}$,
and then take a gradient step on the following objective:
%The objective function optimized during training is the log-likelihood of the training data:
\begin{align} \label{eqn:old_obj}
% PL: we're not doing this, might as well say what we're doing
%\sO(\theta; \tilde \theta) &= \sum_i \log \sum_{ d \in \sD_{\tilde \theta}(x_i) } p_\theta(d \mid x_i; \sD_{\tilde \theta}(x_i)) R(d),
\sO(\theta) &= \log \sum_{d \in D_i} p_\theta(d \mid D_i) R(d),
\end{align} 
where $R(d)$ is a reward function that measures the compatibility of the predicted denotation
$\den{d.z}$ and the true denotation $y_i$.\footnote{For our setup, $\den{d.z}$ and $y_i$ are
both sets of entities, and we compute the $F_1$ score on the two sets.}

The main drawback of fixed-order parsing is that to obtain the $K$ root
derivations $D_i$, the parser must first construct $K$ derivations for all
spans and all categories, many of which will not make it into any root
derivation $d \in D_i$.  Next, we describe agenda-based parsing,
whose goal is to give the parser better control over constructed derivations.
