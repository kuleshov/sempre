G = sfig.serverSide ? global : this;
sfig.importAllMethods(G);

// Latex macros
sfig.latexMacro('R', 0, '\\mathbb{R}');
sfig.latexMacro('P', 0, '\\mathbb{P}');
sfig.latexMacro('E', 0, '\\mathbb{E}');
sfig.latexMacro('diag', 0, '\\text{diag}');
sfig.includeLatex('macros.tex');

sfig.initialize();

G.prez = sfig.presentation();

// Useful functions (not standard enough to put into an sfig library).

G.frameBox = function(a) { return frame(a).padding(1).bg.strokeWidth(1).fillColor('white').end; }
G.strokeFrameBox = function(a,strokeWidth) { return frame(a).padding(1).bg.strokeWidth(strokeWidth).fillColor('white').end; }
G.colorFrameBox = function(a,color) { return frame(a).bg.fillColor(color).dim(90,50).end; }

G.bigLeftArrow = function(s) { return leftArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigRightArrow = function(s) { return rightArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigUpArrow = function(s) { return upArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigDownArrow = function(s) { return downArrow(s || 100).strokeWidth(10).color('brown'); }
G.red = function(x) { return x.fontcolor('red'); }
G.green = function(x) { return x.fontcolor('green'); }
G.blue = function(x) { return x.fontcolor('blue'); }
G.darkblue = function(x) { return x.fontcolor('darkblue'); }

G.xseq = function() { return new sfig.Table([arguments]).center().margin(5); }
G.yseq = function() { return ytable.apply(null, arguments).margin(10); }

G.stmt = function(prefix, suffix) { return prefix.fontcolor('darkblue') + ':' + (suffix ? ' '+suffix : ''); }
G.headerList = function(title) {
  var contents = Array.prototype.slice.call(arguments).slice(1);
  return ytable.apply(null, [title ? stmt(title) : _].concat(contents.map(function(line) {
    if (line == _) return _;
    if (typeof(line) == 'string') return bulletedText([null, line]);
    if (line instanceof sfig.PropertyChanger) return line;
    return indent(line);
  })));
}

G.node = function(x, shaded) { return overlay(circle(20).fillColor(shaded ? 'lightgray' : 'white') , x).center(); }
G.indent = function(x, n) { return frame(x).xpadding(n != null ? n : 20).xpivot(1); }
G.stagger = function(b1, b2) { b1 = std(b1); b2 = std(b2); return overlay(b1, pause(), b2.replace(b1)); }

G.dividerSlide = function(text) {
  return slide(null, nil(), parentCenter(text)).titleHeight(0);
}

G.moveLeftOf = function(a, b, offset) { return transform(a).pivot(1, 0).shift(b.left().sub(offset == null ? 5 : offset), b.ymiddle()); }
G.moveRightOf = function(a, b, offset) { return transform(a).pivot(-1, 0).shift(b.right().add(offset == null ? 5 : offset), b.ymiddle()); }
G.moveTopOf = function(a, b, offset) { return transform(a).pivot(0, 1).shift(b.xmiddle(), b.top().sub(offset == null ? 5 : offset)); }
G.moveBottomOf = function(a, b, offset) { return transform(a).pivot(0, -1).shift(b.xmiddle(), b.bottom().add(offset == null ? 5 : offset)); }
G.moveCenterOf = function(a, b) { return transform(a).pivot(0, 0).shift(b.xmiddle(), b.ymiddle()); }

G.wholeNumbers = function(n) {
  var result = [];
  for (var i = 0; i < n; i++) result[i] = i;
  return result;
}

G.argSort = function(arr, cmp) {
  cmp = cmp || function(a, b) { return a - b; };
  return wholeNumbers(arr.length).sort(function(a, b) { return cmp(arr[a], arr[b]); });
};

G.shuffleArrayInPlace = function(arr) {
  for (var i = arr.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var tmp = arr[j]; arr[j] = arr[i]; arr[i] = tmp;
  }
}

////////////////////////////////////////////////////////////

G.nlcolor = 'green';  // Natural language
G.zlcolor = 'red';  // Logical form
G.wlcolor = 'blue';  // World
G.nl = function(x) { return x.italics(); }
G.zl = function(x) { return '$\\wl{' + x + '}$'; }
G.wl = function(x) { return '$\\wl{' + x + '}$'; }
G.cnl = function(x) { return nl(x).fontcolor(nlcolor); }
G.czl = function(x) { return zl(x).fontcolor(zlcolor); }
G.cwl = function(x) { return wl(x).fontcolor(wlcolor); }

G.gr = function(x) { return '$\\textsc{' + x + '}$'; }
G.grczl = function(x, y) {
  return gr(x) + ':' + czl(y);
}

G.sampleTree = function() {
  T = rootedTree;
  res =
    T(
      intersection = nowrapText(grczl('Root', 'Type.City') + '$\\,\\sqcap\\,$' + czl('PlaceOfBirthOf.AbeLincoln')),
      cnl('what'),
      t1 = T(grczl('Set', 'Type.City'), cnl('city')),
      nowrapText(cnl('was')),
      T(join = text(grczl('Set', 'PlaceOfBirthOf.AbeLincoln')),
        t2 = T(grczl('Entity', 'AbeLincoln'), cnl('abraham lincoln')),
        t3 = T(grczl('Binary', 'PlaceOfBirthOf'), cnl('born in')),
      _),
    _).recnodeBorderWidth(0).recverticalCenterEdges(true);
  return res;
}

G.gridedTriangle = function(cellSize) {

  topX = numWords*cellSize/2;
  topY = numWords*cellSize/2

  res = overlay(
    line([0,0],[numWords*cellSize,0]).color('gray'),
    line([0,0],[topX,topY]).color('gray'),
    line([topX,topY],[numWords*cellSize,0]).color('gray'),
    line([1*cellSize,0],[topX+1*cellSize/2,topY - 1*cellSize/2]).color('gray'),
    line([2*cellSize,0],[topX+2*cellSize/2,topY - 2*cellSize/2]).color('gray'),
    line([3*cellSize,0],[topX+3*cellSize/2,topY - 3*cellSize/2]).color('gray'),
    line([4*cellSize,0],[topX+4*cellSize/2,topY - 4*cellSize/2]).color('gray'),
    line([5*cellSize,0],[topX+5*cellSize/2,topY - 5*cellSize/2]).color('gray'),
    line([6*cellSize,0],[topX+6*cellSize/2,topY - 6*cellSize/2]).color('gray'),
    line([numWords*cellSize - 1*cellSize,0],[topX-1*cellSize/2,topY - 1*cellSize/2]).color('gray'),
    line([numWords*cellSize - 2*cellSize,0],[topX-2*cellSize/2,topY - 2*cellSize/2]).color('gray'),
    line([numWords*cellSize - 3*cellSize,0],[topX-3*cellSize/2,topY - 3*cellSize/2]).color('gray'),
    line([numWords*cellSize - 4*cellSize,0],[topX-4*cellSize/2,topY - 4*cellSize/2]).color('gray'),
    line([numWords*cellSize - 5*cellSize,0],[topX-5*cellSize/2,topY - 5*cellSize/2]).color('gray'),
    line([numWords*cellSize - 6*cellSize,0],[topX-6*cellSize/2,topY - 6*cellSize/2]).color('gray'),
  _);

  return res;
}

G.rightBranchingTree = function(col) {
  strokeWidth = 3;
  res = overlay(
    line([0,10],[10,20]).strokeWidth(strokeWidth).color(col),
    line([10,20],[20,10]).strokeWidth(strokeWidth).color(col),
    line([20,10],[30,0]).strokeWidth(strokeWidth).color(col),
    line([20,10],[10,0]).strokeWidth(strokeWidth).color(col),
  _);
  return res;
}

G.leftBranchingTree = function(col) {
  strokeWidth = 3;
  res = overlay(
    line([0,0],[10,10]).strokeWidth(strokeWidth).color(col),
    line([10,10],[20,20]).strokeWidth(strokeWidth).color(col),
    line([20,20],[30,10]).strokeWidth(strokeWidth).color(col),
    line([10,10],[20,0]).strokeWidth(strokeWidth).color(col),
  _);
  return res;
}

G.state = function(label) {
  agenda = strokeFrameBox(ytable(rightBranchingTree('black'),leftBranchingTree('black'),rightBranchingTree('black')),2);
  numWords = 4;
  cellSize = 35;
  topX = numWords*cellSize/2;
  topY = numWords*cellSize/2;
  strokeWidth = 3;

  chart = overlay(
    line([0,0],[numWords*cellSize,0]).strokeWidth(strokeWidth),
    line([0,0],[topX,topY]).strokeWidth(strokeWidth),
    line([topX,topY],[numWords*cellSize,0]).strokeWidth(strokeWidth),
    line([1*cellSize,0],[topX+1*cellSize/2,topY - 1*cellSize/2]).strokeWidth(strokeWidth),
    line([2*cellSize,0],[topX+2*cellSize/2,topY - 2*cellSize/2]).strokeWidth(strokeWidth),
    line([3*cellSize,0],[topX+3*cellSize/2,topY - 3*cellSize/2]).strokeWidth(strokeWidth),
    line([numWords*cellSize - 1*cellSize,0],[topX-1*cellSize/2,topY - 1*cellSize/2]).strokeWidth(strokeWidth),
    line([numWords*cellSize - 2*cellSize,0],[topX-2*cellSize/2,topY - 2*cellSize/2]).strokeWidth(strokeWidth),
    line([numWords*cellSize - 3*cellSize,0],[topX-3*cellSize/2,topY - 3*cellSize/2]).strokeWidth(strokeWidth),
  _)
  return overlay(
    text(label.bold()).yshift(70).xshift(-5).scale(1.2),
    xtable(chart.scale(0.9),agenda.scale(0.8)),
    ellipse(97,60).xshift(85).yshift(-40).strokeWidth(2),
  _);
}

G.learning = function() {
  function conn(a, b, col) {
    return arrow(a, b).strokeWidth(1.5).dashed().color(col);
  }

  function labelConn(a, b, label, color) {
    var c = conn(a, b, color);
    return overlay(c, moveTopOf(opaquebg(label).scale(0.9), c, 0));
  }

  update = text('$\\delta_t = \\phi(a_t) -\\mathbb{E}_{p_\\theta(a_t\' \\mid s_t)} [ \\phi(a_t\') ]$').scale(0.6).xshift(-150).yshift(-70);
  return overlay(
    a = state('$s_{t-1}$').scale(0.5).xshift(-150),
    b =state('$s_t$').scale(0.5),
    c =state('$s_{t+1}$').scale(0.5).xshift(150),
    d =state('').scale(0.5).xshift(150).yshift(70),
    e =state('').scale(0.5).xshift(150).yshift(-70),
    labelConn(a,b,text('$a_{t-1}$').scale(0.6),'red'),
    labelConn(b,c,text('$a_t$').scale(0.6),'red'),
    conn(b,d,'black'),
    conn(b,e,'black'),
    //update,
  _);
}


G.agendaParsing = function() {
  function conn(a, b, col) {
    return arrow(a, b).strokeWidth(1.5).dashed().color(col);
  }

  numWords = 4;
  cellSize = 100;
  topX = numWords*cellSize/2;
  topY = numWords*cellSize/2;

  chart = overlay(
    line([0,0],[numWords*cellSize,0]),
    line([0,0],[topX,topY]),
    line([topX,topY],[numWords*cellSize,0]),
    line([1*cellSize,0],[topX+1*cellSize/2,topY - 1*cellSize/2]),
    line([2*cellSize,0],[topX+2*cellSize/2,topY - 2*cellSize/2]),
    line([3*cellSize,0],[topX+3*cellSize/2,topY - 3*cellSize/2]),
    line([numWords*cellSize - 1*cellSize,0],[topX-1*cellSize/2,topY - 1*cellSize/2]),
    line([numWords*cellSize - 2*cellSize,0],[topX-2*cellSize/2,topY - 2*cellSize/2]),
    line([numWords*cellSize - 3*cellSize,0],[topX-3*cellSize/2,topY - 3*cellSize/2]),
    pushedCell = text('').xshift(2*cellSize).yshift(0.75*cellSize),
    combinedCell = text('').xshift(3*cellSize).yshift(0.75*cellSize),
  _);

  agenda = strokeFrameBox(ytable(popped = rightBranchingTree('blue'),leftBranchingTree('black'), bottom = rightBranchingTree('red')).margin(3),2);
  return overlay(
    xtable(chart,agenda.scale(1.5)),
    removeOp = conn(popped,pushedCell,'blue'),
    addOp = conn(combinedCell,bottom,'red'),
    addOp2 = conn(pushedCell,bottom,'red'),
    moveTopOf(text('remove'.bold()).scale(0.7).color('blue'),removeOp,50),
    moveBottomOf(text('add'.bold()).scale(0.7).color('red'),addOp2),
  _);
}

//number of entries for "bear" - ./run @mode=test @class=LexiconTest --> 391
//number of entries for "city" - ./run @mode=test @class=LexiconTest --> 252 + 20 + 90
G.chartParse = function() {

  function conn(a, b) {
    return arrow(a, b).strokeWidth(8).dashed();
  }
  numWords = 7;
  cellSize = 200;
  topX = numWords*cellSize/2;
  topY = numWords*cellSize/2

  bornTable = frameBox(ytable(czl('PlaceOfBirthOf'),czl('PlacesLived'),czl('$\\dots$')).center());
  lincolnTable = frameBox(ytable(czl('AbeLincoln'),czl('LincolnTown'),czl('USSLincoln'),czl('$\\dots$')).center());
  cityTable = frameBox(ytable(czl('Type.City'),czl('Type.Loc'),czl('$\\dots$')).center());
  rootFrame = frameBox(rootTable = ytable(
        czl('Type.City $\\sqcap$ PlaceOfBirthOf.AbeLincoln'),
        czl('Type.Loc $\\sqcap$ ContainedBy.LincolnTown'),
        czl('$\\dots$'),
          _).center());

  chart = overlay(
    gridedTriangle(200),
    text(cnl('what')).xshift(20).yshift(30).scale(2),
    text(cnl('city')).xshift(250).yshift(30).scale(2),
    text(cnl('was')).xshift(450).yshift(30).scale(2),
    text(cnl('abraham')).xshift(600).yshift(30).scale(2),
    text(cnl('lincoln')).xshift(825).yshift(30).scale(2),
    text(cnl('born')).xshift(1050).yshift(30).scale(2),
    text(cnl('in')).xshift(1250).yshift(30).scale(2),
    born = text('391').xshift(5.3 * cellSize).yshift(80).scale(2),
    text('508').xshift(6.3 * cellSize).yshift(80).scale(2),
    city = text('362').xshift(1.3 * cellSize).yshift(80).scale(2),
    text('20').xshift(3.3 * cellSize).yshift(80).scale(2),
    abeLincoln=text('20').xshift(4.3 * cellSize).yshift(80).scale(2),
    root = text('$>$1M').xshift(3.2 * cellSize).yshift(670).scale(2),
    bornTable.xshift(6.5 * cellSize).yshift(1 * cellSize).scale(2),
    rootFrame.xshift(5 * cellSize).yshift(2.2 * cellSize).scale(2),
    lincolnTable.xshift(1.6 * cellSize).yshift(2.7 * cellSize).scale(2),
    cityTable.xshift(0.7 * cellSize).yshift(1.3 * cellSize).scale(2),
    conn(root,rootTable),
    conn(abeLincoln,lincolnTable),
    conn(born,bornTable),
    conn(city,cityTable),
  _);
  return chart;
  //return ytable(chart,xtable(nl('what'),nl('city'),nl('was'),nl('lincoln'),nl('born'),nl('in')).scale(3).margin(20));
}

G.parseExample = function() {
  T = rootedTree;

  var lexicon = function() { return opaquebg(blue(gr('Lex'))).scale(0.8); }
  var intersection, join, t1, t2, t3;
  function labelVerticalEdge(label, t) { return moveRightOf(label, t.branches[0].edge); }
  return overlay(
    T(
      intersection = nowrapText(grczl('Root', 'Type.City') + '$\\red{\\,\\sqcap\\,}$' + czl('PlaceOfBirthOf.AbeLincoln')),
      cnl('what'),
      t1 = T(grczl('Set', 'Type.City'), cnl('city')),
      cnl('was'),
      T(join = text(grczl('Set', 'PlaceOfBirthOf.AbeLincoln')),
        t2 = T(grczl('Entity', 'AbeLincoln'), cnl('abraham lincoln')),
        t3 = T(grczl('Binary', 'PlaceOfBirthOf'), cnl('born in')),
      _),
    _).recnodeBorderWidth(0).recverticalCenterEdges(true),
    moveBottomOf(opaquebg(blue(gr('Join'))).scale(0.8), join),
    moveBottomOf(opaquebg(blue(gr('Intersect'))).scale(0.8), intersection),
    labelVerticalEdge(lexicon(), t1),
    labelVerticalEdge(lexicon(), t2),
    labelVerticalEdge(lexicon(), t3),
  _);
}

G.semFunc = function() {

  function deriv(lf,nl) {
    return ytable(lf,eqTriangle(30),nl).center();
  }

  function application(name,left,right,result) {
    return xtable(name+'(',left,',',right,')={',result,'}').center();
  }

  //join
  abeDeriv = deriv(grczl('Entity', 'AbeLincoln'),cnl('abraham lincoln')).center();
  birthDeriv = deriv(grczl('Binary', 'PlaceOfBirthOf'),cnl('born'));
  joinTree = T(
      nowrapText(grczl('Set', 'PlaceOfBirthOf.AbeLincoln')),
      abeDeriv,
      birthDeriv,
    _).recnodeBorderWidth(0).recverticalCenterEdges(true);

  joinDerivs = xtable('$\\textsc{Join}\\bigg($',
      deriv(grczl('Entity', 'AbeLincoln'),cnl('abraham lincoln')).center(),
      ',',
      deriv(grczl('Binary', 'PlaceOfBirthOf'),cnl('born')),
      '$\\bigg) = \\Bigg\\{$',
      joinTree,
      '$\\Bigg\\}$',
      _).center().margin(5);

  //lex
  lincolnDeriv = cnl('lincoln');
  lexTree1 = T(
      nowrapText(grczl('Entity', 'AbeLincoln')),
      lincolnDeriv,
      _).recnodeBorderWidth(0).recverticalCenterEdges(true);
  lexTree2 = T(nowrapText(grczl('Entity', 'LincolnFilm')),lincolnDeriv).recnodeBorderWidth(0).recverticalCenterEdges(true);
  lexDerivs = xtable('$\\textsc{Lex}$(',lincolnDeriv,'$) = \\Bigg\\{$',lexTree1,',',lexTree2,',','...','$\\Bigg\\}$').margin(5).center();

  //merge
  cityDeriv = deriv(grczl('Set', 'Type.City'),cnl('city'));
  setDeriv = deriv(grczl('Set', 'ReleaseDateOf.LincolnFilm'),cnl('abraham lincoln born'));
  mergeDerivs = xtable('$\\textsc{Intersect}\\bigg($',cityDeriv,',',setDeriv,'$\\bigg) = \\{\\}$').margin(5).center();
  allDerivs = ytable(joinDerivs,xtable(lexDerivs,mergeDerivs).margin(50)).center().margin(5);
  return allDerivs;
}

G.streams = function() {

  agenda1 = table(
      ['$G$','$s(g[1])$','$M(g)$','$\\tilde p_\\theta$'],
      ['$[d_1]$','$7$','$1$','$0.84$'],
      ['$\\red{[d_2,d_3,\\dots]}$','$\\red{5}$','$\\red{4}$','$\\red{0.11}$'],
      ['$\\blue{[d_4,d_5,\\dots]}$','$\\blue{4}$','$\\blue{999}$','$\\blue{0.04}$'],
      _).xmargin(18).xjustify('lrrr').scale(0.8);

  agenda2 = table(
      ['$G$','$s(g[1])$','$M(g)$','$\\tilde p_\\theta$'],
      ['$[d_1]$','$7$','$1$','$0.88$'],
      ['$\\red{[d_2]}$','$\\red{5}$','$\\red{1}$','$\\red{0.11}$'],
      ['$\\red{[d_3,\\dots]}$','$\\red{-2}$','$\\red{3}$','$\\red{< \\epsilon}$'],
      ['$\\blue{[d_4]}$','$\\blue{4}$','$\\blue{1}$','$\\blue{0.04}$'],
      ['$\\blue{[d_5,\\dots]}$','$\\blue{-7}$','$\\blue{998}$','$\\blue{< \\epsilon}$'],
      _).xmargin(18).xjustify('lrrr').scale(0.8);

  return xtable(agenda1,bigRightArrow(50),agenda2).center().margin(8);
}

G.genlex = function() {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  alignment = red('lex');
  var bridging = 'bridging'.fontcolor('purple');
  var example = ytable(
    nowrapText('$\\red{\\wl{Type.University} \\sqcap \\wl{Education}.\\wl{BarackObama}}$'),
    table(
      [nil(), nil(), nil(), nil(), nil()],
      [nil(), unary = text(wl('Type.University')), nil(), binary = text(wl('Education')), nil()],
      [nil(), nil(), nil(), entity = text(wl('BarackObama')), nil()],
      [cnl('which'), nlUnary = text(cnl('college')), cnl('did'), nlEntity = text(cnl('obama')), nlBinary = text(cnl('go to'))],
    _).margin(50).center(),
    downArrow(30),
    nowrapText('$\\textsc{Lex}: \\green{\\textit{``go"}} \\Rightarrow \\red{\\wl{Education}}$'),
  _).justify('c', 'l');
  return overlay(
    example,
    labelConn(unary, nlUnary, alignment, 'red'),
    labelConn(entity, nlEntity, alignment, 'red'),
    labelConn(unary, binary, bridging,'purple'),
    conn(binary, entity).color('purple'),
  _);
}

G.compress = function() {
  function conn(a, b, col, width) {
    return arrow(a, b).strokeWidth(width).dashed().color(col);
  }

  function labelConn(a, b, label, color, width) {
    var c = conn(a, b, color, width);
    return overlay(c, moveTopOf(opaquebg(label).scale(0.5), c, 0));
  }

  //tree = T('$d_5$',T('$d_3$','$d_1$'),'$d_4$').recnodeBorderWidth(0).recverticalCenterEdges(true).scale(0.5),
  //var tree = T('$\\red{d_4}$','$\\red{d_1}$','$\\red{d_3}$').recnodeBorderWidth(0).recverticalCenterEdges(true).scale(0.5);
  var tree = T('$\\red{a_4 = \\doracle}$', a1 = eqTriangle(30), a3 = eqTriangle(30)).recnodeBorderWidth(0).recverticalCenterEdges(true).recxmargin(80).scale(0.5);

  function leftLabel(a, b) { return moveLeftOf(text(a).scale(0.5), b); }
  function bottomLabel(a, b) { return moveBottomOf(text(a).scale(0.5), b); }

  return overlay(
    ytable(
      tree,
      xtable(
        text('$\\mathbf{h}:$').scale(0.5),
        xtable(
          a = state(''),
          b = state(''),
          c = state(''),
          d = state(''),
          e = state(''),
        _).margin(150).scale(0.2),
      _),
    _).center().margin(10),

    leftLabel('$\\red{a_1}$', a1),
    leftLabel('$\\red{a_3}$', a3),

    bottomLabel('$s_1$', a),
    bottomLabel('$s_2$', b),
    bottomLabel('$s_3$', c),
    bottomLabel('$s_4$', d),
    bottomLabel('$s_5$', e),

    labelConn(a,b,'$\\red{a_1}$','red',3),
    labelConn(b,c,'$a_2$','black',1),
    labelConn(c,d,'$\\red{a_3}$','red',3),
    labelConn(d,e,'$\\red{a_4}$','red',3),
  _);
}

G.sixTriangle = function() {
  triangle = overlay(
    line([0,0],[numWords*cellSize,0]).color('gray'),
    line([0,0],[topX,topY]).color('gray'),
    line([topX,topY],[numWords*cellSize,0]).color('gray'),
    line([1*cellSize,0],[topX+1*cellSize/2,topY - 1*cellSize/2]).color('gray'),
    line([2*cellSize,0],[topX+2*cellSize/2,topY - 2*cellSize/2]).color('gray'),
    line([3*cellSize,0],[topX+3*cellSize/2,topY - 3*cellSize/2]).color('gray'),
    line([4*cellSize,0],[topX+4*cellSize/2,topY - 4*cellSize/2]).color('gray'),
    line([5*cellSize,0],[topX+5*cellSize/2,topY - 5*cellSize/2]).color('gray'),
    line([numWords*cellSize - 1*cellSize,0],[topX-1*cellSize/2,topY - 1*cellSize/2]).color('gray'),
    line([numWords*cellSize - 2*cellSize,0],[topX-2*cellSize/2,topY - 2*cellSize/2]).color('gray'),
    line([numWords*cellSize - 3*cellSize,0],[topX-3*cellSize/2,topY - 3*cellSize/2]).color('gray'),
    line([numWords*cellSize - 4*cellSize,0],[topX-4*cellSize/2,topY - 4*cellSize/2]).color('gray'),
    line([numWords*cellSize - 5*cellSize,0],[topX-5*cellSize/2,topY - 5*cellSize/2]).color('gray'),
    _);
  return triangle;
}


G.chartCount = function() {

  textScale=2.1;
  numWords=6;
  cellSize = 200;
  topX = numWords*cellSize/2;
  topY = numWords*cellSize/2;

  fixedorder  = overlay(
    sixTriangle(),
    center(text(cnl('what'))).xshift(0.5*cellSize).yshift(-0.1*cellSize).scale(textScale),
    center(text(cnl('currency'))).xshift(1.5*cellSize).yshift(-0.1*cellSize).scale(textScale),
    center(text(cnl('does'))).xshift(2.5*cellSize).yshift(-0.1*cellSize).scale(textScale),
    center(text(cnl('jamaica'))).xshift(3.5*cellSize).yshift(-0.1*cellSize).scale(textScale),
    center(text(cnl('accept'))).xshift(4.5*cellSize).yshift(-0.1*cellSize).scale(textScale),
    center(text(cnl('?'))).xshift(5.5*cellSize).yshift(-0.1*cellSize).scale(textScale),
    center(text('$5$')).xshift(0.5*cellSize).yshift(0.15*cellSize).scale(textScale),
    center(text('$486$')).xshift(1.5*cellSize).yshift(0.15*cellSize).scale(textScale),
    center(text('$524$')).xshift(2.5*cellSize).yshift(0.15*cellSize).scale(textScale),
    center(text('$4,918$')).xshift(3.5*cellSize).yshift(0.15*cellSize).scale(textScale),
    center(text('$59$')).xshift(4.5*cellSize).yshift(0.15*cellSize).scale(textScale),
    center(text('$5$')).xshift(5.5*cellSize).yshift(0.15*cellSize).scale(textScale),
    center(text('$3$')).xshift(1*cellSize).yshift(0.5*cellSize).scale(textScale),
    center(text('$211$')).xshift(2*cellSize).yshift(0.5*cellSize).scale(textScale),
    center(text('$715$')).xshift(3*cellSize).yshift(0.5*cellSize).scale(textScale),
    center(text('$509$')).xshift(4*cellSize).yshift(0.5*cellSize).scale(textScale),
    center(text('$3$')).xshift(5*cellSize).yshift(0.5*cellSize).scale(textScale),
    center(text('$3$')).xshift(1.5*cellSize).yshift(1*cellSize).scale(textScale),
    center(text('$1,139$')).xshift(2.5*cellSize).yshift(1*cellSize).scale(textScale),
    center(text('$373$')).xshift(3.5*cellSize).yshift(1*cellSize).scale(textScale),
    center(text('$3$')).xshift(4.5*cellSize).yshift(1*cellSize).scale(textScale),
    center(text('$573$')).xshift(2*cellSize).yshift(1.5*cellSize).scale(textScale),
    center(text('$415$')).xshift(3*cellSize).yshift(1.5*cellSize).scale(textScale),
    center(text('$3$')).xshift(4*cellSize).yshift(1.5*cellSize).scale(textScale),
    center(text('$89$')).xshift(2.5*cellSize).yshift(2*cellSize).scale(textScale),
    center(text('$3$')).xshift(3.5*cellSize).yshift(2*cellSize).scale(textScale),
    center(text('$488$')).xshift(3*cellSize).yshift(2.5*cellSize).scale(textScale),
  _);

  agenda = overlay(
    sixTriangle(),
    center(text(cnl('what'))).xshift(0.5*cellSize).yshift(-0.1*cellSize).scale(textScale),
    center(text(cnl('currency'))).xshift(1.5*cellSize).yshift(-0.1*cellSize).scale(textScale),
    center(text(cnl('does'))).xshift(2.5*cellSize).yshift(-0.1*cellSize).scale(textScale),
    center(text(cnl('jamaica'))).xshift(3.5*cellSize).yshift(-0.1*cellSize).scale(textScale),
    center(text(cnl('accept'))).xshift(4.5*cellSize).yshift(-0.1*cellSize).scale(textScale),
    center(text(cnl('?'))).xshift(5.5*cellSize).yshift(-0.1*cellSize).scale(textScale),
    center(text('$2$')).xshift(0.5*cellSize).yshift(0.15*cellSize).scale(textScale),
    center(text('$7$')).xshift(1.5*cellSize).yshift(0.15*cellSize).scale(textScale),
    center(text('$2$')).xshift(2.5*cellSize).yshift(0.15*cellSize).scale(textScale),
    center(text('$392$')).xshift(3.5*cellSize).yshift(0.15*cellSize).scale(textScale),
    center(text('$3$')).xshift(4.5*cellSize).yshift(0.15*cellSize).scale(textScale),
    center(text('$2$')).xshift(5.5*cellSize).yshift(0.15*cellSize).scale(textScale),
    center(text('$0$')).xshift(1*cellSize).yshift(0.5*cellSize).scale(textScale),
    center(text('$3$')).xshift(2*cellSize).yshift(0.5*cellSize).scale(textScale),
    center(text('$0$')).xshift(3*cellSize).yshift(0.5*cellSize).scale(textScale),
    center(text('$0$')).xshift(4*cellSize).yshift(0.5*cellSize).scale(textScale),
    center(text('$2$')).xshift(5*cellSize).yshift(0.5*cellSize).scale(textScale),
    center(text('$2$')).xshift(1.5*cellSize).yshift(1*cellSize).scale(textScale),
    center(text('$12$')).xshift(2.5*cellSize).yshift(1*cellSize).scale(textScale),
    center(text('$0$')).xshift(3.5*cellSize).yshift(1*cellSize).scale(textScale),
    center(text('$0$')).xshift(4.5*cellSize).yshift(1*cellSize).scale(textScale),
    center(text('$200$')).xshift(2*cellSize).yshift(1.5*cellSize).scale(textScale),
    center(text('$0$')).xshift(3*cellSize).yshift(1.5*cellSize).scale(textScale),
    center(text('$2$')).xshift(4*cellSize).yshift(1.5*cellSize).scale(textScale),
    center(text('$0$')).xshift(2.5*cellSize).yshift(2*cellSize).scale(textScale),
    center(text('$0$')).xshift(3.5*cellSize).yshift(2*cellSize).scale(textScale),
    center(text('$400$')).xshift(3*cellSize).yshift(2.5*cellSize).scale(textScale),
  _);

  return overlay(
    text('$\\textsc{AgendaRL}$:'.bold()).yshift(-100).scale(2),
    text('$\\textsc{FixedOrder}$:'.bold()).yshift(-800).scale(2),
    ytable(agenda,fixedorder).margin(10),
  _);
}
