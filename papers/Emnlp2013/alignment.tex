\subsection{Alignment} \label{sec:alignment}

We now discuss the construction of a \emph{lexicon} $\sL$, which is a
mapping from natural language phrases to logical predicates accompanied by a
set of features. Specifically, for a phrase $w$ (e.g., \nl{born in}), $\sL(w)$ is a set of
entries $(z, s)$, where $z$ is a predicate and $s$ is the set of features.
%Given a lexicon $\sL$, we generate 
%\paragraph{Lexicon mapping}
%Once the lexicon is constructed it is used to generate derivations for phrases
%$w$ in the question. For each lexicon entry $\sL(w)$ we create a derivation
%that contains both the logical form $z$ and the features $s$.
A lexicon is constructed by alignment of a large text corpus to the knowledge base (KB).
Intuitively, a phrase and a predicate align if they co-occur with many
of the same entities.

Here is a summary of our alignment procedure:
We construct a set of typed\footnote{Freebase associates each entity with a set of types using the \wl{Type} property.} phrases $\sR_1$ 
(e.g., \nl{born in}[\wl{Person},\wl{Location}])
and predicates $\sR_2$ (e.g., \wl{PlaceOfBirth}).
%(\refsec{kbPredicates}).
For each $r \in \sR_1 \cup \sR_2$, we create its \emph{extension} $\sF(r)$,
which is a set of co-occurring entity-pairs (e.g.,
$\sF(\text{\nl{born in}[\wl{Person},\wl{Location}]}) = \{ (\wl{BarackObama}, \wl{Honolulu}), \dots \}$.
The lexicon is generated based on the overlap $\sF(r_1) \cap \sF(r_2)$, for $r_1 \in R_1$ and $r_2 \in R_2$.
%Finally, we construct a bipartite graph over the two sets of nodes $(\sR_1,
%\sR_2)$ and create edges based on their extensions.
%These edges and features extracted from them constitute the final lexicon.
%We now discuss how phrases and predicates are constructed.

\FigTop{figures/alignmentGraph}{0.3}{alignmentGraph}{
We construct a bipartite graph over phrases $\sR_1$ and predicates $\sR_2$.  Each edge $(r_1, r_2)$
is associated with alignment features.
}

\paragraph{Typed phrases}

15 million triples $(e_1, r, e_2)$ (e.g., (\nl{Obama}, \nl{was also
born in}, \nl{August 1961})) were extracted from ClueWeb09 using
the ReVerb open IE system \cite{fader11reverb}.
\newcite{lin2012linking} released a subset of these
triples\footnote{\footnotesize{\url{http://knowitall.cs.washington.edu/linked_extractions/}}}
where they were able to substitute the subject arguments with KB entities. We
downloaded their dataset and heuristically replaced object arguments with KB
entities by walking on the Freebase graph from subject KB entities and
performing simple string matching. In addition, we normalized dates with SUTime
\cite{chang2012sutime}.

%We employ standard entity linking methods \cite{lin2012linking} to
%substitute strings with KB entities\footnote{} (e.g., \nl{Obama} becomes \wl{BarackObama}) and normalize dates with SUTime \cite{chang2012sutime}.

We lemmatize and normalize each text phrase $r \in R_1$ and augment it with a type signature $[t_1,t_2]$
to deal with polysemy (\nl{born in} could either map to \wl{PlaceOfBirth} or \wl{DateOfBirth}).
We add an entity pair $(e_1, e_2)$ to the extension of $\sF(r[t_1,t_2])$ if the
(Freebase) type of $e_1$ ($e_2$) is $t_1$ ($t_2$). For example,
$(\wl{BarackObama},\wl{1961})$ is added to $\sF(\text{\nl{born
in}}[\wl{Person},\wl{Date}]$). 
%We perform a similar procedure to map phrases to
%unary predicates (\nl{city} is mapped to \wl{CityTown}) based
%on Hearst patterns~\cite{hearst1992automatic}.
%From the 15 million triples, we extracted 55,081 typed binary phrases (9,456 untyped)
%and 6,299 unary phrases.
We perform a similar procedure that uses a Hearst-like
pattern~\cite{hearst1992automatic} to map phrases to unary predicates. 
If a text phrase $r \in R_1$ matches the pattern
``(is$|$was a$|$the) $x$ \textsc{in}'', where \textsc{in} is a preposition,
then we add $e_1$ to $\sF(x)$.
For ($\wl{Honolulu}$, \nl{is a city in}, $\wl{Hawaii}$), we extract $x = \nl{city}$ and add
$\wl{Honolulu}$ to $\sF$(\nl{city}). 
From the initial 15M triples, we extracted 55,081 typed binary phrases (9,456 untyped)
and 6,299 unary phrases.

%cut -f 1,2,3 binaryAlignment.txt | sort | uniq | wc -l - typed binary
%cut -f 1 binaryAlignment.txt | sort | uniq | wc -l - untyped binary
%cut -f1 UnaryAlignment.txt | sort | uniq | wc -l
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\paragraph{Logical predicates}

Binary logical predicates contain
(i) all KB properties\footnote{We filter properties from the 
domains \wl{user} and \wl{base}.} and (ii) concatenations of two properties
$p_1.p_2$
if the intermediate type represents an event (e.g., the
\emph{married to} relation is represented by \wl{Marriage.Spouse}).
For unary predicates, we consider
all logical forms ${\wl{Type}}.t$ and $\wl{Profession}.t$ for all (abstract)
entities $t \in \sE$ (e.g.\ {\wl{Type.Book} and {\wl{Profession.Author}).
The types of logical predicates considered during alignment is restricted in this
paper, but automatic induction of more compositional logical predicates is an
interesting direction.
Finally, we define the extension of a logical predicate $r \in \sR_2$ to be its denotation, that
is, the corresponding set of entities or entity pairs.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{Lexicon construction}

Given typed phrases $\sR_1$, logical predicates $\sR_2$, and their
extensions $\sF$, we now generate the lexicon.
It is useful to think of a bipartite graph with left nodes $\sR_1$ and right nodes $\sR_2$
(\reffig{alignmentGraph}).
%Let $C(r_1, r_2) \eqdef \sF(r_1) \cap \sF(r_2)$ be the overlap in extensions.
We add an edge $(r_1, r_2)$
if (i) the type signatures of $r_1$ and $r_2$ match\footnote{
Each Freebase property has a designated type signature,
which can be extended to composite predicates, e.g.,
$\TypeSig(\wl{Marriage}.\wl{StartDate})=(\wl{Person},\wl{Date})$.}
and (ii) their extensions have non-empty overlap ($\sF(r_1) \cap \sF(r_2) \neq \emptyset$).
Our final graph contains 109K edges for binary predicates and 294K edges for
unary predicates. 

% Statistics
% wc -l binaryAlignment.txt
% wc -l UnaryAlignment.txt
%The average degree of a text predicate is XXX and the
%average degree of a KB predicate is XXX.  The highest degree predicates are
%XXX.

\begin{table}[t]
{\footnotesize
\hfill{}
\begin{tabular}{|p{1.9cm}|p{5.3cm}|}
\hline
\textbf{Category} &\textbf{Description} \\
\hline
\hline
Alignment
  & Log of \# entity pairs that occur with the phrase $r_1$ ($|\sF(r_1)|$) \\
  & Log of \# entity pairs that occur with the logical predicate $r_2$ ($|\sF(r_2)|$) \\
  & Log of \# entity pairs that occur with both $r_1$ and $r_2$ ($|\sF(r_1) \cap \sF(r_2)|$) \\
  & Whether $r_2$ is the best match for $r_1$ ($r_2=\argmax_r |\sF(r_1) \cap \sF(r)|$) \\
\hline
Lexicalized & Conjunction of phrase $w$ and predicate $z$ \\
\hline
Text similarity
  & Phrase $r_1$ is equal/prefix/suffix of $s_2$ \\
  & Phrase overlap of $r_1$ and $s_2$ \\
\hline \hline
Bridging
  & Log of \# entity pairs that occur with bridging predicate $b$ ($|\sF(b)|$)  \\
  & Kind of bridging (\# unaries involved) \\
  & The binary $b$ injected\\
\hline \hline
Composition
 %& Number of binaries/unaries/entities and join/intersect/bridging operations [PL: subsumed by counting operations] \\
 & \# of intersect/join/bridging operations \\
 & POS tags in join/bridging and skipped words \\
 & Size of denotation of logical form \\
\hline
\end{tabular}}
\hfill{}
\caption{\footnotesize{
  Full set of features. For the alignment and text similarity,
  $r_1$ is a phrase, $r_2$ is a predicate with Freebase name $s_2$,
  and $b$ is a binary predicate with type signature $(t_1,t_2)$.
}}
\label{tab:features}
\end{table}

Naturally, non-zero overlap by no means guarantees that $r_1$ should map to $r_2$.
In our noisy data, even \nl{born in} and $\wl{Marriage}.\wl{EndDate}$ co-occur 4 times.
Rather than thresholding based on some criterion, we compute a set of features,
which are used by the model downstream in conjunction with other sources of information.

We compute three types of features (\reftab{features}). \emph{Alignment}
features are unlexicalized and measure association based on argument overlap.
\emph{Lexicalized} features are standard conjunctions of the phrase $w$ and the logical form $z$.
\emph{Text similarity}
features compare the (untyped) phrase (e.g., \nl{born}) to the Freebase
name of the logical predicate (e.g., \nl{People born here}):
Given the phrase $r_1$ and the Freebase name $s_2$ of the predicate
$r_2$, we compute string similarity features such as whether $r_1$ and $s_2$ are equal, 
as well as some other measures of token overlap.
%Note that these alignment statistics on $(r_1,r_2)$ 
%aggregate over all entity pairs, which allows us to mitigate correlation effects.
%For example, \nl{from}$[\wl{Person},\wl{Location}]$ has a size 404 overlap with \wl{PlaceOfBirth}
%and a size 343 overlap with \wl{PlacesLived.Location}.
%The top ranked feature provides a strong signal that the former is the correct alignment,
%even though the latter has substantial support.
%One could even add other statistics based on global graph properties.
%These non-linear type-level features would be difficult
%to model in the weak supervision framework of \newcite{krishnamurthy2012weakly}.
% NO_MID  fb:people.person.place_of_birth 488013.0        born in fb:people.person        fb:location.location
% FB_typed_size,Intersection_size_typed,NL-size,NL_typed_size     15477.0,13305.0,15688.0,15430.0 place of birth
% NO_MID  (lambda x (fb:people.person.places_lived (fb:people.place_lived.location (var x))))     180157.0        born in fb:people.person        fb:location.location
% FB_typed_size,Intersection_size_typed,NL-size,NL_typed_size     9471.0,6249.0,15688.0,15430.0   places lived location

%\subsubsection{Additional information sources}

%For all entities and properties, the KB contains a readable name. %$\wl{Name}$ property providing a readable name.
%We add a lexical entry for every unary predicate
%($\wl{Type}.t$ or $\wl{Profession}.t$) that maps the name of $t \in \sE$ to the unary predicate (e.g., \nl{city} to \wl{City}).
%For every binary predicate $p$, we add an entry
%mapping $p$'s name to the binary predicate (e.g., \nl{place of birth} to \wl{PlaceOfBirth}).
%We also manually added 10 entries %which handle common alignments that can not be obtained from triples,
%for function words (e.g., mapping \nl{where} to $\wl{Type.Location}$).
%Last, we add to the lexicon entries for entities, again using the $\wl{Name}$ property.
%For each entity $e \in \sE$,
%we create an entry mapping its name to $e$ with an accompanying popularity statistic
%(number of times $e$ appears in the text triples).  Retrieving these entries is done via a Lucene index so that given the phrase \nl{obama}, entries for $\wl{BarackObama}$ and $\wl{ObamaJapan}$ will be returned.
