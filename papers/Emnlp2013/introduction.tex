\section{Introduction} \label{sec:intro}

% Semantic parsing limited in two ways
We focus on the problem of semantic parsing natural language utterances into logical forms
that can be executed to produce denotations.
Traditional semantic parsers
\cite{zelle96geoquery,zettlemoyer05ccg,wong07synchronous,kwiatkowski10ccg}
have two limitations:
(i) they require annotated logical forms as supervision, and
(ii) they operate in limited domains with a small number of logical predicates.
Recent developments aim to lift these limitations,
either by reducing the amount of supervision
\cite{clarke10world,liang11dcs,goldwasser11confidence,artzi11conversations}
or by increasing the number of logical predicates \cite{cai2013large}.
The goal of this paper is to do both: learn a semantic parser without
annotated logical forms that scales to the large number of predicates on Freebase. 
\FigTop{figures/schema}{0.25}{schema}{Our task is to map questions
to answers via latent logical forms.  To narrow down the space of logical predicates,
we use a (i) coarse \emph{alignment} based on Freebase and a text corpus
and (ii) a \emph{bridging} operation that generates predicates compatible
with neighboring predicates.
}

% Challenge: lexical mapping - alignment for predicate coverage
%At the lexical level, any approach to semantic parsing must acquire a mapping from
%natural language phrases (e.g., \nl{born in}) to knowledge base predicates
%(e.g., \wl{PlaceOfBirth}).
At the lexical level, a major challenge in semantic parsing is
mapping natural language phrases (e.g., \nl{attend}) to logical
predicates (e.g., \wl{Education}).
While limited-domain semantic parsers are able to learn the lexicon from
per-example supervision \cite{kwiatkowski11lex,liang11dcs},
at large scale they have inadequate coverage \cite{cai2013large}.
% [TODO: provide concrete number?]
Previous work on semantic parsing on Freebase uses a combination of 
manual rules \cite{yahya2012natural,unger2012template}, distant supervision
\cite{krishnamurthy2012weakly}, and schema matching \cite{cai2013large}.
We use a large amount of web text and a knowledge base
to build a coarse alignment between phrases and predicates---an approach
similar in spirit to \newcite{cai2013large}.

% Challenge: predicate coverage => bridging
However, this alignment only allows us to generate a subset of the desired
predicates.  Aligning light verbs (e.g., \nl{go}) and prepositions is not very
informative due to polysemy, and rare predicates (e.g., \nl{cover price}) are difficult to
cover even given a large corpus.  To improve coverage,
we propose a new \emph{bridging} operation that
generates predicates based on adjacent predicates rather than on words.
%showing that this improves coverage substantially.

% Composition
At the compositional level, a semantic parser must combine the
predicates into a coherent logical form.  Previous work based on CCG
requires manually specifying combination rules \cite{krishnamurthy2012weakly}
or inducing the rules from annotated logical forms \cite{kwiatkowski10ccg,cai2013large}.
We instead define a few simple composition rules which over-generate
and then use model features to simulate soft rules and categories.
In particular, we use POS tag features
and features on the denotations of the predicted logical forms.

%In our setting, we only have question-answer pairs \cite{liang11dcs},
%requring us to perform an expensive search over possible logical forms
%and execute each on the knowledge base before making a parameter update.
%In the open domain, this cost becomes prohibitive.
%To address this issue, we propose a new learning scheme which interleaves
%execution with parameter updates, leading to much faster convergence.
% TODO: say our system is simpler than Cai/Yates

% Experiments
We experimented with two question answering datasets on Freebase.
First, on the dataset of \newcite{cai2013large},
we showed that our system outperforms their state-of-the-art system
62\% to 59\%, despite using no annotated logical forms.
Second, we collected a new realistic dataset of questions
by performing a breadth-first search using the Google Suggest API;
these questions are then answered by Amazon Mechanical Turk workers.
Although this dataset is much more challenging and noisy,
we are still able to achieve 31.4\% accuracy,
a 4.5\% absolute improvement over a natural baseline.
Both datasets as well as the source code for \textsc{Sempre}, our semantic parser, are publicly released and can be downloaded
from {\small \url{http://nlp.stanford.edu/software/sempre/}}.
