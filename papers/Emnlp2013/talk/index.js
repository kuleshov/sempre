// For node.js
require('./sfig/internal/sfig.js');
require('./sfig/internal/RootedTree.js');
require('./sfig/internal/Graph.js');
require('./sfig/internal/Outline.js');
require('./sfig/internal/metapost.js');
require('./sfig/internal/seedrandom.js');
require('./utils.js');

// Title slide
add(slide(nil(),
  ytable('Semantic Parsing on Freebase','from Question-Answer Pairs').center().scale(1.3).strokeColor('darkblue'),
  ytable(
    parentCenter(image('images/stanford-nlp-logo.jpeg').width(100)),
  _).center().ymargin(20),
  ytable(
    'EMNLP',
    'October 20, 2013',
  _).center(),
  ytable(
    'Jonathan Berant, Andrew Chou, Roy Frostig and Percy Liang',
  _).center().scale(0.8),
_).body.center().end.showHelp(false).showIndex(false));

add(slide('Web search',
  parentCenter(overlay(
    image('images/google-humphrey.png').width(500),
    pause(),
    bigFrameBox(text('Broad but shallow'.bold().fontcolor('green'))).scale(0.8),
  _).center().scale(1.5)),
_));



add(slide('Semantic Parsing',
  parentCenter(systemSpec({pause: true})),
  pause(),
  nil(),
  stmt('Motivation','Natural language interface to large structured knowledge-bases (Freebase, DBPedia, Yelp, ...)'),
_));

add(slide('Statistical semantic parsing',
  stmt('Supervision', 'manually annotated logical forms'),
  indent(table(
    [cnl('What\'s California\'s capital?'), nowrapText('$\\red{\\wl{Capital.California}}$').scale(0.8)],
    [cnl('How long is the Mississippi river?'), nowrapText('$\\red{\\wl{RiverLength.Mississippi}}$').scale(0.8)],
    ['...', '...'],
  _).margin(30, 5).ycenter()).scale(0.75),
  pause(),
  text(['Limitations:',
        'Requires experts &mdash; slow, expensive, does not scale!',
        'Restricted to limited domains']).bulleted(true),
_).rightHeader('[Zelle and Mooney, 1996; Zettlemoyer and Collins, 2005; ...]'));

add(slide('Weakly supervised parsers',
  stmt('Supervision', 'question/answers pairs'),
  indent(table(
    [cnl('What\'s California\'s capital?'), cwl('Sacramento')],
    [cnl('How long is the Mississippi river?'), cwl('3,734km')],
    ['...', '...'],
  _).margin(30, 5).center()).scale(0.75),
  pause(),
  stmt('Advantage', 'obtain from non-experts!'),
  pause(),
  parentCenter(table(['Dataset','\# word types'],
        [sc('GeoQuery'),'279' ],
        [sc('ATIS'),'936' ],
        [sc('KM-NP'),'158' ],
  _).margin(50,5).xjustify('lc')),
_).rightHeader('[Clarke et al. 2010; Liang et al. 2011]'));

add(slide('Scaling to large knowledge-bases',
  text(['Unsupervised systems with no training','Unger et al., 2012; Yahya et al., 2012']).bulleted(true),
  pause(),
  text(['Distant supervision (on a small set of KB predicates)','Krishnamurthy and Mitchell, 2012']).bulleted(true),
  pause(),
  text(['Parser trained from question/logical form pairs','Cai and Yates, 2013']).bulleted(true),
  pause(),
  frameBox(stmt('Our goal'.bold(), 'Training a parser from ' + 'question/answer pairs '.bold() + ' on a ' + 'large knowledge-base'.bold())).scale(0.95).padding(10),
_));

/*
add(slide('Contributions',
  let(s = 0.7),
  stmt('Challenge 1'.bold(), 'mapping correctly lexical items to KB predicates'),
  indent(text(['Thousands of text phrases:', cnl('marry, city, Obama')]).bulleted(true)).scale(0.8),
  indent(text(['Thousands of KB predicates:', cwl('Marriage.Spouse, Type.City, BarackObama')]).bulleted(true)).scale(0.8),
  stmt('Solution'.bold(), '(a) Alignment (b) Bridging'),
  pause(),
  stmt('Challenge 2'.bold(), 'getting a dataset of question-answer pairs'),
  parentCenter(ytable(nowrapText(ex1.x).scale(s), nowrapText(ex1.y).scale(s)).center()),
  stmt('Solution'.bold(), 'create questions via Google Suggest graph traversal and answers through crowdsourcing'),
_));*/

add(slide('Challenge: mapping text to the KB',
  parentCenter(challenge({bridging: true})).scale(0.8),
  pause(-2),
  text('Exhaustive enumeration is intractable [Liang et al. 2011]').bulleted(true).scale(0.8),
  pause(),
  text('String matching is not precise [Yahya et al. 2012]').bulleted(true).scale(0.8),
  pause(),
  text('String matching has coverage issues').bulleted(true).scale(0.8),
_));

add(slide('Contributions',
  parentCenter(contribution({bridging: true})).scale(0.8),
  yspace(10),
  pause(-1),
  text(stmt('Alignment','lexicon from text phrases to KB predicates')).fontSize(23),
  pause(1),
  text(stmt('Bridging','Use context to generate KB predicates')).fontSize(23),
_));

var outline = new sfig.Outline('Semantic parsing');
addOutlineSlide = function(title) {
  add(outline.createSlide(title).rightHeader(image('images/signpost.jpeg').dim(200)));
}
addOutlineSlide('Setup');

add(slide('Setup',
  headerList('Input',
  'Knowledge-base $\\sK$',
  'Training set of question-answer pairs $\\{(x_i,y_i)\\}_1^n$'),
  indent(xtable(cnl('What are the main cities in California?'),'$\\cwl{SF, LA, ...}$').margin(50)),
  pause(),
  headerList('Output',
    'Semantic parser that maps questions $x$ to answers $y$ through logical forms $z$'),
  table([cnl('countries in Asia'),'$\\Rightarrow$','$\\cwl{Type.Country} \\sqcap \\cwl{ContainedBy.Asia}$'],
        ['','$\\Rightarrow$',cwl('China, Japan, Israel, ...')]).margin(10),
//  parentCenter(xtable('$\\Rightarrow$',cwl('China, Japan, Israel, ...')).margin(10)),
_));

add(slide('Freebase knowledge graph',
  parentCenter(obamaKnowledgeGraph({pause: true})),
_));

add(slide('Freebase knowledge graph',
  parentCenter(obamaKnowledgeGraph({})).scale(0.5),
  parentCenter(ytable(
    '41M '+blue('entities')+' (nodes)',
    '19K '+brown('properties')+' (edge labels)',
    '596M assertions (edges)',
  _).margin(10)),
_));

add(slide('Logical forms are graph templates',

  parentCenter(stagger3(
    nowrapText('$\\green{\\wl{Type.Person}} \\sqcap \\wl{PlacesLived.Location.Chicago}$'),
    nowrapText('$\\wl{Type.Person} \\sqcap \\green{\\wl{PlacesLived.Location.Chicago}}$'),
    nowrapText('$\\wl{Type.Person} \\green{\\sqcap} \\wl{PlacesLived.Location.Chicago}$'),
    _)),
  pause(-2),
  parentCenter(xtable(
    stagger3(
    frameBox(queryKnowledgeGraph1({highlightEdges: true}).scale(0.8)).padding(10).bg.round(10).end,
    frameBox(queryKnowledgeGraph2({highlightEdges: true}).scale(0.8)).padding(10).bg.round(10).end,
    frameBox(queryKnowledgeGraph3({highlightEdges: true}).scale(0.8)).padding(10).bg.round(10).end,
    _),
    pause(),
    stagger(
      obamaKnowledgeGraph({}).scale(0.6),
      obamaKnowledgeGraph({highlightEdges: true, highlightNodes: ['BarackObama', 'MichelleObama']}).scale(0.6),
    _),
  _).margin(50)),
_));

addOutlineSlide('Alignment');

add(slide('Alignment',
  parentCenter(alignment({bridging: false})).scale(0.8),
_));

/*
add(slide('Lexical mapping challenges',
  bulletedText('19K Freebase properties, can\'t generate all for each word (was okay for 20 predicates in GeoQuery)'),
  parentCenter(nowrapText(cnl('born') + ' $\\Rightarrow$ ' + 'Type.City PeopleBornHere Profession.Lawyer ...'.split(' ').map(czl).join(', '))).scale(0.8),
  pause(),
  bulletedText('String matching only works partially:'),
  indent(headerList(null,
    cnl('Obama')+' maps to 16 possible entities',
    cnl('born')+' doesn\'t match "Place of birth"',
  _)),
_));*/

add(slide('Alignment: text phrases',
  stmt('ReVerb on ClueWeb09 [Thomas Lin]'),
  parentCenter(xtable(
    gridText(5, 4).scale(0.6),
    bigRightArrow(),
    ytable(
      textAssertion('Barack Obama', 'was born in', 'Honolulu'),
      textAssertion('Albert Einstein', 'was born in', 'Ulm'),
      textAssertion('Barack Obama', 'lived in', 'Chicago'),
      '... 15M triples ...',
    _).center().scale(0.8),
  _).center().margin(20)),
  yspace(20),
  pause(),
  bulletedText('Entities are linked to Freebase'),
  pause(),
  bulletedText('Hearst patterns used for unaries').color('darkblue'),
  pause(),
  '~15,000 text phrases',
_))

add(slide('Alignment: KB predicates',
  stmt('Freebase'),
  parentCenter(xtable(
    obamaKnowledgeGraph({}).scale(0.2),
    bigRightArrow(),
    ytable(
      freebaseAssertion('BarackObama', 'PlaceOfBirth', 'Honolulu'),
      freebaseAssertion('Albert Einstein', 'PlaceOfBirth', 'Ulm'),
      freebaseAssertion('BarackObama', 'PlacesLived.Location', 'Chicago'),
      '... 600M triples ...',
    _).center().scale(0.6),
  _).center().margin(20)),
  pause(),
  stmt('Binaries','paths of length 1 or 2 in the KB graph'),
  pause(),
  stmt('Unaries', cwl('Type.x') + ' or ' + cwl('Profession.x')),
  pause(),
  '~60,000 KB predicates',
_));

add(slide('Alignment: match phrases and predicates',
  parentCenter(alignmentGraph().scale(0.7)),
  pause(),
  indent(stmt('Lexicon', 'Mapping from phrases to predicates with features')),
  pause(),
  parentCenter(frameBox(table(
    ['phrase-count', ':', '15,765'],
    ['predicate-count', ':', '9,182'],
    ['intersection-count', ':', '6,048'],
    ['KB-best-match'.bold().fontcolor('red'), ':', '0'],
  _).xjustify('rcr')).title(opaquebg(darkblue('Alignment features'.bold()))).padding(10).scale(0.6)),
_));

addOutlineSlide('Bridging');

add(slide('Bridging',
  headerList('Often predicates are not expressed explicitly',
    cnl('What government does Chile have?'),
    cnl('What is Italy\'s language?'),
    cnl('Where is Beijing?'),
    cnl('What is the cover price of X-men?'),
    cnl('Who did Humphrey Bogart marry in 1928?')),
  pause(),
  red('Alignment') + ': build coarse mapping from raw text',
  (purple('Bridging') + ': use neighboring predicates / type constraints').bold(),
_))

add(slide('Bridging 1: two unaries',
  parentCenter(bridging1({bridging: true})).scale(0.8),
  yspace(10),
  parentCenter('$\\red{\\wl{Type.University} \\sqcap \\wl{Education.Institution}.\\wl{BarackObama}}$'),
  pause(),
  parentCenter(frameBox(table(
    ['br-popularity', ':', '11.37'],
    ['br-two-unaries', ':', '1'],
    ['br-education.institution', ':', '1'],
  _).xjustify('lcr')).title(opaquebg('features'.bold()))).padding(10).scale(0.6),
_));
/*
add(slide('Bridging 2: a single unary',
  parentCenter(bridging2({bridging: true})).scale(0.8),
  yspace(10),
  parentCenter('$\\red{\\wl{CoverPriceOf.X-Men}}$'),
  parentCenter(frameBox(table(
    ['br-popularity', ':', '2.49'],
    ['br-one-unary', ':', '1'],
    ['br-coverpriceof', ':', '1'],
    ['br-text-equal'.bold(), ':', '1'],
  _).xjustify('lcr')).title(opaquebg('features'.bold()))).padding(10).scale(0.6),
_));
*/
add(slide('Bridging 2: event modifiers',
  parentCenter(bridging3({bridging: false})).scale(0.8),
  yspace(10),
  parentCenter('$\\red{\\wl{Marriage}.(\\wl{Spouse.Madonna} \\sqcap \\wl{StartDate.2000})}$'),
  parentCenter(frameBox(table(
    ['br-popularity', ':', '7.11'],
    ['br-inject', ':', '1'],
    ['br-startdate', ':', '1'],
  _).xjustify('lcr')).title(opaquebg('features'.bold()))).padding(10).scale(0.6),
_));

addOutlineSlide('Composition');

add(slide('One derivation',
  parentCenter(parseExample({pause: true})).scale(0.8),
  pause(),
  'Derivations are constructed using an over-general grammar',
_));
/*
add(slide('Candidate derivations',
  parentCenter(ytable(
    cnl('Where was Obama born?'), pause(),
    xtable(bigDownArrow(60), text(red('?'.bold())).scale(2).orphan(true)).center().margin(20),
    parentCenter('set of candidate derivations $\\sD(x)$'),
    xtable(
      parseExample({}).scale(0.45),
      pause(),
      parseExampleWrong({}).scale(0.45),
      '...',
    _).center().margin(20),
  _).center().margin(20)),
_));
*/
add(slide('Model',
  stmt('Candidate derivations', '$\\red{\\sD(x)}$'),
  pause(),
  stmt('Model', 'distribution over derivations $d$ given utterance $x$'),
  parentCenter('$p(d \\mid x, \\theta) = \\frac{\\exp(\\phi(x,d) \\cdot \\theta)}{\\sum_{d\' \\in \\red{\\sD(x)}} \\exp(\\phi(x,d\') \\cdot \\theta)}$'),
  pause(),
  headerList('Features', 'Alignment and bridging', 'lexicalized', 'syntactic',  'denotation').scale(0.9),
  pause(),
  headerList('Training (estimating $\\theta$)',
    'Stochastic gradient descent (AdaGrad)',
    //'Beam search to approximate candidates $\\red{\\sD(x)}$',
  _),
_));

addOutlineSlide('Dataset creation');

function threePartQuestion(question) {
  var tokens = question.split(' | ');
  return blue(tokens[0]).italics()+' '+red(tokens[1]).italics()+' '+green(tokens[2]).italics();
}

function suggest(question, items, color) {
  return xtable(
    threePartQuestion(question),
    ytable(text('Google Suggest'.bold()).scale(0.6).orphan(true), rightArrow(50).strokeWidth(3)).center(),
    ytable.apply(null, items.split(" | ").map(color)).scale(0.6),
  _).center().margin(20);
}

add(slide('WebQuestions: getting questions',
  stmt('Strategy', 'breadth-first search over Google Suggest graph'),
  pause(),
  threePartQuestion('Where was | Barack Obama | born?'),
  pause(),
  suggest('Where was | _ | born?', 'Barack Obama | Lady Gaga | Steve Jobs', red),
  pause(),
  threePartQuestion('Where was | Steve Jobs | born?'),
  pause(),
  suggest('Where was | Steve Jobs | _?', 'born | raised | on the Forbes list', green),
  pause(),
  threePartQuestion('Where was | Steve Jobs | raised?'),
  pause(),
  stmt('Result: popular web questions'),
  pause(),
  'Answers were obtained through crowdsourcing (AMT)',
_));

/*
add(slide('WebQuestions: getting answers',
  stmt('Strategy', 'use Amazon Mechanical Turk (AMT)'),
  stmt('Question', cnl('Where was Barack Obama born?')), pause(),
  xtable(
    image('images/barack-obama-freebase.png').width(500),
    ytable(
      stmt('Choose answer'),
      indent(ytable.apply(null, 'Michelle Obama | 1961-08-04 | Harvard | Honolulu | Chicago | ...'.split(' | ').map(red))),
    _),
  _).margin(5),
_));
*/
G.questions = function(questions, opts) {
  var items = questions.map(function(x) {
    if (x == _) return _;
    if (typeof(x) == 'string') return nowrapText(green(x.italics()));
    return nowrapText(green(x[0].italics()) + ' $\\Rightarrow$ ' + blue(x[1]));
  });
  return ytable.apply(null, items).scale(0.75).margin(opts.margin || 0);
}

add(slide('Dataset comparison',
  stmt('Free917 [Cai and Yates, 2013]: 917 examples, 2,036 word types'),
  questions([
    'What is the engine in a 2010 Ferrari California?',
    'What was the cover price of the X-men Issue 1?',
  _], {}),
  bulletedText('Generate questions based on Freebase facts'),
  stmt('WebQuestions [our work]: 5,810 examples, 4,525 word types'),
  questions([
    'What character did Natalie Portman play in Star Wars?',
    'What kind of money to take to Bahamas?',
    'What did Edward Jenner do for a living?',
  _], {}),
  bulletedText('Generate questions from Google $\\Rightarrow$ less formulaic'),
_));

addOutlineSlide('Experiments');

add(slide('Results on Free917',
  parentCenter(barGraph([[1, 59], [2, 62]])
    .xtickLabels([nil(), '[Cai and Yates, 2013]', 'our system'])
    .trajectoryColors(['gray', 'red'])
    .xrange(0, 3).xtickIncludeAxis(false).xtickIncrValue(1).yrange(50, 70).xlength(500).barWidth(50).yroundPlaces(0).ytickIncrValue(10)),
  headerList('Differences',
    'We train from answers only, CY13 uses logical forms',
    'We use 12K binary predicates, CY13 used 2k binary predicates',
    pause(),
    'Kwiatkowski et al. obtain larger improvement',
  _),
_));
/*
add(slide('Results on WebQuestions',
  parentCenter(barGraph([[1, 26.9], [2, 31.4]])
    .xtickLabels([nil(), 'Baseline system', 'Full system'])
    .trajectoryColors(['gray', 'red'])
    .xrange(0, 3).xtickIncludeAxis(false).xtickIncrValue(1).yrange(0, 40).xlength(500).barWidth(50).yroundPlaces(0).ytickIncrValue(10)),
  headerList('Baseline system',
    'No alignment features, bridging and denotation features',
  _),
_));
*/
add(slide('Impact of alignment and bridging',
  parentCenter(table(
    [barGraph([[1, 38.0], [2, 66.9], [3, 71.3]])
      .xtickLabels([nil(), 'Alignment', 'Bridging', 'Both'])
      .trajectoryColors(['red', 'purple', 'gray'])
      .xrange(0, 4).xtickIncludeAxis(false).xtickIncrValue(1).yrange(0, 80).xlength(400).barWidth(50).yroundPlaces(0).ytickIncrValue(20),
    barGraph([[1, 30.6], [2, 21.2], [3, 32.9]])
      .xtickLabels([nil(), 'Alignment', 'Bridging', 'Both'])
      .trajectoryColors(['red', 'purple', 'gray'])
      .xrange(0, 4).xtickIncludeAxis(false).xtickIncrValue(1).yrange(0, 40).xlength(400).barWidth(50).yroundPlaces(0).ytickIncrValue(10)],
    ['Free917', 'WebQuestions'],
  _).center().margin(10).scale(0.8)),
  headerList('Conclusions',
    purple('Bridging')+' more important for Free917',
    red('Alignment')+' more important for WebQuestions',
  _),
  pause(),
  'Test accuracy on webQuestions: 35.7',
_));
/*
add(slide('Alignment vs. lexicalized features',
  parentCenter(table(
    [barGraph([[1, 71.3], [2, 68.5]])
      .xtickLabels([nil(), 'Alignment', 'Lexicalized'])
      .trajectoryColors(['gray', 'red'])
      .xrange(0, 3).xtickIncludeAxis(false).xtickIncrValue(1).yrange(0, 100).xlength(400).barWidth(50).yroundPlaces(0).ytickIncrValue(20),
    barGraph([[1, 32.9], [2, 34.2]])
      .xtickLabels([nil(), 'Alignment', 'Lexicalized'])
      .trajectoryColors(['gray', 'red'])
      .xrange(0, 3).xtickIncludeAxis(false).xtickIncrValue(1).yrange(0, 50).xlength(400).barWidth(50).yroundPlaces(0).ytickIncrValue(10)],
    ['Free917', 'WebQuestions'],
  _).center().margin(10).scale(0.8)),
  headerList('Conclusions',
    'Alignment works better for Free917 (smaller dataset)',
    red('Lexicalized')+' works better for WebQuestions',
  _),
_));
*/
function fr(x) { return frameBox(x).padding(5).bg.round(10).end; }
add(slide('Summary',
  parentCenter(overlay(
    ytable(
      a = fr('Learning from question-answer pairs'), pause(),
      b = fr('Scaling up via alignment/bridging [EMNLP 2013]'.bold()), pause(),
      xtable(
        c1 = fr(red('Paraphrases')),
        c2 = fr(red('Compositionality')),
      _).margin(80),
    _).center().margin(100),
    pause(-1),
    arrow(a, b),
    pause(),
    arrow(b, c1),
    arrow(b, c2),
  _)),
_));

add(slide(null,
  yspace(50),
  parentCenter(xtable(
    parentCenter(obamaKnowledgeGraph({}).scale(0.4)),
  _).margin(100).center()),
  stmt('All data and code'),
  parentCenter(blue('http://www-nlp.stanford.edu/software/sempre/')),
  pause(),
  yspace(30),
  parentCenter('Thank you!'.bold()),
_).body.end);

// For node.js: output PDF (you must have Metapost installed).
prez.writePdf({outPrefix: 'index'});
