G = sfig.serverSide ? global : this;

sfig.importAllMethods(G);

sfig.latexMacro('bx', 0, '\\mathbf{x}');
sfig.latexMacro('bs', 0, '\\mathbf{s}');
sfig.latexMacro('bt', 0, '\\mathbf{t}');
sfig.latexMacro('wl', 1, '\\text{#1}');
sfig.latexMacro('cwl', 1, '\\blue{\\wl{#1}}');
sfig.latexMacro('zl', 1, '\\text{#1}');
sfig.latexMacro('czl', 1, '\\red{\\zl{#1}}');
sfig.latexMacro('R', 0, '\\mathbb{R}');
sfig.latexMacro('P', 0, '\\mathbb{P}');
sfig.latexMacro('E', 0, '\\mathbb{E}');
sfig.latexMacro('sF', 0, '\\mathcal{F}');
sfig.latexMacro('sS', 0, '\\mathcal{S}');
sfig.latexMacro('sZ', 0, '\\mathcal{Z}');
sfig.latexMacro('sD', 0, '\\mathcal{D}');
sfig.latexMacro('sK', 0, '\\mathcal{K}');
sfig.latexMacro('diag', 0, '\\text{diag}');
sfig.latexMacro('deriv', 0, '\\quad\\Rightarrow\\quad');
sfig.latexMacro('ts', 1, '\\text{#1}');
sfig.latexMacro('phix', 0, 't');
sfig.latexMacro('reverse', 1, '\\mathbf{R}[#1]');
sfig.latexMacro('den', 1, '\[ #1 \]_{\\sK}');

sfig.Text.defaults.setProperty('font', 'Arial');

sfig.initialize();

G.prez = sfig.presentation();
G.add = function(slide) { prez.addSlide(slide); }

// Colors
G.nlcolor = 'green';
G.zlcolor = 'red';
G.wlcolor = 'blue';
G.nl = function(x) { return x.italics().fontcolor(nlcolor); }
G.zl = function(x) { return ('<tt>' + x + '</tt>').fontcolor(zlcolor); }
G.wl = function(x) { return ('<tt>' + x + '</tt>').fontcolor(wlcolor); }
//function yl(x) { return x.italics().fontcolor(zlcolor); }
G.sc= function(x) { return ('<tt>' + x + '</tt>'); }

G.frameBox = function(a) { return frame(a).bg.strokeWidth(1).fillColor('white').end; }
G.bigFrameBox = function(a) { return frameBox(a).padding(10).scale(1.4); }

G.bigLeftArrow = function(s) { return leftArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigRightArrow = function(s) { return rightArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigUpArrow = function(s) { return upArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigDownArrow = function(s) { return downArrow(s || 100).strokeWidth(10).color('brown'); }

G.red = function(x) { return x.fontcolor('red'); }
G.green = function(x) { return x.fontcolor('green'); }
G.blue = function(x) { return x.fontcolor('blue'); }
G.darkblue = function(x) { return x.fontcolor('darkblue'); }
G.brown = function(x) { return x.fontcolor('brown'); }
G.purple = function(x) { return x.fontcolor('purple'); }

G.wholeNumbers = function(n) {
  var result = [];
  for (var i = 0; i < n; i++) result[i] = i;
  return result;
}

G.randomChoice = function(list) {
  var i = Math.floor(Math.random() * list.length);
  return list[i];
}

////////////////////////////////////////////////////////////

G.stmt = function(prefix, suffix) {
  var m;
  if (!suffix && (m = prefix.match(/^([^:]+): (.+)$/))) {
    prefix = m[1];
    suffix = m[2];
  }
  return prefix.fontcolor('darkblue') + ':' + (suffix ? ' '+suffix : '');
}

G.xseq = function() { return new sfig.Table([arguments]).center().margin(5); }
G.yseq = function() { return ytable.apply(null, arguments).margin(10); }

G.headerList = function(title) {
  var contents = Array.prototype.slice.call(arguments).slice(1);
  return yseq.apply(null, [title ? stmt(title) : _].concat(contents.map(function(line) {
    if (line == _) return _;
    if (typeof(line) == 'string') return bulletedText([null, line]);
    if (line instanceof sfig.PropertyChanger) return line;
    return indent(line);
  })));
}

G.node = function(x, shaded) { return overlay(circle(20).fillColor(shaded ? 'lightgray' : 'white'), std(x).orphan(true)).center(); }

G.zspan = function(i, j) { return text('$s_{'+i+j+'}$'); }
G.xleaf = function(i) { return text('$x_{'+i+'}$'); }

G.paramMatrix = function(x) { return text(x).opacity(1).scale(0.6); }
G.paramPi = function() { return paramMatrix('$\\green{\\pi}$'); }
G.paramT = function() { return paramMatrix('$\\blue{T}$'); }
G.paramB = function() { return paramMatrix('$\\blue{B}$'); }
G.paramO = function() { return paramMatrix('$\\red{O}$'); }

var Tmargin = 40;
G.T = function() {
  return sfig.rootedTree.apply(null, arguments).margin(Tmargin, Tmargin).nodeRound(10).verticalCenterEdges(true);
}
G.B = function(edgeLabel, child) {
  if (edgeLabel != null) edgeLabel = frame(edgeLabel).padding(2).bg.fillColor('white').end;
  return sfig.rootedTreeBranch(edgeLabel, child);
}
G.Lf = function(x) {
  var t = sfig.rootedTree(x).nodeRound(10);
  t.headBox.content.bg.fillColor('lightgray');
  return t;
}

G.sym = function(label) { return zl(label); }
G.obs = function(label) { return Lf(nl(label)); }

G.syntax = function() {
  return T(sym('S'),
    T(sym('N'), obs('I')),
    T(sym('VP'), T(sym('V'), obs('like')), T(sym('N'), obs('algorithms'))),
  _);
}

G.syntaxAlt = function() {
  return T(sym('S'),
    T(sym('NV'), T(sym('N'), obs('I')), T(sym('V'), obs('like'))),
    T(sym('N'), obs('algorithms')),
  _);
}

G.semantics = function() {
  return text('$\\text{like}(\\text{percy}, \\text{algorithms})$').color(wlcolor);
}

G.indent = function(x, n) { return frame(x).xpadding(n != null ? n : 20).xpivot(1); }
G.header = function(x) { return text(x.fontcolor('darkblue')); }
G.change = function(a, b) { return overlay(a, pause(), b.replace(a)); }

G.nlcolor = 'green';  // Natural language
G.zlcolor = 'red';  // Logical form
G.wlcolor = 'blue';  // World
G.nl = function(x) { return x.italics(); }
G.cnl = function(x) { return nl(x).fontcolor(nlcolor); }
G.zl = function(x) { return '$\\zl{' + x + '}$'; }
G.wl = function(x) { return '$\\wl{' + x + '}$'; }
G.czl = function(x) { return '$\\czl{' + x + '}$'; }
G.cwl = function(x) { return '$\\cwl{' + x + '}$'; }


G.stagger = function(b1, b2) { b1 = std(b1); b2 = std(b2); return overlay(b1.numLevels(1), pause(), b2); }
G.stagger3 = function(b1, b2, b3) { b1 = std(b1); b2 = std(b2); b3 = std(b3); return overlay(b1.numLevels(1), pause(), b2.numLevels(1), pause(),b3); }

G.dividerSlide = function(text) {
  return slide(null, nil(), parentCenter(text)).titleHeight(0);
}

G.moveLeftOf = function(a, b, offset) { return transform(a).pivot(1, 0).shift(b.left().sub(offset == null ? 5 : offset), b.ymiddle()); }
G.moveRightOf = function(a, b, offset) { return transform(a).pivot(-1, 0).shift(b.right().add(offset == null ? 5 : offset), b.ymiddle()); }
G.moveTopOf = function(a, b, offset) { return transform(a).pivot(0, 1).shift(b.xmiddle(), b.top().sub(offset == null ? 5 : offset)); }
G.moveBottomOf = function(a, b, offset) { return transform(a).pivot(0, -1).shift(b.xmiddle(), b.bottom().add(offset == null ? 5 : offset)); }
G.moveCenterOf = function(a, b) { return transform(a).pivot(0, 0).shift(b.xmiddle(), b.ymiddle()); }

// An example of parsing a sentence.
G.parseExample = function(opts) {
  var myPause = opts.pause ? pause : function() { return _; };
  T = rootedTree;

  var intersection, join, t1, t2, t3;
  function labelVerticalEdge(label, t) { return moveRightOf(label, t.branches[0].edge); }
  return overlay(
    T(
      intersection = nowrapText(czl('Type.City') + '$\\,\\sqcap\\,$' + czl('PeopleBornHere.BarackObama')),
      cnl('what'),
      t1 = T(czl('Type.CityTown'), cnl('city')),
      cnl('was'),
      T(join = text(czl('PeopleBornHere.BarackObama')),
        t2 = T(czl('BarackObama'), cnl('Obama')),
        t3 = T(czl('PeopleBornHere'), cnl('born')),
      _),
      cnl('?'),
    _).recnodeBorderWidth(0).recymargin(90),
    myPause(),
    labelVerticalEdge(('Alignment'.bold()), t1),
    labelVerticalEdge(('Alignment'.bold()), t2),
    labelVerticalEdge(('Alignment'.bold()), t3),
    myPause(),
    moveBottomOf(('join'.bold()), join),
    myPause(),
    moveBottomOf(('intersect'.bold()), intersection),
  _);
}

G.parseExampleWrong = function(opts) {
  var myPause = opts.pause ? pause : function() { return _; };
  T = rootedTree;

  var intersection, join, t1, t2, t3;
  function labelVerticalEdge(label, t) { return moveRightOf(label, t.branches[0].edge); }
  return overlay(
    T(
      join = nowrapText(czl('Length.Song325')),
      t1 = T(czl('Song325'), cnl('where')),
      t2 = T(czl('Length'), cnl('was')),
      cnl('Obama'),
      cnl('born'),
      cnl('?'),
    _).recnodeBorderWidth(0).recymargin(90),
    labelVerticalEdge(('lexicon'.bold()), t1),
    labelVerticalEdge(('lexicon'.bold()), t2),
    moveBottomOf(('join'.bold()), join),
  _);
}

G.drawKnowledgeBaseGraph = function(entries, opts) {
  var nodes = {}; // Map node name => object
  var nodePositions = {};  // Map node name => position
  var out = [];
  var getNode = function(name, dx, dy) {
    var posScale = 140;
    var o = nodes[name];
    if (!o) {
      o = nodes[name] = center(name.bold().fontcolor('blue')).scale(0.8);
      if (opts.highlightNodes && opts.highlightNodes.indexOf(name) != -1)
        o = frameBox(o).bg.strokeWidth(2).round(5).end;
      o.shift(dx*posScale, down(dy*posScale));
      nodePositions[name] = [dx, dy];
      out.push(o);
    }
    return o;
  }
  entries.forEach(function(info) {
    var e1 = info[0];
    var property = info[1][0];
    var dx = info[1][1];
    var dy = info[1][2];
    var highlight = info[1][3];
    var e2 = info[2];

    if (opts.pause)
      out.push(pause());

    var e1obj = getNode(e1, 0, 0);
    var e2obj = getNode(e2, nodePositions[e1][0]+dx, nodePositions[e1][1]+dy);
    var link = arrow(e1obj, e2obj);
    if (opts.highlightEdges && highlight) link.strokeWidth(6).strokeColor('green');
    else link.strokeWidth(2);
    out.push(link);
    out.push(opaquebg(center(property.fontcolor('brown'))).scale(0.6).shift(link.xmiddle(), link.ymiddle()));
  });
  return new Overlay(out);
}

G.queryKnowledgeGraph1 = function(opts) {
  var x = 'o';
  return drawKnowledgeBaseGraph([
    [x, ['Type', -0.5, 1,true], 'Person'],
    [x, ['PlacesLived', 0.5, 1], '?'],
    ['?', ['Location', 0, 1], 'Chicago'],
  ], opts);
}

G.queryKnowledgeGraph2 = function(opts) {
  var x = 'o';
  return drawKnowledgeBaseGraph([
    [x, ['Type', -0.5, 1], 'Person'],
    [x, ['PlacesLived', 0.5, 1,true], '?'],
    ['?', ['Location', 0, 1,true], 'Chicago'],
  ], opts);
}

G.queryKnowledgeGraph3 = function(opts) {
  var x = 'o';
  return drawKnowledgeBaseGraph([
    [x, ['Type', -0.5, 1,true], 'Person'],
    [x, ['PlacesLived', 0.5, 1,true], '?'],
    ['?', ['Location', 0, 1,true], 'Chicago'],
  ], opts);
}

G.obamaKnowledgeGraph = function(opts) {
  var ObamaMarriage = 'Event8';
  var BarackObamaLiveInChicago = 'Event3';
  var MichelleObamaLiveInChicago = 'Event21';

  return drawKnowledgeBaseGraph([
    ['BarackObama', ['Type', -2, +1, true], 'Person'],
    ['BarackObama', ['Profession', +1, +1], 'Politician'],

    ['BarackObama', ['DateOfBirth', -0.5, 1], '1961.08.04'],
    ['BarackObama', ['PlaceOfBirth', 2, 0], 'Honolulu'],
    ['Honolulu', ['ContainedBy', 0, -1], 'Hawaii'],
    ['Honolulu', ['Type', 0, 1], 'City'],
    ['Hawaii', ['ContainedBy', -1.5, +0.5], 'UnitedStates'],
    ['Hawaii', ['Type', 0, -0.7], 'USState'],

    ['BarackObama', ['Marriage', -1, -1], ObamaMarriage],
    [ObamaMarriage, ['Spouse', -1, -1], 'MichelleObama'],
    ['MichelleObama', ['Type', 1, 0, true], 'Person'],
    ['MichelleObama', ['Gender', 1.5, 0.3], 'Female'],
    [ObamaMarriage, ['StartDate', 1.5, -0.5], '1992.10.03'],

    ['BarackObama', ['PlacesLived', -1.5, +0.4, true], BarackObamaLiveInChicago],
    [BarackObamaLiveInChicago, ['Location', -1, -0.4, true], 'Chicago'],
    ['MichelleObama', ['PlacesLived', -0.5, +1, true], MichelleObamaLiveInChicago],
    [MichelleObamaLiveInChicago, ['Location', 0, 0, true], 'Chicago'],
    ['Chicago', ['ContainedBy', 0, 0, true], 'UnitedStates'],
  ], opts);
}

G.framework = function(opts) {
  var myPause = opts.pause ? pause : function() { return _; };
  return overlay(
    table(
      [nil(), x = node('$\\green{x}$', true)], myPause(),
      [theta = node('$\\red{\\theta}$'), z = node('$\\red{z}$')], myPause(),
      [w = node('$\\blue{w}$', true), y = node('$\\blue{y}$', true),],
    _).margin(100, 100),
    myPause(-2),
    moveRightOf(ytable(cnl('city in'), cnl('California?')), x),
    myPause(),
    arrow(x, z), arrow(theta, z),
    moveTopOf(red('parameters'), theta),
    moveRightOf(text('$\\red{\\wl{Type.City} \\sqcap \\wl{ContainedBy.CA}}$').scale(0.6), z),
    myPause(),
    arrow(w, y), arrow(z, y),
    moveTopOf(blue('database'), w),
    moveRightOf(blue('{SF,LA,...}'), y),
  _);
}

/*G.ex1 = {
  x: cnl('Of countries that don\'t border an ocean, which has the most people?'),
  z: '$\\red{\\wl{argmax}(\\wl{Type.Country} \\sqcap \\neg \\wl{Border.Ocean}, \\wl{Population})}$',
  y: cwl('Egypt'),
};*/
/*G.ex1 = {
  x: cnl('Which states\' capitals are also their largest cities by area?'),
  z: '$\\red{\\mu x . \\wl{Type.USState} \\sqcap \\wl{Capital}. \\wl{argmax}(\\wl{Type.City} \\sqcap \\wl{ContainedBy}.x, \\wl{Area})}$',
  y: cwl('Arizona,Hawaii,Idaho,Indiana,Iowa,Oklahoma,Utah'),
};*/
G.ex1 = {
  x: cnl('Who did Humphrey Bogart marry in 1928?'),
  z: '$\\red{ \\wl{Type.Person} \\sqcap \\wl{Marriage}.(\\wl{Spouse.HumphreyBogart} \\sqcap \\wl{StartDate.1928})}$',
  y: cwl('Mary Philips'),
};

G.gridText = function(nr, nc) {
  function textDocumentImage() {
    return image('images/simple-document.jpg').width(30);
  }
  var contents = wholeNumbers(nr).map(function() {
    return wholeNumbers(nc).map(function() {
      return Math.random() < 1 ? textDocumentImage() : nil();
    });
  });
  return table.apply(null, contents).margin(10);
}

G.challenge= function(opts) {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  var alignment = red('alignment');
  var bridging = 'bridging'.fontcolor('purple');
  var allPred = ytable(cwl('BarackObama'),cwl('TopGun'),cwl('MichelleObama'),cwl('Type.Country'),cwl('Type.city'),cwl('Profession.Lawyer'),cwl('PeopleBornHere'),cwl('InventorOf'),cwl('CapitalOf'),peopleKb = text(cwl('$\\vdots \\vdots$'))).center().scale(0.3);
  var example =
    ytable(
    xtable(image('images/database.png').dim(100),ytable(cwl('BarackObama'),cwl('TopGun'),cwl('MichelleObama'),cwl('Type.Country'),cwl('Type.city'),cwl('Profession.Lawyer'),cwl('PeopleBornHere'),cwl('InventorOf'),cwl('CapitalOf'),cwl('$\\vdots \\vdots$')).center().scale(0.3)).margin(20),
    yspace(30),
    table(
      [
        nil(),
        pause(2),
        ytable(cwl('Type.HumanLanguage'),cwl('SpeechLanguagePathology'),languageKb = text(cwl('LanguageAcquisition'))).center().scale(0.7),
        nil(),
        pause(-1),
        allPred,
        nil(),
        pause(1),
        nil(),//ytable(cwl('Brazil'),cwl('BrazilNut'),cwl('BrazilPresident'), brazilKb = text(cwl('BrazilIndiana'))).center().scale(0.7),
        pause(1),
        ytable(useKb = text(cwl('LanguagesSpoken'))).center().scale(0.7),
        _],
      pause(-3),
      [cnl('What'.bold()), languageText = text(cnl('languages'.bold())), cnl('do'.bold()), peopleText = text(cnl('people'.bold())), cnl('in'.bold()), brazilText = text(cnl('Brazil'.bold())), useText = text(cnl('use'.bold()))],
    _).margin(35).center(),
  _);
  //var a = conn(languageKb,languageText);
  return overlay(
      example,
      pause(1),
      conn(peopleText,peopleKb),
      pause(1),
      conn(languageText,languageKb),
 //     conn(brazilText,brazilKb),
      pause(1),
      conn(useText,useKb),
  _);
}
G.contribution= function(opts) {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  var alignment = red('alignment');
  var bridging = 'bridging'.fontcolor('purple');
  var example =
    ytable(
    xtable(image('images/database.png').dim(100),ytable(cwl('BarackObama'),cwl('TopGun'),cwl('Type.Country'),cwl('Profession.Lawyer'),cwl('PeopleBornHere'),cwl('InventorOf'),cwl('$\\vdots \\vdots$')).center().scale(0.3)).margin(20),
    table(
      pause(2),
      [nil(),nil(),nil(),ytable(cwl('LanguagesSpoken')).scale(0.7),nil(),nil(),nil()],
      [ pause(-1),
        nil(),
        ytable(cwl('Type.HumanLanguage'),languageKb = text(cwl('Type.ProgrammingLanguage'))).center().scale(0.7),
        nil(),
        nil(),
        nil(),
        ytable(cwl('Brazil'),brazilKb = text(cwl('BrazilFootballTeam'))).center().scale(0.7),
//        ytable(cwl('LanguagesSpoken'),cwl('CurrencyUsed'),useKb = text(cwl('Timezone'))).center().scale(0.7),
        nil(),
        _],
      [nil(),nil(),nil(),nil(),nil(),nil(),nil()],
      pause(-1),
      [cnl('What'.bold()), languageText = text(cnl('languages'.bold())), cnl('do'.bold()), peopleText = text(cnl('people'.bold())), cnl('in'.bold()), brazilText = text(cnl('Brazil'.bold())), useText = text(cnl('use'.bold()))],
    _).margin(35).center().scale(0.9),
    yspace(5),
  _);
  //var a = conn(languageKb,languageText);
  return overlay(
      example,
      pause(1),
      labelConn(languageText,languageKb,alignment.bold(),'red'),
      labelConn(brazilText,brazilKb,alignment.bold(),'red'),
   //   labelConn(useText,useKb,alignment.bold(),'red'),
      pause(1),
      labelConn(brazilKb,languageKb,label = bridging.bold(),'purple'),
  _);
}

G.alignment=function(opts) {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  var alignment = red('alignment');
  var example =
    ytable(
    xtable(image('images/database.png').dim(100),ytable(cwl('BarackObama'),cwl('TopGun'),cwl('Type.Country'),cwl('Profession.Lawyer'),cwl('PeopleBornHere'),cwl('InventorOf'),cwl('$\\vdots \\vdots$')).center().scale(0.6)).margin(20),
    yspace(100),
    table(
      [
        nil(),
        ytable(cwl('Type.HumanLanguage'),languageKb = text(cwl('Type.ProgrammingLanguage'))).center().scale(0.7),
        nil(),
        nil(),
        nil(),
        ytable(cwl('Brazil'),brazilKb = text(cwl('BrazilFootballTeam'))).center().scale(0.7),
//        ytable(cwl('LanguagesSpoken'),cwl('CurrencyUsed'),useKb = text(cwl('Timezone'))).center().scale(0.7),
        nil(),
        _],
      [nil(),nil(),nil(),nil(),nil(),nil(),nil()],
      [cnl('What'.bold()), languageText = text(cnl('languages'.bold())), cnl('do'.bold()), peopleText = text(cnl('people'.bold())), cnl('in'.bold()), brazilText = text(cnl('Brazil'.bold())), useText = text(cnl('use'.bold()))],
    _).margin(35).center().scale(0.9),
    yspace(5),
  _);
  //var a = conn(languageKb,languageText);
  return overlay(
      example,
      labelConn(languageText,languageKb,alignment.bold(),'red'),
      labelConn(brazilText,brazilKb,alignment.bold(),'red'),
  _);
}



G.bridging1= function(opts) {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  alignment = red('alignment');
  var bridging = 'bridging'.fontcolor('purple');
  var example = ytable(
    /*text(blue('\\{Occidental College, Columbia University\\}')),
    overlay(
      bigUpArrow(130),
      frameBox('Execute on Database').bg.round(10).end,
    _).center(),*/
    //'$\\red{\\wl{University} \\sqcap \\wl{Education}.\\wl{BarackObama}}$',
    //nowrapText('$\\lambda x . \\wl{University}(x) \\wedge \\wl{Education}(\\wl{BarackObama}, x)$'),
    yspace(50),
    table(
      [nil(), unary = text(wl('Type.University')), nil(), nil(), nil(), nil()],
      [nil(), nil(), nil(), pause(), stagger(binary = text(wl('Education.Institution')),text(wl('HonorRecepient'))), nil(), nil()],
      opts.bridging ? pause(-2) : _,
      [nil(), nil(), nil(), entity = text(wl('BarackObama')), nil(), nil()],
      [cnl('Which'), nlUnary = text(cnl('college')), cnl('did'), nlEntity = text(cnl('Obama')), nlBinary = text(cnl('go to')), cnl('?')],
    _).margin(50).center(),
  _).justify('c', 'l').margin(10);
  return overlay(
    example,
    labelConn(unary, nlUnary, alignment.bold(), 'red'),
    labelConn(entity, nlEntity, alignment.bold(), 'red'),
    //labelConn(binary, nlBinary, '(alignment)'.fontcolor('gray'), 'gray'),
    //conn(binary, nlBinary).color('gray'),
    //labelConn(unary, binary, bridging.bold(), 'purple'),
    opts.bridging ? overlay(
      pause(),
      center(bridging.bold()).shift(binary.xmiddle(), binary.top().up(40)),
      conn(unary, binary).color('purple'),
      //labelConn(binary, entity, bridging.bold(), 'purple'),
      conn(binary, entity).color('purple'),
    _) : _,
  _);
}
G.bridging2= function(opts) {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  alignment = red('alignment');
  var bridging = 'bridging'.fontcolor('purple');
  var example = ytable(
    /*text(blue('\\{Occidental College, Columbia University\\}')),
    overlay(
      bigUpArrow(130),
      frameBox('Execute on Database').bg.round(10).end,
    _).center(),*/
    //'$\\red{\\wl{University} \\sqcap \\wl{Education}.\\wl{BarackObama}}$',
    //nowrapText('$\\lambda x . \\wl{University}(x) \\wedge \\wl{Education}(\\wl{BarackObama}, x)$'),
    yspace(50),
    table(
      [nil(), nil(), nil(), nil() ,nil() , nil()],
      [nil(), nil(), nil(), nil() ,opts.bridging ? pause() : _,(binary = text(wl('CoverPriceOf'))) , nil()],
      opts.bridging ? pause(-1) : _,
      [nil(), nil(), nil(), nil(), entity = text(wl('X-Men')), nil()],
      [cnl('What'), text(cnl('is')), cnl('the'), text(cnl('cover price of')), nlEntity = text(cnl('X-men')), cnl('?')],
    _).margin(50).center(),
  _).justify('c', 'l').margin(10);
  return overlay(
    example,
    labelConn(entity, nlEntity, alignment.bold(), 'red'),
    opts.bridging ? overlay(
      pause(),
      center(bridging.bold()).shift(binary.xmiddle(), binary.top().up(40)),
      //conn(unary, binary).color('purple'),
      //labelConn(binary, entity, bridging.bold(), 'purple'),
      conn(binary, entity).color('purple'),
    _) : _,
  _);
}

G.bridging3= function(opts) {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  alignment = red('alignment');
  var bridging = 'bridging'.fontcolor('purple');
  var example = ytable(
    yspace(50),
    table(
      [nil(), nil(), nil(), nil(), nil(), nil()],
      [nil(), nil(), join = text(wl('Marriage.Spouse.Madonna')), nil(), nil(), pause(), bridge = text(wl('Marriage.StartDate'))],
      pause(-1),
      [nil(), nil(), entity = text(wl('Madonna')), binary = text(wl('Marriage.Spouse')), nil(), date = text(wl('2000'))],
      [cnl('Who'), cnl('did'), nlEntity = text(cnl('Madonna')), nlBinary = text(cnl('marry')), cnl('in'), nlDate = text(cnl('2000'))],
    _).margin(50).center().scale(0.77),
  _).justify('c', 'l').margin(10);
  return overlay(
    example,
    labelConn(entity, nlEntity, alignment.bold(), 'red'),
    labelConn(binary, nlBinary, alignment.bold(), 'red'),
    conn(date, nlDate),
    labelConn(join,binary,'join'.bold(), 'black'),
    labelConn(join,entity,'join'.bold(), 'black'),
    pause(),
    center(bridging.bold()).shift(binary.xmiddle(), binary.top().up(75)),
    conn(bridge,date).color('purple'),
    conn(bridge,join).color('purple'),

    opts.bridging ? overlay(
      pause(-1),
      center(bridging.bold()).shift(binary.xmiddle(), binary.top().up(75)),
    _) : _,
  _);
}


G.bridging4 = function() {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  lexicon = red('lexicon');
  var bridging = 'bridging'.fontcolor('purple');
  var example = ytable(
//    nowrapText('$\\lambda x . \\wl{University}(x) \\wedge \\wl{Education}(\\wl{BarackObama}, x)$'),
    nowrapText('$\\wl{Type.FormOfGovernment} \\sqcap \\wl{GovernmentInCountry.Chile}$'),
    table(
      [nil(), nil(), nil(), binary = text(wl('Country')), nil(), nil()],
      [nil(), unary = text(wl('Type.FormOfGovernment')), nil(), entity = text(wl('Chile')), nil(), nil()],
      [cnl('What'), nlUnary = text(cnl('government')), cnl('does'), nlEntity = text(cnl('Chile')), nlBinary = text(cnl('have')), cnl('?')],
    _).margin(50).center(),
  _).justify('c', 'l').margin(10);
  return overlay(
    example,
    labelConn(unary, nlUnary, lexicon.bold(), 'red'),
    labelConn(entity, nlEntity, lexicon.bold(), 'red'),
    //labelConn(binary, nlBinary, '(lexicon)'.fontcolor('gray'), 'gray'),
    //conn(binary, nlBinary).color('gray'),
    labelConn(unary, binary, bridging.bold(), 'purple'),
    //conn(unary, binary).color('purple'),
    //labelConn(binary, entity, bridging.bold(), 'purple'),
    conn(binary, entity).color('purple'),
  _);
}

G.nonConvexFunction = function(heightScale) {
  n = 10;
  width = 350;
  px = [], py = [], pr = [];
  Math.seedrandom(9);
  for (var i = 0; i < n; i++) {
    px.push(Math.random() * width);
    py.push(Math.random() * 140 * (heightScale || 1));
    pr.push(50 + Math.random() * 50);
  }
  f = function(x) {
    var y = 0;
    for (var i = 0; i < px.length; i++)
      y -= py[i]*Math.exp(-Math.abs((px[i] - x)*(px[i] - x)/pr[i]));
    return y;
  }
  return overlay.apply(null,
    wholeNumbers(width).map(function(i) {
      return line([i-1, down(f(i-1))], [i, down(f(i))]).color('brown');
    }),
  _).scale(2);
}

G.pt = function(x, y) { return circle(5).fillColor('black').shift(x, y); }
G.localOptimization = function() {
 return overlay(
    ellipse(250, 80).fillColor('pink').strokeWidth(2),
    p0 = pt(-130, down(40)),
    moveLeftOf('$\\hat\\theta_0$', p0),
    pem = pt(-70, down(10)),
    moveRightOf('$\\hat\\theta_\\text{EM}$', pem),
    polyline(p0.middle(), [-100, down(35)], [-110, down(20)], pem.middle()),
  _);
}

G.momentMappingDiagram = function(opts) {
  return parentCenter(overlay(
    xtable(
      overlay(
        Theta = ellipse(120, 80).fillColor('pink').strokeWidth(2),
        moveLeftOf('$\\Theta$', Theta),
        opts.pause ? showLevel(2) : _,
        p1 = pt(20, up(40)), moveLeftOf('$\\theta^*$', p1),
        opts.pause ? showLevel(4) : _,
        opts.inverse ? overlay(p2 = pt(30, up(10)), moveLeftOf('$\\hat\\theta$', p2)) : _,
      _),
      opts.pause ? showLevel(1) : _,
      overlay(
        Moments = ellipse(180, 100).fillColor('lightblue').strokeWidth(2),
        moveRightOf('$\\R^p$', Moments),
        opts.pause ? showLevel(2) : _,
        q1 = pt(-70, up(40)), moveRightOf('$m^*$', q1),
        opts.pause ? showLevel(3) : _,
        opts.inverse ? overlay(q2 = pt(-100, up(10)), moveRightOf('$\\hat m$', q2)) : _,
      _),
    _).xmargin(80).center(),
    opts.pause ? showLevel(2) : _,
    M = arrow(p1, q1).strokeWidth(2).color('gray'), moveTopOf('$\\blue{M}$', M),
    opts.pause ? showLevel(4) : _,
    opts.inverse ? overlay(Minv = arrow(q2, p2).strokeWidth(2).color('gray'), moveBottomOf('$\\red{M^{-1}}$', Minv)) : _,
  _));
}

G.alignmentGraph = function() {
  spacing = 10
  function ctnlb(x, t1, t2) { return text(cnl(x) + '[' + wl(t1) + ',' + wl(t2) + ']'); }
  function cwlb(x) { return text(cwl(x)); }

  typedWords = ytable(
    pause(),
    t1 = ctnlb('grew up in', 'Person', 'Location'),
    t2 = ctnlb('born in', 'Person', 'Date'),
    t3 = ctnlb('married in', 'Person', 'Date'),
    pause(-1),
    t4 = ctnlb('born in', 'Person', 'Location'),
  _).margin(spacing)

  freebase = ytable(
    pause(),
    f1 = cwlb('DateOfBirth'),
    f2 = cwlb('PlaceOfBirth'),
    f3 = cwlb('Marriage.StartDate'),
    pause(-1),
    f4 = cwlb('PlacesLived.Location'),
  _).margin(spacing);

  graph = xtable(
    typedWords,
    freebase,
  _).justify('r').xmargin(100).scale(0.9);

  assertion = function(e1, e2) { return center(text('(' + wl(e1) + ',' + wl(e2) + ')')).scale(0.5); }

  venn = overlay(
    textSet = ellipse(250, 100).strokeWidth(2).xshift(-100).strokeColor('brown'),
    kbSet = ellipse(250, 100).strokeWidth(2).xshift(100).strokeColor('brown'),
    assertion('BarackObama', 'Honolulu').shift(0, -50),
    assertion('MichelleObama', 'Chicago').shift(0, +50),
    assertion('RandomPerson', 'Seattle').shift(-250, 0),
    assertion('BarackObama', 'Chicago').shift(250, 0),
    //opaquebg(center('$\\darkblue{\\mathcal{F}(r_1)}$')).shift(x=-250, up(y=100)),
    //opaquebg(center('$\\darkblue{\\mathcal{F}(r_2)}$')).shift(-x, up(y)),
    //center('$\\darkblue{C(r_1, r_2)}$'),
  _);

  //ALIGNMENT (lambda x (fb:people.person.places_lived (fb:people.place_lived.location (var x)))) 187107.0  bear in fb:people.person  fb:location.location      FB_typed_size,Intersection_size_typed,NL-size,NL_typed_size 9182.0,6048.0,18095.0,15765.0 places lived###location

  // TODO: take the log
  stats = frameBox(table(
    ['phrase-count', ':', '15,765'],
    ['predicate-count', ':', '9,182'],
    ['intersection-count', ':', '6,048'],
    ['KB-best-match', ':', '0'],
  _).xjustify('rcr')).title(opaquebg(darkblue('Alignment features'.bold()))).padding(10).scale(0.8);

  stack = ytable(
    graph,
    venn,
    //stats,
  _).margin(30).center();

  edges = [
    [t4, f4],
    pause(),
    [t1, f2],
    [t1, f3],
    [t2, f2],
    [t2, f3],
    [t3, f1],
    [t3, f4],
    [t4, f1],
  ].map(function(ab) {
    if (ab instanceof Array)
      return line([ab[0].right(), ab[0].ymiddle()], [ab[1].left(), ab[1].ymiddle()]).strokeWidth(2).dashed();
    return ab;
  });

  //edges.push(arrow(t4, textSet));
  //edges.push(arrow(f4, kbSet));

  return new Overlay([stack].concat(edges));
}

G.jointModel = function(opts) {
  return overlay(
    table(
      [z = node('$z$', false), x = node('$x$', true)],
    _).margin(40),
    (opts.directed ? arrow : line)(z.items[0], x.items[0]),
  _);
}

G.conditionalModel = function() {
  return overlay(
    table(
      [z = node('$z$', false), x = node('$x$', true)],
      [y = node('$y$', true), nil()],
    _).margin(40),
    arrow(x.items[0], y.items[0]), arrow(z.items[0], y.items[0]),
  _);
}

G.mixtureModel = function(directed) {
  var edge = directed ? arrow : line;
  return overlay(
    table(
      [nil(), h = node('$h$'), nil()],
      [x1 = node('$x_1$', true), x2 = node('$x_2$', true), x3 = node('$x_3$', true)],
    _).margin(50, 50),
    edge(h, x1),
    edge(h, x2),
    edge(h, x3),
  _);
}

G.logLinearSchema = function() {
  boxed = function(a) { return frame(a).padding(5).bg.strokeWidth(2).strokeColor('gray').end; }
  style = function(x) { return x.strokeWidth(10).color('brown'); }
  return overlay(
    table(
      [boxed('Data $x^{(1)}, \\dots, x^{(n)}$'), nil(), pause(2), boxed('Parameters $\\theta$')],
      pause(-2),
      [a1 = style(downArrow(s=70)), nil(), pause(2), a3 = style(upArrow(s))],
      pause(-2),
      [boxed('Observed moments $\\E[t(x)]$'), pause(), a2 = style(rightArrow(200)), boxed('Latent moments $\\E[\\phi(x, h) \\mid h]$')],
    _).margin(50, 20).center(),
    pause(-1),
    moveRightOf('(1) aggregation'.bold(), a1),
    pause(),
    moveBottomOf('(2) factorization'.bold(), a2),
    pause(),
    moveRightOf('(3) optimization'.bold(), a3),
  _);
}

G.landscape = function(opts) {
  var nx = 600, ny = 400;
  function axis(a, b) { return arrow(a, b).strokeWidth(2); }
  function pt(title, x, y, scale) {
    return overlay(
      circle(5).color('black').shift(x, down(y)),
      transform(brown(title)).pivot(-1, -1).shift(x + 5, down(y)).scale(scale || 0.8),
    _);
  }
  var items = [];
  items.push(axis([0, 0], [nx, 0]));
  items.push(axis([0, 0], [0, down(ny)]));
  items.push(center(red('Breadth')).shift(nx/2, up(20)));
  items.push(center(text(blue('Depth')).rotate(-90)).shift(-20, down(ny/2)));

  if (opts.index >= 0) {
    if (opts.index == 0) items.push(pause());
    items.push(pt('web search', nx*0.9, 0.05*ny));
    items.push(pt('SHRDLU', nx*0.1, 0.9*ny));
    if (opts.index == 0) items.push(pause());
    items.push(pt('topic models', nx*0.7, 0.1*ny));
    items.push(pt('HMMs, CRFs', nx*0.65, 0.2*ny));
    items.push(pt('probabilistic grammars', nx*0.6, 0.3*ny));
    if (opts.index == 0) items.push(pause());
    var sp = pt('semantic parsing (GeoQuery)', nx*0.2, 0.8*ny);
    items.push(sp);
  }

  if (opts.index >= 1) {
    if (opts.index == 1) items.push(pause());
    var sp2 = pt('semantic parsing (Freebase)'.bold().fontcolor('green'), nx*0.5, 0.73*ny, 1);
    items.push(sp2);
    items.push(arrow(sp.items[0], sp2.items[0]).color('green').strokeWidth(2));
  }
  return new Overlay(items);
}

G.systemSpec = function(opts) {
  var myPause = opts.pause ? pause : function() { return _; };
  return ytable(
    nowrapText(ex1.x).scale(0.85),
    myPause(),
    opts.hideArrows ? _ : xtable(bigDownArrow(), text('semantic parsing').orphan(true)).center().margin(10),
    //bigDownArrow(),
    nowrapText(ex1.z).scale(0.7),
    myPause(),
    opts.hideArrows ? _ : xtable(bigDownArrow(), text('execute logical form').orphan(true)).center().margin(10),
    //bigDownArrow(),
    nowrapText(ex1.y).scale(0.85),
  _).center().ymargin(10)
}

G.unaryAssertion = function(a, b) {
  return '(' + nl(a) + ', ' + cnl(b)+ ')';
}
G.textAssertion = function(a, b, c) {
  return '(' + nl(a) + ', ' + cnl(b) + ', ' + nl(c) + ')';
}
G.freebaseAssertion = function(a, b, c) {
  return '(' + wl(a) + ', ' + cwl(b) + ', ' + wl(c) + ')';
}
G.lexicalEntry = function(a,b) {
  return cnl(a) + '$\\Rightarrow$' + cwl(b);
}
