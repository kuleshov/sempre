\section{Setup} \label{sec:setup}

\paragraph{Problem Statement}

% Problem statement
Our task is as follows:
Given
(i) a knowledge base $\sK$,
and (ii) a training set of question-answer pairs $\{ (x_i,y_i) \}_{i=1}^n$,
output a semantic parser that maps new questions $x$ to answers $y$
via latent logical forms $z$ and the knowledge base $\sK$.

%We would like to train a semantic parser that given a question in natural
%language answerable by a knowledge-base, converts the question into a logical
%form, queries the knowledge-base and returns an answer. In this section we will
%describe the knowledge-base and the logical forms employed for querying the
%knowledge-base.

\subsection{Knowledge base} \label{sec:kb}

Let $\sE$ denote a set of \emph{entities} (e.g., \wl{BarackObama}),
and let $\sP$ denote a set of \emph{properties} (e.g., \wl{PlaceOfBirth}).
A \emph{knowledge base} $\sK$ is a set of \emph{assertions}
$(e_1,p,e_2) \in \sE \times \sP \times \sE$
(e.g., $(\wl{BarackObama}, \wl{PlaceOfBirth}, \wl{Honolulu})$).
%It is convenient to think of a knowledge base as a directed graph
%whose nodes are entities and edges are assertions labeled with the property.

% PL: graph is nice but takes up too much space - not worth it, since we're not
% doing much with exploring different paths.
%\FigTop{figures/knowledgeBaseGraph}{0.3}{knowledgeBaseGraph}{A fragment of the
%Freebase knowledge base: nodes are entities, edges represent assertions, and
%edge labels represent properties.}

% Freebase
%\reffig{knowledgeBaseGraph} shows a small fragment of the knowledge base we
%use.  Note that nodes include concrete entities (\wl{BarackObama}), abstract
%entities (\wl{Person}), dates (\wl{1961.08.04}), and reified events
%(\wl{\ObamaMarriage}).
%We assume that each property (e.g., \wl{ContainedBy}) has a reverse property (e.g., \wl{Contains}).
We use the Freebase knowledge base \cite{freebase2013dump},
which has 41M non-numeric entities, 19K properties, and 596M
assertions.\footnote{In this paper, we condense Freebase names for readability
(\wl{/people/person} becomes \wl{Person}).}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Logical forms} \label{sec:logicalForms}

% Introduce $\lambda$-DCS
To query the knowledge base, we use a logical language called
Lambda Dependency-Based Compositional Semantics ($\lambda$-DCS)---see
\newcite{liang2013lambdadcs} for details.
For the purposes of this paper,
we use a restricted subset called \emph{simple $\lambda$-DCS},
which we will define below for the sake of completeness.

The chief motivation of $\lambda$-DCS is to produce
logical forms that are simpler than
lambda calculus forms.
For example,
$\lambda x . \exists a . p_1(x,a) \wedge \exists b . p_2(a,b) \wedge p_3(b,e)$
is expressed compactly in $\lambda$-DCS as $p_1.p_2.p_3.e$.
Like DCS \cite{liang11dcs}, $\lambda$-DCS makes existential quantification implicit,
thereby reducing the number of variables.
Variables are only used for anaphora and building composite
binary predicates; these do not appear in simple $\lambda$-DCS.

% Define properly
Each logical form in simple $\lambda$-DCS is either a
\emph{unary} (which denotes a subset of $\sE$)
or a \emph{binary} (which denotes a subset of $\sE \times \sE$).
The basic $\lambda$-DCS logical forms $z$ and their denotations $\den{z}$
are defined recursively as follows:
\begin{itemize}[noitemsep,topsep=0pt]
  \item Unary base case: If $e \in \sE$ is an entity (e.g., $\wl{Seattle}$),
  then $e$ is a unary logical form with $\den{z} = \{ e \}$.

  \item Binary base case: If $p \in \sP$ is a property (e.g., $\wl{PlaceOfBirth}$),
    then $p$ is a binary logical form with
    $\den{p} = \{ (e_1,e_2) : (e_1,p,e_2) \in \sK \}$.\footnote{Binaries
    can be also built out of lambda abstractions (e.g., $\lambda x . \wl{Performance}.\wl{Actor}.x$),
    but as these constructions are not central to this paper, we defer to \cite{liang2013lambdadcs}.}

  \item Join: If $b$ is a binary and $u$ is a unary, then
    $b.u$ (e.g., $\wl{PlaceOfBirth.Seattle}$) is a unary denoting
    a join and project: $\den{b.u} = \{ e_1 \in \sE : \exists e_2 . (e_1,e_2) \in \den{b} \wedge e_2 \in \den{u} \}$.

  \item Intersection: If $u_1$ and $u_2$ are both unaries,
    then $u_1 \sqcap u_2$
    (e.g., $\wl{Profession.Scientist} \sqcap \wl{PlaceOfBirth.Seattle}$) denotes
    set intersection: $\den{u_1 \sqcap u_2} = \den{u_1} \cap \den{u_2}$.

  \item Aggregation: If $u$ is a unary, then $\wl{count}(u)$ denotes the cardinality:
  $\den{\wl{count}(u)} = \{ |\den{u}| \}$.
\end{itemize}

As a final example, \nl{number of dramas starring Tom Cruise}
in lambda calculus would be represented as $\wl{count}(\lambda x . \wl{Genre}(x, \wl{Drama}) \wedge \exists y. \wl{Performance}(x,y) \wedge \wl{Actor}(y,\wl{TomCruise}))$;
in $\lambda$-DCS, it is simply $\wl{count}(\wl{Genre}.\wl{Drama} \sqcap
\wl{Performance}.\wl{Actor}.\wl{TomCruise})$.

It is useful to think of the knowledge base $\sK$ as a directed graph in which
entities are nodes and properties are labels on the edges.  Then simple
$\lambda$-DCS unary logical forms are tree-like graph patterns which pick
out a subset of the nodes.
