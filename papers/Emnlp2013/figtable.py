#!/usr/bin/env python

from __future__ import print_function

import argparse
import pystig.stig as stig
import subprocess
import sys


def parse_args():
    p = argparse.ArgumentParser()
    p.add_argument('--prefix', required=True, help='Path to exec directories.')
    p.add_argument(
        '--fields', required=True, help= \
        'Configuration file with lines as fig field descriptions ' + \
        'prepended with "* " if numeric.'
    )
    p.add_argument(
        '--experiment-field', type=int, help= \
        'Index of field (in fields file) being changed in this experiment.'
    )
    p.add_argument('--execs', required=True, type=int, nargs='+')
    p.add_argument('--output-format')
    return p.parse_args()


def sublist(xs, inds):
    assert min(inds) >= 0, inds
    assert max(inds) < len(xs), inds
    assert len(xs) > len(inds)
    return [xs[i] for i in inds]


def superlist(xs, inds, n, fill):
    assert all(n > i for i in inds)
    assert len(xs) == len(inds)
    xs = iter(xs)
    if isinstance(fill, list):
        fill = iter(fill)
    else:
        fill = iter([fill] * n)
    return [next(xs) if i in inds else next(fill) for i in xrange(n)]


def elementwise_sum(xs, ys):
    return [x + y for x, y in zip(xs, ys)]


def str_fix_width(s, width=6):
    s = str(s)
    if not s:
        s = '-'
    if len(s) < width:
        return s + ' ' * (width - len(s))
    else:
        return s[:width]


def read_field_descs(path):
    with open(path, 'r') as f:
        return [l.strip() for l in f]


def summarize_rows(rows, is_numeric):
    n = len(rows)
    numeric_cols = [i for i, is_num in enumerate(is_numeric) if is_num]
    sums = [0] * len(numeric_cols)
    for row in rows:
        sums = elementwise_sum(sums, [float(x or 'NaN') for x in sublist(row, numeric_cols)])
    means = [float(x) / float(n) for x in sums]
    return superlist(means, numeric_cols, len(row), None)


def extract_exec_value(path, key_desc):
    def parse_desc(s):
        assert len(s) > 0
        if s[0] == '$':
            return s[1:].split(':', 1)
        else:
            return (None, s)

    keys_by_file = [parse_desc(s) for s in key_desc.split()]
    vals = []
    for basename, key in keys_by_file:
        if basename is not None:
            with open(path + '/' + basename, 'r') as f:
                map_ = dict((s[0], s[1]) for s in [l.strip().split() for l in f] if len(s) > 1)
                vals.append(map_.get(key, ''))
        else:
            vals.append(key)
    return ''.join(vals)


def process_field_descs(field_descs):
    return zip(*[
        (True, f[2:]) if f[0] == '*' else (False, f)
        for f in field_descs
    ])


def get_rows(prefix, execs, field_descs):
    paths = ['{}/{}.exec/'.format(prefix, str(e)) for e in execs]
    is_numeric, field_descs = process_field_descs(field_descs)
    return [
        [extract_exec_value(p, f) for f in field_descs]
        for p in paths
    ]    


def get_summary(rows, field_descs):
    is_numeric, field_descs = process_field_descs(field_descs)
    return summarize_rows(rows, is_numeric)


def make_gnuplot_input(names, summaries,
                       name_pfx='Change beam size',
                       x_colname='beam',
                       y_colnames=['dev.cor', 'dev.oracle'],
                       out=None):
    def gnuplot_columns(x, y, summaries):
        for i, (rows, summary) in enumerate(summaries):
            print(summary[x] + ' ' + summary[y], file=out)

    for name, summaries in summaries_by_name(names, summaries):
        if name.startswith(name_pfx):
            gnuplot_columns(COLS['beam'], COLS['dev'], summaries)


def build_tables(summaries_by_expt_field_value, field_descs):
    '''
    For a type of experiment, table layout is:

      Name of experiment (fig column whose value is being changed)
      ---------------------------------------------------------------
                   value1 value2 ... value3 (in "increasing" order)
                \____________________________
      stat1 name | trial1 trial2 ... trial3 |
      stat2 name | trial1 trial2 ... trial3 |
      ...        | ... _____________________|
    '''

    def build_table_from_changing_column(vals_and_summaries):
        header = [None] + [val for val, _ in vals_and_summaries]
        body = [
            [field_descs[i]] + [summary[i] for _, summary in vals_and_summaries]
            for i in xrange(len(field_descs))
            ]
        return [header] + body;

    return build_table_from_changing_column(
        sorted(summaries_by_expt_field_value.items(), key=stig.fst))


def print_ascii_table(table, name, out=None):
    def print_hr(width, n, under='-'):
        print((under * width).join(['+'] * (n+1)), file=out)
    def print_row(width, n, row):
        print('|' + '|'.join(str_fix_width(x, width) for x in row) + '|', file=out)

    width = 10

    header = table[0]
    assert header[0] is None
    n = len(header)

    print_hr(width, n)
    print_row(width, n, [''] + header[1:])
    print_hr(width, n, under='=')
    for row in table[1:]:
        print_row(width, n, row)
    print_hr(width, n)


def print_ascii_tables(tables, names):
    for table, name in zip(tables, names):
        print(name[1])
        print_ascii_table(table, name)
        print('')


def print_latex_table(table, name):
    proc = subprocess.Popen('pandoc -t latex', shell=True, stdin=subprocess.PIPE)
    print_ascii_table(table, name, out=proc.stdin)
    proc.stdin.close()
    proc.wait()


def print_latex_tables(tables, names, out=None):
    for table, name in zip(tables, names):
        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n% ' + name[1] + '\n')
        print_latex_table(table, name)
        print('')


def prompt_for_experiment_field(fields):
    print('Which field to summarize?')
    print('\n'.join(str(i) + ') ' + f for i, f in enumerate(fields)))
    return int(sys.stdin.readline().strip())


def main(args):
    field_descs = read_field_descs(args.fields)
    expt_field = args.experiment_field or prompt_for_experiment_field(field_descs)

    rows = get_rows(args.prefix, args.execs, field_descs)
    rows_by_expt_field_value = stig.dict_collect((row[expt_field], row) for row in rows)
    summaries_by_expt_field_value = dict(
        (i, get_summary(rows, field_descs)) for i, rows in rows_by_expt_field_value.items()
    )
    table = build_tables(summaries_by_expt_field_value, field_descs)

    if args.output_format == 'ascii':
        print_ascii_tables([table], [field_descs[expt_field]])
    elif args.output_format == 'latex':
        print_latex_tables([table], [field_descs[expt_field]])


if __name__ == '__main__':
    args = parse_args()
    main(args)

