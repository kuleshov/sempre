G = sfig.serverSide ? global : this;
sfig.importAllMethods(G);

// Latex macros
sfig.latexMacro('R', 0, '\\mathbb{R}');
sfig.latexMacro('P', 0, '\\mathbb{P}');
sfig.latexMacro('E', 0, '\\mathbb{E}');
sfig.latexMacro('diag', 0, '\\text{diag}');
sfig.includeLatex('macros.tex');

sfig.initialize();

G.prez = sfig.presentation();

// Useful functions (not standard enough to put into an sfig library).

G.frameBox = function(a) { return frame(a).padding(5).bg.strokeWidth(1).fillColor('white').end; }

G.bigLeftArrow = function(s) { return leftArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigRightArrow = function(s) { return rightArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigUpArrow = function(s) { return upArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigDownArrow = function(s) { return downArrow(s || 100).strokeWidth(10).color('brown'); }
G.red = function(x) { return x.fontcolor('red'); }
G.green = function(x) { return x.fontcolor('green'); }
G.blue = function(x) { return x.fontcolor('blue'); }
G.darkblue = function(x) { return x.fontcolor('darkblue'); }

G.xseq = function() { return new sfig.Table([arguments]).center().margin(5); }
G.yseq = function() { return ytable.apply(null, arguments).margin(10); }

G.stmt = function(prefix, suffix) { return prefix.fontcolor('darkblue') + ':' + (suffix ? ' '+suffix : ''); }
G.headerList = function(title) {
  var contents = Array.prototype.slice.call(arguments).slice(1);
  return ytable.apply(null, [title ? stmt(title) : _].concat(contents.map(function(line) {
    if (line == _) return _;
    if (typeof(line) == 'string') return bulletedText([null, line]);
    if (line instanceof sfig.PropertyChanger) return line;
    return indent(line);
  })));
}

G.node = function(x, shaded) { return overlay(circle(20).fillColor(shaded ? 'lightgray' : 'white') , x).center(); }
G.indent = function(x, n) { return frame(x).xpadding(n != null ? n : 20).xpivot(1); }
G.stagger = function(b1, b2) { b1 = std(b1); b2 = std(b2); return overlay(b1, pause(), b2.replace(b1)); }

G.dividerSlide = function(text) {
  return slide(null, nil(), parentCenter(text)).titleHeight(0);
}

G.moveLeftOf = function(a, b, offset) { return transform(a).pivot(1, 0).shift(b.left().sub(offset == null ? 5 : offset), b.ymiddle()); }
G.moveRightOf = function(a, b, offset) { return transform(a).pivot(-1, 0).shift(b.right().add(offset == null ? 5 : offset), b.ymiddle()); }
G.moveTopOf = function(a, b, offset) { return transform(a).pivot(0, 1).shift(b.xmiddle(), b.top().sub(offset == null ? 5 : offset)); }
G.moveBottomOf = function(a, b, offset) { return transform(a).pivot(0, -1).shift(b.xmiddle(), b.bottom().add(offset == null ? 5 : offset)); }
G.moveCenterOf = function(a, b) { return transform(a).pivot(0, 0).shift(b.xmiddle(), b.ymiddle()); }

G.wholeNumbers = function(n) {
  var result = [];
  for (var i = 0; i < n; i++) result[i] = i;
  return result;
}

G.argSort = function(arr, cmp) {
  cmp = cmp || function(a, b) { return a - b; };
  return wholeNumbers(arr.length).sort(function(a, b) { return cmp(arr[a], arr[b]); });
};

G.shuffleArrayInPlace = function(arr) {
  for (var i = arr.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var tmp = arr[j]; arr[j] = arr[i]; arr[i] = tmp;
  }
}

////////////////////////////////////////////////////////////

G.nlcolor = 'green';  // Natural language
G.zlcolor = 'red';  // Logical form
G.wlcolor = 'blue';  // World
G.nl = function(x) { return x.italics(); }
G.zl = function(x) { return '$\\wl{' + x + '}$'; }
G.wl = function(x) { return '$\\wl{' + x + '}$'; }
G.cnl = function(x) { return nl(x).fontcolor(nlcolor); }
G.czl = function(x) { return zl(x).fontcolor(zlcolor); }
G.cwl = function(x) { return wl(x).fontcolor(wlcolor); }

// This is the two step alignment schema, not representative of the EMNLP paper.
G.schemaOld = function() {
  var rawText = ytable(
    green('Raw text'),
    '``...Obama, born in Honolulu, ..."'.italics(),
  _).center();
  var kb = ytable(
    blue('Knowledge base'),
    '('+zl('BarackObama')+','+zl('PlaceOfBirth')+','+zl('Honolulu')+')',
  _).center();
  var alignment = frameBox('Alignment');

  var lexicon = ytable(
    red('Lexicon'),
    nowrapText('(``born in",'+zl('PlaceOfBirth')+')\\{intersection:13305, text:15430, ...\\}'),
  _).center();

  var training = frameBox('Training');

  var semanticParser = text(red('Semantic parser'));

  var trainExamples = ytable(
    green('Training examples'),
    table(
      ['Question:', '``Which US senators'.italics()],
      [nil(), 'were born in Japan?"'.italics()],
      ['Answer:', '``Hirono"'.italics()],
    _).xjustify('rl'),
  _).center();

  return overlay(
    ytable(
      xtable(rawText, kb).margin(10),
      alignment,
      lexicon,
      xtable(trainExamples.orphan(true), training).center().margin(50),
      semanticParser,
    _).center().margin(40),

    arrow(rawText, alignment),
    arrow(kb, alignment),
    arrow(alignment, lexicon).strokeWidth(s=2),
    arrow(lexicon, training).strokeWidth(s),
    arrow(training, semanticParser).strokeWidth(s),
    arrow(trainExamples, training).strokeWidth(s),
  _);
}

G.schema = function() {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  alignment = red('alignment');
  var bridging = 'bridging'.fontcolor('purple');
  var example = ytable(
    text(blue('\\{Occidental College, Columbia University\\}')),
    overlay(
      bigUpArrow(130),
      frameBox('Execute on Database').bg.round(10).end,
    _).center(),
    '$\\wl{Type.University} \\sqcap \\wl{Education}.\\wl{BarackObama}$',
    //nowrapText('$\\lambda x . \\wl{Type.University}(x) \\wedge \\wl{Education}(\\wl{BarackObama}, x)$'),
    table(
      [nil(), unary = text(wl('Type.University')), nil(), nil(), nil(), nil()],
      [nil(), nil(), nil(), binary = text(wl('Education')), nil(), nil()],
      [nil(), nil(), nil(), entity = text(wl('BarackObama')), nil(), nil()],
      [cnl('Which'), nlUnary = text(cnl('college')), cnl('did'), nlEntity = text(cnl('Obama')), nlBinary = text(cnl('go to')), cnl('?')],
    _).margin(50).center(),
  _).justify('c', 'l').margin(10);
  return overlay(
    example,
    labelConn(unary, nlUnary, alignment.bold(), 'red'),
    labelConn(entity, nlEntity, alignment.bold(), 'red'),
    //labelConn(binary, nlBinary, '(alignment)'.fontcolor('gray'), 'gray'),
    //conn(binary, nlBinary).color('gray'),
    //labelConn(unary, binary, bridging.bold(), 'purple'),
    center(bridging.bold()).shift(binary.xmiddle(), binary.top().add(20)),
    conn(unary, binary).color('purple'),
    //labelConn(binary, entity, bridging.bold(), 'purple'),
    conn(binary, entity).color('purple'),
  _);
}

G.bridging = function() {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  lexicon = red('lexicon');
  var bridging = 'bridging'.fontcolor('purple');
  var example = ytable(
//    nowrapText('$\\lambda x . \\wl{University}(x) \\wedge \\wl{Education}(\\wl{BarackObama}, x)$'),
    nowrapText('$\\wl{Type.FormOfGovernment} \\sqcap \\wl{GovernmentInCountry.Chile}$'),
    table(
      [nil(), nil(), nil(), binary = text(wl('Country')), nil(), nil()],
      [nil(), unary = text(wl('Type.FormOfGovernment')), nil(), entity = text(wl('Chile')), nil(), nil()],
      [cnl('What'), nlUnary = text(cnl('government')), cnl('does'), nlEntity = text(cnl('Chile')), nlBinary = text(cnl('have')), cnl('?')],
    _).margin(50).center(),
  _).justify('c', 'l').margin(10);
  return overlay(
    example,
    labelConn(unary, nlUnary, lexicon.bold(), 'red'),
    labelConn(entity, nlEntity, lexicon.bold(), 'red'),
    //labelConn(binary, nlBinary, '(lexicon)'.fontcolor('gray'), 'gray'),
    //conn(binary, nlBinary).color('gray'),
    labelConn(unary, binary, bridging.bold(), 'purple'),
    //conn(unary, binary).color('purple'),
    //labelConn(binary, entity, bridging.bold(), 'purple'),
    conn(binary, entity).color('purple'),
  _);
}


G.alignmentGraph = function() {
  spacing = 10
  function ctnlb(x, t1, t2) { return text(cnl(x) + '[' + wl(t1) + ',' + wl(t2) + ']'); }
  function cwlb(x) { return text(cwl(x)); }

  typedWords = ytable(
    t1 = ctnlb('grew up in', 'Person', 'Location'),
    t2 = ctnlb('born in', 'Person', 'Date'),
    t3 = ctnlb('married in', 'Person', 'Date'),
    t4 = ctnlb('born in', 'Person', 'Location'),
  _).margin(spacing)

  freebase = ytable(
    f1 = cwlb('DateOfBirth'),
    f2 = cwlb('PlaceOfBirth'),
    f3 = cwlb('Marriage.StartDate'),
    f4 = cwlb('PlacesLived.Location'),
  _).margin(spacing);

  graph = xtable(
    typedWords,
    freebase,
  _).center().xmargin(100);

  assertion = function(e1, e2) { return center(text('(' + wl(e1) + ',' + wl(e2) + ')')).scale(0.7); }

  venn = overlay(
    textSet = ellipse(250, 100).strokeWidth(2).xshift(-100).strokeColor('brown'),
    kbSet = ellipse(250, 100).strokeWidth(2).xshift(100).strokeColor('brown'),
    assertion('BarackObama', 'Honolulu').shift(0, -50),
    assertion('MichelleObama', 'Chicago').shift(0, +50),
    assertion('BarackObama', 'Chicago').shift(250, 0),
    assertion('RandomPerson', 'Seattle').shift(-250, 0),
    //opaquebg(center('$\\mathcal{F}(\\green{\\textit{born in}})$')).shift(-150, y=100),
    //opaquebg(center('$\\mathcal{F}(\\blue{\\wl{PlacesLived}.\\wl{Location}})$')).shift(+350, y),
    opaquebg(center('$\\darkblue{\\mathcal{F}(r_1)}$')).shift(x=-250, y=100),
    opaquebg(center('$\\darkblue{\\mathcal{F}(r_2)}$')).shift(-x, y),
    center('$\\darkblue{C(r_1, r_2)}$'),
  _);

//ALIGNMENT (lambda x (fb:people.person.places_lived (fb:people.place_lived.location (var x)))) 187107.0  bear in fb:people.person  fb:location.location      FB_typed_size,Intersection_size_typed,NL-size,NL_typed_size 9182.0,6048.0,18095.0,15765.0 places lived###location

  // TODO: take the log
  stats = frameBox(table(
    ['log-phrase-count', ':', '$\\log(15765)$'],
    ['log-predicate-count', ':', '$\\log(9182)$'],
    ['log-intersection-count', ':', '$\\log(6048)$'],
    ['KB-best-match', ':', '0'],
  _).xjustify('rcr')).title(opaquebg(darkblue('Alignment features'.bold()))).padding(10).scale(0.8);

  stack = ytable(
    graph,
    venn,
    stats,
  _).margin(30).center();

  edges = [
    [t1, f2],
    [t1, f3],
    [t2, f2],
    [t2, f3],
    [t3, f1],
    [t3, f4],
    [t4, f1],
    [t4, f4],
  ].map(function(ab) {
    return line([ab[0].right(), ab[0].ymiddle()], [ab[1].left(), ab[1].ymiddle()]).strokeWidth(2).dashed();
  });

  edges.push(arrow(t4, textSet));
  edges.push(arrow(f4, kbSet));

  return new Overlay([stack].concat(edges));
}

// An example of parsing a sentence.
G.parseExample = function() {
  T = rootedTree;

  var lexicon = function() { return opaquebg(blue('lexicon').bold()).scale(0.8); }
  var intersection, join, t1, t2, t3;
  function labelVerticalEdge(label, t) { return moveRightOf(label, t.branches[0].edge); }
  return overlay(
    T(
      intersection = nowrapText(czl('Type.Location') + '$\\,\\sqcap\\,$' + czl('PeopleBornHere.BarackObama')),
      t1 = T(czl('Type.Location'), cnl('where')),
      cnl('was'),
      T(join = text(czl('PeopleBornHere.BarackObama')),
        t2 = T(czl('BarackObama'), cnl('Obama')),
        t3 = T(czl('PeopleBornHere'), cnl('born')),
      _),
      cnl('?'),
    _).recnodeBorderWidth(0).recverticalCenterEdges(true),
    moveBottomOf(opaquebg(blue('join').bold()).scale(0.8), join),
    moveBottomOf(opaquebg(blue('intersection').bold()).scale(0.8), intersection),
    labelVerticalEdge(lexicon(), t1),
    labelVerticalEdge(lexicon(), t2),
    labelVerticalEdge(lexicon(), t3),
  _);

/*
  features1 = [
    ['$\\binaryKBLogPopularity$', 13.098],
    ['$\\binaryUntypedTextLogCount$', 9.789],
    ['$\\binaryKBLogCount$', 9.647],
    ['$\\binaryTextLogCount$', 9.644],
    ['$\\binaryIntersectionLogCount$', 9.496],
    ['$\\binaryTopRanked$', 1],
  ];
  features2 = [
    ['$\\unaryKBLogPopularity$', 14.108],
    ['$\\entityKBLogPopularity$', 9.433],
    ['$\\entitySuffix$', 1],
    ['$\\skipPos{\\textsc{vbd-aux}}$', 1],
  ];

  formatFeatures = function(features) {
    return new Table(features.map(function(kv) { return [kv[0], ':', kv[1]]; })).xjustify('rcl').xmargin(20);
  }

  features = frameBox(
    xtable(formatFeatures(features1), formatFeatures(features2)).scale(0.65).margin(30),
  _).title(opaquebg(darkblue('Active features'.bold())));

  //[ binary.popularity ]                              -5.635 = 13.098 * -0.430
  //[ binary.NL-size ]                                 -1.722 = 9.789 * -0.176
  //[ binary.FB_typed_size ]                           -1.711 = 9.647 * -0.177
  //[ binary.NL_typed_size ]                           -1.046 = 9.644 * -0.108
  //[ binary.Intersection_size_typed ]                 13.054 = 9.496 * 1.375
  //[ binary.top ]                                      0.169 = 1 * 0.169

  //[ unary.popularity ]                               -2.422 = 14.108 * -0.172
  //[ entity.popularity ]                               7.397 = 9.433 * 0.784
  //[ entity.queryIsSuffix ]                            0.453 = 1 * 0.453

  //[ skipPos.VBD-AUX ]                                -0.617 = 1 * -0.617

  //[ binary.ALIGNMENT ]                               -1.176 = 1 * -1.176
  //[ unary.HARD ]                                      0.608 = 1 * 0.608

  lexical = frameBox(table(
    [nl('Barack Obama'), ':', wl('BarackObama')],
    [nl('where'), ':', wl('Type.Location')],
    [nl('born in')+'['+wl('Person')+','+wl('Location')+']', ':', wl('PlaceOfBirth')],
  _)).title(opaquebg(darkblue('Lexical entries used'.bold()))).padding(10);

  return ytable(
    tree,
    xtable(lexical, features).margin(50),
  _).center().margin(50);
*/
}

G.forEachBeamfigFileLine = function(path, callback) {
  var header = {};
  sfig.readFile(path).split(/\n/).forEach(function(line, lineIndex, allLines) {
    if (lineIndex < 3) {
      var tokens = line.split(' ');
      if (tokens[0] == 'iters') header.numIters = parseInt(tokens[1]);
      else if (tokens[0] == 'examples') header.numExamples = parseInt(tokens[1]);
      else if (tokens[0] == 'beamsize') header.beamSize = parseInt(tokens[1]);
    } else if (line != '') {
      callback(header, line, lineIndex, allLines);
    }
  });
}

G.readMetaFile = function(path) {
  var res = { beamSizes: [], lengths: [] };
  var numExamples;
  var i = 0;
  forEachBeamfigFileLine(path, function(header, line) {
    numExamples = header.numExamples;
    if (line == '') return;
    var tokens = line.split(' ', 2);
    if (i < numExamples) {
      res.lengths.push(parseInt(tokens[1]));
      res.beamSizes.push([]);
    }
    res.beamSizes[i % numExamples].push(parseInt(tokens[0]));
    i++;
  });
  return res;
};

G.beamTrajectory = function(opts) {
  var width = 5;
  var height = 5;
  var dotWidth = 2.5;
  var lastCorrectPositions = [];
  var displayIters = [0, 1, 2];
  var maxs = {
    numExamples: 50,
    numIters: Math.max.apply(null, displayIters) + 1,
    beamSize: 50
  };
  var meta = readMetaFile(opts.meta);
  var header;
  var maxSeenBeamSize = -Infinity;

  function posToItem(ex, pos) {
    return rect(width, height).color('green').shiftBy(ex * width, pos * height);
  }

  var iterPositionsPerExample = [];
  var iterItems = [];
  forEachBeamfigFileLine(opts.corbmp, function(hdr, line) {
    header = hdr;
    var numIters = Math.min(header.numIters, maxs.numIters);
    var numExamples = Math.min(header.numExamples, maxs.numExamples);
    var beamSize = Math.min(header.beamSize, maxs.beamSize);
    maxSeenBeamSize = Math.max(maxSeenBeamSize, beamSize);
    var tokens = line.split(' ');
    var iter = parseInt(tokens[0]);
    var ex = parseInt(tokens[1]);
    var pos = parseInt(tokens[2]);
    if (ex >= numExamples) return;
    if (iter >= numIters) return;

    var positionsPerExample = iterPositionsPerExample[iter];
    if (positionsPerExample == null)
      positionsPerExample = iterPositionsPerExample[iter] = [];
    var positions = positionsPerExample[ex];
    if (positions == null)
      positions = positionsPerExample[ex] = [];
    positions.push(pos);

    if (pos >= beamSize) return;
    var items = iterItems[iter];
    if (iter == numIters-1)
      lastCorrectPositions[ex] = pos;
    if (items == null)
      items = iterItems[iter] = [];
    items.push(posToItem(ex, pos));
  });

  // Simulate iter 0 as random of iter 1
  var iter0Items = [];
  var from = iterPositionsPerExample[0];
  for (var ex = 0; ex < from.length; ex++) {
    var fromBeam = from[ex];
    if (!fromBeam)
      continue;
    var beamSize = meta.beamSizes[ex][0];
    var perm = wholeNumbers(beamSize);
    shuffleArrayInPlace(perm);
    for (var i = 0; i < fromBeam.length; i++) {
      var pos = fromBeam[i];
      if (perm[pos] < Math.min(beamSize, maxs.beamSize))
        iter0Items.push(posToItem(ex, perm[pos]));
    }
  }
  iterItems.unshift(iter0Items);  

  var numIters = header.numIters, numExamples = header.numExamples;

  var iterDots = [];
  var iterProbs = [];
  var ex = 0, iter = 0, i = -1;
  forEachBeamfigFileLine(opts.items, function(header, line, lineNum) {
    if (iter >= maxs.numIters) return;
    var beamSize = meta.beamSizes[ex][iter];
    while (++i == beamSize) {
      i = -1;
      if (++ex == numExamples) {
        ex = 0;
        ++iter;
      }
      beamSize = meta.beamSizes[ex][iter];
    }
    if (ex >= maxs.numExamples) return;
    if (i >= maxs.beamSize) return;
    var tokens = line.split(' ');
    var compat = parseFloat(tokens[0]);
    var score = parseFloat(tokens[1]);
    var prob = parseFloat(tokens[2]);
    var dots = iterDots[iter];
    var probs = iterProbs[iter];
    if (!dots) {
      dots = iterDots[iter] = [];
      probs = iterProbs[iter] = [];
    }
    var x = ex * dotWidth * 2;
    var yScore = -score * dotWidth * 2;
    var yProb = -prob * dotWidth * 2 * 100;
    // Push greens, unshift dots, so that greens appear above.
    if (compat > 0.95) {
      dots.push(circle(dotWidth).color('green').opacity(0.3).shiftBy(x, yScore));
      probs.push(circle(dotWidth).color('green').opacity(0.3).shiftBy(x, yProb));
    } else {
      dots.unshift(circle(dotWidth).color('red').opacity(0.1).shiftBy(x, yScore));
      probs.unshift(circle(dotWidth).color('red').opacity(0.1).shiftBy(x, yProb));
    }
  });

  // For each iteration
  var posPlots = [], scorePlots = [], probPlots = [];
  var captions = [];
  for (var i = 0; i < displayIters.length; i++) {
    // for (var ex = 0; ex < numExamples; ex += 10) iterItems[iter].push(text(ex+'').xshift(ex * width));
    // var bgBeams = [];
    // for (var ex = 0; ex < Math.min(numExamples, maxs.numExamples); ex++) {
    //   var beamSize = Math.min(meta.beamSizes[ex][iter], maxs.beamSize);
    //   bgBeams.push(rect(width, beamSize * height).color('red').opacity(0.5).shiftBy(ex * width, 0));
    // }
    var j = displayIters[i];
    var nE = Math.min(numExamples, maxs.numExamples);
    var nB = Math.min(maxSeenBeamSize, maxs.beamSize);
    posPlots[i] = overlay(
      rect(nE * width, nB * height).color('red').opacity(0.5),
      // new Overlay(bgBeams),
      new Overlay(iterItems[j] || []),
    _);
    // scorePlots[j] = new Overlay(iterDots[j]);
    // probPlots[j] = new Overlay(iterProbs[j]);
    captions[i] = j + ' iterations';
  }

  var res = [];
  [posPlots /*, scorePlots, probPlots*/].forEach(function(plots) {
    if (opts.stagger) {
      var items = [];
      for (var iter = 0; iter < Math.min(numIters, maxs.numIters); iter++) {
        var item = ytable(plots[iter], captions[iter]).margin(20).center();
        item.showLevel(iter);
        if (iter+1 < numIters) item.hideLevel(iter+1);
        items.push(item);
      }
      res.push(new Overlay(items));
    } else {
      res.push(table(plots, captions).margin(20).center());
    }
  });
  return res;
};
