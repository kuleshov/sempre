// For node.js
require('./sfig/internal/sfig.js');
require('./sfig/internal/Graph.js');
require('./sfig/internal/RootedTree.js');
require('./sfig/internal/metapost.js');
require('./sfig/internal/seedrandom.js');
require('./utils.js');

// For visualizing the beams
// {path: sfig_.urlParams.path, stagger: true}));
var base = sfig_.urlParams.path;
var bt = beamTrajectory({
  meta: base + '/beamfigs-meta-train.vis',
  corbmp: base + '/beamfigs-corbmp-train.vis',
  items: base + '/beamfigs-items-train.vis',
  stagger: true
});
for (var i = 0; i < bt.length; i++)
  prez.addSlide(bt[i]);
