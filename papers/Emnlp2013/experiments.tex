\section{Experiments} \label{sec:experiments}

% Overview
We now evaluate our semantic parser empirically.
In Section~\ref{sec:mainResults}, we compare our approach to
\newcite{cai2013large} on their recently released dataset
(henceforth, \CaiYatesDataset)
and present results on a new dataset that we
collected (henceforth, \WebQuestionsDataset).
In \refsec{detailedAnalysis},
we provide detailed experiments to provide additional insight on our system.

\paragraph{Setup}

We implemented a standard beam-based bottom-up parser which stores the
$k$-best derivations for each span.  We use $k=500$ for all our
experiments on \CaiYatesDataset{} and $k=200$ on \WebQuestionsDataset.
The root beam yields the candidate set $\tD(x)$ and is used to
approximate the sum in the objective function $\sO(\theta)$ in
\refeqn{objective}.
In experiments on \WebQuestionsDataset{}, $\tD(x)$ contained 197
derivations on average.
% I just looked at numCandidates of iter=6.train in the ablation of +lexAlign and it was 196.92 196.845 196.490

We write the approximate objective as $
  \sO(\theta; \tilde \theta) = \sum_i
    \log \sum_{d \in \tD(x_i; \tilde\theta) : \den{d.z} = y_i}
      p(d \mid x_i; \theta)
$
to explicitly show dependence on the parameters $\tilde\theta$
used for beam search.
We optimize the objective by initializing $\theta_0$ to $0$ and
applying AdaGrad (stochastic gradient ascent with per-feature adaptive
step size control) \cite{duchi10adagrad}, so that $\theta_{t+1}$ is
set based on taking a stochastic approximation of $\frac{\partial
  \sO(\theta; \theta_t)}{\partial \theta}\big|_{\theta = \theta_t}$.
We make six passes over the training examples.

We used POS tagging and named-entity recognition to restrict
what phrases in the utterance could be mapped by the lexicon.
Entities must be named entities, 
proper nouns or a sequence of at least two tokens.
Unaries must be a sequence of nouns, and binaries must be either a content word, or a verb followed by either a noun phrase
or a particle.
In addition, we used 17 hand-written rules to map question words such as
\nl{where} and \nl{how many} to logical forms such as \wl{Type.Location} and
\wl{Count}.

%Let $\tD(x)$ denote the set of final hypotheses returned by the beam-based parser;
%the dependence on $\theta$ is due to the $k$-best beam approximation.
%Recall that our learning objective $\sO$ sums over derivations and
%thereby depends on the current parameters $\theta$ as well, by way of
%$\tD(x)$.
% Formally making this dependence explicit in the objective, we could write
% $\sO(\theta; \tilde \theta) =
%    \sum_i \log
%      \sum_{d : \den{d.z} = y_i} p(d \mid x_i; D_{\tilde \theta}, \theta)$
% where $\tilde\theta$ are the parameters used for the beam search.
To compute denotations, we convert a logical form $z$ into a SPARQL query and
execute it on our copy of Freebase using the Virtuoso engine.
On \WebQuestionsDataset, a full run over the training examples involves
approximately 600,000 queries.
% each taking 8.6 milliseconds on average (and up to tens of seconds
% in some cases).
For evaluation, we predict the answer from the derivation with highest probability.
\subsection{Main results} \label{sec:mainResults}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{\CaiYatesDataset} \label{sec:caiYates}

\newcite{cai2013large} created a dataset consisting of 917 questions
involving 635 Freebase relations, annotated with lambda calculus forms. We converted all 917 questions into 
%(comparing to 6,000 relations in our system),
%and evaluated against a new dataset consisting 917 questions.
simple $\lambda$-DCS, executed them on Freebase and used the
resulting answers to train and evaluate. To map phrases to Freebase entities we 
used the manually-created entity lexicon used by \newcite{cai2013large}, 
which contains 1,100 entries. Because entity disambiguation is a
challenging problem in semantic parsing, the entity lexicon simplifies the problem.

Following \newcite{cai2013large}, we held out 30\% of the examples for the final test, and performed all development on
the remaining 70\%.  During development, we split the data and used 512
examples (80\%) for training and the remaining 129 (20\%) for validation. All reported
development numbers are averaged across 3 random splits.
We evaluated using accuracy, the fraction of examples
where the predicted answer exactly matched the correct answer.

Our main empirical result is that 
our system, which was trained only on question-answer pairs, obtained 62\%
accuracy on the test set, outperforming the 59\% accuracy reported by
\newcite{cai2013large}, who trained on full logical forms.
%\footnote{We also note that the
%accuracy reported by \newcite{cai2013large} is for the best parser obtained
%by tuning a parameter on the test set. We on the other hand ran our parser on
%the test set exactly once.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{\WebQuestionsDataset} \label{sec:webQuestions}

% 5641 90.exec/0+1.ttl

\paragraph{Dataset collection}

% Logical forms versus answers
Because \CaiYatesDataset\ requires logical forms, it is difficult to scale up
due to the required expertise of annotating logical forms.
We therefore created a new dataset, \WebQuestionsDataset, of question-answer pairs
obtained from non-experts.

%One pitfall of logical forms that it is easy to make errors; we
%semi-automatically corrected many of the logical forms found in
%\CaiYatesDataset.

% Distribution over questions
To collect this dataset, we used the Google Suggest API to obtain questions that
begin with a wh-word and contain exactly one entity.
We started with the question \nl{Where was Barack Obama born?}
and performed a breadth-first search over questions (nodes),
using the Google Suggest API supplying the edges of the graph.
Specifically, we queried the question excluding the entity, the phrase
before the entity, or the phrase after it; each query generates 5
candidate questions, which are added to the queue.
We iterated until 1M questions were visited; a random 100K were submitted to Amazon Mechanical Turk (AMT).

The AMT task requested that
workers answer the question using only the Freebase page of the questions' entity,
or otherwise mark it as unanswerable by Freebase.
The answer was restricted to be one of the possible entities, values, or list of entities
on the page.  As this list was long, we allowed the user to filter the list by typing.
We paid the workers \$0.03 per question.
Out of 100K questions, 6,642 were annotated identically by at least two AMT workers.  
%, and they answered all of the questions in three days.
% python createSemanticParserInputAmtCsv.py 6
% ./mergeExamples

We again held out a 35\% random subset of the questions for the final test,
and performed all development on the remaining 65\%, which was further divided
into an 80\%--20\% split for training and validation. To map entities, we
built a Lucene index over the 41M Freebase entities.

\reftab{datasets} provides some statistics about the new questions.
One major difference in the datasets is the distribution of questions:
\CaiYatesDataset\ starts from Freebase properties and solicits questions about these properties;
these questions tend to be tailored to the properties.
\WebQuestionsDataset\ starts from questions completely independent of Freebase,
and therefore the questions tend to be more natural and varied.
For example, for the Freebase property \wl{ComicGenre}, \CaiYatesDataset\
contains the question \nl{What genre is Doonesbury?}, while
\WebQuestionsDataset{} for the property \wl{MusicGenre} contains \nl{What music did Beethoven compose?}.
% TODO: pick new question

The number of word types in \WebQuestionsDataset{} is larger than in datasets
such as ATIS and GeoQuery (\reftab{datasets}), making lexical mapping much more challenging. On the other hand, 
in terms of structural complexity \WebQuestionsDataset{} is simpler and many questions
contain a unary, a binary and an entity. 

% Projection onto Freebase
In some questions, the answer provided by AMT workers is only roughly accurate, because workers
are restricted to selecting answers from the Freebase page.
For example, the answer given by workers to the question 
\nl{What is James Madison most famous for?} is \nl{President of the United States}
rather than \nl{Authoring the Bill of Rights}.

\begin{table}[t]
\begin{center}
{\footnotesize
\hfill{}
\begin{tabular}{|l|r|r|}
\hline
\textbf{Dataset}&\textbf{\# examples}&\textbf{\# word types}\\
\hline
GeoQuery               & 880           & 279             \\ 
ATIS                   & 5,418         & 936             \\ 
\CaiYatesDataset       & 917           & 2,036             \\ 
\WebQuestionsDataset   & 5,810           & 4,525             \\
\hline
\end{tabular}}
\hfill{}
\caption{\footnotesize{Statistics on various semantic parsing datasets.  Our new
dataset, \WebQuestionsDataset, is much larger than \CaiYatesDataset{} and much more lexically diverse than ATIS.}}
\label{tab:datasets}
\end{center}
\end{table}
% TODO: what other statistics to show?

\paragraph{Results}

AMT workers sometimes provide partial answers, e.g., the answer to
\nl{What movies does Taylor Lautner play in?} is a set of 17 entities,
out of which only 10 appear on the Freebase page.
We therefore
allow partial credit and score an answer using the F$_1$ measure, comparing the
predicted set of entities to the annotated set of entities.

As a baseline, we omit from our system the main contributions presented in this
paper---that is, we disallow bridging, and remove denotation and
alignment features.  The accuracy on the test set of this system is 26.9\%,
whereas our full system obtains 31.4\%, a significant improvement.

Note that the number of possible derivations for questions
in \WebQuestionsDataset{} is quite large. In the question \nl{What kind of system of
government does the United States have?} the phrase \nl{United States} maps to
231 entities in our lexicon, the verb \nl{have} maps to 203 binaries,
%egrep "lexeme.*\"have\"" binaryInfoStringAndAlignment.txt | wc -l
and the phrases \nl{kind}, \nl{system}, and \nl{government} all map to many
different unary and binary predicates. Parsing correctly involves skipping some
words, mapping other words to predicates, while resolving many
ambiguities in the way that the various predicates can combine.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Detailed analysis} \label{sec:detailedAnalysis}

We now delve deeper to explore the contributions of the various components of our
system.  All ablation results
reported next were run on the development set (over 3 random splits).
%by performing
%ablation tests on the development set. 
%Specifically, we focus on the contribution of alignment vs. bridging and on
%syntactic and semantic features. 

%\paragraph{Correct logical forms versus correct answers}

%Incorrect logical forms can sometimes serendipitously lead to correct answers.
%For example, we answer \nl{who succeeded president Lincoln after his
%assassination?} correctly by incorrectly mapping \nl{succeeded} to
%\wl{USVicePresident.\hole}.
%Note that this phenomenon also exists in relation extraction,
%where correct facts are extracted for the wrong reasons.

%\todo{How many possible distinct logical forms lead to an answer?}

%\todo{How many possible distinct derivations lead to an answer?}

\paragraph{Generation of binary predicates}

\begin{table}[t]
\begin{center}
{\footnotesize
\begin{tabular}{|l|r|r|}
\hline
\textbf{System}     & \CaiYatesDataset        & WebQ. \\
\hline
\Lexical            & 38.0                 & 30.6 \\
\Bridging           & 66.9                    & 21.2 \\
\Lexical+\Bridging  & {\bf 71.3}              & {\bf 32.9} \\
\hline
\end{tabular}}
\caption{\footnotesize{
  Accuracies on the development set under different schemes of binary predicate generation.
  In \Lexical, binaries  are generated only via the alignment lexicon.
  In \Bridging, binaries are generated through the bridging operation only.
  {\Lexical+\Bridging} corresponds to the full system.}
}
\label{tab:truncateGrammar}
\end{center}
\end{table}

Recall that our system has two mechanisms for suggesting binaries:
from the alignment lexicon or via the bridging operation. 
\reftab{truncateGrammar} shows accuracies when
only one or both is used.
Interestingly, alignment alone is better than bridging alone on
\WebQuestionsDataset{}, whereas for \CaiYatesDataset{}, it is the opposite.
The reason for this is that \CaiYatesDataset{} contains questions on rare predicates.
These are often missing from the lexicon,
but tend to have distinctive types and hence
can be predicted from neighboring predicates.
In contrast, \WebQuestionsDataset{} contains questions that are
commonly searched for and focuses on popular predicates, therefore exhibiting
larger lexical variation.

For instance, when training without an alignment
lexicon, the system errs on \nl{When did Nathan Smith
die?}. Bridging suggests binaries that are compatible with the
common types \wl{Person} and \wl{Datetime}, and the binary \wl{PlaceOfBirth} is
chosen. On the other hand, without bridging, the system errs on
\nl{In which comic book issue did Kitty Pryde first appear?}, which
refers to the rare predicate \wl{ComicBookFirstAppearance}. With bridging, the
parser can identify the correct binary, by linking the types \wl{ComicBook} and
\wl{ComicBookCharacter}. On both datasets, best performance is achieved by
combining the two sources of information. 

Overall, running on \WebQuestionsDataset{}, the parser
constructs derivations that contain about 12,000 distinct binary predicates. 

%We see that both are useful, and that
%\textbf{bridging} is essential on \CaiYatesDataset{} but less so on
%WebQuestions.  Moreover, if we generate only from the lexicon, then
%the oracle score\footnote{
 % The oracle score is the fraction of examples for which the set of
 % hypotheses $\tD(x)$ at all contains the correct answer.
%}
%on \CaiYatesDataset{} drops from 86.8 to 50.0
%(On WebQuestions the decrease is merely from 35.0 to 34.3.)

\begin{table}[t]
\begin{center}
{\footnotesize
\begin{tabular}{|l|r|r|}
\hline
\textbf{System}                     & \CaiYatesDataset  & WebQ. \\
\hline
\Full                               & {\bf 71.3}        & {\bf 32.9} \\ % FINAL SYSTEM
-\FeatPos                           & 70.5              & 28.9 \\
-\FeatDenotation                    & 58.6              & 28.0 \\
\hline
\end{tabular}}
\caption{\footnotesize{
Accuracies on the development set  with features removed.  \FeatPos{} and \FeatDenotation{} refer to
the POS tag and denotation features from \refsec{grammar}.}
}
\label{tab:featureAblations}
\end{center}
\end{table}

\begin{table}[t]
\begin{center}
{\footnotesize
\begin{tabular}{|l|r|r|}
\hline
\textbf{System}              & \CaiYatesDataset        & WebQ. \\
\hline
\FeatAlignScores                        & \textbf{71.3}              & 32.9 \\ % FINAL SYSTEM
\FeatLexAlign                           & 68.5                    & 34.2 \\
\FeatLexAlign+\FeatAlignScores          & 69.0                    & {\bf 36.4} \\ % EVERYTHING
\hline
\end{tabular}}
\caption{\footnotesize{
Accuracies on the development set using either unlexicalized alignment features (\FeatAlignScores{})
or lexicalized features (\FeatLexAlign{}).} 
}
\label{tab:lexResults}
\end{center}
\end{table}

\paragraph{Feature variations}

\reftab{featureAblations} shows the results of feature ablation
studies.  Accuracy drops when POS tag features are omitted,
e.g., in the question \nl{What number is Kevin Youkilis on the Boston Red
Sox} the parser happily skips the NNPs \nl{Kevin Youkilis} and returns the
numbers of all players on the Boston Red Sox.
A significant loss is incurred without denotation features, largely due to the
parser returning logical forms with empty denotations.
For instance, the question \nl{How many people were at the 2006 FIFA world cup final?} is answered with a logical form containing the property \wl{PeopleInvolved} rather than \wl{SoccerMatchAttendance}, resulting in an empty denotation.

%When denotation features are absent, we obtained many additional errors due to
%empty denotations. 

Next we study the impact of lexicalized versus unlexicalized features (\reftab{lexResults}).
%On \CaiYatesDataset, \Full{} improves over all other settings.
In the large \WebQuestionsDataset{} dataset, lexicalized features helped, and
so we added those features to our model
when running on the test set. In \CaiYatesDataset{} lexicalized features result in overfitting due
to the small number of training examples. Thus, we ran our final parser on the test set without lexicalized features.
%In  \CaiYatesDataset{}
%Due to the smaller number of questions in \CaiYatesDataset{}, lexicalized features cause
%overfitting and hurt accuracy. On the other hand, in the much larger \WebQuestionsDataset{},
%lexicalized features help.
%We therefore included them in the final \WebQuestionsDataset{} test set experiments.  

%\todo{If we just greedily choose the best lexical item for a given word, what happens? (``lexicon truncation'' in fig)}

\paragraph{Effect of beam size}

\FigTop{figures/beamTrajectory-0.pdf}{0.29}{beamCorrectPlot}{
  Beam of candidate derivations $\tD(x)$ for 50 \WebQuestionsDataset{} examples.
  In each matrix, columns correspond to examples and rows correspond
  to beam position (ranked by decreasing model score).
  Green cells mark the positions of derivations with correct denotations.
  Note that both the number of good derivations and their positions
  improve as $\theta$ is optimized.
}

An intrinsic challenge in semantic parsing is to handle the exponentially large set of
possible derivations.  We rely heavily on the
$k$-best beam approximation in the parser keeping good derivations that
lead to the correct answer.
Recall that the set of candidate derivations $\tD(x)$ depends on the parameters $\theta$.
In the initial stages of learning, $\theta$ is far from optimal,
so good derivations are likely to fall below the $k$-best cutoff of internal
parser beams.  As a result, $\tD(x)$ contains few derivations with the correct answer.
Still, placing these few derivations on the
beam allows the training procedure to bootstrap $\theta$ into a good solution.
\reffig{beamCorrectPlot} illustrates this improvement in $\tD(x)$ across early training iterations.

\newcommand\TwoFigs[8]{
  \begin{figure}[t] \begin{center}
      \subfigure[#5]{\includegraphics[scale=#3]{#1}}
      \subfigure[#6]{\includegraphics[scale=#4]{#2}}
      \vspace{-1.2em}
    \end{center} {\caption{\label{fig:#7} #8}} \end{figure}
}
\TwoFigs{figures/beamIncreaseYates.pdf}{figures/beamIncreaseWebq.pdf}{1}{1}{
  \CaiYatesDataset}{\WebQuestionsDataset{}}{beamIncrease}{
  Accuracy and oracle as beam size $k$ increases.
}

% Beam size
Smaller choices of $k$ yield a coarser approximation in beam
search.  As we increase $k$ (\reffig{beamIncrease}), we
see a tapering improvement in accuracy.  We also see a widening gap
between accuracy and oracle score,\footnote{
Oracle score is the fraction of examples for which
$\tD(x)$ contains any derivation with the correct denotation.
}
as including a good derivation in $\tD(x)$ is made easier but the
learning problem is made more difficult.

\paragraph{Error analysis}

The accuracy on \WebQuestionsDataset{} is much lower than on
\CaiYatesDataset{}. We analyzed \WebQuestionsDataset{} examples and found
several main causes of error: (i) Disambiguating entities in
\WebQuestionsDataset{} is much harder because the entity lexicon has 41M
entities. For example, given \nl{Where did the battle of New Orleans start?}
the system identifies \nl{New Orleans} as the target entity rather than its
surrounding noun phrase. Recall that all \CaiYatesDataset\ experiments used a
carefully chosen entity lexicon.
(ii) Bridging can often fail when the question's
entity is compatible with many binaries. For example, in \nl{What did Charles
Babbage make?}, the system chooses a wrong binary compatible with the type
% TODO: which predicate?
\wl{Person}. (iii) The system sometimes incorrectly draws verbs from subordinate clauses. For
example, in \nl{Where did Walt Disney live before he died?} it returns the
place of death of Walt Disney, ignoring the matrix verb \emph{live}.
%and the temporal connective \emph{before}.
% PL: this is not really an issue specific to temporal constructions

%\todo{
%Talk  about how we simulate jaacard - original text: Other measures of similarity such as Jaccard and pointwise mutual information can be derived by
%taking particular weighted combinations of the log counts,
%so we do not include them explicitly.
%}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \paragraph{Effect of dataset size}

% Table~\ref{tb:results} presents the development accuracy for all systems.
% \TopFive\ performs the worst, showing that pruning the lexicon too early hurts
% performance; \BasicStats\ performs only a bit better, because it has no capacity
% to remember alignment information.
% \Unlex, \Lex, and \All\ perform similarly on the development set,
% which is quite encouraging since \Unlex\ has only 98 parameters
% while \Lex\ has 11K parameters.
% Indeed, on the training accuracy, we can see that systems with lexicalized
% features overfit substantially.

% \begin{table}[systemResultsTable]
% \begin{center}
% {\small
% \begin{tabular}{|l|r|r|}
% \hline
% \textbf{System} & \textbf{Train accuracy} & \textbf{Dev. accuracy} \\
% \hline
% \TopFive        & 19.7                    & 18.6 \\
% \BasicStats     & 21.7                    & 21.2 \\
% \Lex            & 40.3                    & 29.6 \\
% \Unlex          & 31.9                    & 30.9 \\
% \All            & 39.3                    & 31.1 \\
% \hline
% \end{tabular}}
% \caption{Results on the WebQuestions data set}
% \label{tb:results}
% \end{center}
% \end{table}

% To study generalization in further detail, we analyze the learning curves of \Lex\ and \Unlex\
% as the number of training examples increases (\reffig{learningCurve}).
% The unlexicalized parser generalizes much faster: the development accuracy of
% \Unlex\ given 100 training examples is comparable to the accuracy of \Lex\ given 10
% times more examples.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\paragraph{Error analysis}

%\todo{How many errors are due to entity disambiguation?}
