\subsection{Bridging} \label{subsec:bridging}

%\FigStar{figures/bridging}{0.5}{bridging}{Bridging examples}

\begin{table*}[t]
{\footnotesize
\hfill{}
\begin{tabular}{|p{0.2cm}|p{5cm}|p{1cm}|p{8.5cm}|}
\hline
\textbf{\#} &\textbf{Form 1} & \textbf{Form 2} & \textbf{Bridging} \\
\hline
1 & \wl{Type.FormOfGovernment} & \wl{Chile} & \wl{Type.FormOfGovernment} $\sqcap$ \wl{\textbf{GovernmentTypeOf}.Chile} \\
2 &                            & \wl{X-Men} & \wl{\textbf{ComicBookCoverPriceOf}.X-Men} \\
3 & \wl{Marriage.Spouse.TomCruise} & \wl{2006} & \wl{Marriage.(Spouse.TomCruise $\sqcap$ \textbf{StartDate}.2006)} \\
\hline
\end{tabular}}
\hfill{}
\caption{\footnotesize{Three examples of the bridging operation. The bridging binary predicate $b$ is in boldface.}}
\label{tab:bridging}
\end{table*}

While alignment can cover many predicates, it is unreliable
for cases where the predicates are expressed weakly or
implicitly. For example, in \nl{What government does Chile have?},
the predicate is expressed by the light verb \emph{have},
in \nl{What actors are in Top Gun?}, it is expressed by a highly ambiguous
preposition, and in \nl{What is Italy money?}\ [sic], it is omitted altogether.
Since natural language doesn't offer much help here, let us turn elsewhere for guidance.
Recall that at
this point our main goal is to generate a manageable set of candidate logical
forms to be scored by the log-linear model.

In the first example, suppose the phrases \nl{Chile} and \nl{government} are parsed
as \wl{Chile} and \wl{Type.FormOfGovernment}, respectively,
and we hypothesize a connecting binary.
The two predicates impose strong type constraints on that binary,
so we can afford to generate all the binary predicates that type check
(see \reftab{bridging}).
%A binary predicate can bridging two unaries if
%its type signature matches the types of the unaries.
More formally, given two
unaries $z_1$ and $z_2$ with
types $t_1$ and $t_2$,
we generate a logical form $z_1 \sqcap b.z_2$
for each binary $b$ whose type signature is $(t_1,t_2)$. Figure~\ref{fig:schema}
visualizes bridging of the unaries \wl{Type.University} and \wl{Obama}.

Now consider the example \nl{What
is the cover price of X-men?}
Here, the binary \wl{ComicBookCoverPrice} is expressed explicitly, but is not in our lexicon
since the language use is rare.
%For example, we have an entity \wl{X-Men} and the phrase
%\nl{cover price} that is missing from our lexicon.
To handle this, we allow bridging to generate a binary based on a single unary;
in this case, based on the unary \wl{X-Men} (\reftab{bridging}),
we generate several binaries including \wl{ComicBookCoverPrice}.
%with a binary whose type signature is compatible with the entity, and in this
%case suggest the binary \wl{ComicBookCoverPrice} (see
%In general, we denote the type signature of a binary $b$ by
%$(t_1(b),t_2(b))$.
Generically, given a unary $z$ with type $t$, we construct a
logical form $b.z$ for any predicate $b$ with type $(*,t)$.

Finally, consider the question \nl{Who did Tom Cruise marry
in 2006?}.
%The last case of bridging (also called \emph{injecting}) occurs when a phrase
%has more than two arguments.
%Using our entity lexicon (see Section~\ref{sec:experiments}), the
%alignment lexicon and a join operation we can generate the logical form
Suppose we parse the phrase \nl{Tom Cruise marry} into
\wl{Marriage.Spouse.TomCruise}, or more explicitly,
$\lambda x . \exists e . \wl{Marriage}(x,e) \wedge \wl{Spouse}(e,\wl{TomCruise})$.
Here, the neo-Davidsonian event variable $e$ is an intermediate quantity,
but needs to be further modified (in this case, by the temporal modifier \emph{2006}).
To handle this, we apply bridging to a unary and the intermediate event
(see \reftab{bridging}).
%This allows us to incorporate the temporal argument \emph{2006} by
%``injecting'' it into the
%event (see Table~\ref{tab:bridging}).
Generically, given a logical form $p_1.p_2.z'$ where $p_2$ has type $(t_1,*)$,
and a unary $z$ with type $t$, bridging injects $z$ and constructs a logical form $p_1.(p_2.z'
\sqcap b.z)$ for each logical predicate $b$ with type $(t_1, t)$.

In each of the three examples, bridging generates a binary predicate based on
neighboring logical predicates rather than on explicit lexical material.
In a way, our \emph{bridging} operation shares with bridging anaphora
\cite{clark1975bridging} the idea of establishing a novel relation between
distinct parts of a sentence.
%Note that bridging both suggests logical predicates and constructs
%compositional logical forms that contain join and intersect operations. 
Naturally, we need features to distinguish between
the generated predicates, or decide whether bridging is even appropriate at all.
Given a binary $b$, features include
the log of the predicate count $\log |\sF(b)|$,
indicators for the kind of bridging,
an indicator on the binary $b$ for injections
(\reftab{features}).
In addition, we add all text similarity features by
comparing the Freebase name of $b$ with content words in the question.
