#!/bin/bash

set -e

title="$1"
if [ -z $title ]; then
    title="plot"
fi

gnuplot <<EOF
set autoscale                          # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels
# set xlabel "Beam size"
set xtic 100                           # set xtics automatically
set ytic 0.2                           # set ytics automatically
set key right bottom
set xrange [-20:520]
set yrange [0:1]
set terminal pdf size 1.5,1
set output "figures/beamIncreaseYates.pdf"
plot "beamIncreaseYates.ora.gp" using 1:2 with linespoints title "oracle", \
     "beamIncreaseYates.cor.gp" using 1:2 with linespoints title "accuracy"
EOF

gnuplot <<EOF
set autoscale                          # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels
# set xlabel "Beam size"
set xtic 100                           # set xtics automatically
set ytic 0.2                           # set ytics automatically
set key right top
set xrange [-10:210]
set yrange [0:1]
set terminal pdf size 1.5,1
set output "figures/beamIncreaseWebq.pdf"
plot "beamIncreaseWebQ.ora.gp" using 1:2 with linespoints title "oracle", \
     "beamIncreaseWebQ.cor.gp" using 1:2 with linespoints title "accuracy"
EOF
