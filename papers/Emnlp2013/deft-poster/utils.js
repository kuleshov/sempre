G = sfig.serverSide ? global : this;
sfig.importAllMethods(G);

// Latex macros
sfig.latexMacro('R', 0, '\\mathbb{R}');
sfig.latexMacro('P', 0, '\\mathbb{P}');
sfig.latexMacro('E', 0, '\\mathbb{E}');
sfig.latexMacro('diag', 0, '\\text{diag}');
sfig.latexMacro('wl', 1, '\\text{#1}');
sfig.latexMacro('cwl', 1, '\\blue{\\wl{#1}}');
sfig.latexMacro('sK', 0, '\\mathcal{K}');
sfig.latexMacro('czl', 1, '\\red{\\zl{#1}}');
sfig.latexMacro('zl', 1, '\\text{#1}');
sfig.latexMacro('sD', 0, '\\mathcal{D}');
sfig.initialize();

G.prez = sfig.presentation();

// Useful functions (not standard enough to put into an sfig library).

G.frameBox = function(a) { return frame(a).bg.strokeWidth(1).fillColor('white').end; }
G.bigFrameBox = function(a) { return frameBox(a).padding(10).scale(1.4); }

G.bigLeftArrow = function(s) { return leftArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigRightArrow = function(s) { return rightArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigUpArrow = function(s) { return upArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigDownArrow = function(s) { return downArrow(s || 100).strokeWidth(10).color('brown'); }

var colors = ['red', 'green', 'blue', 'darkblue', 'brown', 'purple'];
colors.forEach(function(color) {
  G[color] = function(x) { return x.fontcolor(color); };
});

G.xseq = function() { return new sfig.Table([arguments]).center().margin(5); }
G.yseq = function() { return ytable.apply(null, arguments).margin(10); }

G.stmt = function(prefix, suffix) {
  var m;
  if (!suffix && (m = prefix.match(/^([^:]+): (.+)$/))) {
    prefix = m[1];
    suffix = m[2];
  }
  return prefix.fontcolor('darkblue') + ':' + (suffix ? ' '+suffix : '');
}
G.headerList = function(title) {
  var contents = Array.prototype.slice.call(arguments).slice(1);
  return ytable.apply(null, [title ? stmt(title) : _].concat(contents.map(function(line) {
    if (line == _) return _;
    if (typeof(line) == 'string') return bulletedText([null, line]);
    if (line instanceof sfig.PropertyChanger) return line;
    return indent(line);
  })));
}

G.node = function(x, shaded) { return overlay(circle(20).fillColor(shaded ? 'lightgray' : 'white') , x).center(); }
G.indent = function(x, n) { return frame(x).xpadding(n != null ? n : 20).xpivot(1); }
G.stagger = function(b1, b2) { b1 = std(b1); b2 = std(b2); return overlay(b1.numLevels(1), b2); }

G.dividerSlide = function(text) {
  return slide(null, nil(), parentCenter(text)).titleHeight(0);
}

G.moveLeftOf = function(a, b, offset) { return transform(a).pivot(1, 0).shift(b.left().sub(offset == null ? 5 : offset), b.ymiddle()); }
G.moveRightOf = function(a, b, offset) { return transform(a).pivot(-1, 0).shift(b.right().add(offset == null ? 5 : offset), b.ymiddle()); }
G.moveTopOf = function(a, b, offset) { return transform(a).pivot(0, 1).shift(b.xmiddle(), b.top().sub(offset == null ? 5 : offset)); }
G.moveBottomOf = function(a, b, offset) { return transform(a).pivot(0, -1).shift(b.xmiddle(), b.bottom().add(offset == null ? 5 : offset)); }
G.moveCenterOf = function(a, b) { return transform(a).pivot(0, 0).shift(b.xmiddle(), b.ymiddle()); }

G.nlcolor = 'green';  // Natural language
G.zlcolor = 'red';  // Logical form
G.wlcolor = 'blue';  // World
G.nl = function(x) { return x.italics(); }
G.cnl = function(x) { return nl(x).fontcolor(nlcolor); }
G.zl = function(x) { return '$\\zl{' + x + '}$'; }
G.wl = function(x) { return '$\\wl{' + x + '}$'; }
G.czl = function(x) { return '$\\czl{' + x + '}$'; }
G.cwl = function(x) { return '$\\cwl{' + x + '}$'; }

G.ex1 = {
  x: cnl('Who did Humphrey Bogart marry in 1928?'),
  z: '$\\red{ \\wl{Type.Person} \\sqcap \\wl{Marriage}.(\\wl{Spouse.HumphreyBogart} \\sqcap \\wl{StartDate.1928})}$',
  y: cwl('Mary Philips'),
};

G.systemSpec = function() {
  return ytable(
    nowrapText(ex1.x).scale(0.85),
    xtable(bigDownArrow(), text('semantic parsing').orphan(true)).center().margin(10),
    //bigDownArrow(),
    nowrapText(ex1.z).scale(0.7),
    xtable(bigDownArrow(), text('execute logical form').orphan(true)).center().margin(10),
    //bigDownArrow(),
    nowrapText(ex1.y).scale(0.85),
  _).center().ymargin(10)
}

G.contribution= function() {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  var alignment = red('alignment');
  var bridging = 'bridging'.fontcolor('purple');
  var example =
    ytable(
    xtable(image('images/database.png').dim(100),ytable(cwl('BarackObama'),cwl('TopGun'),cwl('Type.Country'),cwl('Profession.Lawyer'),cwl('PeopleBornHere'),cwl('InventorOf'),cwl('$\\vdots \\vdots$')).center().scale(0.3)).margin(20),
    table(
      [nil(),nil(),nil(),ytable(cwl('LanguagesSpoken')).scale(0.7),nil(),nil(),nil()],
      [ nil(),
        ytable(cwl('Type.HumanLanguage'),languageKb = text(cwl('Type.ProgrammingLanguage'))).center().scale(0.7),
        nil(),
        nil(),
        nil(),
        ytable(cwl('Brazil'),brazilKb = text(cwl('BrazilFootballTeam'))).center().scale(0.7),
        nil(),
        _],
      [nil(),nil(),nil(),nil(),nil(),nil(),nil()],
      [cnl('What'), languageText = text(cnl('languages')), cnl('do'), peopleText = text(cnl('people')), cnl('in'), brazilText = text(cnl('Brazil')), useText = text(cnl('use'))],
    _).xmargin(5).ymargin(35).center().scale(0.9),
    yspace(5),
  _);
  return overlay(
      example,
      labelConn(languageText,languageKb,alignment.bold(),'red'),
      labelConn(brazilText,brazilKb,alignment.bold(),'red'),
      labelConn(brazilKb,languageKb,label = bridging.bold(),'purple'),
  _);
}

G.drawKnowledgeBaseGraph = function(entries, opts) {
  var nodes = {}; // Map node name => object
  var nodePositions = {};  // Map node name => position
  var out = [];
  var getNode = function(name, dx, dy) {
    var posScale = 140;
    var o = nodes[name];
    if (!o) {
      o = nodes[name] = center(name.bold().fontcolor('blue')).scale(0.8);
      if (opts.highlightNodes && opts.highlightNodes.indexOf(name) != -1)
        o = frameBox(o).bg.strokeWidth(2).round(5).end;
      o.shift(dx*posScale, down(dy*posScale));
      nodePositions[name] = [dx, dy];
      out.push(o);
    }
    return o;
  }
  entries.forEach(function(info) {
    var e1 = info[0];
    var property = info[1][0];
    var dx = info[1][1];
    var dy = info[1][2];
    var highlight = info[1][3];
    var e2 = info[2];

    if (opts.pause)
      out.push(pause());

    var e1obj = getNode(e1, 0, 0);
    var e2obj = getNode(e2, nodePositions[e1][0]+dx, nodePositions[e1][1]+dy);
    var link = arrow(e1obj, e2obj);
    if (opts.highlightEdges && highlight) link.strokeWidth(6).strokeColor('green');
    else link.strokeWidth(2);
    out.push(link);
    out.push(opaquebg(center(property.fontcolor('brown'))).scale(0.6).shift(link.xmiddle(), link.ymiddle()));
  });
  return new Overlay(out);
}

G.obamaKnowledgeGraph = function(opts) {
  var ObamaMarriage = 'Event8';
  var BarackObamaLiveInChicago = 'Event3';
  var MichelleObamaLiveInChicago = 'Event21';

  return drawKnowledgeBaseGraph([
    ['BarackObama', ['Type', -2, +1, true], 'Person'],
    ['BarackObama', ['Profession', +1, +1], 'Politician'],

    ['BarackObama', ['DateOfBirth', -0.5, 1], '1961.08.04'],
    ['BarackObama', ['PlaceOfBirth', 2, 0], 'Honolulu'],
    ['Honolulu', ['ContainedBy', 0, -1], 'Hawaii'],
    ['Honolulu', ['Type', 0, 1], 'City'],
    ['Hawaii', ['ContainedBy', -1.5, +0.5], 'UnitedStates'],
    ['Hawaii', ['Type', 0, -0.7], 'USState'],

    ['BarackObama', ['Marriage', -1, -1], ObamaMarriage],
    [ObamaMarriage, ['Spouse', -1, -1], 'MichelleObama'],
    ['MichelleObama', ['Type', 1, 0, true], 'Person'],
    ['MichelleObama', ['Gender', 1.5, 0.3], 'Female'],
    [ObamaMarriage, ['StartDate', 1.5, -0.5], '1992.10.03'],

    ['BarackObama', ['PlacesLived', -1.5, +0.4, true], BarackObamaLiveInChicago],
    [BarackObamaLiveInChicago, ['Location', -1, -0.4, true], 'Chicago'],
    ['MichelleObama', ['PlacesLived', -0.5, +1, true], MichelleObamaLiveInChicago],
    [MichelleObamaLiveInChicago, ['Location', 0, 0, true], 'Chicago'],
    ['Chicago', ['ContainedBy', 0, 0, true], 'UnitedStates'],
  ], opts);
}

G.queryKnowledgeGraph1 = function(opts) {
  var x = 'o';
  return drawKnowledgeBaseGraph([
    [x, ['Type', -0.5, 1], 'Person'],
    [x, ['PlacesLived', 0.5, 1], '?'],
    ['?', ['Location', 0, 1], 'Chicago'],
  ], opts);
}

G.wholeNumbers = function(n) {
  var result = [];
  for (var i = 0; i < n; i++) result[i] = i;
  return result;
}

G.gridText = function(nr, nc) {
  function textDocumentImage() {
    return image('images/simple-document.jpg').width(30);
  }
  var contents = wholeNumbers(nr).map(function() {
    return wholeNumbers(nc).map(function() {
      return Math.random() < 1 ? textDocumentImage() : nil();
    });
  });
  return table.apply(null, contents).margin(10);
}

G.textAssertion = function(a, b, c) {
  return '(' + nl(a) + ', ' + cnl(b) + ', ' + nl(c) + ')';
}

G.freebaseAssertion = function(a, b, c) {
  return '(' + wl(a) + ', ' + cwl(b) + ', ' + wl(c) + ')';
}
G.lexicalEntry = function(a,b) {
  return cnl(a) + '$\\Rightarrow$' + cwl(b);
}


G.alignmentGraph = function() {
  spacing = 10
  function ctnlb(x, t1, t2) { return text(cnl(x) + '[' + wl(t1) + ',' + wl(t2) + ']'); }
  function cwlb(x) { return text(cwl(x)); }

  typedWords = ytable(
    t1 = ctnlb('grew up in', 'Person', 'Location'),
    t2 = ctnlb('born in', 'Person', 'Date'),
    t3 = ctnlb('married in', 'Person', 'Date'),
    t4 = ctnlb('born in', 'Person', 'Location'),
  _).margin(spacing)

  freebase = ytable(
    f1 = cwlb('DateOfBirth'),
    f2 = cwlb('PlaceOfBirth'),
    f3 = cwlb('Marriage.StartDate'),
    f4 = cwlb('PlacesLived.Location'),
  _).margin(spacing);

  graph = xtable(
    typedWords,
    freebase,
  _).justify('r').xmargin(100).scale(0.9);

  assertion = function(e1, e2) { return center(text('(' + wl(e1) + ',' + wl(e2) + ')')).scale(0.5); }
 venn = overlay(
    textSet = ellipse(250, 100).strokeWidth(2).xshift(-100).strokeColor('brown'),
    kbSet = ellipse(250, 100).strokeWidth(2).xshift(100).strokeColor('brown'),
    assertion('BarackObama', 'Honolulu').shift(0, -50),
    assertion('MichelleObama', 'Chicago').shift(0, +50),
    assertion('RandomPerson', 'Seattle').shift(-250, 0),
    assertion('BarackObama', 'Chicago').shift(250, 0),
  _);

  stack = ytable(
    graph,
    venn,
  _).margin(30).center();

  edges = [
    [t4, f4],
    [t1, f2],
    [t1, f3],
    [t2, f2],
    [t2, f3],
    [t3, f1],
    [t3, f4],
    [t4, f1],
  ].map(function(ab) {
    if (ab instanceof Array)
      return line([ab[0].right(), ab[0].ymiddle()], [ab[1].left(), ab[1].ymiddle()]).strokeWidth(2).dashed();
    return ab;
  });
  return new Overlay([stack].concat(edges));
}

G.bridgingobama= function() {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  alignment = red('alignment');
  var bridging = 'bridging'.fontcolor('purple');
  var example = ytable(
    yspace(50),
    table(
      [nil(), unary = text(wl('Type.University')), nil(), nil(), nil(), nil()],
      [nil(), nil(), nil(), binary = text(wl('Education.Institution')), nil(), nil()],
      [nil(), nil(), nil(), entity = text(wl('BarackObama')), nil(), nil()],
      [cnl('Which'), nlUnary = text(cnl('college')), cnl('did'), nlEntity = text(cnl('Obama')), nlBinary = text(cnl('go to')), cnl('?')],
    _).xmargin(15).ymargin(50).center(),
  _).justify('c', 'l').margin(10);
  return overlay(
    example,
    labelConn(unary, nlUnary, alignment.bold(), 'red'),
    labelConn(entity, nlEntity, alignment.bold(), 'red'),
    //labelConn(binary, nlBinary, '(alignment)'.fontcolor('gray'), 'gray'),
    //conn(binary, nlBinary).color('gray'),
    //labelConn(unary, binary, bridging.bold(), 'purple'),
   overlay(
      center(bridging.bold()).shift(binary.xmiddle(), binary.top().up(40)),
      conn(unary, binary).color('purple'),
      //labelConn(binary, entity, bridging.bold(), 'purple'),
      conn(binary, entity).color('purple'),
    _),
  _);
}

G.parseExample = function(opts) {
  var myPause = opts.pause ? pause : function() { return _; };
  T = rootedTree;

  var intersection, join, t1, t2, t3;
  function labelVerticalEdge(label, t) { return moveRightOf(label, t.branches[0].edge); }
  return overlay(
    T(
      intersection = nowrapText(czl('Type.CityTown') + '$\\,\\sqcap\\,$' + czl('PeopleBornHere.BarackObama')),
      cnl('what'),
      t1 = T(czl('Type.CityTown'), cnl('city')),
      cnl('was'),
      T(join = text(czl('PeopleBornHere.BarackObama')),
        t2 = T(czl('BarackObama'), cnl('Obama')),
        t3 = T(czl('PeopleBornHere'), cnl('born')),
      _),
      cnl('?'),
    _).recnodeBorderWidth(0).recymargin(90),
    myPause(),
    labelVerticalEdge(('alignment'.bold()), t1),
    labelVerticalEdge(('alignment'.bold()), t2),
    labelVerticalEdge(('alignment'.bold()), t3),
    myPause(),
    moveBottomOf(('join'.bold()), join),
    myPause(),
    moveBottomOf(('intersect'.bold()), intersection),
  _);
}
