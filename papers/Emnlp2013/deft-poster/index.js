// For node.js
require('./sfig/internal/sfig.js');
require('./sfig/internal/Graph.js');
require('./sfig/internal/RootedTree.js');
require('./sfig/internal/metapost.js');
require('./sfig/internal/seedrandom.js');
require('./utils.js');

function block(title) {
  var width = Text.defaults.getProperty('width').get();
  var padding = 20;

  var contents = Array.prototype.slice.call(arguments, 1);
  var interior = ytable.apply(null, contents).width(width);

  var exterior = frame(interior);
  exterior.title(opaquebg(frame(darkbluebold(title))).xpadding(10));
  exterior.padding(padding);
  exterior.bg.strokeWidth(4).strokeColor('darkblue').end;

  return exterior;
}

function threePartQuestion(question) {
  var tokens = question.split(' | ');
  return blue(tokens[0]).italics()+' '+red(tokens[1]).italics()+' '+green(tokens[2]).italics();
}

function suggest(question, items, color) {
  return xtable(
    threePartQuestion(question),
    ytable(text('Google Suggest'.bold()).scale(0.6).orphan(true), rightArrow(50).strokeWidth(3)).center(),
    ytable.apply(null, items.split(" | ").map(color)).scale(0.6),
  _).center().margin(20);
}

function fr(x) { return frameBox(x).padding(5).bg.round(10).end; }
////////////////////////////////////////////////////////////

var introduction = block('Goal',
  parentCenter(systemSpec()),
_);

var background = block('Background',
  stmt('Supervision', 'question/answers pairs' + ' [Clarke et al. 2010; Liang et al. 2011]'),
  yspace(10),
  indent(table(
    [cnl('What\'s California\'s capital?'), cwl('Sacramento')],
    [cnl('How long is the Mississippi river?'), cwl('3,734km')],
  _).margin(30, 5)).scale(0.75),
  yspace(10),
  stmt('Scale','Large Knowledge-bases'+ ' [Cai and Yates, 2013]'),
  yspace(30),
  frameBox(stmt('Our goal'.bold(), 'Training a parser from ' + 'question/answer pairs '.bold() + ' on a ' + 'large knowledge-base'.bold())).scale(0.95).padding(10),
_);

var contributionslide =  block('Contribution: Lexical Mapping',
  parentCenter(contribution()).scale(0.8),
  yspace(10),
  stmt('Alignment','lexicon from text phrases to KB predicates'),
  stmt('Bridging','use context to generate KB predicates'),
_);

var setup = block('Setup',
  headerList('Input',
  'Knowledge-base $\\sK$',
  'Training set of question-answer pairs $\\{(x_i,y_i)\\}_1^n$'),
  indent(xtable(cnl('What are the main cities in California?'),'$\\cwl{SF, LA, ...}$').margin(50)),
  yspace(10),
  headerList('Output',
    'Semantic parser that maps questions $x$ to answers $y$ through logical forms $z$'),
  table([cnl('countries in Asia'),'$\\Rightarrow$','$\\cwl{Type.Country} \\sqcap \\cwl{ContainedBy.Asia}$'],
        ['','$\\Rightarrow$',cwl('China, Japan, Israel, ...')]).margin(10),
_);

var freebasegraph = block('Querying the Freebase Knolwedge Graph',
  parentCenter(nowrapText('$\\wl{Type.Person} \\sqcap \\wl{PlacesLived.Location.Chicago}$')),
  yspace(20),
  parentCenter(xtable(
    frameBox(queryKnowledgeGraph1({highlightEdges: false}).scale(0.8)).padding(10).bg.round(10).end,
      obamaKnowledgeGraph({highlightEdges: true, highlightNodes: ['BarackObama', 'MichelleObama']}).scale(0.6),
  _).margin(50)),
  yspace(20),
  parentCenter('41M '+blue('entities, ')+
    '19K '+brown('properties, ')+
    '596M assertions',
  _),
_);

var alignmentslide = block('Alignment',
  stmt('ReVerb on ClueWeb09 [Thomas Lin]'),
  yspace(10),
  parentCenter(xtable(
    gridText(5, 4).scale(0.6),
    bigRightArrow(),
    ytable(
      textAssertion('Barack Obama', 'was born in', 'Honolulu'),
      textAssertion('Albert Einstein', 'was born in', 'Ulm'),
      textAssertion('Barack Obama', 'lived in', 'Chicago'),
      '... 15M triples ...',
    _).center().scale(0.8),
  _).center().margin(20)),
  yspace(10),
  stmt('Freebase'),
  yspace(10),
  parentCenter(xtable(
    obamaKnowledgeGraph({}).scale(0.2),
    bigRightArrow(),
    ytable(
      freebaseAssertion('BarackObama', 'PlaceOfBirth', 'Honolulu'),
      freebaseAssertion('Albert Einstein', 'PlaceOfBirth', 'Ulm'),
      freebaseAssertion('BarackObama', 'PlacesLived.Location', 'Chicago'),
      '... 400M triples ...',
    _).center().scale(0.6),
  _).center().margin(20)),
  yspace(10),
  parentCenter(alignmentGraph().scale(0.7)),
  parentCenter(frameBox(table(
    ['phrase-count', ':', '15,765'],
    ['predicate-count', ':', '9,182'],
    ['intersection-count', ':', '6,048'],
    ['KB-best-match', ':', '0'],
  _).xjustify('rcr')).title(opaquebg(darkblue('Alignment features'.bold()))).padding(10).scale(0.6)),
  yspace(10),
  indent(stmt('Lexicon', 'mapping from phrases to predicates with features')),
_);

var bridgingslide = block('Bridging',
  headerList('Often predicates are not expressed explicitly',
    cnl('What is Italy\'s language?'),
    cnl('Where is Beijing?'),
  _),
  red('Alignment') + ': build coarse mapping from raw text',
  (purple('Bridging') + ': use neighboring predicates / type constraints').bold(),
  yspace(10),
  parentCenter(bridgingobama()).scale(0.8),
  yspace(10),
  parentCenter('$\\red{\\wl{Type.University} \\sqcap \\wl{Education.Institution}.\\wl{BarackObama}}$'),
  parentCenter(frameBox(table(
    ['br-popularity', ':', '11.37'],
    ['br-two-unaries', ':', '1'],
    ['br-education.institution', ':', '1'],
  _).xjustify('lcr')).title(opaquebg('features'.bold()))).padding(10).scale(0.6),
_);

var composition = block('Composition',
 parentCenter(parseExample({pause: false})).scale(0.8),
  stmt('Candidate derivations', '$\\red{\\sD(x)}$'),
  stmt('Model', 'distribution over derivations $d$ given utterance $x$'),
  parentCenter('$p(d \\mid x, \\theta) = \\frac{\\exp(\\phi(x,d) \\cdot \\theta)}{\\sum_{d\' \\in \\red{\\sD(x)}} \\exp(\\phi(x,d\') \\cdot \\theta)}$'),
  headerList('Training (estimating $\\theta$)',
    'Stochastic gradient descent (AdaGrad)',
  _),
_);

var datasetcreation = block('WebQuestions Dataset',
  stmt('Strategy', 'breadth-first search over Google Suggest graph'),
  threePartQuestion('Where was | Barack Obama | born?'),
  suggest('Where was | _ | born?', 'Barack Obama | Lady Gaga | Steve Jobs', red),
  threePartQuestion('Where was | Steve Jobs | born?'),
  suggest('Where was | Steve Jobs | _?', 'born | raised | on the Forbes list', green),
  threePartQuestion('Where was | Steve Jobs | raised?'),
  stmt('Result: popular web questions'),
  'Answers were obtained through crowdsourcing (AMT)',
_);

var experimentsslide = block('Experiments',
    parentCenter(barGraph([[1, 59], [2, 62]])
    .xtickLabels([nil(), '[Cai & Yates, 2013]', 'our system'])
    .trajectoryColors(['gray', 'red'])
    .xrange(0, 3).xtickIncludeAxis(false).xtickIncrValue(1).yrange(50, 70).xlength(500).barWidth(50).yroundPlaces(0).ytickIncrValue(10)),
  headerList('Differences',
    'We train from answers only, CY13 uses logical forms',
    'We use 12K binary predicates, CY13 used 2k binary predicates',
  _),
  yspace(20),
  parentCenter(table(
    [barGraph([[1, 38.0], [2, 66.9], [3, 71.3]])
      .xtickLabels([nil(), 'Alignment', 'Bridging', 'Both'])
      .trajectoryColors(['red', 'purple', 'gray'])
      .xrange(0, 4).xtickIncludeAxis(false).xtickIncrValue(1).yrange(0, 80).xlength(400).barWidth(50).yroundPlaces(0).ytickIncrValue(20),
    barGraph([[1, 30.6], [2, 21.2], [3, 32.9]])
      .xtickLabels([nil(), 'Alignment', 'Bridging', 'Both'])
      .trajectoryColors(['red', 'purple', 'gray'])
      .xrange(0, 4).xtickIncludeAxis(false).xtickIncrValue(1).yrange(0, 40).xlength(400).barWidth(50).yroundPlaces(0).ytickIncrValue(10)],
    ['Free917', 'WebQuestions'],
  _).center().margin(10).scale(0.8)),
  headerList('Conclusions',
    purple('Bridging')+' more important for Free917',
    red('Alignment')+' more important for WebQuestions',
  _),
  'Test accuracy on WebQuestions: 35.5',
_);

var summary = block('Summary',
   parentCenter(overlay(
    ytable(
      a = fr('Learning from question-answer pairs'),
      b = fr('Scaling up via alignment/bridging [EMNLP 2013]'.bold()),
      xtable(
        c1 = fr(red('Paraphrases')),
        c2 = fr(red('Compositionality')),
      _).margin(80),
    _).center().margin(100),
    arrow(a, b),
    arrow(b, c1),
    arrow(b, c2),
  _)),
_);

////////////////////////////////////////////////////////////

// Settings
var xspacing = 50;
var yspacing = 70;
var widthInches = 2 * 12;
var heightInches = 2.3 * 12;

var poster = slide(
  // Heading
  ytable(
    xtable(nowrapText(darkbluebold('Semantic Parsing on Freebase from Question-Answer Pairs')).scale(1.5)).center(),
    xtable(
      'Jonathan Berant',
      'Andrew Chou',
      'Roy Frostig',
      'Percy Liang',
    _).margin(100).center(),
  _).center(),

  // Contents
  parentCenter(xtable(
    ytable(
      introduction,
      background,
      contributionslide,
      setup,
      freebasegraph,
    _).ymargin(yspacing),
    ytable(
      alignmentslide,
      bridgingslide,
      composition,
    _).ymargin(yspacing),
    ytable(
    datasetcreation,
    experimentsslide,
    summary,
    _).ymargin(yspacing),
    ytable(),
  _).xmargin(xspacing)),
_);
poster.leftHeader(frame(image('images/stanford-nlp-logo.jpeg').width(260)).padding(50));
poster.dim(96*widthInches, 96*heightInches);
poster.borderWidth(0);
poster.showIndex(false);

prez.addSlide(poster);
prez.writePdf({outPrefix: 'poster'});
