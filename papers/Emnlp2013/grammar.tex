\subsection{Composition} \label{sec:grammar}

So far, we have mainly focused on the generation of predicates.  We now discuss
three classes of features pertaining to their composition.

% Rule features
\paragraph{Rule features}

Each derivation $d$ is the result of applying some number of intersection, join, and
bridging operations.  To control this number, we define indicator features on
each of these counts.  This is in contrast to the norm of having a single feature whose
value is equal to the count, which can only represent one-sided preferences for
having more or fewer of a given operation.  Indicator features stabilize
the model, preferring derivations with a well-balanced inventory of operations.
%Although our indicator features do not
%decompose, we found them to make the model more stable.

% POS features
\paragraph{Part-of-speech tag features}

To guide the composition of predicates, we use POS tags in two ways.
First, we introduce features indicating when a word of a given POS tag is skipped,
which could capture the fact that
skipping auxiliaries is generally acceptable, while skipping proper nouns is not.
Second, we introduce features on the POS tags involved in a composition,
inspired by dependency parsing \cite{mcdonald05online}.
Specifically, when we combine logical forms $z_1$ and
$z_2$ via a join or bridging, we include a feature on the POS tag of (the first word spanned by) $z_1$
conjoined with the POS tag corresponding to $z_2$.
Rather than using head-modifier information from dependency trees
\cite{branavan2012learning,krishnamurthy2012weakly,cai2013large,poon2013gusp},
we can learn the appropriate relationships tailored for downstream accuracy.
For example, the phrase \nl{located} is aligned to the predicate \wl{ContainedBy}. 
POS features can detect that if \nl{located} precedes a noun phrase
(\nl{What is located in Beijing?}), then the noun phrase is the object of the predicate, and if
it follows the noun phrase (\nl{Where is Beijing located?}), then it is in subject position.

Note that our three operations (intersection, join, and bridging) are quite permissive,
and we rely on features, which encode soft, overlapping rules.
In contrast, CCG-based methods \cite{kwiatkowski10ccg,kwiatkowski11lex}
encode the combination preferences structurally in non-overlapping rules;
these could be emulated with features with weights clamped to $-\infty$.

%The purpose of the recursive definition is to describe in very simple
%terms a manageable set of derivations that hopefully carry the correct
%logical form.
%In contrast, CCG lexical entries contain more constraints on their
%allowed combination, but this information must originate somehow.
%If we had logical forms, we could use higher-order unification to
%induce these rules \cite{kwiatkowski10ccg}.
%We could also define the rules manually
%\cite{krishnamurthy2012weakly}, or define an auxiliary task for
%predicting CCG rules \cite{cai2013large}.

% Denotation features
\paragraph{Denotation features}

While it is clear that learning from denotations rather than logical forms is a
drawback since it provides less information,
it is less obvious that working with denotations actually gives us
additional information.
Specifically, we include four features indicating whether the denotation of the
predicted logical form has size 0, 1, 2, or at least 3.
This feature encodes presupposition constraints in a soft way:
when people ask a question, usually there is an answer and it is often unique.
This allows us to favor logical forms with this property.
