// For node.js
require('./sfig/internal/sfig.js');
require('./sfig/internal/Graph.js');
require('./sfig/internal/RootedTree.js');
require('./sfig/internal/metapost.js');
require('./sfig/internal/seedrandom.js');
require('./utils.js');

Text.defaults.setProperty('font', 'Times New Roman');  // For the paper

prez.addSlide(schema().id('schema'));
prez.addSlide(alignmentGraph().id('alignmentGraph'));
prez.addSlide(parseExample().id('parseExample'));
prez.addSlide(bridging().id('bridging'));


var bt = beamTrajectory({
  meta: 'beamfigs-meta-train.vis',
  corbmp: 'beamfigs-corbmp-train.vis',
  items: 'beamfigs-items-train.vis',
  stagger: false
});
for (var i = 0; i < bt.length; i++)
  prez.addSlide(bt[i].id('beamTrajectory-' + i));

// For node.js: output PDF (you must have Metapost installed).
prez.writePdf({outPrefix: 'figures'});
