\subsection{Lazy Agenda Parsing}

Our lazy agenda parser is based on two ideas. First (as in standard agenda parsers), we would like to have better control on the order
in which the derivation space is explored, and find a strategy that does not exhaustively generate the best $K$ derivations for all spans and categories. Second (unlike standard agenda parsers), whenever a rule is applied that generates a set of derivations, we want
to generate this set in a lazy fashion.

Agenda parsers have both a table $T$, but also an agenda $Q$. The agenda holds ``agenda items", which in our case are \emph{derivation streams}. Derivation streams are iterators over derivations, that hold the information required for computing and scoring derivations, but crucially perform this computation lazily only at the last moment. 

The algorithm outline is as follows: at each step, a derivation stream is popped from the 
agenda, and a single derivation $d$ is generated from the stream. If the derivation stream is not empty, 
its first elemnt is scored and the stream is pushed back onto the agenda. The derivation
$d$ is then added to the table, and combined using the grammar $G$ with all existing table derivations. 
This is done in a lazy fashion by generating derivation streams in pushing them onto $A$.

\refalg{lazy} describes the agenda parser. The algorithm handles binary grammar rules only, but unary
rules are treated similarly.
We denote a stream of derivations with category $A$ over span $(i,j)$ by $D_{i,j}^A$ (as opposed to a set
which is denoted by $\sD_{i,j}^A$. The parser first initializes the
agenda by pushing derivations for all text spans with category \textsc{Phrase}.
Then, the parser enters the main loop and creates derivations created until $K$ root derivations over $x$ are found,
or the agenda is empty. 

As mentioned, at each step a derivation stream is popped and a derivation $d_{i,j}^A$ is popped from the stream. The parser 
maintains that the first derivation in each stream is fully computed and scored, while the computation for the rest of the 
derivations is differed.
This maintains the invariant that the score for a derivation stream is equal to the score of its first element, that is,
$s(D)=D.\text{peek}().s$. To maintain this invariant, after popping the first derivation, the next stream derivation is computed 
and scored, and the stream is pushed back onto the agenda.

New derivation streams are created by adding $d_{i,j}^A$ to the table. If $d_{i,j}^A$ is in the top-$K$ derivations with
category $A$ spanning $(i,j)$ then it is added to the table and combined with all other possible derivations. This is done by 
going over all spans $(j,k)$ and $(k,i)$, and all compatible grammar rules, and creating a derivation stream for each one. For example, 
for the span $(j,k)$ and the rule $B \rightarrow A \ C$ we will combine $d_{i,j}^A$ with the derivations in $T_{j,k}^C$. All created
derivation streams are pushed onto the agenda.

Derivation streams store the information for computing all derivations, but only the first derivation is actually computed.
However, the stream must know in what order to produce new derivations. This is done by sorting candidate derivations on the stream 
by the score $d_{rgt} + s(r,l)$ or $d_{lft} + s(r,l)$.   If derivation scores are decomposable, 
then this is guaranteed to compute the best $K$ root derivations over $x$. However,
we hope that while a beam parser computes the best $K$ derivations for every span and category, we will substantially reduce the 
number of computed derivations by having a better strategy for exploring the derivation space. 

\begin{algorithm} \label{alg:lazy} 
\caption{Lazy agenda parser}
\begin{algorithmic} 
\Procedure {Parse}{$G$, $x$}
\State \Call{initAgenda}{\null}
\While{$|Q|>0 \land |T_{0,|x|}^S|< K$}
\State $D \leftarrow Q\text{.popDeriv()}$ 
\State $d \leftarrow D\text{.pop()}$
\State \Call{AddToTable}{$T$,$d$}
\If{$D.\text{hasNext()}$}
\State $Q.\text{push}(D,$\Call{scoreStream}{D}$)$
\EndIf
\EndWhile
\EndProcedure
\Function {addToTable}{$d_{i,j}^A$}
\If{$T_{i,j}^A.\text{add}(d_{i,j}^A)$}
\For{$k>j$,$r:(B \rightarrow A \ C,\sL,f)$}
\State \Call{buildStream}{$d_{i,j}^A$,$T_{j,k}^C,r,$\text{right}} 
\EndFor
\For{$k<j$,$r:(B \rightarrow C \ A,\sL,f)$}
\State \Call{buildStream}{$d_{i,j}^A$,$T_{k,j}^C,r,$\text{left}} 
\EndFor
\EndIf
\EndFunction
\Function {scoreStream}{$D$}
\State \Return $\text{score}(D.\text{peek}())$
\EndFunction
\Function {buildStream}{$d$,$D$,$r$,\text{dir}}
\If {$\text{dir}=\text{right}$}
\State $D \leftarrow \{F(l,d,d_{rgt}) : d_{rgt} \in D, l \in L\}$
\Else
\State $D \leftarrow \{F(l,d_{left},d) : d_{lft} \in D, l \in L\}$
\EndIf
\State $A.\text{push}(D,$\Call{scoreStream}{D}$)$
\EndFunction
\Function{initAgenda}{\null}
\For{$(i,j) \in x$}
\State $A.\text{push}((i,j,\textsc{Phrase},\textsc{null},\phi),0)$
\EndFor
\EndFunction
\end{algorithmic}
\end{algorithm}
