\section{Task} \label{sec:task}

% Problem statement
Our task is as follows:
given
(i) a knowledge base $\sK$,
(ii) a corpus of raw text $\sT$,
and (iii) a training set of question-answer pairs $\sD = \{ (x_i,y_i) \}_{i=1}^n$,
output a semantic parser that maps new questions $x$ to answers $y$
via latent logical forms $z$ and the knowledge base $\sK$.

%We would like to train a semantic parser that given a question in natural
%language answerable by a knowledge-base, converts the question into a logical
%form, queries the knowledge-base and returns an answer. In this section we will
%describe the knowledge-base and the logical forms employed for querying the
%knowledge-base.

\subsection{Knowledge base} \label{sec:kb}

Let $\sE$ denote a set of \emph{entities} (e.g., \wl{BarackObama}),
and let $\sP$ denote a set of \emph{properties} (e.g., \wl{PlaceOfBirth}).
A \emph{knowledge base} $\sK$ is a set of \emph{assertions}
$(e_1,p,e_2) \in \sE \times \sP \times \sE$
(e.g., $(\wl{BarackObama}, \wl{PlaceOfBirth}, \wl{Honolulu})$).
It is convenient to think of a knowledge base as a directed graph
whose nodes are entities and edges are assertions labeled with the property.

\FigTop{figures/knowledgeBaseGraph}{0.3}{knowledgeBaseGraph}{A fragment of the
Freebase knowledge base: nodes are entities, edges represent assertions, and
edge labels represent properties.}

%as a directed graph $G_{KB}=(V_{KB},E_{KB})$, where each node represents an
%entity in the knowledge base and each edge $e \in E_{KB}$ is labeled by a
%property $prop(e)$ representing a relation between the pair of entities. Thus,
%every edge $e=(s,o)$ expresses a single triple $(s,prop(e),o)$. For example,
%two representative triples from the graph in Figure~{1} are
%$\tt{(BarackObama,BornIn,Hawaii)}$

% Freebase
\reffig{knowledgeBaseGraph} shows a small fragment of the knowledge base we
use.  Note that nodes include concrete entities (\wl{BarackObama}), abstract
entities (\wl{Person}), dates (\wl{1961.08.04}), and reified events
(\wl{\ObamaMarriage}).
%We assume that each property (e.g., \wl{ContainedBy}) has a reverse property (e.g., \wl{Contains}).
We use the Freebase knowledge base\footnote{We filtered out \wl{/user/} and \wl{/base/}.} (\url{www.freebase.com})
which contains 29M non-numeric entities, 6,000 properties, and 414M
assertions.\footnote{We condense Freebase names for readability
(\wl{/people/person} becomes \wl{Person}).}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Logical forms} \label{sec:logicalForms}

% Graph templates
Freebase contains a wealth of raw information, %knowledge bases such as Freebase, YAGO, and DBpedia contain a wealth
and querying it is non-trivial.
For example, even simple predicates such as \nl{spouse} do not
correspond to single edges, but logical forms.

We define a simple logical language inspired by description logic,
which is more compact than general lambda calculus.
The main idea is to make existentials implicit, so that we can write
$p_1.p_2.p_3.\hole$ to denote all pairs connected by edges with labels $p_1,p_2,p_3$
rather than $\lambda x . \lambda y . \exists a . \exists b . p_1(x,a) \wedge p_2(a,b) \wedge p_3(b,y)$.
Formally, our language is equivalent to
first-order logic restricted to conjunction and existential quantification,
and to basic Dependency Compositional Semantics (DCS) \cite{liang11dcs}.
We do not consider universal quantification and negation, since our focus is on lexical mapping.

Formally, we define a logical form $z$ to be either
(i) an entity $e \in \sE$;
(ii) a placeholder $\hole$;
(iii) a join $p.z$, where $p \in \sP$ is a property and $z$ is a logical form; or 
(iv) an intersection $z_1 \sqcap z_2$, where $z_1$ and $z_2$ are logical forms.
The denotation $\den{z}$ of a logical form $z$ without $\hole$ is defined as follows:
$\den{e} = \{e\}$, $\den{p.z} = \{ x \in \sE : (x,p,y) \in \sK \wedge y \in \den{z}\}$,
and $\den{z_1 \sqcap z_2} = \den{z_1} \cap \den{z_2}$.
If $z$ contains $\hole$, then
define $\den{z} = \{ (e_1, e_2) : e_2 \in \sE, e_1 \in \den{\beta(z, e_2)} \}$,
where $\beta(z_1,e_2)$ acts like an application operator that returns
$z_1$ with its placeholder $\hole$ replaced by $e_2$.\footnote{
To compute denotations, we convert a logical form $z$ into a SPARQL query and
execute it using Apache Jena Fuseki.}

Unary predicates can be represented using $\hole$-free logical forms
(e.g., \nl{politician} is represented by
$\wl{Profession}.\wl{Politician}$).
Binary predicates are represented by logical forms with a $\hole$ representing the second argument.
For example, \nl{spouse} is represented by $\wl{Marriage}.\wl{Spouse}.\hole$.
As a more complex example, \nl{wife} is represented by
$\wl{Marriage}.\wl{Spouse}.(\hole \sqcap \wl{Gender}.\wl{Female})$.
%the equivalent lambda calculus expression is:
%$\lambda y . \lambda x . \exists e . \wl{Marriage}(x, e) \wedge \wl{Spouse}(e, y) \wedge \wl{Gender}(y, \wl{Female})$.

%For example, $\beta(\wl{PlaceOfBirth}.\hole, \wl{Honolulu}) = \wl{PlaceOfBirth}.\wl{Honolulu}$.

