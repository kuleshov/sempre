#!/usr/bin/ruby

$: << '../rfig/lib'
require 'rfig/FigureSet'
require 'utils'

initFigureSet(:outPrefix => 'figures', :defaultFont => 'times',
              :latexHeader => IO.readlines('std-macros.tex') +
                              IO.readlines('macros.tex'),
              :outputStrings => false,
              :lazy => true)

def wl(label); _('\wl{'+label+'}') end
def nl(label); _('\nl{'+label+'}') end
def tnl(label, t1, t2); _('\nl{'+label+'}[\wl{'+t1+','+t2+'}]') end # Typed natural language
def cnl(label); nl(label).color(greenfour) end
def cwl(label); wl(label).color(red) end
def ctnl(label, t1, t2); tnl(label, t1, t2).color(greenfour) end # Typed natural language

############################################################

def box(label); ctable(label).ospace.border(2) end

def argument(mainText, exampleText=nil)
  rtable(mainText, exampleText).center.ospace
end

def schema
  out = []

  # Example of a training example
  qa = table(
    ['Query:', '\nl{Which US senator}'],
    ['', '\nl{was born in Japan?}'],
    ['Answer:', '\nl{Hirono}'],
  nil).cjustify('rl')

  out << rtable(
    ctable(
      text = argument(_('Raw text').color(greenfour), '\nl{...Obama, born in Honolulu,...}'),
      kb = argument(_('Knowledge base').color(blue), _('$(\wl{BarackObama}, \wl{PlaceOfBirth}, \wl{Honolulu})$').scale(0.9)),
    nil),
    align = box('Alignment'),
    # NO_MID  fb:people.person.place_of_birth 488013.0        born in fb:people.person        fb:location.location
    # FB_typed_size,Intersection_size_typed,NL-size,NL_typed_size     15477.0,13305.0,15688.0,15430.0 place of birth
    # NO_MID  (lambda x (fb:people.person.places_lived (fb:people.place_lived.location (var x))))     180157.0        born in fb:people.person        fb:location.location
    # FB_typed_size,Intersection_size_typed,NL-size,NL_typed_size     9471.0,6249.0,15688.0,15430.0   places lived location
    lexicon = argument(_('Lexicon').color(red), '(\nl{born in}, \wl{PlaceOfBirth})$\{\text{intersection}\!:\!13305,\text{text}\!:\!15430,\dots\}$'),
    ctable(
      examples = argument(_('Training examples').color(greenfour), qa),
      training = box('Training'),
    nil).center.cmargin(u(0.8)).cNumGhosts(1, 0),
    ctable(
      rtable(
        'Latent logical form:',
        _('$\wl{Type}.\wl{Senator} \sqcap \wl{PlaceOfBirth}.\wl{Japan}$').scale(0.8),
      nil).color(black),
      parser = argument(_('Semantic parser').color(red)),
    nil).cNumGhosts(1, 0),
  nil).center.rmargin(u(0.5))

  # Add arrows
  [[text, align], [kb, align], [align, lexicon], [lexicon, training], [examples, training], [training, parser]].each { |a,b|
    out << clippedpath(a, b).arrow.arrowSize(10)
  }

  overlay(*out)
end
printObj(:obj => schema.signature(35), :outPrefix => 'schema')

############################################################

# Return [{node name => node object} map, graph Object to display].
def drawKnowledgeBaseGraph(entries)
  nodes = {} # Map node name => object
  nodePositions = {} # Map node name => position
  out = []
  getNode = lambda {|name,dx,dy|
    o = nodes[name]
    posScale = 2
    if not o
      o = nodes[name] = overlay(_(wl(name)).bold.color(blue)).center.shift(upair(dx*posScale, dy*posScale))
      nodePositions[name] = [dx, dy]
      out << o
    end
    o
  }
  entries.each { |e1,(property,dx,dy),e2|
    #p e1, e2
    e1obj = getNode.call(e1, 0, 0)
    e2obj = getNode.call(e2, nodePositions[e1][0]+dx, nodePositions[e1][1]+dy)
    link = clippedpath(e1obj, e2obj).arrow.thickness(2)
    out << link
    out << overlay(ctable(wl(property)).color(brown).opaque).scale(0.7).center.shift(tcenter(link))
  }
  [nodes, overlay(*out)]
end

############################################################

def knowledgeBaseGraph
  nodes, graph = drawKnowledgeBaseGraph([
    ['BarackObama', ['Type', -2, +1], 'Person'],
    ['BarackObama', ['Profession', +1, +1], 'Politician'],

    ['BarackObama', ['DateOfBirth', -0.5, 1], '1961.08.04'],
    ['BarackObama', ['PlaceOfBirth', 2, 0], 'Honolulu'],
    ['Honolulu', ['ContainedBy', 0, -1], 'Hawaii'],
    ['Honolulu', ['Type', 0, 1], 'City'],
    ['Hawaii', ['ContainedBy', -1.5, +0.5], 'UnitedStates'],
    ['Hawaii', ['Type', 0, -0.7], 'USState'],

    ['BarackObama', ['Marriage', -1, -1], '\ObamaMarriage'],
    ['\ObamaMarriage', ['Spouse', -1, -1], 'MichelleObama'],
    ['MichelleObama', ['Type', 1, 0], 'Person'],
    ['MichelleObama', ['Gender', 1.5, 0.3], 'Female'],
    ['\ObamaMarriage', ['StartDate', 1.5, -0.5], '1992.10.03'],

    ['BarackObama', ['PlacesLived', -1.5, +0.4], '\BarackObamaLiveInChicago'],
    ['\BarackObamaLiveInChicago', ['Location', -1, -0.4], 'Chicago'],
    ['MichelleObama', ['PlacesLived', -0.5, +1], '\MichelleObamaLiveInChicago'],
    ['\MichelleObamaLiveInChicago', ['Location', 0, 0], 'Chicago'],
    ['Chicago', ['ContainedBy', 0, 0], 'UnitedStates'],
  nil].compact)
  graph
end
printObj(:obj => knowledgeBaseGraph.signature(87), :outPrefix => 'knowledgeBaseGraph')

############################################################

def assertion(e1, e2)
  _('$(\wl{'+e1+','+e2+'})$').scale(0.6)
end

def alignmentGraph
  spacing = u(0.3)

  typedWords = rtable(
    t1 = ctnl('grew up in', 'Person', 'Location'),
    t3 = ctnl('born in', 'Person', 'Date'),
    t4 = ctnl('married in', 'Person', 'Date'),
    t2 = ctnl('born in', 'Person', 'Location'),
    #t2 = ctnl('played in', 'Actor', 'Movie'),
    #t3 = ctnl('played in', 'Athlete', 'SportsTeam'),
    #t1 = ctnl('directed by', 'Movie', 'Person'),
    #t2 = ctnl('played in', 'Actor', 'Movie'),
    #t3 = ctnl('played in', 'Athlete', 'SportsTeam'),
  nil).rmargin(spacing)

  freebase = rtable(
    f1 = cwl('DateOfBirth.\hole'),
    f3 = cwl('PlaceOfBirth.\hole'),
    f4 = cwl('Marriage.StartDate.\hole'),
    f2 = cwl('PlacesLived.Location.\hole'),
    #f2 = wl('\wl{Performances.Actor}'),
    #f3 = wl('\wl{SportsTeam}'),
  nil).rmargin(spacing)

  graph = ctable(
    #words,
    typedWords,
    freebase,
  nil).center.cmargin(u(0.5)).scale(0.95)

  venn = overlay(
    text = circle(u(5.8), u(2)).thickness(2).color(darkblue),
    kb = circle(u(5.8), u(2)).thickness(2).shift(upair(2, 0)).color(darkblue),
    assertion('BarackObama', 'Honolulu').shift(upair(-0, -0.3)),
    assertion('MichelleObama', 'Chicago').shift(upair(-0, +0.3)),
    assertion('BarackObama', 'Chicago').shift(upair(+2.8, +0.2)),
    assertion('RandomPerson', 'Seattle').shift(upair(-2.8, -0.2)),
  nil)

  # NO_MID  (lambda x (fb:people.person.places_lived (fb:people.place_lived.location (var x))))     180157.0        born in fb:people.person        fb:location.location
  # FB_typed_size,Intersection_size_typed,NL-size,NL_typed_size     9471.0,6249.0,15688.0,15430.0   places lived location
  stats = table(
    ['intersection-count', ':', '6,048'],
    ['phrase-count', ':', '15,765'],
    ['predicate-count', ':', '187,107'],
    ['KB-best-match', ':', '0'],
  nil).cjustify('rcr').border(1).ospace

  stack = rtable(
    graph,
    ctable(venn),
    rtable(
      '\textbf{\darkblue{Alignment features}}',
      stats.scale(0.8),
    nil).center,
  nil).rmargin(u(0.5)).center

  edges = [
    [t1, f2],
    [t1, f3],
    [t2, f2],
    [t2, f3],
    [t3, f1],
    [t3, f4],
    [t4, f1],
    [t4, f4],
  ].map { |a,b|
    clippedpath(a, b).thickness(2).dashed
  }
  edges << clippedpath(t2, text).begindir(-90).curved.arrow
  edges << clippedpath(f2, kb).begindir(-90).curved.arrow

  overlay(stack, *edges)
end
printObj(:obj => alignmentGraph.signature(161), :outPrefix => 'alignmentGraph')

############################################################

#def assertion(e1, e2)
#  _('$(\wl{'+e1+','+e2+'})$')
#end
#
#def intersectionExample
#  overlay(
#    text = circle(u(5), u(4)).thickness(3),
#    kb = circle(u(5), u(4)).thickness(3).shift(upair(3, 0)),
#    assertion('BarackObama', 'Honolulu').shift(upair(-1, 1)),
#    assertion('BarackObama', 'Chicago'),
#    assertion('AlGore', 'DC').shift(upair(2, -1.5)),
#    overlay(ctnl('born in', 'Person', 'Location')).center.shift(tup(text).add(upair(-1, 0.2))),
#    overlay(cwl('PlacesLived')).center.shift(tup(kb).add(upair(1, 0.2))),
#  nil)
#end
#printObj(:obj => intersectionExample.signature(114), :outPrefix => 'intersectionExample')

############################################################

def lexEntry(word, *items)
  rtable(
    cnl(word+':'),
    ind(rtable(*items)),
  nil)
end

# An example of parsing a sentence.
def parseExample
  # Lambda calculus - complicated
  tree = parseTree([
    '\Composite : $\red{\lambda x. \wl{Location}(x) \wedge \wl{PlaceOfBirth}(x, \wl{BarackObama})}$',
    ['\gr{Set} : $\red{\lambda x. \wl{Location}(x)}$', ['\gr{Unary} : $\red{\lambda x .\wl{Location}(x)}$', cnl('where')]],
    cnl('was'),
    ['\Composite : $\red{\lambda x. \wl{PlaceOfBirth}(\wl{BarackObama}, x)}$',
      ['\gr{Entity} : $\red{\lambda x . x = \wl{BarackObama}}$', cnl('obama')],
      ['\gr{Binary} : $\red{\lambda y . \lambda f . \lambda x. \exists y . \wl{PlaceOfBirth}(x,y) \wedge f(y)}$', cnl('born')],
    ],
    cnl('?'),
  ])

  tree = parseTree([
    '\Composite : $\red{\wl{Type.Location} \sqcap \wl{PeopleBornHere}.\wl{BarackObama}}$',
    ['\gr{Set} : \red{\wl{Type.Location}}', ['\gr{Unary} : \red{\wl{Type.Location}}', cnl('where')]],
    cnl('was'),
    ['\Composite : $\red{\wl{PeopleBornHere}.\wl{BarackObama}}$',
      ['\gr{Entity} : \red{\wl{BarackObama}}', cnl('obama')],
      ['\gr{Binary} : $\red{\wl{PeopleBornHere}.\hole}$', cnl('born')],
    ],
    cnl('?'),
  ])

  features1 = [
    ['\binaryKBLogPopularity', 13.098],
    ['\binaryUntypedTextLogCount', 9.789],
    ['\binaryKBLogCount', 9.647],
    ['\binaryTextLogCount', 9.644],
    ['\binaryIntersectionLogCount', 9.496],
    ['\binaryTopRanked', 1],
  ]
  features2 = [
    ['\unaryKBLogPopularity', 14.108],
    ['\entityKBLogPopularity', 9.433],
    ['\entitySuffix', 1],
    ['\skipPos{\textsc{vbd-aux}}', 1],
  ]

  def formatFeatures(features)
    table(*features.map{|k,v| [k, ':', v]}).cjustify('rcl')
  end

  features = rtable(
    '\darkblue{\textbf{Active features}}:',
    ctable(formatFeatures(features1), formatFeatures(features2)).scale(0.8).cmargin(u(0.4)).border(1).ospace,
  nil)

  #[ binary.popularity ]                              -5.635 = 13.098 * -0.430
  #[ binary.NL-size ]                                 -1.722 = 9.789 * -0.176
  #[ binary.FB_typed_size ]                           -1.711 = 9.647 * -0.177
  #[ binary.NL_typed_size ]                           -1.046 = 9.644 * -0.108
  #[ binary.Intersection_size_typed ]                 13.054 = 9.496 * 1.375
  #[ binary.top ]                                      0.169 = 1 * 0.169

  #[ unary.popularity ]                               -2.422 = 14.108 * -0.172
  #[ entity.popularity ]                               7.397 = 9.433 * 0.784
  #[ entity.queryIsSuffix ]                            0.453 = 1 * 0.453

  #[ skipPos.VBD-AUX ]                                -0.617 = 1 * -0.617

  #[ binary.ALIGNMENT ]                               -1.176 = 1 * -1.176
  #[ unary.HARD ]                                      0.608 = 1 * 0.608

  lexical = rtable(
    '\darkblue{\textbf{Lexical entries used}}:',
    table(
      ['\nl{Barack Obama}', ':', '\wl{BarackObama}'],
      ['\nl{where}', ':', '\wl{Type.Location}'],
      ['\nl{born in}[\wl{Person},\wl{Location}]', ':', '\wl{PlaceOfBirth.\hole}'],
    nil).scale(0.8).cmargin(u(0.4)).border(1).ospace,
  nil)

  rtable(
    tree,
    ctable(lexical, features).cmargin(u(0.5)),
  nil).center.rmargin(u(0.4))
end
printObj(:obj => parseExample.signature(129), :outPrefix => 'parseExample')

############################################################

def lexiconExample
  lexicon = rtable(
    lexEntry('born', '$\wl{PeopleBornHere}.\hole$ $\{\text{popularity}:30,\text{distance}:40,\text{intersect}:100\}$', '$\wl{PlacesLived}.\hole$ [pop=80]', '$\wl{PeopleBornNow}.\hole$', '...'),
    lexEntry('obama', '$\wl{BarackObama}$', '$\wl{ObamaJapan}$', '...'),
    lexEntry('where', '$\wl{Location}$', '$\wl{Organization}$', '...'),
  nil)
end
printObj(:obj => lexiconExample.signature(116), :outPrefix => 'lexiconExample')

############################################################

def m100(a); a.map{|b| b.map{|x| x*100}} end
def learningCurve
  t = DataTable.new(
    :rowLabels => [nil, 'Lexicalized', 'Unlexicalized'],
    :colName => 'number of training examples $n$',
    :cellName => 'dev. accuracy',
    :contents => [
      [10, 20, 50, 100, 200, 500, 1000],
      m100([[0.131, 0.149, 0.19, 0.229, 0.235, 0.272, 0.296]])[0],
      m100([[0.173, 0.217, 0.251, 0.286, 0.288, 0.296, 0.31]])[0],
    ],
    :errorBars => [
      nil,
      m100([[0.1021, 0.1599], [0.0995, 0.1965], [0.1755, 0.2039], [0.2147, 0.2420], [0.2213, 0.2487], [0.2576, 0.2857], [0.2894, 0.3026]]),
      m100([[0.0852, 0.2595], [0.1676, 0.2664], [0.2239, 0.2781], [0.2798, 0.2922], [0.2826, 0.2927], [0.2922, 0.2992], [0.3078, 0.3109]]),
    ]
  )

  g = lineGraph(t)
  g.yrange(0, 32).ytickIncrValue(10)
  g.xrange(0, 1000)
  g.xroundPlaces(0).yroundPlaces(0).ylength(u(2)).legendPosition(+1, -1)
  g.colors(blue, red)
end
printObj(:obj => learningCurve.signature(146), :outPrefix => 'learningCurve')

finishFigureSet
