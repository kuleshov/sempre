\section{Experimental Evaluation} \label{sec:experimental_evaluation}

%\subsection{Datasets} \label{sec:datasets}

We first compare our approach to \newcite{Krishnamurthy:12} on their
dataset (henceforth, KM-NP).  Next, we describe a new dataset
that we collected (WebQuestions) and conduct a detailed empirical
investigation.

\subsection{KM-NP} \label{sec:kmnp}

\newcite{Krishnamurthy:12} released a dataset consisting of 100 noun phrase
queries (e.g., \nl{languages of China}, \nl{currency in Finland}).  The dataset
is annotated with logical forms, but we executed the logical forms on Freebase
and use the resulting answers to train and evaluate.

Of the 100 queries in the dataset,
we use 25 for training,
25 for development, and 50 for testing.
We evaluated using accuracy, the fraction of predicted answers which exactly
matched the answer returned by executing the annotated logical form.
Our system obtained 55\% test accuracy,
which is comparable to the 56\% accuracy reported by \newcite{Krishnamurthy:12}.
Note that they train on 125K distantly-supervised examples,
while our unlexicalized parser is trained on only 50 examples labeled with answers (plus a lexicon),
which is much faster. While they do not require any per-example annotations,
the queries they handle are taken directly from declarative utterances. We
believe that to handle actual questions, access to some interrogative
constructions is necessary. Finally, our system is configured to
consider thousands of knowledge base predicates, while they restrict to 77
predicates.

% Error analysis
A brief error analysis reveals that our parser ranks the correct logical form in
the top 5 derivations in 68\% of the questions, and in the top 10 derivations
in 74\% of the questions. For another 12\% of the questions the correct logical
form is either ranked lower than that or not found, and the remaining 14\% are
simply not parsed, mainly due to POS tagging errors, NER errors, or coverage
problems in the grammar.

\subsection{WebQuestions} \label{sec:webQuestions}

The KM-NP dataset consists of noun phrases derived from declarative sentences,
and do not represent real questions that people ask.
To address this shortcoming, we created a new dataset, \emph{WebQuestions},
which consists of 2,000 search queries representing short-answer, fact-seeking
questions.  Some examples include:
\begin{itemize}[noitemsep,nolistsep]
\item{\nl{What year was Sparta destroyed?}}
\item{\nl{What religion is Seth Macfarlane?}}
\item{\nl{Which continent is located south of Europe?}}
\end{itemize}
To collect this dataset,
we first queried the Google Suggest API for questions
that begin with a wh-word and contains exactly one entity.
%The questions that we collected are constrained to be of the form:
%\centerline{\nl{(IN) Wh-word * Entity *?}}
\reftab{datasets} provides some statistics about the new questions.
We submitted these questions to Amazon Mechanical Turk (AMT). The AMT task asked
workers to answer the question using only the Freebase page of the query's entity,
or to mark it as unanswerable by Freebase. We paid the workers \$0.02 per 
question, and they answered all of the questions in three days.
% python createSemanticParserInputAmtCsv.py 6
The AMT workers found that 22.8\% of the questions are answerable by Freebase;\footnote{
56.6\% of questions deemed answerable by a 
single AMT worker are deemed answerable by a second independent worker, but
the second pass wasn't used to filter the dataset.}
we chose 2,000 of these randomly for our experiments.
% ./mergeExamples
We further split the questions into 1,000 training and 1,000 for development.
We will report results on an unseen test set for the final version of this paper.

\begin{table}[t]
{\small
\hfill{}
\begin{tabular}{|l|r|r|}
\hline
\textbf{Dataset}&\textbf{\# examples}&\textbf{\# word types}\\
\hline
GeoQuery            & 880           & 279             \\ 
ATIS                & 5,418          & 936             \\ 
KM-NP               & 100           & 158             \\ 
{\bf WebQuestions}  & 2,000          & 2,254            \\
\hline
\end{tabular}}
\hfill{}
\caption{Statistics of various semantic parsing datasets.  Our new
dataset, WebQuestions, contains much more lexical diversity than previous datasets.}
\label{tab:datasets}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Systems}

We trained the following variants of our system:

\begin{itemize}[noitemsep,nolistsep]
  \item \Unlex: Our unlexicalized model with alignment features.

\item \Lex: A lexicalized model that replaces alignment features
with indicator features that fire whenever
a text predicate is mapped to a KB predicate.
%Given a question and a derivation a feature
%fires whenever the KB predicate is triggered by the text predicate.
This simulates the lexicalized features proposed by
\newcite{Krishnamurthy:12}.

\item \BasicStats: A model that only uses KB predicate popularity, entity popularity,
and surface similarity features.
These features are used directly as the scoring function (without training)
in \newcite{Yahya:12}.
%are used as similarity measures between text predicates
%and logical forms in the semantic parser proposed by \newcite{Yahya:12}.

\item \TopFive: A model that has the same features as \BasicStats\
but uses a hard threshold instead of the alignment features:
for every text predicate, we sort the aligned KB
predicates by intersection count and keep the top 5.

\item \All: A model that uses both lexicalized and unlexicalized alignment features.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Evaluation metric} \label{sec:evaluation}

Proper evaluation on this dataset is difficult for several reasons.
First, AMT workers sometimes provide incorrect answers.
On the other hand, incorrect logical forms can serendipitously lead to correct answers.
For example, we answer \nl{who succeeded president Lincoln after his
assassination?} correctly by incorrectly mapping \nl{succeeded} to
\wl{USVicePresident.\hole}.
Note that this phenomenon also exists in relation extraction,
where correct facts are extracted for the wrong reasons.
%evaluated against a knowledge base.
%but indicates that accuracy results are approximate.
%We perform in Section~\ref{subsec:erroranalysis} manual error analysis on a
%smaller sample of questions.

% Matching
Second, AMT worker's answers sometimes do not exactly match the system's predicted 
answers even when they
ought to be deemed equal, e.g., \nl{Obama} versus \nl{Barack Obama}.
%We cannot simply by turkers to exactly match  answers retrieved from Freebase.
%To obtain robust approximate accuracy scores,
We say two answers match if the tokens in one is a subsequence of the
tokens in the other.
For example, \nl{Barack Hussein Obama} matches \nl{Barack Obama} and \nl{Obama} but not
\nl{Michelle Obama} and \nl{Obama Barack}.
Finally, a few of the answers are lists; we split on punctuation and coordination words,
and say two lists match if any pair of elements from the two lists match.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Results}

Table~\ref{tb:results} presents the development accuracy for all systems.
\TopFive\ performs the worst, showing that pruning the lexicon too early hurts
performance; \BasicStats\ performs only a bit better, because it has no capacity
to remember alignment information.
\Unlex, \Lex, and \All\ perform similarly on the development set,
which is quite encouraging since \Unlex\ has only 98 parameters
while \Lex\ has 11K parameters.
Indeed, on the training accuracy, we can see that systems with lexicalized
features overfit substantially.

%and \Lex\ by 1.3 accuracy points, and the simpler baselines by a much
%larger margin. Looking at \BasicStats\ and \TopFive\ it is clear that alignment
%statistics dramatically improve performance. Combining both lexical features
%with alignment scores improves does not raise performance substantially.

\begin{table}[systemResultsTable]
{\small
\begin{tabular}{|l|r|r|}
\hline
\textbf{System} & \textbf{Train accuracy} & \textbf{Dev. accuracy} \\
\hline
\TopFive        & 19.7                    & 18.6 \\
\BasicStats     & 21.7                    & 21.2 \\
\Lex            & 40.3                    & 29.6 \\
\Unlex          & 31.9                    & 30.9 \\
\All            & 39.3                    & 31.1 \\
\hline
\end{tabular}}
\caption{Results on the WebQuestions data set}
\label{tb:results}
\end{table}

%A fundamental property of our unlexicalized parser is that it can quickly
%generalize from a small number of training examples.
To study generalization in further detail, we analyze the learning curves of \Lex\ and \Unlex\
as the number of training examples increases (\reffig{learningCurve}).
%presents the learning curve as the size of the training set increases.
The unlexicalized parser generalizes much faster: the development accuracy of
\Unlex\ given 100 training examples is comparable to the accuracy of \Lex\ given 10
times more examples.

\FigTop{figures/learningCurve}{0.5}{learningCurve}{Learning curves: the unlexicalized
parser generalizes much faster than the lexicalized one.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Error analysis} \label{subsec:erroranalysis}
%5. Maybe use an unsupervised measure like Jaccard instead of the counts?
Table~\ref{tb:errorAnalysisTable} shows error analysis for 200 randomly 
sampled development set questions. 31\% of
the sampled answers are correct and 69\% are incorrect.
Among incorrect examples, 36\% are deemed impossible to answer by
any Freebase-driven semantic parser. This class of errors includes:
%the dataset 
%has three main classes of questions that we cannot hope to answer:
\begin{description}[topsep=0pt, partopsep=1pt]
  \itemsep-0.5em 
  \item[Vague questions] \nl{What does baby Jessica look like today?}
  \item[Mislabeled answers] \nl{When was the Berlin Wall finished?} Turker answer: \nl{1989}
  \item[Answers not in Freebase] \nl{Where is The Steve Harvey Show filmed?}
  %was labelled with an incorrect answer of 
\end{description}
%These three classes account for nearly a quarter of all questions.
The remaining 64\% are genuine errors made by our system.
They mostly fall into the following four categories:
%There are also four main types of errors made by our system:
\begin{description}[topsep=0pt, partopsep=1pt]
  \itemsep-0.5em 
  \item[POS or NER] The POS and NER recognizers, used to determine
  which spans of the query to look up in the lexicon make mistakes.
  For example, for \nl{What river is Victoria Falls found on?},
  \nl{Falls} is not recognized as a named entity.
  NER is difficult here since all queries are lowercase.
  \item[Lexicon] Although we used a web corpus, there are still coverage issues.
  For example, for \nl{Where does the president of the United States sleep?},
  the word \nl{sleep} is not in our lexicon so it is ignored, and a list of
  US presidents is returned instead.
  \item[Grammar] \nl{Where is Ithaca College?} Our grammar requires a binary predicate, so
  this question does not parse. However, \nl{Where is Ithaca College in?} does
  parse.
  \item[Features] \nl{Who is in charge of China right now?} Our prediction is a 
  list of people who live in China. The correct answer is ranked sixth
because the logical form {\wl{OfficeHolder}.\wl{Jurisdiction}.\hole}
  is less common (and thus has a lower score) than the logical form \wl{PlacesLived.Location.\hole}.
\end{description}
Overall, our parser generates at least one derivation for 70\% of the examples,
and the beam contains the correct answer for 42\% of the examples.
%This percentage leaves some room for improvement in the grammar.
%It is also possible that a looser type checking scheme is necessary to parse the noisy questions,
%or that many of these questions are not possible to answer as shown in Table~\ref{tb:errorAnalysisTable}.
%Another space for improvement is the lexical coverage.
Note that the error due to bad feature weights is only 6.5\%, suggesting
that using unlexicalized features is fairly adequate.
The largest class of errors is due to lexical coverage.
Our system is currently restricted to common patterns that occur in triples from ReVerb.  It is worthwhile considering
information from other linguistic constructions.

\begin{table}
{\small
\hfill{}
\begin{tabular}{|l|r|}
\hline
\textbf{Category}          &\textbf{\% of examples}\\
\hline
Correct                    & 31.0\%           \\ 
Incorrect                  & 69.0\%          \\ 
\hline
\multicolumn{2}{|l|}{\textbf{Incorrect: impossible}} \\ 
\hline
Vague question             & 15.0\%           \\ 
Mislabeled answer          & 6.5\%           \\ 
%Bad AMT ``Ground Truth''   & 6.5\%           \\ 
Not answerable by Freebase & 3.0\%            \\ 
\hline
Total           & 24.5\%          \\
\hline
\multicolumn{2}{|l|}{\textbf{Incorrect: system errors}} \\ 
\hline
POS or NER errors    & 10.0\%          \\
Lexicon coverage     & 12.5\%          \\
Grammar coverage     & 11.0\%          \\
Features             & 6.5\%          \\
Other                & 4.5\%          \\
\hline
Total & 44.5\%          \\
%\hline
%In Oracle                  & 12.5\%          \\
%Not in Oracle              & 56.5\%          \\
\hline
\end{tabular}}
\hfill{}
\caption{Breakdown of errors on 200 randomly sampled examples from the WebQuestions development set.}
\label{tb:errorAnalysisTable}
\end{table}
