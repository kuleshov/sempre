// For node.js
require('./sfig/internal/sfig.js');
require('./sfig/internal/RootedTree.js');
require('./sfig/internal/Graph.js');
require('./sfig/internal/Outline.js');
require('./sfig/internal/metapost.js');
require('./sfig/internal/seedrandom.js');
require('./utils.js');

// Title slide
add(slide(nil(),
  ytable('Semantic Parsing via Paraphrasing').center().scale(1.3).strokeColor('darkblue'),
  ytable(
    parentCenter(image('images/stanford-nlp-logo.jpeg').width(100)),
  _).center().ymargin(20),
  ytable(
    'ACL',
    'June 25, 2014',
  _).center(),
  ytable(
    'Jonathan Berant and Percy Liang',
  _).center().scale(0.8),
_).body.center().end.showHelp(false).showIndex(false));

add(slide('Semantic parsing',
  parentCenter(systemSpec({pause: true})),
  pause(),
  nil(),
  stmt('Motivation','Natural language interface to large structured knowledge-bases (Freebase, DBPedia, Yelp, ...)'),
_));


add(slide('Learning from questions and answers',
  stmt('Supervision', 'manually annotated logical forms'),
  indent(table(
    [cnl('What\'s California\'s capital?'), nowrapText('$\\red{\\wl{Capital.California}}$').scale(0.8)],
    //[cnl('How long is the Mississippi river?'), nowrapText('$\\red{\\wl{RiverLength.Mississippi}}$').scale(0.8)],
    ['...', '...'],
  _).margin(30, 5).ycenter()).scale(0.75),
  text('Zelle and Mooney 1996, Zettlemoyer and Collins 2005,...').scale(0.6).bulleted(true),
  pause(),
  text(['Limitations:',
        'Requires experts &mdash; slow, expensive, does not scale!',
        'Restricted to limited domains']).bulleted(true),
  pause(),
  yspace(10),
  stmt('Supervision', 'question/answers pairs'),
  indent(table(
    [cnl('What\'s California\'s capital?'), cwl('Sacramento')],
    //[cnl('How long is the Mississippi river?'), cwl('3,734km')],
    ['...', '...'],
  _).margin(30, 5).center()).scale(0.75),
  text('Clarke et al. 2010, Liang et al. 2011').scale(0.6).bulleted(true),
  stmt('Advantage', 'obtain from non-experts!'),
_));

add(slide('Scaling to large domains',
  text([sc('GeoQuery:') + ' (Zelle and Mooney, 1996)',
    '40 binary predicates',
    '279 word types',
    _]).bulleted(true),
  parentCenter(ytable(cnl('What is the most populous city in California?'),
      cnl('How many states border Oregon?')).center().scale(0.70)),
  //parentCenter(ytable(cnl('How many states border Oregon?'),
  //    czl('count(Type.State $\\sqcap$ Border.Oregon)'),
  //    _).center().scale(0.70)),
  pause(),
  text([sc('WebQuestions:') + ' (Berant et al., 2013)',
    '~15,000 predicates',
    '4,500 word types',
    _]).bulleted(true),
  parentCenter(ytable(cnl('What character did Natalie Portman play in Star Wars?'),
      cnl('What killed Sammy Davis Jr?'),
      cnl('What kind of money to take to Bahamas?'),
   //   cnl('What did Obama study in school?'),
      _).scale(0.7)),
  pause(),
_));

add(slide('Challenge: language variability',
  parentCenter(xtable(cnl('What is the currency in the US?'),czl('CurrencyOf.UnitedStates')).margin(20)),
  yspace(10),
  parentCenter(ytable(
      pause(),
      cnl('What money do they use in the states?'),
      pause(),
      cnl('How do you pay in America?'),
      pause(),
      cnl('What\'s the currency of the US?'),
      pause(),
      cnl('What money is accepted in the United states?'),
      pause(),
      cnl('What money to take to the US?'),
      '$\\dots$',
      '$\\dots$','$\\dots$').center().margin(3)),
  pause(),
  'Need to handle text-KB mismatch in large scale'.bold().fontcolor('darkblue'),
_));
//add(slide('Lexical variability vs. compositionality',
//  text([sc('WebQuestions:'),
//    '~15,000 predicates',
//    '4,500 word types',
//    'Limited compositionality',
//    _]).bulleted(true),
//  parentCenter(ytable(cnl('What character did Natalie Portman play in Star Wars?'),
//      cnl('What killed Sammy Davis Jr?'),
//      cnl('What kind of money to take to Bahamas?'),
//      cnl('What did Obama study in school?'),
//      _).scale(0.7)),
//  pause(),
//  stmt('Challenge','Mismatch between language and the KB'),
//_))
//
add(slide('Text-KB mismatch',
  'Alignment',
  parentCenter(textToKbProblem()),
  yspace(10),
  parentCenter(ytable(cnl('happen') + '$\\Rightarrow$' + czl('HolidayDate'),
    cnl('occur') + '$\\Rightarrow$' + czl('HolidayDate'),
    _).scale(0.7)),
  //parentCenter(cnl('happen') + '$\\Rightarrow$' + czl('HolidayDate')).scale(0.7),
  //parentCenter(cnl('occur') + '$\\Rightarrow$' + czl('HolidayDate')).scale(0.7),
  pause(),
  stmt('Problems','(a) KB specific (b) coverage'),
  pause(),
  parentCenter(paraphraseMotivation().scale(0.7)),
  stmt('Observation','There is much more general text than alignable text'),
  stmt('Idea','Utilize large amounts of general KB-independent text'),
  //parentCenter(cnl('occur') + '$\\Leftrightarrow$'+ cnl('happen')  + ' (3)'),
  //'Only 2\% of relation phrases on Reverb can be aligned to Freebase',
  //parentCenter(overlay(
  //labeledColoredEllipse('aligned text','brown',80,20),
  //coloredEllipse('brown',350,60),
  //text('text').shift(-20,+70),
  //_)),
_).rightHeader('Cai and Yates 2013, Berant et al. 2013, Yao and Van Durme 2014'));

add(slide('Contribution: reduce to paraphrasing',
  parentCenter(parsingSchema({pause: true, text: true})).scale(0.65),
_));

add(slide('Contribution: reduce to paraphrasing',
  parentCenter(paraphraseSchema({pause: true, text: true})).scale(0.65),
  yspace(5),
  pause(-4),
  text('Simple model suggests candidate logical forms').bulleted(true).scale(0.8),
  pause(),
  text('Simple model generates canonical utterances').bulleted(true).scale(0.8),
  pause(),
  text('Ranking of canonical utterances').bulleted(true).scale(0.8),
  pause(2),
  yspace(10),
  text(stmt('Advantage','Use a lot of text and paraphrase methods'.bold())).fontSize(23),
_));

var outline = new sfig.Outline('Semantic parsing');
addOutlineSlide = function(title) {
  add(outline.createSlide(title).rightHeader(image('images/signpost.jpeg').dim(200)));
}
addOutlineSlide('Task setup');


add(slide('Setup',
  headerList('Input',
  'Knowledge-base $\\sK$',
  'Training set of question-answer pairs $\\{(x_i,y_i)\\}_1^n$'),
  indent(xtable(cnl('What are the main cities in California?'),'$\\cwl{SF, LA, ...}$').margin(50)),
  pause(),
  headerList('Output',
    'Semantic parser that maps questions $x$ to answers $y$ through logical forms $z$'),
  table([cnl('countries in Asia'),'$\\Rightarrow$','$\\czl{Type.Country} \\red{\\sqcap} \\czl{ContainedBy.Asia}$'],
        ['','$\\Rightarrow$',cwl('China, Japan, India, ...')]).margin(10),
_));

add(slide('Freebase knowledge graph',
  parentCenter(obamaKnowledgeGraph({pause: false})),
_));

add(slide('Freebase knowledge graph',
  parentCenter(obamaKnowledgeGraph({pause: false})).scale(0.7),
  parentCenter(ytable('40M nodes','600M edges','20k edge labels')),
_));

add(slide('Logical forms are graph templates',

  parentCenter(stagger3(
    nowrapText('$\\green{\\wl{Type.Person}} \\sqcap \\wl{PlacesLived.Location.Chicago}$'),
    nowrapText('$\\wl{Type.Person} \\sqcap \\green{\\wl{PlacesLived.Location.Chicago}}$'),
    nowrapText('$\\wl{Type.Person} \\green{\\sqcap} \\wl{PlacesLived.Location.Chicago}$'),
    _)),
  pause(-2),
  parentCenter(xtable(
    stagger3(
    frameBox(queryKnowledgeGraph1({highlightEdges: true}).scale(0.8)).padding(10).bg.round(10).end,
    frameBox(queryKnowledgeGraph2({highlightEdges: true}).scale(0.8)).padding(10).bg.round(10).end,
    frameBox(queryKnowledgeGraph3({highlightEdges: true}).scale(0.8)).padding(10).bg.round(10).end,
    _),
    pause(),
    stagger(
      obamaKnowledgeGraph({}).scale(0.6),
      obamaKnowledgeGraph({highlightEdges: true, highlightNodes: ['BarackObama', 'MichelleObama']}).scale(0.6),
    _),
  _).margin(50)),
_));

addOutlineSlide('Generating logical forms');

//add(slide('Overview and Notation',
//  parentCenter(paraphraseSchema({pause: true,text: false})).scale(0.65),
//  yspace(5),
//  pause(-4),
//  '$x$: input question',
//  pause(),
//  '$Z_x$: candidate logical forms',
//  pause(),
//  '$C_z$: generated canonical utterances',
//  pause(2),
//  '$y$: answer',
//_));

add(slide('Generating logical forms',
  parentCenter(paraphraseSchema1({pause: true,text: false})).scale(0.65),
  yspace(5),
  '$x$: input question',
  '$Z_x$: candidate logical forms',
  'generated canonical utterances'.fontcolor('white'),
  'answer'.fontcolor('white'),
_));

add(slide('Candidate logical forms ($Z_x$)',
  stmt('Grow logical forms around entities'),
  parentCenter(candidateLogicalForms()),
  pause(2),
_));

add(slide('Candidate logical forms ($Z_x$)',
  frame(table(
    ['Template','Example','Question'],
    ['$p.e$',czl('Directed.TopGun'),cnl('who directed Top Gun')],
    ['$p_1.p_2.e$',czl('Employment.EmployerOf.SteveBalmer'),cnl('Where does Steve Balmer work?')],
    ['$p.(p_1.e_1 \\sqcap p_2.e_2)$',czl('Character.')+czl('(Actor.BradPitt')+'$\\red{\\sqcap}$'+czl('Film.Troy)'),cnl('Who did Brad Pitt play in Troy?')],
    ['$\\wl{Type}.t \\sqcap z$',czl('Type.Composer') +'$\\red{\\sqcap}$'+czl('SpeakerOf.French'),cnl('What composers spoke French?')],
    ['$\\text{count}(z)$','$\\red{\\text{count}}$'+czl('(BoatDesigner.NatHerreshoff)'),cnl('How many ships were designed')],
    ['','',cnl('by Nat Herreshoff?')],
  _).margin(5,10).scale(0.6)).bg.strokeWidth(1).end,
  '645 logical forms on average per example',
_));

add(slide('Oracle results on WebQuestions',
  parentCenter(barGraph([[1, 47.9], [2, 63]])
    .xtickLabels([nil(), text('Beam parsing').scale(0.6),text('Paraphrasing').scale(0.6)])
    .trajectoryColors(['gray', 'red'])
    .xrange(0, 3).xtickIncludeAxis(false).xtickIncrValue(1).yrange(0, 70).xlength(500).barWidth(50).yroundPlaces(0).ytickIncrValue(10).scale(1.3)),
  'Improve oracle accuracy on WebQuestions'.bold().fontcolor('darkblue'),
_));

addOutlineSlide('Generating canonical utterances');

add(slide('Generate canonical utterances',
  parentCenter(paraphraseSchema2({pause: true,text: false})).scale(0.65),
  yspace(5),
  '$x$: input question',
  '$Z_x$: candidate logical forms',
  '$C_z$: generated canonical utterances',
  'answer'.fontcolor('white'),
_));

add(slide('Generating canonical utterances ($C_z$)',
  parentCenter(generateUtterances()),
  pause(2),
  '1500 canonical utterances on average per example',
_));

addOutlineSlide('Paraphrasing');

add(slide('Paraphrasing',
  parentCenter(paraphraseSchema3({pause: true,text: false})).scale(0.65),
  yspace(5),
  '$x$: input question',
  '$Z_x$: candidate logical forms',
  '$C_z$: generated canonical utterances',
  'answer'.fontcolor('white'),
_));

add(slide('Reranking canonical utterances',
  stmt('Model', 'distribution over logical forms and canonical utterances'),
  parentCenter('$p_{\\theta}(c,z \\mid x) = \\frac{\\exp(\\phi(x,c,z)^\\top \\theta)}{\\sum_{z\' \\in Z_x, c\' \\in C_z \\exp(\\phi(x,z\',c\')^\\top \\theta)}}$'),
  pause(),
  stmt('Decomposition to paraphrase model and logical form model'),
  parentCenter('$\\phi(x,c,z) = \\phi_\\text{pr}(x,c) + \\phi_\\text{lf}(z)$'),
  pause(),
  'Need to estimate parameters $\\theta_\\text{pr}$ and $\\theta_\\text{lf}$',
_)),

add(slide('Learning model parameters',
  stmt('Training data','$\\{(x_i,y_i)\\}_{i=1}^n$'),
  pause(),
  stmt('Objective function'),
  parentCenter('$p_\\theta(y \\mid x) = \\sum\\limits_{z \\in Z_x: y = \\den{z}} \\sum\\limits_{c \\in C_z} p_\\theta(c,z \\mid x)$'),
  parentCenter('$O(\\theta) = \\sum\\limits_{i=1}^n \\log p_\\theta(y_i \\mid x_i) - \\lambda \\|\\theta\\|_1$'),
  stmt('Training','Online learning with SGD and AdaGrad'),
_)),


add(slide('Paraphrase model',
  'At this point our problem involves text only:',
  parentCenter(cnl('What countries in the world speak Arabic?')),
  parentCenter(purple(nl('What country is Arabic language spoken in?'))),
  pause(),
  'Simple'.bold() + ' paraphrase model utilizing ' + 'a lot of text'.bold(),
  text('Association model - ' + sc('Paralex')).bulleted(true),
  text('Vector space model - ' + sc('Wikipedia')).bulleted(true),
  parentCenter('$\\phi_\\text{pr}(x,c) = \\phi_\\text{as}(x,c) + \\phi_\\text{vs}(x,c)$'),
_));

add(slide('Association model',
  parentCenter(paraphraseSchema4({pause: true,text: false})).scale(0.65),
  yspace(5),
  '$x$: input question',
  '$Z_x$: candidate logical forms',
  '$C_z$: generated canonical utterances',
  'answer'.fontcolor('white'),
_));

add(slide('Extracting candidate association',
  text([sc('Paralex') + ' dataset (Fader et al., 2013)','18M word aligned question pairs','Generated through links in ' + sc('WikiAnswers')]).bulleted(true),
  parentCenter(table([cnl('Who wrote the Winnie the Pooh books?'),cnl('Who is poohs creator?')],
        [cnl('What relieves a hangover?'),cnl('What is the best cure for a hangover?')],
        [cnl('How do you say Santa Clause in Sweden?'),cnl('Say Santa Clause in Sweden?')],
    _).scale(0.7).margin(20)),
  pause(),
  'Consistent phrase pair heuristic (Och and Ney, 2004):',
  parentCenter(phrasetable([
      ['type of music','musical genre'],
      ['born in','birth place'],
      ])),
_));

add(slide('Association model',
  'Association: pair of spans $(x_{ij},c_{i\'j\'})$',
  parentCenter(phrasetable([
      ['type of music','musical genre'],
     ])),
  parentCenter(associationExample()),
  'Generate all associations and extract features:',
  parentCenter(table(
    ['identical lemma', '3'],
    ['type of music $\\land$ musical genre','1'],
    ['play $\\land$ the', '1'],
    ['WN derivation','1'],
    ['delete IN', '1'],
    ['delete of', '1'],
    ['...', '...'],
  _).xmargin(20)).scale(0.75),
_));


add(slide('Association model',
  parentCenter(paraphraseSchema5({pause: true,text: false})).scale(0.65),
  yspace(5),
  '$x$: input question',
  '$Z_x$: candidate logical forms',
  '$C_z$: generated canonical utterances',
  'answer'.fontcolor('white'),
_));

add(slide('Vector space model',
  stmt('Associations disadvantage','coverage'),
  pause(),
  stmt('Train word vectors $v(w)$ with ' + sc('word2vec')),
  '$C$: content words in utterance $x$',
  parentCenter('$v(x) = \\frac{1}{|C|}\\sum\\limits_{x_i \\in C} v(x_i)$'),
  pause(),
  'Learn a matrix $W$ to estimate ``similarity\'\' score',
  //parentCenter('$s(x,c) = v(x)^{\\top} W v(c) = \\sum v(x_i)^{\\top} W v(c_j)$'),
  parentCenter('$s(x,c) = v(x)^{\\top} W v(c)$'),
  'Soft similarity between all word pairs'.bold(),
_));

//add(slide('Association vs. vector space',
//  cnl('$x:$ What type of music did Richard Wagner play?'),
//  purple(nl('$\\text{as}:$ What is the musical genres of Richard Wagner?')),
//  purple(nl('$\\text{vs}:$ What composition has Richard Wagner as lyricist?')),
//  yspace(10),
//  pause(),
//  cnl('$x:$ Where is made Kia car?'),
//  purple(nl('$\\text{as}:$ What place is founded by Kia motors?')),
//  purple(nl('$\\text{vs}:$ What city is Kia motors a headquarters of?')),
//_));

add(slide('Recap',
  parentCenter(paraphraseSchema({pause: true,text: false})).scale(0.65),
  yspace(5),
  pause(-4),
  '$x$: input question',
  pause(),
  '$Z_x$: candidate logical forms',
  pause(),
  '$C_z$: generated canonical utterances',
  pause(2),
  '$y$: answer',
_));

addOutlineSlide('Experiments');

G.questions = function(questions, opts) {
  var items = questions.map(function(x) {
    if (x == _) return _;
    if (typeof(x) == 'string') return nowrapText(green(x.italics()));
    return nowrapText(green(x[0].italics()) + ' $\\Rightarrow$ ' + blue(x[1]));
  });
  return ytable.apply(null, items).scale(0.75).margin(opts.margin || 0);
}

add(slide('WebQuestions dataset',
  questions([
    ['What character did Natalie Portman play in Star Wars?', 'Padm&eacute; Amidala'],
    ['What kind of money to take to Bahamas?', 'Bahamian dollar'],
    ['What currency do you use in Costa Rica?', 'Costa Rican col&oacute;n'],
    ['What did Obama study in school?', 'political science'],
    ['What do Michelle Obama do for a living?', 'writer, lawyer'],
    ['What killed Sammy Davis Jr?', 'throat cancer'],
  _], {margin: 50}),
yspace(10),
'5,810 questions crawled from Google Suggest and answered using AMT',
_).rightHeader('[Berant et al. 2013]'));


add(slide('Free917 dataset',
  cnl('What is the engine in a 2010 Ferrari California?'),
  cnl('What was the cover price of the X-men Issue 1?'),
  cnl('At what institutions was Marshall Hall a professor?'),
  cnl('What fuel does an internal combustion engine use?'),
  cnl('How many companies are traded by the NYSE?'),
  yspace(10),
'917 manually authored questions',
_));

add(slide('Results on WebQuestions',
  parentCenter(barGraph([[1, 35.4], [2, 35.7], [3,37.5], [4, 39.9]])
    .xtickLabels([nil(), text('[Yao and Van Durme, 2014]').scale(0.55), text('[Berant et al., 2013]').scale(0.55),text('[Bao et al., 2014]').scale(0.55),text('Paraphrasing').scale(0.55)])
    .trajectoryColors(colors = ['blue', 'gray', 'purple', 'red'])
    .yvalueFunc(function(i, x, y) { return text((''+y).fontcolor(colors[x-1])).scale(0.8); })
    .xrange(0, 5).xtickIncludeAxis(false).xtickIncrValue(1).yrange(0, 50).xlength(500).barWidth(50).yroundPlaces(0).ytickIncrValue(10).scale(1.3)),
  '4.2 point improvement in accuracy'.bold().fontcolor('darkblue'),
_));

add(slide('Results on Free917',
  parentCenter(barGraph([[1, 63], [2, 68], [3,68.5]])
    .xtickLabels([nil(), text('[Berant et al., 2013]').scale(0.6), text('[Kwiatkowski et al., 2013]').scale(0.6),text('Paraphrasing').scale(0.6)])
    .trajectoryColors(colors = ['gray', 'purple', 'red'])
    .yvalueFunc(function(i, x, y) { return text((''+y).fontcolor(colors[x-1])).scale(0.8); })
    .xrange(0, 4).xtickIncludeAxis(false).xtickIncrValue(1).yrange(0, 100).xlength(500).barWidth(50).yroundPlaces(0).ytickIncrValue(20).scale(1.3)),
  'Paraphrasing matches state-of-the-art'.bold().fontcolor('darkblue'),
_));


//add(slide('Ablations',
//  'Results on development set',
//  parentCenter(table(
//    ['',sc('Free917'),sc('WebQuestions')],
//    ['Our system','73.9'.bold(),'41.2'.bold()],
//    [sc('-VSM'),'71.0','40.5'],
//    [sc('-Assoc.'),'52.7','35.3'],
//    [sc('-Para.'),'31.8','21.3'],
////    [sc('SimpGen'),'73.4','40.4'],
//  _).center().margin(20)),
//  'Association model performs better than vector space model'.bold().fontcolor('darkblue'),
//_));

add(slide('Baselines',
  'Results on development set',
  parentCenter(table(
    ['',sc('Free917'),sc('WebQuestions')],
    ['Our system','73.9'.bold(),'41.2'.bold()],
    [sc('Jaccard'),'69.7','31.3'],
    //[sc('Edit'),'40.8','24.8'],
    [sc('WDDC06'),'71.0','29.8'],
  _).center().margin(20)),
  'Text-KB mismatch is larger on WebQuestions'.bold().fontcolor('darkblue'),
_));

function labelArrow(a, b, label, color) {
  var c = arrow(a, b).color(color);
  return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
}
add(slide('Related work',
  parentCenter(
    overlay(
    ytable(
        a = nowrapText(cnl('How many people live in Seattle?')),
        xtable(b1 = nowrapText(cnl('What is the population of Seattle?')),
          b2 = nowrapText(czl('count(Type.Person $\\sqcap$ LiveIn.Seattle)'))).margin(25),
        c = nowrapText(czl('PopulationOf(Seattle)')),
  _).center().margin(100).scale(0.75),
    labelArrow(a,b1,'text','blue'),
    arrow(a,b2),
    arrow(b1,c),
    labelArrow(b2,c,'logical form','blue'))),
  stmt('Kwiatkowski et al. 2013', 'intermediate representation'),
  pause(),
  stmt('Text-KB mismatch','Yao and Van Durme 2014, Bao et al., 2014, Fader et al., 2014'),
_));

add(slide('Summary',
  text('Semantic parsing for factoid questions through language generation and paraphrasing'),
  pause(),
  stmt('Advantage','Exploiting raw text not related to the KB'),
  pause(),
  text(['Future work','Large scale KB and high compositionality']).bulleted(true),
_));


add(slide(null,
  yspace(50),
  parentCenter(xtable(
    parentCenter(obamaKnowledgeGraph({}).scale(0.4)),
  _).margin(100).center()),
  stmt('Data and code'),
  parentCenter(blue('http://www-nlp.stanford.edu/software/sempre/')),
  pause(),
  yspace(30),
  parentCenter('Thank you!'.bold()),
_));
// For node.js: output PDF (you must have Metapost installed).
prez.writePdf({outPrefix: 'index'});
