G = sfig.serverSide ? global : this;

sfig.importAllMethods(G);

sfig.latexMacro('bx', 0, '\\mathbf{x}');
sfig.latexMacro('bs', 0, '\\mathbf{s}');
sfig.latexMacro('bt', 0, '\\mathbf{t}');
sfig.latexMacro('wl', 1, '\\text{#1}');
sfig.latexMacro('cwl', 1, '\\blue{\\wl{#1}}');
sfig.latexMacro('zl', 1, '\\text{#1}');
sfig.latexMacro('czl', 1, '\\red{\\zl{#1}}');
sfig.latexMacro('R', 0, '\\mathbb{R}');
sfig.latexMacro('P', 0, '\\mathbb{P}');
sfig.latexMacro('E', 0, '\\mathbb{E}');
sfig.latexMacro('sF', 0, '\\mathcal{F}');
sfig.latexMacro('sS', 0, '\\mathcal{S}');
sfig.latexMacro('sZ', 0, '\\mathcal{Z}');
sfig.latexMacro('sD', 0, '\\mathcal{D}');
sfig.latexMacro('sK', 0, '\\mathcal{K}');
sfig.latexMacro('diag', 0, '\\text{diag}');
sfig.latexMacro('deriv', 0, '\\quad\\Rightarrow\\quad');
sfig.latexMacro('ts', 1, '\\text{#1}');
sfig.latexMacro('phix', 0, 't');
sfig.latexMacro('reverse', 1, '\\mathbf{R}[#1]');
sfig.latexMacro('den', 1, '\[ #1 \]_{\\sK}');

sfig.Text.defaults.setProperty('font', 'Arial');

sfig.initialize();

G.prez = sfig.presentation();
G.add = function(slide) { prez.addSlide(slide); }

// Colors
G.nlcolor = 'green';
G.zlcolor = 'red';
G.wlcolor = 'blue';
G.nl = function(x) { return x.italics().fontcolor(nlcolor); }
G.zl = function(x) { return ('<tt>' + x + '</tt>').fontcolor(zlcolor); }
G.wl = function(x) { return ('<tt>' + x + '</tt>').fontcolor(wlcolor); }
//function yl(x) { return x.italics().fontcolor(zlcolor); }
G.sc= function(x) { return ('<tt>' + x + '</tt>'); }

G.frameBox = function(a) { return frame(a).bg.strokeWidth(1).fillColor('white').end; }
G.bigFrameBox = function(a) { return frameBox(a).padding(10).scale(1.4); }

G.bigLeftArrow = function(s) { return leftArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigRightArrow = function(s) { return rightArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigUpArrow = function(s) { return upArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigDownArrow = function(s) { return downArrow(s || 100).strokeWidth(10).color('brown'); }

G.red = function(x) { return x.fontcolor('red'); }
G.green = function(x) { return x.fontcolor('green'); }
G.blue = function(x) { return x.fontcolor('blue'); }
G.darkblue = function(x) { return x.fontcolor('darkblue'); }
G.brown = function(x) { return x.fontcolor('brown'); }
G.purple = function(x) { return x.fontcolor('purple'); }

G.wholeNumbers = function(n) {
  var result = [];
  for (var i = 0; i < n; i++) result[i] = i;
  return result;
}

G.randomChoice = function(list) {
  var i = Math.floor(Math.random() * list.length);
  return list[i];
}


////////////////////////////////////////////////////////////

G.stmt = function(prefix, suffix) {
  var m;
  if (!suffix && (m = prefix.match(/^([^:]+): (.+)$/))) {
    prefix = m[1];
    suffix = m[2];
  }
  return prefix.fontcolor('darkblue') + ':' + (suffix ? ' '+suffix : '');
}

G.xseq = function() { return new sfig.Table([arguments]).center().margin(5); }
G.yseq = function() { return ytable.apply(null, arguments).margin(10); }

G.headerList = function(title) {
  var contents = Array.prototype.slice.call(arguments).slice(1);
  return yseq.apply(null, [title ? stmt(title) : _].concat(contents.map(function(line) {
    if (line == _) return _;
    if (typeof(line) == 'string') return bulletedText([null, line]);
    if (line instanceof sfig.PropertyChanger) return line;
    return indent(line);
  })));
}

G.node = function(x, shaded) { return overlay(circle(20).fillColor(shaded ? 'lightgray' : 'white'), std(x).orphan(true)).center(); }
G.labeledColoredEllipse = function(label,color,x,y) { return overlay(ellipse(x,y).strokeWidth(2).strokeColor(color), std(label).orphan(true)).center(); }
G.coloredEllipse = function(color,x,y) { return overlay(ellipse(x,y).strokeWidth(2).strokeColor(color)); }

G.agenda = function(x,y,text) { return overlay(rect(x,y),std(text).orphan(true)).center(); }

G.derivItemLex = function(semfunc,input) {return semfunc + '('+cnl(input)+')';}
G.derivItemFormula = function(semfunc,input) {return semfunc + '('+cwl(input)+')';}

G.zspan = function(i, j) { return text('$s_{'+i+j+'}$'); }
G.xleaf = function(i) { return text('$x_{'+i+'}$'); }

G.paramMatrix = function(x) { return text(x).opacity(1).scale(0.6); }
G.paramPi = function() { return paramMatrix('$\\green{\\pi}$'); }
G.paramT = function() { return paramMatrix('$\\blue{T}$'); }
G.paramB = function() { return paramMatrix('$\\blue{B}$'); }
G.paramO = function() { return paramMatrix('$\\red{O}$'); }

var Tmargin = 40;
G.T = function() {
  return sfig.rootedTree.apply(null, arguments).margin(Tmargin, Tmargin).nodeRound(10).verticalCenterEdges(true);
}
G.B = function(edgeLabel, child) {
  if (edgeLabel != null) edgeLabel = frame(edgeLabel).padding(2).bg.fillColor('white').end;
  return sfig.rootedTreeBranch(edgeLabel, child);
}
G.Lf = function(x) {
  var t = sfig.rootedTree(x).nodeRound(10);
  t.headBox.content.bg.fillColor('lightgray');
  return t;
}

G.sym = function(label) { return zl(label); }
G.obs = function(label) { return Lf(nl(label)); }

G.syntax = function() {
  return T(sym('S'),
    T(sym('N'), obs('I')),
    T(sym('VP'), T(sym('V'), obs('like')), T(sym('N'), obs('algorithms'))),
  _);
}

G.syntaxAlt = function() {
  return T(sym('S'),
    T(sym('NV'), T(sym('N'), obs('I')), T(sym('V'), obs('like'))),
    T(sym('N'), obs('algorithms')),
  _);
}

G.semantics = function() {
  return text('$\\text{like}(\\text{percy}, \\text{algorithms})$').color(wlcolor);
}

G.indent = function(x, n) { return frame(x).xpadding(n != null ? n : 20).xpivot(1); }
G.header = function(x) { return text(x.fontcolor('darkblue')); }
G.change = function(a, b) { return overlay(a, pause(), b.replace(a)); }

G.nlcolor = 'green';  // Natural language
G.zlcolor = 'red';  // Logical form
G.wlcolor = 'blue';  // World
G.nl = function(x) { return x.italics(); }
G.cnl = function(x) { return nl(x).fontcolor(nlcolor); }
G.zl = function(x) { return '$\\zl{' + x + '}$'; }
G.wl = function(x) { return '$\\wl{' + x + '}$'; }
G.czl = function(x) { return '$\\czl{' + x + '}$'; }
G.cwl = function(x) { return '$\\cwl{' + x + '}$'; }


G.stagger = function(b1, b2) { b1 = std(b1); b2 = std(b2); return overlay(b1.numLevels(1), pause(), b2); }
G.stagger3 = function(b1, b2, b3) { b1 = std(b1); b2 = std(b2); b3 = std(b3); return overlay(b1.numLevels(1), pause(), b2.numLevels(1), pause(),b3); }
G.stagger4 = function(b1, b2, b3, b4) { b1 = std(b1); b2 = std(b2); b3 = std(b3); b4=std(b4); return overlay(b1.numLevels(1), pause(), b2.numLevels(1), pause(),b3.numLevels(1),
    pause(),b4); }

G.dividerSlide = function(text) {
  return slide(null, nil(), parentCenter(text)).titleHeight(0);
}

G.moveLeftOf = function(a, b, offset) { return transform(a).pivot(1, 0).shift(b.left().sub(offset == null ? 5 : offset), b.ymiddle()); }
G.moveRightOf = function(a, b, offset) { return transform(a).pivot(-1, 0).shift(b.right().add(offset == null ? 5 : offset), b.ymiddle()); }
G.moveTopOf = function(a, b, offset) { return transform(a).pivot(0, 1).shift(b.xmiddle(), b.top().sub(offset == null ? 5 : offset)); }
G.moveBottomOf = function(a, b, offset) { return transform(a).pivot(0, -1).shift(b.xmiddle(), b.bottom().add(offset == null ? 5 : offset)); }
G.moveCenterOf = function(a, b) { return transform(a).pivot(0, 0).shift(b.xmiddle(), b.ymiddle()); }

// An example of parsing a sentence.
G.parseExample = function(opts) {
  var myPause = opts.pause ? pause : function() { return _; };
  T = rootedTree;

  var intersection, join, t1, t2, t3;
  function labelVerticalEdge(label, t) { return moveRightOf(label, t.branches[0].edge); }
  return overlay(
    T(
      intersection = nowrapText(czl('Type.City') + '$\\,\\sqcap\\,$' + czl('PeopleBornHere.BarackObama')),
      cnl('what'),
      t1 = T(czl('Type.CityTown'), cnl('city')),
      cnl('was'),
      T(join = text(czl('PeopleBornHere.BarackObama')),
        t2 = T(czl('BarackObama'), cnl('Obama')),
        t3 = T(czl('PeopleBornHere'), cnl('born')),
      _),
      cnl('?'),
    _).recnodeBorderWidth(0).recymargin(90),
    myPause(),
    labelVerticalEdge(('lexicon'.bold()), t1),
    labelVerticalEdge(('lexicon'.bold()), t2),
    labelVerticalEdge(('lexicon'.bold()), t3),
    myPause(),
    moveBottomOf(('join'.bold()), join),
    myPause(),
    moveBottomOf(('intersect'.bold()), intersection),
  _);
}

G.parseExampleWrong = function(opts) {
  var myPause = opts.pause ? pause : function() { return _; };
  T = rootedTree;

  var intersection, join, t1, t2, t3;
  function labelVerticalEdge(label, t) { return moveRightOf(label, t.branches[0].edge); }
  return overlay(
    T(
      join = nowrapText(czl('Length.Song325')),
      t1 = T(czl('Song325'), cnl('where')),
      t2 = T(czl('Length'), cnl('was')),
      cnl('Obama'),
      cnl('born'),
      cnl('?'),
    _).recnodeBorderWidth(0).recymargin(90),
    labelVerticalEdge(('lexicon'.bold()), t1),
    labelVerticalEdge(('lexicon'.bold()), t2),
    moveBottomOf(('join'.bold()), join),
  _);
}

G.drawKnowledgeBaseGraph = function(entries, opts) {
  var nodes = {}; // Map node name => object
  var nodePositions = {};  // Map node name => position
  var out = [];
  var getNode = function(name, dx, dy) {
    var posScale = 140;
    var o = nodes[name];
    if (!o) {
      o = nodes[name] = center(name.bold().fontcolor('blue')).scale(0.8);
      if (opts.highlightNodes && opts.highlightNodes.indexOf(name) != -1)
        o = frameBox(o).bg.strokeWidth(2).round(5).end;
      o.shift(dx*posScale, down(dy*posScale));
      nodePositions[name] = [dx, dy];
      out.push(o);
    }
    return o;
  }
  entries.forEach(function(info) {
    var e1 = info[0];
    var property = info[1][0];
    var dx = info[1][1];
    var dy = info[1][2];
    var highlight = info[1][3];
    var e2 = info[2];

    if (opts.pause)
      out.push(pause());

    var e1obj = getNode(e1, 0, 0);
    var e2obj = getNode(e2, nodePositions[e1][0]+dx, nodePositions[e1][1]+dy);
    var link = arrow(e1obj, e2obj);
    if (opts.highlightEdges && highlight) link.strokeWidth(6).strokeColor('green');
    else link.strokeWidth(2);
    out.push(link);
    out.push(opaquebg(center(property.fontcolor('brown'))).scale(0.6).shift(link.xmiddle(), link.ymiddle()));
  });
  return new Overlay(out);
}

G.queryKnowledgeGraph1 = function(opts) {
  var x = 'o';
  return drawKnowledgeBaseGraph([
    [x, ['Type', -0.5, 1,true], 'Person'],
    [x, ['PlacesLived', 0.5, 1], '?'],
    ['?', ['Location', 0, 1], 'Chicago'],
  ], opts);
}

G.queryKnowledgeGraph2 = function(opts) {
  var x = 'o';
  return drawKnowledgeBaseGraph([
    [x, ['Type', -0.5, 1], 'Person'],
    [x, ['PlacesLived', 0.5, 1,true], '?'],
    ['?', ['Location', 0, 1,true], 'Chicago'],
  ], opts);
}

G.queryKnowledgeGraph3 = function(opts) {
  var x = 'o';
  return drawKnowledgeBaseGraph([
    [x, ['Type', -0.5, 1,true], 'Person'],
    [x, ['PlacesLived', 0.5, 1,true], '?'],
    ['?', ['Location', 0, 1,true], 'Chicago'],
  ], opts);
}

G.obamaKnowledgeGraph = function(opts) {
  var ObamaMarriage = 'Event8';
  var BarackObamaLiveInChicago = 'Event3';
  var MichelleObamaLiveInChicago = 'Event21';

  return drawKnowledgeBaseGraph([
    ['BarackObama', ['Type', -2, +1, true], 'Person'],
    ['BarackObama', ['Profession', +1, +1], 'Politician'],

    ['BarackObama', ['DateOfBirth', -0.5, 1], '1961.08.04'],
    ['BarackObama', ['PlaceOfBirth', 2, 0], 'Honolulu'],
    ['Honolulu', ['ContainedBy', 0, -1], 'Hawaii'],
    ['Honolulu', ['Type', 0, 1], 'City'],
    ['Hawaii', ['ContainedBy', -1.5, +0.5], 'UnitedStates'],
    ['Hawaii', ['Type', 0, -0.7], 'USState'],

    ['BarackObama', ['Marriage', -1, -1], ObamaMarriage],
    [ObamaMarriage, ['Spouse', -1, -1], 'MichelleObama'],
    ['MichelleObama', ['Type', 1, 0, true], 'Person'],
    ['MichelleObama', ['Gender', 1.5, 0.3], 'Female'],
    [ObamaMarriage, ['StartDate', 1.5, -0.5], '1992.10.03'],

    ['BarackObama', ['PlacesLived', -1.5, +0.4, true], BarackObamaLiveInChicago],
    [BarackObamaLiveInChicago, ['Location', -1, -0.4, true], 'Chicago'],
    ['MichelleObama', ['PlacesLived', -0.5, +1, true], MichelleObamaLiveInChicago],
    [MichelleObamaLiveInChicago, ['Location', 0, 0, true], 'Chicago'],
    ['Chicago', ['ContainedBy', 0, 0], 'UnitedStates'],
  ], opts);
}

G.framework = function(opts) {
  var myPause = opts.pause ? pause : function() { return _; };
  return overlay(
    table(
      [nil(), x = node('$\\green{x}$', true)], myPause(),
      [theta = node('$\\red{\\theta}$'), z = node('$\\red{z}$')], myPause(),
      [w = node('$\\blue{w}$', true), y = node('$\\blue{y}$', true),],
    _).margin(100, 100),
    myPause(-2),
    moveRightOf(ytable(cnl('city in'), cnl('California?')), x),
    myPause(),
    arrow(x, z), arrow(theta, z),
    moveTopOf(red('parameters'), theta),
    moveRightOf(text('$\\red{\\wl{Type.City} \\sqcap \\wl{ContainedBy.CA}}$').scale(0.6), z),
    myPause(),
    arrow(w, y), arrow(z, y),
    moveTopOf(blue('database'), w),
    moveRightOf(blue('{SF,LA,...}'), y),
  _);
}

G.ex1 = {
  x: cnl('Who coached the Argentina national football team from 2011?'),
  z: '$\\red{ \\wl{Type.Person} \\sqcap \\wl{Coach}.(\\wl{Team.Argentina} \\sqcap \\wl{StartDate.2011})}$',
  y: cwl('Alejandro Sabella'),
};

G.gridText = function(nr, nc) {
  function textDocumentImage() {
    return image('images/simple-document.jpg').width(30);
  }
  var contents = wholeNumbers(nr).map(function() {
    return wholeNumbers(nc).map(function() {
      return Math.random() < 1 ? textDocumentImage() : nil();
    });
  });
  return table.apply(null, contents).margin(10);
}

G.challenge= function(opts) {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  var alignment = red('alignment');
  var bridging = 'bridging'.fontcolor('purple');
  var allPred = ytable(cwl('BarackObama'),cwl('TopGun'),cwl('MichelleObama'),cwl('Type.Country'),cwl('Type.city'),cwl('Profession.Lawyer'),cwl('PeopleBornHere'),cwl('InventorOf'),cwl('CapitalOf'),peopleKb = text(cwl('$\\vdots \\vdots$'))).center().scale(0.3);
  var example =
    ytable(
    xtable(image('images/database.png').dim(100),ytable(cwl('BarackObama'),cwl('TopGun'),cwl('MichelleObama'),cwl('Type.Country'),cwl('Type.city'),cwl('Profession.Lawyer'),cwl('PeopleBornHere'),cwl('InventorOf'),cwl('CapitalOf'),cwl('$\\vdots \\vdots$')).center().scale(0.3)).margin(20),
    yspace(30),
    table(
      [
        nil(),
        pause(2),
        ytable(cwl('Type.HumanLanguage'),cwl('SpeechLanguagePathology'),languageKb = text(cwl('LanguageAcquisition'))).center().scale(0.7),
        nil(),
        pause(-1),
        allPred,
        nil(),
        pause(1),
        nil(),//ytable(cwl('Brazil'),cwl('BrazilNut'),cwl('BrazilPresident'), brazilKb = text(cwl('BrazilIndiana'))).center().scale(0.7),
        pause(1),
        ytable(useKb = text(cwl('LanguagesSpoken'))).center().scale(0.7),
        _],
      pause(-3),
      [cnl('What'.bold()), languageText = text(cnl('languages'.bold())), cnl('do'.bold()), peopleText = text(cnl('people'.bold())), cnl('in'.bold()), brazilText = text(cnl('Brazil'.bold())), useText = text(cnl('use'.bold()))],
    _).margin(35).center(),
  _);
  //var a = conn(languageKb,languageText);
  return overlay(
      example,
      pause(1),
      conn(peopleText,peopleKb),
      pause(1),
      conn(languageText,languageKb),
 //     conn(brazilText,brazilKb),
      pause(1),
      conn(useText,useKb),
  _);
}
G.contribution= function(opts) {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  var alignment = red('alignment');
  var bridging = 'bridging'.fontcolor('purple');
  var example =
    ytable(
    xtable(image('images/database.png').dim(100),ytable(cwl('BarackObama'),cwl('TopGun'),cwl('Type.Country'),cwl('Profession.Lawyer'),cwl('PeopleBornHere'),cwl('InventorOf'),cwl('$\\vdots \\vdots$')).center().scale(0.3)).margin(20),
    table(
      pause(2),
      [nil(),nil(),nil(),ytable(cwl('LanguagesSpoken')).scale(0.7),nil(),nil(),nil()],
      [ pause(-1),
        nil(),
        ytable(cwl('Type.HumanLanguage'),languageKb = text(cwl('Type.ProgrammingLanguage'))).center().scale(0.7),
        nil(),
        nil(),
        nil(),
        ytable(cwl('Brazil'),brazilKb = text(cwl('BrazilFootballTeam'))).center().scale(0.7),
//        ytable(cwl('LanguagesSpoken'),cwl('CurrencyUsed'),useKb = text(cwl('Timezone'))).center().scale(0.7),
        nil(),
        _],
      [nil(),nil(),nil(),nil(),nil(),nil(),nil()],
      pause(-1),
      [cnl('What'.bold()), languageText = text(cnl('languages'.bold())), cnl('do'.bold()), peopleText = text(cnl('people'.bold())), cnl('in'.bold()), brazilText = text(cnl('Brazil'.bold())), useText = text(cnl('use'.bold()))],
    _).margin(35).center().scale(0.9),
    yspace(5),
  _);
  //var a = conn(languageKb,languageText);
  return overlay(
      example,
      pause(1),
      labelConn(languageText,languageKb,alignment.bold(),'red'),
      labelConn(brazilText,brazilKb,alignment.bold(),'red'),
   //   labelConn(useText,useKb,alignment.bold(),'red'),
      pause(1),
      labelConn(brazilKb,languageKb,label = bridging.bold(),'purple'),
  _);
}

G.alignment=function(opts) {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  var alignment = red('alignment');
  var example =
    ytable(
    xtable(image('images/database.png').dim(100),ytable(cwl('BarackObama'),cwl('TopGun'),cwl('Type.Country'),cwl('Profession.Lawyer'),cwl('PeopleBornHere'),cwl('InventorOf'),cwl('$\\vdots \\vdots$')).center().scale(0.6)).margin(20),
    yspace(100),
    table(
      [
        nil(),
        ytable(cwl('Type.HumanLanguage'),languageKb = text(cwl('Type.ProgrammingLanguage'))).center().scale(0.7),
        nil(),
        nil(),
        nil(),
        ytable(cwl('Brazil'),brazilKb = text(cwl('BrazilFootballTeam'))).center().scale(0.7),
//        ytable(cwl('LanguagesSpoken'),cwl('CurrencyUsed'),useKb = text(cwl('Timezone'))).center().scale(0.7),
        nil(),
        _],
      [nil(),nil(),nil(),nil(),nil(),nil(),nil()],
      [cnl('What'.bold()), languageText = text(cnl('languages'.bold())), cnl('do'.bold()), peopleText = text(cnl('people'.bold())), cnl('in'.bold()), brazilText = text(cnl('Brazil'.bold())), useText = text(cnl('use'.bold()))],
    _).margin(35).center().scale(0.9),
    yspace(5),
  _);
  //var a = conn(languageKb,languageText);
  return overlay(
      example,
      labelConn(languageText,languageKb,alignment.bold(),'red'),
      labelConn(brazilText,brazilKb,alignment.bold(),'red'),
  _);
}



G.bridging1= function(opts) {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  alignment = red('lexicon');
  var bridging = 'bridging'.fontcolor('purple');
  var example = ytable(
    yspace(50),
    table(
      [nil(), unary = text(wl('Type.University')), nil(), nil(), nil(), nil()],
      [nil(), nil(), nil(), binary = text(wl('Education.Institution')), nil(), nil()],
      [nil(), nil(), nil(), entity = text(wl('BarackObama')), nil(), nil()],
      [cnl('Which'), nlUnary = text(cnl('college')), cnl('did'), nlEntity = text(cnl('Obama')), nlBinary = text(cnl('go to')), cnl('?')],
    _).margin(50).center(),
  _).justify('c', 'l').margin(10);
  return overlay(
    example,
    labelConn(unary, nlUnary, alignment.bold(), 'red'),
    labelConn(entity, nlEntity, alignment.bold(), 'red'),
    opts.bridging ? overlay(
      center(bridging.bold()).shift(binary.xmiddle(), binary.top().up(40)),
      conn(unary, binary).color('purple'),
      conn(binary, entity).color('purple'),
    _) : _,
  _);
}

G.bridgingPrez2= function() {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  alignment = red('lexicon');
  var bridging = 'bridging'.fontcolor('purple');
  var example = ytable(
    table(
      [nil(),nil()],
      [nil(), entity = text(cwl('France'))],
      [cnl('prez of'), nlEntity = text(cnl('France'))],
    _).margin(50).center(),
  _).justify('c', 'c').margin(10);
  return overlay(
    example,
    labelConn(entity, nlEntity, alignment.bold(), 'red'),
  _);
}

G.bridgingPrez3= function() {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  alignment = red('lexicon');
  var bridging = 'bridging'.fontcolor('purple');
  var example = ytable(
    table(
      [nil() , binary = text(cwl('CapitalOf.France'))],
      [nil(), entity = text(cwl('France'))],
      [cnl('prez of'), nlEntity = text(cnl('France'))],
    _).margin(50).center(),
  _).justify('c', 'c').margin(10);
  return overlay(
    example,
    labelConn(entity, nlEntity, alignment.bold(), 'red'),
    overlay(
      center(bridging.bold()).shift(binary.xmiddle(), binary.top().up(40)),
      conn(binary, entity).color('purple'),
    _),
  _);
}
G.bridgingPrez4= function() {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  alignment = red('lexicon');
  var bridging = 'bridging'.fontcolor('purple');
  var example = ytable(
    table(
      [nil() , binary = text(cwl('ContainedBy.France'))],
      [nil(), entity = text(cwl('France'))],
      [cnl('prez of'), nlEntity = text(cnl('France'))],
    _).margin(50).center(),
  _).justify('c', 'c').margin(10);
  return overlay(
    example,
    labelConn(entity, nlEntity, alignment.bold(), 'red'),
    overlay(
      center(bridging.bold()).shift(binary.xmiddle(), binary.top().up(40)),
      conn(binary, entity).color('purple'),
    _),
  _);
}
G.bridgingPrez5= function() {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  alignment = red('lexicon');
  var bridging = 'bridging'.fontcolor('purple');
  var example = ytable(
    table(
      [nil() , binary = text(cwl('PresidentOf.France'))],
      [nil(), entity = text(cwl('France'))],
      [cnl('prez of'), nlEntity = text(cnl('France'))],
    _).margin(50).center(),
  _).justify('c', 'c').margin(10);
  return overlay(
    example,
    labelConn(entity, nlEntity, alignment.bold(), 'red'),
    overlay(
      center(bridging.bold()).shift(binary.xmiddle(), binary.top().up(40)),
      conn(binary, entity).color('purple'),
    _),
  _);
}

G.bridgingPrez1= function(opts) {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  alignment = red('lexicon');
  var bridging = 'bridging'.fontcolor('purple');
  var example = ytable(
    table(
      [nil() ,binary = stagger4(text(cwl('PresidentOf.France')),text(cwl('CapitalOf.France')),text(cwl('ContainedBy.France')),text(cwl('LizardsIn.France')))],
      pause(-3),
      [nil(), entity = text(cwl('France'))],
      [cnl('prez of'), nlEntity = text(cnl('France'))],
    _).margin(50).center(),
  _).justify('c', 'l').margin(10);
  return overlay(
    example,
    labelConn(entity, nlEntity, alignment.bold(), 'red'),
    opts.bridging ? overlay(
      center(bridging.bold()).shift(binary.xmiddle(), binary.top().up(40)),
      conn(binary, entity).color('purple'),
    _) : _,
  _);
}

G.nonConvexFunction = function(heightScale) {
  n = 10;
  width = 350;
  px = [], py = [], pr = [];
  Math.seedrandom(9);
  for (var i = 0; i < n; i++) {
    px.push(Math.random() * width);
    py.push(Math.random() * 140 * (heightScale || 1));
    pr.push(50 + Math.random() * 50);
  }
  f = function(x) {
    var y = 0;
    for (var i = 0; i < px.length; i++)
      y -= py[i]*Math.exp(-Math.abs((px[i] - x)*(px[i] - x)/pr[i]));
    return y;
  }
  return overlay.apply(null,
    wholeNumbers(width).map(function(i) {
      return line([i-1, down(f(i-1))], [i, down(f(i))]).color('brown');
    }),
  _).scale(2);
}

G.pt = function(x, y) { return circle(5).fillColor('black').shift(x, y); }
G.localOptimization = function() {
 return overlay(
    ellipse(250, 80).fillColor('pink').strokeWidth(2),
    p0 = pt(-130, down(40)),
    moveLeftOf('$\\hat\\theta_0$', p0),
    pem = pt(-70, down(10)),
    moveRightOf('$\\hat\\theta_\\text{EM}$', pem),
    polyline(p0.middle(), [-100, down(35)], [-110, down(20)], pem.middle()),
  _);
}

G.momentMappingDiagram = function(opts) {
  return parentCenter(overlay(
    xtable(
      overlay(
        Theta = ellipse(120, 80).fillColor('pink').strokeWidth(2),
        moveLeftOf('$\\Theta$', Theta),
        opts.pause ? showLevel(2) : _,
        p1 = pt(20, up(40)), moveLeftOf('$\\theta^*$', p1),
        opts.pause ? showLevel(4) : _,
        opts.inverse ? overlay(p2 = pt(30, up(10)), moveLeftOf('$\\hat\\theta$', p2)) : _,
      _),
      opts.pause ? showLevel(1) : _,
      overlay(
        Moments = ellipse(180, 100).fillColor('lightblue').strokeWidth(2),
        moveRightOf('$\\R^p$', Moments),
        opts.pause ? showLevel(2) : _,
        q1 = pt(-70, up(40)), moveRightOf('$m^*$', q1),
        opts.pause ? showLevel(3) : _,
        opts.inverse ? overlay(q2 = pt(-100, up(10)), moveRightOf('$\\hat m$', q2)) : _,
      _),
    _).xmargin(80).center(),
    opts.pause ? showLevel(2) : _,
    M = arrow(p1, q1).strokeWidth(2).color('gray'), moveTopOf('$\\blue{M}$', M),
    opts.pause ? showLevel(4) : _,
    opts.inverse ? overlay(Minv = arrow(q2, p2).strokeWidth(2).color('gray'), moveBottomOf('$\\red{M^{-1}}$', Minv)) : _,
  _));
}

G.alignmentGraph = function() {
  spacing = 10
  function ctnlb(x, t1, t2) { return text(cnl(x) + '[' + wl(t1) + ',' + wl(t2) + ']'); }
  function cwlb(x) { return text(cwl(x)); }

  typedWords = ytable(
    pause(),
    t1 = ctnlb('grew up in', 'Person', 'Location'),
    t2 = ctnlb('born in', 'Person', 'Date'),
    t3 = ctnlb('married in', 'Person', 'Date'),
    pause(-1),
    t4 = ctnlb('born in', 'Person', 'Location'),
  _).margin(spacing)

  freebase = ytable(
    pause(),
    f1 = cwlb('DateOfBirth'),
    f2 = cwlb('PlaceOfBirth'),
    f3 = cwlb('Marriage.StartDate'),
    pause(-1),
    f4 = cwlb('PlacesLived.Location'),
  _).margin(spacing);

  graph = xtable(
    typedWords,
    freebase,
  _).justify('r').xmargin(100).scale(0.9);

  assertion = function(e1, e2) { return center(text('(' + wl(e1) + ',' + wl(e2) + ')')).scale(0.5); }

  venn = overlay(
    textSet = ellipse(250, 100).strokeWidth(2).xshift(-100).strokeColor('brown'),
    kbSet = ellipse(250, 100).strokeWidth(2).xshift(100).strokeColor('brown'),
    assertion('BarackObama', 'Honolulu').shift(0, -50),
    assertion('MichelleObama', 'Chicago').shift(0, +50),
    assertion('RandomPerson', 'Seattle').shift(-250, 0),
    assertion('BarackObama', 'Chicago').shift(250, 0),
  _);

  // TODO: take the log
  stats = frameBox(table(
    ['phrase-count', ':', '15,765'],
    ['predicate-count', ':', '9,182'],
    ['intersection-count', ':', '6,048'],
    ['KB-best-match', ':', '0'],
  _).xjustify('rcr')).title(opaquebg(darkblue('Alignment features'.bold()))).padding(10).scale(0.8);

  stack = ytable(
    graph,
    venn,
  _).margin(30).center();

  edges = [
    [t4, f4],
    pause(),
    [t1, f2],
    [t1, f3],
    [t2, f2],
    [t2, f3],
    [t3, f1],
    [t3, f4],
    [t4, f1],
  ].map(function(ab) {
    if (ab instanceof Array)
      return line([ab[0].right(), ab[0].ymiddle()], [ab[1].left(), ab[1].ymiddle()]).strokeWidth(2).dashed();
    return ab;
  });

  //edges.push(arrow(t4, textSet));
  //edges.push(arrow(f4, kbSet));

  return new Overlay([stack].concat(edges));
}

G.jointModel = function(opts) {
  return overlay(
    table(
      [z = node('$z$', false), x = node('$x$', true)],
    _).margin(40),
    (opts.directed ? arrow : line)(z.items[0], x.items[0]),
  _);
}

G.conditionalModel = function() {
  return overlay(
    table(
      [z = node('$z$', false), x = node('$x$', true)],
      [y = node('$y$', true), nil()],
    _).margin(40),
    arrow(x.items[0], y.items[0]), arrow(z.items[0], y.items[0]),
  _);
}

G.mixtureModel = function(directed) {
  var edge = directed ? arrow : line;
  return overlay(
    table(
      [nil(), h = node('$h$'), nil()],
      [x1 = node('$x_1$', true), x2 = node('$x_2$', true), x3 = node('$x_3$', true)],
    _).margin(50, 50),
    edge(h, x1),
    edge(h, x2),
    edge(h, x3),
  _);
}

G.logLinearSchema = function() {
  boxed = function(a) { return frame(a).padding(5).bg.strokeWidth(2).strokeColor('gray').end; }
  style = function(x) { return x.strokeWidth(10).color('brown'); }
  return overlay(
    table(
      [boxed('Data $x^{(1)}, \\dots, x^{(n)}$'), nil(), pause(2), boxed('Parameters $\\theta$')],
      pause(-2),
      [a1 = style(downArrow(s=70)), nil(), pause(2), a3 = style(upArrow(s))],
      pause(-2),
      [boxed('Observed moments $\\E[t(x)]$'), pause(), a2 = style(rightArrow(200)), boxed('Latent moments $\\E[\\phi(x, h) \\mid h]$')],
    _).margin(50, 20).center(),
    pause(-1),
    moveRightOf('(1) aggregation'.bold(), a1),
    pause(),
    moveBottomOf('(2) factorization'.bold(), a2),
    pause(),
    moveRightOf('(3) optimization'.bold(), a3),
  _);
}

G.landscape = function(opts) {
  var nx = 600, ny = 400;
  function axis(a, b) { return arrow(a, b).strokeWidth(2); }
  function pt(title, x, y, scale) {
    return overlay(
      circle(5).color('black').shift(x, down(y)),
      transform(brown(title)).pivot(-1, -1).shift(x + 5, down(y)).scale(scale || 0.8),
    _);
  }
  var items = [];
  items.push(axis([0, 0], [nx, 0]));
  items.push(axis([0, 0], [0, down(ny)]));
  items.push(center(red('Breadth')).shift(nx/2, up(20)));
  items.push(center(text(blue('Depth')).rotate(-90)).shift(-20, down(ny/2)));

  if (opts.index >= 0) {
    if (opts.index == 0) items.push(pause());
    items.push(pt('web search', nx*0.9, 0.05*ny));
    items.push(pt('SHRDLU', nx*0.1, 0.9*ny));
    if (opts.index == 0) items.push(pause());
    items.push(pt('topic models', nx*0.7, 0.1*ny));
    items.push(pt('HMMs, CRFs', nx*0.65, 0.2*ny));
    items.push(pt('probabilistic grammars', nx*0.6, 0.3*ny));
    if (opts.index == 0) items.push(pause());
    var sp = pt('semantic parsing (GeoQuery)', nx*0.2, 0.8*ny);
    items.push(sp);
  }

  if (opts.index >= 1) {
    if (opts.index == 1) items.push(pause());
    var sp2 = pt('semantic parsing (Freebase)'.bold().fontcolor('green'), nx*0.5, 0.73*ny, 1);
    items.push(sp2);
    items.push(arrow(sp.items[0], sp2.items[0]).color('green').strokeWidth(2));
  }
  return new Overlay(items);
}

G.systemSpec = function(opts) {
  var myPause = opts.pause ? pause : function() { return _; };
  return ytable(
    nowrapText(ex1.x).scale(0.85),
    myPause(),
    opts.hideArrows ? _ : xtable(bigDownArrow(), text('semantic parsing').orphan(true)).center().margin(10),
    //bigDownArrow(),
    nowrapText(ex1.z).scale(0.7),
    myPause(),
    opts.hideArrows ? _ : xtable(bigDownArrow(), text('execute logical form').orphan(true)).center().margin(10),
    //bigDownArrow(),
    nowrapText(ex1.y).scale(0.85),
  _).center().ymargin(10)
}

G.unaryAssertion = function(a, b) {
  return '(' + nl(a) + ', ' + cnl(b)+ ')';
}
G.textAssertion = function(a, b, c) {
  return '(' + nl(a) + ', ' + cnl(b) + ', ' + nl(c) + ')';
}
G.freebaseAssertion = function(a, b, c) {
  return '(' + wl(a) + ', ' + cwl(b) + ', ' + wl(c) + ')';
}
G.lexicalEntry = function(a,b) {
  return cnl(a) + '$\\Rightarrow$' + cwl(b);
}
// ACL 2014
G.parsingSchema = function(opts) {
  var yellow = '#F7F9D0';
  return overlay(
    ytable(
      x = frameBox(cnl('What languages do people in Brazil use?')).padding(3).bg.dashed().end,
      pause(),
      table(
        [z1 = nowrapText('$\\red{\\wl{Type.HumanLanguage} \\sqcap \\wl{LanguagesSpoken}.\\wl{Brazil}}$').scale(1),
         '...',
         z2 = nowrapText('$\\red{\\wl{CapitalOf}.\\wl{Brazil}}$').scale(1),
        _],
      _).margin(20, 0).center(),
      pause(),
      y = text(blue('Portuguese, ...')),
    _).center().margin(40),
    pause(-1),
    arrow(x, z1),
    arrow(x, z2),
    pause(),
    arrow(z1, y),
  _);
}

G.paraphraseSchema = function(opts) {
  var myPause = opts.pause ? pause : function() { return _; };
  var s = 1;
  var yellow = '#F7F9D0';
  return overlay(
    ytable(
      x = frameBox(cnl('What languages do people in Brazil use?')).padding(3).bg.dashed().end,
      myPause(3),
      paraphrase = frameBox('paraphrase model').padding(10).bg.strokeWidth(2).fillColor(yellow).end,
      myPause(-2),
      table(
        myPause(),
        [x1 = nowrapText(purple(nl('What language is the language of Brazil?'))).scale(s),
         '...',
         x2 = nowrapText(purple(nl('What city is the capital of Brazil?'))).scale(s),
        _],
        [upArrow(30).strokeWidth(4), nil(), upArrow(30).strokeWidth(1)],
        myPause(-1),
        [z1 = nowrapText('$\\red{\\wl{Type.HumanLanguage} \\sqcap \\wl{LanguagesSpoken}.\\wl{Brazil}}$').scale(1),
         '...',
         z2 = nowrapText('$\\red{\\wl{CapitalOf}.\\wl{Brazil}}$').scale(1),
        _],
      _).margin(20, 0).center(),
      myPause(3),
      y = text(blue('Portuguese, ...')),
    _).center().margin(40),
    myPause(-1),
    arrow(x, paraphrase),
    arrow(x1, paraphrase).strokeWidth(4),
    arrow(x2, paraphrase),
    myPause(1),
    arrow(z1, y).strokeWidth(4),
    opts.text ? pause() : nil(),
    opts.text ? img = image('images/simple-document.jpg').scale(0.5) : nil(),
    opts.text ? arrow(img,paraphrase) : nil(),
  _);
}

G.paraphraseSchema1 = function(opts) {
  var myPause = opts.pause ? pause : function() { return _; };
  var s = 1;
  var yellow = '#F7F9D0';
  return overlay(
    ytable(
      x = frameBox(cnl('What languages do people in Brazil use?')).padding(3).bg.dashed().end,
      paraphrase = frameBox('paraphrase model'.fontcolor('white')).padding(10).bg.strokeWidth(0).end,
      table(
        [x1 = nowrapText(white(nl('What language is the language of Brazil?'))).scale(s),
         '...'.fontcolor('white'),
         x2 = nowrapText(white(nl('What city is the capital of Brazil?'))).scale(s),
        _],
        [upArrow(30).strokeWidth(4).strokeColor('white'), nil(), upArrow(30).strokeWidth(1).strokeColor('white')],
        [z1 = nowrapText('$\\red{\\wl{Type.HumanLanguage} \\sqcap \\wl{LanguagesSpoken}.\\wl{Brazil}}$').scale(1),
         '...',
         z2 = nowrapText('$\\red{\\wl{CapitalOf}.\\wl{Brazil}}$').scale(1),
        _],
      _).margin(20, 0).center(),
      y = text(white('Portuguese, ...')),
    _).center().margin(40),
  _);
}

G.paraphraseSchema2 = function(opts) {
  var myPause = opts.pause ? pause : function() { return _; };
  var s = 1;
  var yellow = '#F7F9D0';
  return overlay(
    ytable(
      x = frameBox(cnl('What languages do people in Brazil use?')).padding(3).bg.dashed().end,
      paraphrase = frameBox('paraphrase model'.fontcolor('white')).padding(10).bg.strokeWidth(0).end,
      table(
        [x1 = nowrapText(purple(nl('What language is the language of Brazil?'))).scale(s),
         '...',
         x2 = nowrapText(purple(nl('What city is the capital of Brazil?'))).scale(s),
        _],
        [upArrow(30).strokeWidth(4), nil(), upArrow(30).strokeWidth(1)],
        [z1 = nowrapText('$\\red{\\wl{Type.HumanLanguage} \\sqcap \\wl{LanguagesSpoken}.\\wl{Brazil}}$').scale(1),
         '...',
         z2 = nowrapText('$\\red{\\wl{CapitalOf}.\\wl{Brazil}}$').scale(1),
        _],
      _).margin(20, 0).center(),
      y = text(white('Portuguese, ...')),
    _).center().margin(40),
  _);
}

G.paraphraseSchema3 = function(opts) {
  var myPause = opts.pause ? pause : function() { return _; };
  var s = 1;
  var yellow = '#F7F9D0';
  return overlay(
    ytable(
      x = frameBox(cnl('What languages do people in Brazil use?')).padding(3).bg.dashed().end,
      paraphrase = frameBox('paraphrase model').padding(10).bg.strokeWidth(2).fillColor(yellow).end,
      table(
        [x1 = nowrapText(purple(nl('What language is the language of Brazil?'))).scale(s),
         '...',
         x2 = nowrapText(purple(nl('What city is the capital of Brazil?'))).scale(s),
        _],
        [upArrow(30).strokeWidth(4), nil(), upArrow(30).strokeWidth(1)],
        [z1 = nowrapText('$\\red{\\wl{Type.HumanLanguage} \\sqcap \\wl{LanguagesSpoken}.\\wl{Brazil}}$').scale(1),
         '...',
         z2 = nowrapText('$\\red{\\wl{CapitalOf}.\\wl{Brazil}}$').scale(1),
        _],
      _).margin(20, 0).center(),
      y = text(white('Portuguese, ...')),
    _).center().margin(40),
    arrow(x, paraphrase),
    arrow(x1, paraphrase).strokeWidth(4),
    arrow(x2, paraphrase),
  _);
}


G.paraphraseSchema4 = function(opts) {
  var myPause = opts.pause ? pause : function() { return _; };
  var s = 1;
  var yellow = '#F7F9D0';
  return overlay(
    ytable(
      x = frameBox(cnl('What languages do people in Brazil use?')).padding(3).bg.dashed().end,
      paraphrase = xtable(frameBox('association model'.bold().fontcolor('darkblue')).padding(10).bg.strokeWidth(2).fillColor(yellow).end,frameBox('vector space model').padding(10).bg.strokeWidth(2).fillColor(yellow).end),
      table(
        [x1 = nowrapText(purple(nl('What language is the language of Brazil?'))).scale(s),
         '...',
         x2 = nowrapText(purple(nl('What city is the capital of Brazil?'))).scale(s),
        _],
        [upArrow(30).strokeWidth(4), nil(), upArrow(30).strokeWidth(1)],
        [z1 = nowrapText('$\\red{\\wl{Type.HumanLanguage} \\sqcap \\wl{LanguagesSpoken}.\\wl{Brazil}}$').scale(1),
         '...',
         z2 = nowrapText('$\\red{\\wl{CapitalOf}.\\wl{Brazil}}$').scale(1),
        _],
      _).margin(20, 0).center(),
      y = text(white('Portuguese, ...')),
    _).center().margin(40),
    arrow(x, paraphrase),
    arrow(x1, paraphrase).strokeWidth(4),
    arrow(x2, paraphrase),
  _);
}

G.paraphraseSchema5 = function(opts) {
  var myPause = opts.pause ? pause : function() { return _; };
  var s = 1;
  var yellow = '#F7F9D0';
  return overlay(
    ytable(
      x = frameBox(cnl('What languages do people in Brazil use?')).padding(3).bg.dashed().end,
      paraphrase = xtable(frameBox('association model').padding(10).bg.strokeWidth(2).fillColor(yellow).end,frameBox('vector space model'.bold().fontcolor('darkblue')).padding(10).bg.strokeWidth(2).fillColor(yellow).end),
      table(
        [x1 = nowrapText(purple(nl('What language is the language of Brazil?'))).scale(s),
         '...',
         x2 = nowrapText(purple(nl('What city is the capital of Brazil?'))).scale(s),
        _],
        [upArrow(30).strokeWidth(4), nil(), upArrow(30).strokeWidth(1)],
        [z1 = nowrapText('$\\red{\\wl{Type.HumanLanguage} \\sqcap \\wl{LanguagesSpoken}.\\wl{Brazil}}$').scale(1),
         '...',
         z2 = nowrapText('$\\red{\\wl{CapitalOf}.\\wl{Brazil}}$').scale(1),
        _],
      _).margin(20, 0).center(),
      y = text(white('Portuguese, ...')),
    _).center().margin(40),
    arrow(x, paraphrase),
    arrow(x1, paraphrase).strokeWidth(4),
    arrow(x2, paraphrase),
  _);
}

G.paraphraseMotivation = function() {
  function conn(a, b) {
    return line(a, b).strokeWidth(4);
  }
  myTable = table([x0 = nowrapText(cnl('Where does')), x1 = nowrapText(cnl('the world cup final')), x2 = nowrapText(cnl('occur')), cnl('?')],
        [y0 = nowrapText(cnl('In what stadium does')), y1 = nowrapText(cnl('the Mundial 2014 final')), y2 = nowrapText(cnl('happen')), cnl('?')]).margin(10,30);
  return overlay(
    myTable,
    conn(x1,y1),
    conn(x2,y2),
    conn(x0,y0),
  _);
}

G.textToKbProblem = function() {

  kbTable = table([czl('StPatricksDay'),czl('HolidayDate'),czl('March 17th')]).xmargin(20);

  textTable = table(
      [cnl('St. Patrick\'s day happens on March 17th')],
      [cnl('St. Patrick\'s day occurs on March 17th')]).xmargin(10);

  textKbTable = frame(xtable(textTable,kbTable).margin(40).scale(0.7));
  return textKbTable;
}

G.candidateLogicalForms = function() {
  var nl, e1, e2, join, t1, t2, t3;
  return overlay(
    T(
      nl = nowrapText(cnl('What countries in the world speak Arabic?')).scale(1.5),
      pause(),
      e1 = nowrapText(czl('ArabicAlphabet')).scale(1.3),
      T(e2 = nowrapText(czl('ArabicLang')).scale(1.3),
        pause(),
        t2 = T(nowrapText(cwl('LangSpoken.ArabicLang')).scale(1.3),
          pause(),
          T(nowrapText(cwl('Type.Country $\\sqcap$ LangSpoken.ArabicLang')).scale(1.3),
            nowrapText(cwl('Count(Type.Country $\\sqcap$ LangSpoken.ArabicLang)')).scale(1.3))),
        pause(-1),
        join2 = nowrapText(cwl('LangFamily.Arabic')).scale(1.3),
      _),
    _).recnodeBorderWidth(0).recymargin(50).scale(0.5),
  _);
}

G.generateUtterances  = function() {
  var myTable1 = ytable(
    xtable(x1 = nowrapText(czl('Type.Country')),czl('$\\sqcap$'),x2 = nowrapText(czl('LangSpoken.')),x3 = nowrapText(czl('ArabicLang'))).center(),
    xtable(y1 = nowrapText(cnl('country')), y2 = nowrapText(cnl('spoken in')), y3 = nowrapText(cnl('languages spoken')), y4 = nowrapText(cnl('Arabic language'))).margin(30).center(),
    pause(),
    xtable(bigDownArrow(),'simple syntactic analysis').margin(20)).margin(40).center();

  var myTable2 = ytable(
    xtable(purple(nl('What country is Arabic language spoken in?'))),
    xtable(purple(nl('What country spoken the languages Arabic language?')))).margin(5).center();

  var myTable3 = ytable(myTable1,pause(),myTable2);
  return overlay(
    myTable3,
    pause(-2),
    line(x1,y1),
    line(x2,y2),
    line(x2,y3),
    line(x3,y4),
  _);
}


G.associationExample= function() {
  function conn(a, b) {
    return line([a.xmiddle(), a.bottom()], [b.xmiddle(), b.top()]);
  }
  function colorConn(a, b, color) {
    return conn(a, b).color(color);
  }
  function colorWideConn(a, b, color, width) {
    return colorConn(a,b,color).strokeWidth(width);
  }
  function conditionalColorConn(a, b, width) {
    var scale=3;
    var color;
    if(width<0)
      return colorConn(a,b,'red').strokeWidth(scale*width);
    else
      return colorConn(a,b,'blue').strokeWidth(scale*width).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  var example =
    ytable(
      xtable('$x:$',x1 = nowrapText(cnl('What')), x2 = nowrapText(cnl('type')), x3 = nowrapText(cnl('of')), x4 = nowrapText(cnl('music')), x5 = nowrapText(cnl('did')),
                    x6 = nowrapText(cnl('Richard')), x7 = nowrapText(cnl('Wagner')), x8 = nowrapText(cnl('play'))).margin(25),
      yspace(50),
      xtable('$c:$', c1 = nowrapText(purple(nl('What'))), c2 = nowrapText(purple(nl('is'))), c3 = nowrapText(purple(nl('the'))), c4 = nowrapText(purple(nl('musical'))),
                      c5 = nowrapText(purple(nl('genres'))), c6 = nowrapText(purple(nl('of'))), c7 = nowrapText(purple(nl('Richard'))), c8 = nowrapText(purple(nl('Wagner')))).margin(25).center(),
    _);
  return overlay(
    example,
    pause(),
    conditionalColorConn(x1,c1,2.97),
    conditionalColorConn(x6,c7,2.97),
    conditionalColorConn(x7,c8,2.97),
    conditionalColorConn(x2,c5,0.53),
    conditionalColorConn(x4,c4,2.59),
    conditionalColorConn(x1,c3,-0.21),
    conditionalColorConn(x4,c1,-0.38),
    conditionalColorConn(x8,c3,-1.53),
    conditionalColorConn(x4,c5,1.13),
    conditionalColorConn(x8,c2,-0.33),
  _);
}

G.phrasetable= function(entries) {
  var items = entries.map(function(x) {
    if (x == _) return _;
    if (typeof(x) == 'string') return nowrapText(green(x.italics()));
    return nowrapText(green(x[0].italics()) + ' $\\Leftrightarrow$ ' + purple(x[1]));
  });
  return ytable.apply(null, items).scale(0.75);
}


G.colorFrameBox = function(a,color) { return frame(a).bg.fillColor(color).dim(90,50).end; }

G.vsm = function() {
  function draw(x) {
    if(x > 15)
      return colorFrameBox(x,'rgb(80,80,80)');
    else if(x > 10)
      return colorFrameBox(x,'rgb(105,105,105)');
    else if(x > 5)
      return colorFrameBox(x,'rgb(130,130,130)');
    else if(x > 0)
      return colorFrameBox(x,'rgb(155,155,155)');
    else if(x > 5)
      return colorFrameBox(x,'rgb(180,180,180)');
    else if(x > -10)
      return colorFrameBox(x,'rgb(205,205,205)');
    else
      return colorFrameBox(x,'rgb(230,230,230)');

  }
  var example =
    table(
        [brownbold('Full')  ,nowrapText(cnl('do')),nowrapText(cnl('people')),nowrapText(cnl('czech')),nowrapText(cnl('republic')),nowrapText(cnl('speak'))],
        [nowrapText(cnl('offical')),draw(0.7),draw(8.09),draw(15.34),draw(21.62),draw(24.44)],
        [nowrapText(cnl('language')),draw(3.86),draw(-3.13),draw(7.81),draw(2.58),draw(14.74)],
        [nowrapText(cnl('czech')),draw(0.67),draw(16.55),'','',draw(2.76)],
        [nowrapText(cnl('republic')),draw(-8.71),draw(12.47),'','',draw(-10.75)],
        [yspace(20), nil(), nil(), nil(), nil(), nil()],
        [brownbold('Diagonal'),nowrapText(cnl('do')),nowrapText(cnl('people')),nowrapText(cnl('czech')),nowrapText(cnl('republic')),nowrapText(cnl('speak'))],
        [nowrapText(cnl('offical')),draw(2.31),draw(-0.72),draw(1.88),draw(0.27),draw(-0.49)],
        [nowrapText(cnl('language')),draw(.27),draw(4.72),draw(11.51),draw(12.33),draw(11.0)],
        [nowrapText(cnl('czech')),draw(1.4),draw(8.13),'','',draw(5.21)],
        [nowrapText(cnl('republic')),draw(-0.16),draw(6.72),'','',draw(9.69)],
        [yspace(20), nil(), nil(), nil(), nil(), nil()],
        [brownbold('Identity'),nowrapText(cnl('do')),nowrapText(cnl('people')),nowrapText(cnl('czech')),nowrapText(cnl('republic')),nowrapText(cnl('speak'))],
        [nowrapText(cnl('offical')),draw(2.26),draw(-1.41),draw(0.89),draw(0.07),draw(-0.58)],
        [nowrapText(cnl('language')),draw(0.62),draw(4.19),draw(11.91),draw(10.78),draw(12.7)],
        [nowrapText(cnl('czech')),draw(2.88),draw(7.31),'','',draw(5.42)],
        [nowrapText(cnl('republic')),draw(-1.82),draw(4.34),'','',draw(9.44)],
      _).center();
  return overlay(example,
      _);
}


