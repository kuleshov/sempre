\section{Model overview} \label{sec:framework}

We now present the general framework for semantic parsing via
paraphrasing, including the model and the learning algorithm.
In Sections \ref{sec:generation} and \ref{sec:paraph},
we provide the details of our implementation.

\paragraph{Canonical utterance construction} Given an utterance $x$ and the
KB, we construct a set of candidate logical forms $\sZ_x$, 
and then for each $z \in \sZ_x$ generate a small set of canonical
natural language utterances $\sC_z$. Our goal at this point is only to generate
a manageable set of logical forms containing the correct one, and then generate
an appropriate canonical utterance from it. This strategy
is feasible in factoid QA where compositionality is
low, and so the size of $\sZ_x$ is limited (\refsec{generation}).

\paragraph{Paraphrasing}
We score the canonical utterances in $\sC_z$ with respect to the input
utterance $x$ using a paraphrase model, which offers two advantages.
First, the paraphrase model is decoupled from the KB, so we can
train it from large text corpora. Second,
natural language utterances often do not express predicates explicitly, e.g., 
the question \nl{What is Italy's money?} expresses the binary predicate \wl{CurrencyOf} with a possessive
construction. Paraphrasing methods are well-suited for handling such
text-to-text gaps.
Our framework accommodates any paraphrasing method,
and in this paper we propose an \emph{association model} that learns to
associate natural language phrases that co-occur frequently in a monolingual
parallel corpus, combined with a \emph{vector space model}, which learns to
score the similarity between vector representations
of natural language utterances (\refsec{paraph}).

\paragraph{Model}
%The two subparts are embedded in a joint probabilistic model,
%which we now formally define.
We define a discriminative log-linear model that places a probability
distribution over pairs of logical forms and canonical utterances $(c,z)$,
given an utterance $x$:
\begin{align*}
p_\theta(c,z \mid x) = \frac{\exp \{\phi(x,c,z)^\top \theta \}}{\sum_{z' \in \sZ_x,c' \in \sC_z} \exp\{\phi(x,c',z')^\top \theta\}},
\end{align*}
where 
$\theta \in \R^b$ is the vector of parameters to be learned,
and $\phi(x,c,z)$ is a feature vector extracted from the input utterance $x$, the
canonical utterance $c$, and the logical form $z$.
Note that the candidate set of logical forms $\sZ_x$
and canonical utterances $\sC_x$ are constructed during the canonical utterance
construction phase.

The model score decomposes into two terms: 
\begin{align*}
\phi(x,c,z)^\top \theta = \phi_{\text{pr}}(x,c)^\top\theta_{\text{pr}} + \phi_{\text{lf}}(x,z)^\top\theta_{\text{lf}},
\end{align*} 
where the parameters $\theta_{\text{pr}}$ define the paraphrase model (\refsec{paraph}), which is based on features
extracted from text only (the input and canonical utterance). The parameters $\theta_{\text{lf}}$
correspond to semantic parsing features based on the logical form and input utterance, and
are briefly described in this section.

Many existing paraphrase models introduce latent variables to describe the
\emph{derivation} of $c$ from $x$, e.g., with transformations
\cite{heilman2010tree,stern2011transformation} or alignments
\cite{haghighi05robust,das2009paraphrase,chang2010discriminative}.
However, we opt for a simpler paraphrase model without latent variables
in the interest of efficiency.
%Our model can easily accommodate this by marginalizing over derivations that
%lead to the same utterance: $p_\theta(c,z \mid x)=\sum_d p_\theta(c,z,d \mid
%x)$ and modifying the log-linear model accordingly.

\paragraph{Logical form features}
The parameters $\theta_{\text{lf}}$ correspond to the following
features adopted from \newcite{berant2013freebase}. For a logical form $z$, we
extract the size of its denotation $\den{z}$. We also add all binary predicates
in $z$ as features. Moreover, we extract a popularity feature for predicates based on
the number of instances they have in $\sK$. For Freebase entities, we extract a popularity
feature based on the entity frequency in an entity linked subset of Reverb
\cite{lin2012linking}. Lastly, Freebase formulas have types (see \refsec{generation}), 
and we conjoin the type of $z$ with the first
word of $x$, to capture the correlation between a word (e.g., \nl{where})
with the Freebase type (e.g., \wl{Location}).

\paragraph{Learning}
%% joberant - maybe we have more room to describe this in a bit more detail (have the gradient)

As our training data consists of question-answer pairs $(x_i,y_i)$,
we maximize the log-likelihood of the correct answer.
%summing over the latent logical forms and generated questions $(c,z)$.
The probability of an answer $y$ is obtained by marginalizing over
canonical utterances $c$ and logical forms $z$ whose denotation is $y$.
Formally, our objective function $\sO(\theta)$ is as follows:
\begin{align*}
&\sO(\theta) =
  \sum_{i=1}^n \log p_\theta(y_i \mid x_i) - \lambda \|\theta\|_1, \\
&p_\theta(y \mid x) = \sum_{z \in \sZ_x: y = \den{z}} \sum_{c \in \sC_z} p_\theta(c,z \mid x).
\end{align*}
The strength $\lambda$ of the $L_1$ regularizer is set based on
cross-validation.
We optimize the objective by initializing the parameters $\theta$ to zero
and running AdaGrad \cite{duchi10adagrad}.
We approximate the set of pairs of logical forms and canonical utterances with
a beam of size 2,000.
%We optimize the objective by initializing $\theta^{(0)}$ to $0$ and
%run AdaGrad \cite{duchi10adagrad}, so that $\theta^{(t+1)}$ is
%set based on taking a stochastic approximation of $\frac{\partial
%\sO(\theta; \theta^{(t)})}{\partial \theta}\big|_{\theta = \theta^{(t)}}$.

