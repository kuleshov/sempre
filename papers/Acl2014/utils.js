G = sfig.serverSide ? global : this;
sfig.importAllMethods(G);

// Latex macros
sfig.latexMacro('R', 0, '\\mathbb{R}');
sfig.latexMacro('P', 0, '\\mathbb{P}');
sfig.latexMacro('E', 0, '\\mathbb{E}');
sfig.latexMacro('diag', 0, '\\text{diag}');
sfig.includeLatex('macros.tex');

sfig.initialize();

G.prez = sfig.presentation();

// Useful functions (not standard enough to put into an sfig library).

G.frameBox = function(a) { return frame(a).padding(5).bg.strokeWidth(1).fillColor('white').end; }
G.colorFrameBox = function(a,color) { return frame(a).bg.fillColor(color).dim(90,50).end; }

G.bigLeftArrow = function(s) { return leftArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigRightArrow = function(s) { return rightArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigUpArrow = function(s) { return upArrow(s || 100).strokeWidth(10).color('brown'); }
G.bigDownArrow = function(s) { return downArrow(s || 100).strokeWidth(10).color('brown'); }
G.red = function(x) { return x.fontcolor('red'); }
G.green = function(x) { return x.fontcolor('green'); }
G.blue = function(x) { return x.fontcolor('blue'); }
G.darkblue = function(x) { return x.fontcolor('darkblue'); }

G.xseq = function() { return new sfig.Table([arguments]).center().margin(5); }
G.yseq = function() { return ytable.apply(null, arguments).margin(10); }

G.stmt = function(prefix, suffix) { return prefix.fontcolor('darkblue') + ':' + (suffix ? ' '+suffix : ''); }
G.headerList = function(title) {
  var contents = Array.prototype.slice.call(arguments).slice(1);
  return ytable.apply(null, [title ? stmt(title) : _].concat(contents.map(function(line) {
    if (line == _) return _;
    if (typeof(line) == 'string') return bulletedText([null, line]);
    if (line instanceof sfig.PropertyChanger) return line;
    return indent(line);
  })));
}

G.node = function(x, shaded) { return overlay(circle(20).fillColor(shaded ? 'lightgray' : 'white') , x).center(); }
G.indent = function(x, n) { return frame(x).xpadding(n != null ? n : 20).xpivot(1); }
G.stagger = function(b1, b2) { b1 = std(b1); b2 = std(b2); return overlay(b1, pause(), b2.replace(b1)); }

G.dividerSlide = function(text) {
  return slide(null, nil(), parentCenter(text)).titleHeight(0);
}

G.moveLeftOf = function(a, b, offset) { return transform(a).pivot(1, 0).shift(b.left().sub(offset == null ? 5 : offset), b.ymiddle()); }
G.moveRightOf = function(a, b, offset) { return transform(a).pivot(-1, 0).shift(b.right().add(offset == null ? 5 : offset), b.ymiddle()); }
G.moveTopOf = function(a, b, offset) { return transform(a).pivot(0, 1).shift(b.xmiddle(), b.top().sub(offset == null ? 5 : offset)); }
G.moveBottomOf = function(a, b, offset) { return transform(a).pivot(0, -1).shift(b.xmiddle(), b.bottom().add(offset == null ? 5 : offset)); }
G.moveCenterOf = function(a, b) { return transform(a).pivot(0, 0).shift(b.xmiddle(), b.ymiddle()); }

G.wholeNumbers = function(n) {
  var result = [];
  for (var i = 0; i < n; i++) result[i] = i;
  return result;
}

G.argSort = function(arr, cmp) {
  cmp = cmp || function(a, b) { return a - b; };
  return wholeNumbers(arr.length).sort(function(a, b) { return cmp(arr[a], arr[b]); });
};

G.shuffleArrayInPlace = function(arr) {
  for (var i = arr.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var tmp = arr[j]; arr[j] = arr[i]; arr[i] = tmp;
  }
}

////////////////////////////////////////////////////////////

G.nlcolor = 'green';  // Natural language
G.zlcolor = 'red';  // Logical form
G.wlcolor = 'blue';  // World
G.nl = function(x) { return x.italics(); }
G.zl = function(x) { return '$\\wl{' + x + '}$'; }
G.wl = function(x) { return '$\\wl{' + x + '}$'; }
G.cnl = function(x) { return nl(x).fontcolor(nlcolor); }
G.czl = function(x) { return zl(x).fontcolor(zlcolor); }
G.cwl = function(x) { return wl(x).fontcolor(wlcolor); }

G.textFreebaseMapping = function() {
  return overlay(
    xtable(
      x = frameBox(green('utterance')).padding(5, 150),
      ytable(
        a = nil(),
        ulf = frameBox(ytable('underspecified', 'logical', 'form').center()).strokeColor('red').padding(5).bg.dashed().end,
        cu = frameBox(ytable('canonical', 'utterance').center()).strokeColor('green').padding(5).bg.dashed().end,
      _).margin(30).center(),
      y = frameBox(ytable(red('logical'), red('form')).center()).padding(5, 130),
    _).center().margin(100),
    nil(),
    direct = arrow([x.right(), a.ymiddle()], [y.left(), a.ymiddle()]).strokeWidth(5).color('purple'),
    a1 = arrow([x.right(), ulf.ymiddle()], [ulf.left(), ulf.ymiddle()]),
    a2 = arrow([ulf.right(), ulf.ymiddle()], [y.left(), ulf.ymiddle()]).strokeWidth(5).color('purple'),
    b1 = arrow([x.right(), cu.ymiddle()], [cu.left(), cu.ymiddle()]).strokeWidth(5).color('purple'),
    b2 = arrow([cu.right(), cu.ymiddle()], [y.left(), cu.ymiddle()]),
    moveTopOf(ytable('ontology', 'matching').center(), a2).scale(0.7),
    moveTopOf('paraphrase', b1).scale(0.7),
    moveTopOf('direct', direct).scale(0.7),

    moveBottomOf(ytable('(traditional)').center(), a).scale(0.7),
    moveBottomOf(ytable('(Kwiatkowski et al. 2013)').center(), ulf).scale(0.7),
    moveBottomOf(ytable('(this work)').center(), cu).scale(0.7),
  _);
}

G.paraphraseSchema = function() {
  var s = 0.7;
  var yellow = '#F7F9D0';
  return overlay(
    ytable(
      x = frameBox(cnl('What party did Clay establish?')).padding(3).bg.dashed().end,
      //x = text(cnl('What party did Clay establish?')),
      paraphrase = frameBox('paraphrase model').padding(10).bg.strokeWidth(2).fillColor(yellow).end,
      table(
        [x1 = nowrapText(purple(nl('What political party founded by Henry Clay?'))).scale(s),
         '...',
         x2 = nowrapText(purple(nl('What event involved the people Henry Clay?'))).scale(s),
        _],
        [upArrow(30).strokeWidth(4), nil(), upArrow(30).strokeWidth(1)],
        [z1 = nowrapText('$\\red{\\wl{Type.PoliticalParty} \\sqcap \\wl{Founder}.\\wl{HenryClay}}$').scale(1),
         '...',
         z2 = nowrapText('$\\red{\\wl{Type.Event} \\sqcap \\wl{Involved}.\\wl{HenryClay}}$').scale(1),
        _],
        //[downArrow(30).strokeWidth(2), nil(), downArrow(30).strokeWidth(2)],
        //[y1 = text(blue('Whig Party')).scale(s), nil(), y2 = text(blue('Wrong answer')).scale(s)],
      _).margin(20, 0).center(),
      //y = text(blue('Whig Party')),
      y = frameBox(blue('Whig Party')).padding(3).bg.dashed().end,
    _).center().margin(40),
    arrow(x, paraphrase),
    arrow(x1, paraphrase).strokeWidth(4),
    arrow(x2, paraphrase),
    arrow(z1, y).strokeWidth(4),
    //line(y2, y),
    //line(y1, y), line(y2, y),
  _);
}

G.associationExample= function() {
  function conn(a, b) {
    return line(a, b).strokeWidth(2).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  var example =
    ytable(
      xtable('$x:$',x1 = nowrapText(cnl('Where')), x2 = nowrapText(cnl('James')), x3 = nowrapText(cnl('Madison')), x4 = nowrapText(cnl('was')), x5 = nowrapText(cnl('born'))).margin(50),
      yspace(50),
      xtable('$c:$', c1 = nowrapText(purple(nl('What location'))), c2 = nowrapText(purple(nl('birthplace of'))), c3 = nowrapText(purple(nl('James'))), c4 = nowrapText(purple(nl('Madison')))).margin(50).center(),
    _);
  return overlay(
    example,
    conn(c1,x1),
    conn(c2,x5),
    conn(c3,x2),
    conn(c4,x3),
  _);
}

G.associationExample2= function() {
  function conn(a, b) {
    return line([a.xmiddle(), a.bottom()], [b.xmiddle(), b.top()]);
  }
  function colorConn(a, b, color) {
    return conn(a, b).color(color);
  }
  function colorWideConn(a, b, color, width) {
    return colorConn(a,b,color).strokeWidth(width);
  }
  function conditionalColorConn(a, b, width) {
    var scale=3;
    var color;
    if(width<0)
      return colorConn(a,b,'red').strokeWidth(scale*width);
    else
      return colorConn(a,b,'blue').strokeWidth(scale*width).dashed();
  }
  function labelConn(a, b, label, color) {
    var c = conn(a, b).color(color);
    return overlay(c, moveCenterOf(opaquebg(label).scale(0.9), c));
  }
  var example =
    ytable(
      xtable('$x:$',x1 = nowrapText(cnl('What')), x2 = nowrapText(cnl('type')), x3 = nowrapText(cnl('of')), x4 = nowrapText(cnl('music')), x5 = nowrapText(cnl('did')),
                    x6 = nowrapText(cnl('Richard')), x7 = nowrapText(cnl('Wagner')), x8 = nowrapText(cnl('play'))).margin(25),
      yspace(50),
      xtable('$c:$', c1 = nowrapText(cnl('What')), c2 = nowrapText(cnl('is')), c3 = nowrapText(cnl('the')), c4 = nowrapText(cnl('musical')),
                      c5 = nowrapText(cnl('genres')), c6 = nowrapText(cnl('of')), c7 = nowrapText(cnl('Richard')), c8 = nowrapText(cnl('Wagner'))).margin(25).center(),
    _);
  return overlay(
    example,
    conditionalColorConn(x1,c1,2.97),
    conditionalColorConn(x6,c7,2.97),
    conditionalColorConn(x7,c8,2.97),
    conditionalColorConn(x2,c5,0.53),
    conditionalColorConn(x4,c4,2.59),
    conditionalColorConn(x1,c3,-0.21),
    conditionalColorConn(x4,c1,-0.38),
    conditionalColorConn(x8,c3,-1.53),
    conditionalColorConn(x4,c5,1.13),
    conditionalColorConn(x8,c2,-0.33),
  _);
}

G.vsm = function() {
  function draw(x) {

    if(x > 15)
      return colorFrameBox(x,'rgb(80,80,80)');
    else if(x > 10)
      return colorFrameBox(x,'rgb(105,105,105)');
    else if(x > 5)
      return colorFrameBox(x,'rgb(130,130,130)');
    else if(x > 0)
      return colorFrameBox(x,'rgb(155,155,155)');
    else if(x > 5)
      return colorFrameBox(x,'rgb(180,180,180)');
    else if(x > -10)
      return colorFrameBox(x,'rgb(205,205,205)');
    else
      return colorFrameBox(x,'rgb(230,230,230)');

  }
  var example =
    table(
        [brownbold('Full')  ,nowrapText(cnl('do')),nowrapText(cnl('people')),nowrapText(cnl('czech')),nowrapText(cnl('republic')),nowrapText(cnl('speak'))],
        [nowrapText(cnl('offical')),draw(0.7),draw(8.09),draw(15.34),draw(21.62),draw(24.44)],
        [nowrapText(cnl('language')),draw(3.86),draw(-3.13),draw(7.81),draw(2.58),draw(14.74)],
        [nowrapText(cnl('czech')),draw(0.67),draw(16.55),'','',draw(2.76)],
        [nowrapText(cnl('republic')),draw(-8.71),draw(12.47),'','',draw(-10.75)],
        [yspace(20), nil(), nil(), nil(), nil(), nil()],
        [brownbold('Diagonal'),nowrapText(cnl('do')),nowrapText(cnl('people')),nowrapText(cnl('czech')),nowrapText(cnl('republic')),nowrapText(cnl('speak'))],
        [nowrapText(cnl('offical')),draw(2.31),draw(-0.72),draw(1.88),draw(0.27),draw(-0.49)],
        [nowrapText(cnl('language')),draw(.27),draw(4.72),draw(11.51),draw(12.33),draw(11.0)],
        [nowrapText(cnl('czech')),draw(1.4),draw(8.13),'','',draw(5.21)],
        [nowrapText(cnl('republic')),draw(-0.16),draw(6.72),'','',draw(9.69)],
        [yspace(20), nil(), nil(), nil(), nil(), nil()],
        [brownbold('Identity'),nowrapText(cnl('do')),nowrapText(cnl('people')),nowrapText(cnl('czech')),nowrapText(cnl('republic')),nowrapText(cnl('speak'))],
        [nowrapText(cnl('offical')),draw(2.26),draw(-1.41),draw(0.89),draw(0.07),draw(-0.58)],
        [nowrapText(cnl('language')),draw(0.62),draw(4.19),draw(11.91),draw(10.78),draw(12.7)],
        [nowrapText(cnl('czech')),draw(2.88),draw(7.31),'','',draw(5.42)],
        [nowrapText(cnl('republic')),draw(-1.82),draw(4.34),'','',draw(9.44)],
      _).center();
/*
  var diagonal=
    table(
        ['Diag.',nowrapText(cnl('do')),nowrapText(cnl('people')),nowrapText(cnl('czech')),nowrapText(cnl('republic')),nowrapText(cnl('speak'))],
        [nowrapText(cnl('offical')),draw(.035),draw(.033),draw(.0348),draw(.034),draw(.033)],
        [nowrapText(cnl('language')),draw(.034),draw(.037),draw(.042),draw(.043),draw(.042)],
        [nowrapText(cnl('czech')),draw(.034),draw(.039),draw(.199),draw(.062),draw(.037)],
        [nowrapText(cnl('republic')),draw(.033),draw(.038),draw(.062),draw(.087),draw(.041)],
      _).margin(20,0);
  var dotprod=
    table(
        ['Dot prod.',nowrapText(cnl('do')),nowrapText(cnl('people')),nowrapText(cnl('czech')),nowrapText(cnl('republic')),nowrapText(cnl('speak'))],
        [nowrapText(cnl('offical')),draw(.032),draw(.03),draw(.031),draw(.031),draw(.03)],
        [nowrapText(cnl('language')),draw(.031),draw(.033),draw(.039),draw(.038),draw(.04)],
        [nowrapText(cnl('czech')),draw(.032),draw(.035),draw(.239),draw(.064),draw(.034)],
        [nowrapText(cnl('republic')),draw(.03),draw(.033),draw(.064),draw(.096),draw(.037)],
      _).margin(20,0);
  var example = ytable(full,diagonal,dotprod).margin(20);*/
  return overlay(example,
      _);
}


