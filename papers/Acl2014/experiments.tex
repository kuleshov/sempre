\section{Empirical evaluation} \label{sec:experiments}

In this section, we evaluate our system on
\WebQuestionsDataset{} and \CaiYatesDataset{}.
After describing the setup (\refsec{expsetup}), we
present our main empirical results
and analyze the components of the system (\refsec{results}).

\subsection{Setup} \label{sec:expsetup}

We use the \WebQuestionsDataset{} dataset \cite{berant2013freebase}, which
contains 5,810 question-answer pairs. This dataset was created by crawling
questions through the Google Suggest API, and then obtaining answers using
Amazon Mechanical Turk. We use the original train-test split, and divide the
training set into 3 random 80\%--20\% splits for development. This dataset is
characterized by questions that are commonly asked on the web (and are
not necessarily grammatical), such as \nl{What
character did Natalie Portman play in Star Wars?} and \nl{What kind of money to
take to Bahamas?}.

\begin{table}[t]
\begin{center}
{\footnotesize
\hfill{}
\begin{tabular}{|l|r|r|}
\hline
\textbf{Dataset}&\textbf{\# examples}&\textbf{\# word types}\\
\hline
\CaiYatesDataset       & 917           & 2,036             \\
\WebQuestionsDataset   & 5,810           & 4,525             \\
\hline
\end{tabular}}
\hfill{}
\caption{\footnotesize{Statistics on \WebQuestionsDataset{} and \CaiYatesDataset{}.}}
\label{tab:datasets}
\end{center}
\end{table}

The \CaiYatesDataset{} dataset contains 917 questions, authored by two annotators and annotated with logical forms.
This dataset contains questions on rarer topics (for example, \nl{What is the engine in a 2010 Ferrari California?} and \nl{What was the cover price of the X-men Issue 1?}),
% PL: natural => diverse; natural is a bit too subjective
but the phrasing of questions tends to be more rigid compared to
\WebQuestionsDataset{}. Table \ref{tab:datasets} provides some statistics on
the two datasets.
Following \newcite{cai2013large}, we hold out 30\% of the data for the final test, and perform 3 random 80\%-20\% splits of the training set for development. Since we train from question-answer pairs, we collect answers by executing the gold logical forms against Freebase. 

We execute $\lambda$-DCS queries by converting them into SPARQL and executing them against a copy of Freebase using the Virtuoso database engine. We evaluate our system with accuracy, that is, the proportion of questions we answer correctly. We run all questions through the Stanford CoreNLP pipeline \cite{toutanova2003tagger,finkel2005incorporating,klein03unlexicalized}.

We tuned the $L_1$ regularization strength,
developed features,
and ran analysis experiments on the development set (averaging across random splits).
On \WebQuestionsDataset{},
without $L_1$ regularization, the number of non-zero features was 360K;
$L_1$ regularization brings it down to 17K.

\subsection{Results} \label{sec:results}

We compare our system to \newcite{cai2013large} (\textsc{CY13}), \newcite{berant2013freebase} (\textsc{BCFL13}), and \newcite{kwiatkowski2013scaling} (\textsc{KCAZ13}).
For \textsc{BCFL13}, we obtained results using the \textsc{Sempre} package\footnote{http://www-nlp.stanford.edu/software/sempre/} and running \newcite{berant2013freebase}'s system on the datasets.

\reftab{results} presents results on the test set.
We achieve a substantial relative improvement of 12\% in accuracy 
 on \WebQuestionsDataset{}, and match the best results on \CaiYatesDataset{}. 
Interestingly, our system gets an \emph{oracle accuracy} of 63\% on
\WebQuestionsDataset{} compared to 48\% obtained by \textsc{BCFL13}, where
the oracle accuracy is the fraction of questions for which at least
one logical form in the candidate set produced by the system is correct.
This demonstrates that our method for constructing candidate logical forms is
reasonable. To further examine this, we ran \textsc{BCFL13} on the development
set, allowing it to use only predicates from logical forms suggested by
our logical form construction step. This improved oracle accuracy on the development set to 64.5\%, but accuracy was 32.2\%. This shows that the improvement in accuracy should not be attributed only to better logical form generation, but also to the paraphrase model.

\begin{table}[t]
{\footnotesize
\hfill{}
\begin{tabular}{l|c|c}
 & \CaiYatesDataset & \WebQuestionsDataset \\
\hline
\textsc{CY13} & 59.0 & -- \\
\textsc{BCFL13} & 62.0 & 35.7 \\
\textsc{KCAZ13} & 68.0 & -- \\
This work & \textbf{68.5} & \textbf{39.9} \\
\end{tabular}
}
\hfill{}
\caption{\footnotesize{
Results on the test set.
}
}
\label{tab:results}
\end{table}

\begin{table}[t]
{\footnotesize
\hfill{}
\begin{tabular}{l|c|c}
 & \CaiYatesDataset & \WebQuestionsDataset \\
\hline
Our system & \textbf{73.9} & \textbf{41.2} \\
--\Vsm & 71.0 & 40.5 \\
--\Association & 52.7 & 35.3 \\
--\Paraphrase & 31.8 & 21.3 \\
\hline
\SimpleGeneration & 73.4 & 40.4 \\
\hline
Full matrix & 52.7 & 35.3 \\
Diagonal & 50.4 & 30.6 \\
Identity & 50.7 & 30.4 \\
\hline
\Jaccard & 69.7 & 31.3 \\ 
\Edit & 40.8 &  24.8 \\
\WDDC & 71.0 & 29.8 \\
\end{tabular}
}
\hfill{}
\caption{\footnotesize{
Results for ablations and baselines on development set.
}
}
\label{tab:ablations}
\end{table}

We now perform more extensive analysis of our system's components and compare it to various baselines.

\paragraph{Component ablation}
We %examine the contribution of the system's components by
ablate the association model,
the VS model, and the entire paraphrase model (using only logical form features).
\reftab{results} shows that our full system obtains highest accuracy,
and that removing the association model results in a much larger degradation
compared to removing the VS model. 

\paragraph{Utterance generation}
Our system generates relatively natural utterances from logical forms using simple rules based on Freebase descriptions (\refsec{generation}).
We now consider simply concatenating Freebase descriptions.
%For a logical form $z$, we generate the utterance
%\texttt{WH $d(t)$ $d(e_1)$ $d(b)$ $d(e_2)$}, where $d(t)$, $d(e_1)$, $d(b)$ and $d(e_2)$
%are the Freebase descriptions of the logical
%form type, the entity, the properties in the binary predicate,
%and optionally another entity.
For example, the logical form
\wl{$\bR$[PlaceOfBirth].ElvisPresley} would generate the utterance \nl{What location Elvis Presley place of birth?}.
Row \SimpleGeneration{} in \reftab{ablations}
demonstrates that we still get good results in this setup.
This is expected given that our paraphrase models are not sensitive to the 
syntactic structure of the generated utterance.

\FigTop{figures.slides/vsm}{0.30}{vsm}{
\footnotesize{
Values of the paraphrase score $v_{x_i}^\top W v_{c_{i'}}$ for 
all content word tokens $x_i$ and $c_{i'}$,
where $W$ is an arbitrary full matrix, a diagonal matrix, or the identity matrix. 
We omit scores for the words \nl{czech} and \nl{republic} since they
appear in all canonical utterances for this example.%where this entity was matched.
}
}

\paragraph{VS model}
Our system learns parameters for a full $W$ matrix. We now examine 
results when learning parameters for a full matrix $W$, a diagonal
matrix $W$, and when setting $W$ to be the identity matrix.
\reftab{ablations} (third section) illustrates that learning a full matrix substantially improves accuracy.
\reffig{vsm} gives an example for a 
correct paraphrase pair, where the full matrix model boosts the overall model score.
Note that the full matrix assigns
a high score for the phrases \nl{official language} and \nl{speak} compared to the simpler models,
but other pairs are less interpretable.
%suggesting that this model can capture more intricate semantic similarities.

\paragraph{Baselines}
We also compared our system to the following implemented baselines:
\begin{itemize} [noitemsep,topsep=0pt]
\item\Jaccard{}: We compute the Jaccard score between the tokens of $x$ and $c$ and 
define $\phi_{\text{pr}}(x,c)$ to be this single feature.
\item \Edit: We compute the token edit distance between $x$ and $c$ and 
define $\phi_{\text{pr}}(x,c)$ to be this single feature.
\item \WDDC: We re-implement 13 features from \newcite{wan2006dependency}, who obtained close to state-of-the-art performance on the Microsoft Research paraphrase corpus.\footnote{We implement all features that do not require dependency parsing.}
\end{itemize}

\reftab{ablations} demonstrates that we improve performance over all baselines. Interestingly, \Jaccard{} and 
\WDDC{} obtain reasonable performance on \CaiYatesDataset{} but perform much worse on \WebQuestionsDataset{}.
We surmise this is because questions in \CaiYatesDataset{} were generated by annotators
prompted by Freebase facts, whereas questions in \WebQuestionsDataset{} originated independently of Freebase.
Thus, word choice in \CaiYatesDataset{}
is often close to the generated Freebase descriptions, allowing simple baselines to perform well.

\paragraph{Error analysis}

We sampled examples from the development set to examine the main reasons \textsc{ParaSempre} makes errors. We notice
that in many cases the paraphrase model can be further improved. For example, \textsc{ParaSempre} suggests that 
the best paraphrase for \nl{What company did Henry Ford work for?} is \nl{What written work novel by Henry Ford?} rather than
\nl{The employer of Henry Ford}, due to the exact match of the word \nl{work}. Another example is the question 
\nl{Where is the Nascar hall of fame?}, where \textsc{ParaSempre} suggests that 
\nl{What hall of fame discipline has Nascar hall of fame as halls of fame?} is the best canonical utterance. This is because
our simple model allows to associate \nl{hall of fame} with the canonical utterance three times. Entity recognition also accounts
for many errors, e.g., the entity chosen in \nl{where was the gallipoli campaign waged?} is \wl{Galipoli} and not \wl{GalipoliCampaign}. Last, \textsc{ParaSempre}
does not handle temporal information, which causes errors in questions like \nl{Where did Harriet Tubman live after the civil war?}
