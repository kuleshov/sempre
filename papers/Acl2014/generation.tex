\section{Canonical utterance construction} \label{sec:generation}

We construct canonical utterances in two steps.
Given an input utterance $x$,
we first construct a set of logical forms $\sZ_x$, and then
generate canonical utterances from each $z \in \sZ_x$.
Both steps are performed
with a small and simple set of deterministic rules,
which suffices for our datasets,
as they consist of factoid questions with a modest amount of compositional
structure.
We describe these rules below for completeness.
Due to its soporific effect though,
we advise the reader to skim it quickly.

\paragraph{Candidate logical forms}

\begin{table*}[t]
{\footnotesize
\hfill{}
\begin{tabular}{p{0.2cm}|p{2.3cm}|p{7cm}|p{4.5cm}}
\footnotesize{\textbf{\#}} & \footnotesize{\textbf{Template}} & \footnotesize{\textbf{Example}} & \footnotesize{\textbf{Question}} \\
\hline
1 & $p.e$ & \wl{Directed.TopGun} & \footnotesize{\emph{Who directed Top Gun?}} \\
2  & $p_1.p_2.e$ & \wl{Employment.EmployerOf.SteveBalmer} & \footnotesize{\emph{Where does Steve Balmer work?}} \\
3 & $p.(p_1.e_1 \sqcap p_2.e_2)$ & \wl{Character.(Actor.BradPitt $\sqcap$ Film.Troy)} & \footnotesize{\emph{Who did Brad Pitt play in Troy?}} \\
4 & $\wl{Type}.t \sqcap z$ & \wl{Type.Composer $\sqcap$ SpeakerOf.French} & \footnotesize{\emph{What composers spoke French?}} \\
5 & \wl{count}$(z)$ & \wl{count(BoatDesigner.NatHerreshoff)} & \footnotesize{\emph{How many ships were designed by Nat Herreshoff?}} \\
\end{tabular}}
\hfill{}
\caption{\footnotesize{
Logical form templates, where $p,p_1,p_2$ are Freebase properties, $e,e_1,e_2$ are Freebase entities, $t$ is a
Freebase type, and $z$ is a logical form.}
}
\label{tab:templates}
\end{table*}

% Set of logical forms
We consider logical forms
defined by a set of templates, summarized in \reftab{templates}.
The basic template is a join of a binary and an entity,
where a binary can either be one property $p.e$  (\#1 in the table) or two properties $p_1.p_2.e$ (\#2). To handle cases of events involving multiple arguments 
(e.g., \nl{Who did Brad Pitt play in Troy?}), we introduce the template $p.(p_1.e_1 \sqcap p_2.e_2)$ (\#3), where the main event
is modified by more than one entity. Logical forms
can be further modified by a unary ``filter", e.g.,  the answer to \nl{What composers spoke French?} is a set
of composers, i.e., a subset of all people (\#4).  Lastly, we handle
aggregation formulas for utterances such as \nl{How many teams are in the
NCAA?} (\#5). 

% High-level
To construct candidate logical forms $\sZ_x$ for a given
utterance $x$, our strategy is to find an entity in $x$
and grow the logical form from that entity.
As we show later, this procedure actually produces a
set with better coverage than constructing logical forms recursively from spans of $x$,
as is done in traditional semantic parsing.
%we look for a cue (an entity description) and then use
%templates as a top-down prior to guide candidate set construction.
Specifically, for every span of $x$,
we take at most 10 entities whose Freebase descriptions approximately match the
span.
%using a Lucene index over Freebase.
Then, we join each entity
$e$ with all type-compatible\footnote{Entities in Freebase are associated with
a set of types, and 
properties have a type signature $(t_1,t_2)$
%specifying the expected types of its arguments.
We use these types to compute an expected type $t$ for any logical form $z$.
}
binaries $b$, and add these logical forms to $\sZ_x$ (\#1 and \#2). 

To construct logical forms with multiple entities (\#3) we do the following:
For any logical form $z=p.p_1.e_1$, where $p_1$ has type signature $(t_1,*)$, 
we look for other entities $e_2$ that were matched in $x$. Then, we add the
logical form $p.(p_1.e_1 \sqcap p_2.e_2)$,
if there exists a binary $p_2$ with a compatible type signature $(t_1,t_2)$, where $t_2$ is one of $e_2$'s types.
For example, for the logical form
\wl{Character.Actor.BradPitt}, if we match the entity \wl{Troy} in $x$, we obtain
\wl{Character.(Actor.BradPitt $\sqcap$ Film.Troy)}. We further modify 
 logical forms by intersecting with a unary filter (\#4): given
a formula $z$ with some Freebase type (e.g., \wl{People}), 
we look at all Freebase sub-types $t$ (e.g., \wl{Composer}), and check whether
one of their Freebase descriptions (e.g., \nl{composer}) appears in $x$. 
If so, we add the formula $\wl{Type}.t \sqcap z$ to $\sZ_x$. Finally, we check whether $x$
is an aggregation formula by identifying whether it
starts with phrases such as \nl{how many} or \nl{number of} (\#5). 

On \WebQuestionsDataset{}, this results in 645 formulas per utterance on average. 
Clearly, we can increase the expressivity of this step by expanding the
template set.
For example,
we could handle superlative utterances (\nl{What NBA player
is tallest?}) by adding a template with an \wl{argmax} operator.

\paragraph{Utterance generation}

\begin{table*}[t]
{\footnotesize
\hfill{}
\begin{tabular}{p{0.8cm}|p{1.7cm}|p{4cm}|p{7.6cm}}
& \footnotesize{$\boldsymbol{d(p)}$ \textbf{Categ.}} & \footnotesize{\textbf{Rule}} & \footnotesize{\textbf{Example}} \\
\hline
$p.e$ & \textsc{NP} & \texttt{WH} $d(t)$ \texttt{has} $d(e)$ \texttt{as} \textsc{NP} ?& \emph{What \textbf{election contest} has \textbf{George Bush} as \textbf{winner?}}\\
& \textsc{VP} & \texttt{WH} $d(t)$ \texttt{(AUX)} \textsc{VP} $d(e)$ ?& \emph{What \textbf{radio station} \textbf{serves area} \textbf{New-York}?}\\
& \textsc{PP} & \texttt{WH} $d(t)$ \textsc{PP} $d(e)$ ? & \emph{What \textbf{beer} \textbf{from region Argentina}?} \\
& \textsc{NP VP} & \texttt{WH} $d(t)$ \textsc{VP} \texttt{the} \textsc{NP} $d(e)$ ? & \emph{What \textbf{mass transportation system} \textbf{served} the \textbf{area} \textbf{Berlin}?}\\
\hline
$\bR(p).e$ & \textsc{NP} & \texttt{WH} $d(t)$ \texttt{is the} \textsc{NP} \texttt{of} $d(e)$ ? & \emph{What \textbf{location} is the \textbf{place of birth} of \textbf{Elvis Presley}?}\\
& \textsc{VP} & \texttt{WH} $d(t)$ \texttt{AUX} $d(e)$ \textsc{VP} ?& \emph{What \textbf{film} is \textbf{Brazil} \textbf{featured in}?}\\
& \textsc{PP} & \texttt{WH} $d(t)$ $d(e)$ \textsc{PP} ? & \emph{What \textbf{destination} \textbf{Spanish steps} \textbf{near travel destination}?}\\
& \textsc{NP VP} & \texttt{WH} \textsc{NP} \texttt{is} \textsc{VP} \texttt{by} $d(e)$ ? & \emph{What \textbf{structure} is \textbf{designed} by Herod?} \\
\end{tabular}}
\hfill{}
\caption{
\footnotesize{
Generation rules for templates of the form $p.e$ and $\bR[p].e$ 
based on the syntactic category of the property description. Freebase descriptions for the type, entity, and property are denoted by
$d(t)$, $d(e)$ and $d(p)$ respectively. The surface form of the auxiliary \texttt{AUX} is determined by the POS tag of the verb inside the \textsc{VP} tree.}
}
\label{tab:rules}
\end{table*}

While mapping general language utterances to logical forms is hard,
we observe that it is much easier to
generate a \emph{canonical} natural language utterances of our choice
given a logical form.
%using a few simple rules that exploit the typical structure
%of web questions.
\reftab{rules} summarizes the rules used to generate canonical utterances from the template
$p.e$. Questions begin with a question word, are followed by the Freebase
description of the expected answer type ($d(t)$), and followed by Freebase
descriptions of the entity ($d(e)$) and binary ($d(p)$).
To fill in auxiliary verbs, determiners, and prepositions,
we parse the description $d(p)$ into one of
\textsc{NP},
\textsc{VP},
\textsc{PP},
or \textsc{NP VP}.
This determines the generation rule to be used.
%We now elaborate how to choose the generation rule according
%to \reftab{rules}, and explain how we generalize to other templates.

%Thus, to generate natural canonical utterances we take into 
%account the logical form, the Freebase
%description of its predicates, and the syntactic parse tree of the Freebase description of the binary 
%properties. We now describe the process of utterance generation.
%This typical structure can be summarized
%using the following \emph{skeleton} forms that take into account the variability in the order of 
%binary and entity descriptions:
%\begin{itemize} [noitemsep,topsep=0pt]
%\item[(a)] \texttt{WH T E$_1$ B (in E$_2$)}
%\item[(b)] \texttt{WH T B E$_1$ (in E$_2$)}
%\end{itemize}

% Reversing
Each Freebase property $p$ has an explicit property $p'$ equivalent to the reverse $\bR[p]$
(e.g., \wl{ContainedBy} and $\bR[\wl{Contains}]$).
For each logical form $z$, we also generate using equivalent logical forms
where $p$ is replaced with $\bR[p']$.
%we use \emph{reversing} to generate an equivalent logical form.
Reversed formulas have different generation rules,
since entities in these formulas are in the subject position rather than object position.

We generate the description $d(t)$ from the Freebase description of the type of
$z$ (this handles \#4).
For the template $p_1.p_2.e$ (\#2), we have a similar set of rules, which
depends on the syntax of $d(p_1)$ and $d(p_2)$ and is omitted for brevity. 
The template $p.(p_1.e_1 \sqcap p_2.e_2)$ (\#3) is generated by
appending the prepositional phrase \texttt{in $d(e_2)$}, 
e.g, \nl{What character is the character of Brad Pitt in Troy?}.
Lastly, we choose the question phrase \nl{How many}
for aggregation formulas (\#5), and \nl{What} for all other formulas.

% Lexicon
We also generate canonical utterances using an alignment lexicon,
released by \newcite{berant2013freebase}, which maps text phrases to Freebase
binary predicates. For a binary predicate $b$
mapped from text phrase $d(b)$, we generate the utterance 
\texttt{WH $d(t)$ $d(b)$ $d(e)$ ?}.  On the \WebQuestionsDataset{} dataset,
we generate an average of 1,423 canonical
utterances $c$ per input utterance $x$.
In \refsec{experiments}, we show that an even simpler
method of generating canonical utterances by concatenating Freebase
descriptions hurts accuracy by only a modest amount.
