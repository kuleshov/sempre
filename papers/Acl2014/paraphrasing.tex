\section{Paraphrasing} \label{sec:paraph}

Once the candidate set of logical forms paired with canonical utterances is constructed,
our problem is reduced to scoring pairs $(c,z)$ based on a paraphrase model.
The NLP paraphrase literature is vast and ranges from simple methods employing surface
features \cite{wan2006dependency}, through vector space models \cite{socher2011paraphrase}, to 
latent variable models \cite{das2009paraphrase,wang2010paraphrasing,stern2011transformation}.

In this paper, we focus on two paraphrase models that emphasize simplicity and efficiency.
This is important since for each question-answer pair,
we consider thousands of canonical utterances as potential paraphrases.
%and over the course of training end up scoring millions of potential paraphrases.
In contrast, traditional paraphrase detection
\cite{dolan2004unsupervised} and Recognizing Textual Entailment (RTE) tasks
\cite{dagan2013rte} consider examples consisting of only a single pair of
candidate paraphrases.

Our paraphrase model decomposes into an \emph{association model} and a 
\emph{vector space model}:
\begin{align*}
\phi_{\text{pr}}(x,c)^\top \theta_{\text{pr}} = \phi_\text{as}(x,c)^\top \theta_\text{as}  + \phi_\text{vs}(x,c)^\top \theta_\text{vs}.
\end{align*}

\subsection{Association model} \label{subsec:association}

\FigTop{figures.slides/associationExample2}{0.30}{associationExample}{
\footnotesize{
Token associations extracted for a paraphrase pair. Blue and dashed (red and solid)
indicate positive (negative) score.  Line width is proportional
to the absolute value of the score.
}
}

% Association
The goal of the association model is to determine whether $x$ and $c$
contain phrases that are likely to be paraphrases.
Given an utterance $x=\langle x_0,x_1,..,x_{n-1}\rangle$, we denote by $x_{i:j}$ the
span from token $i$ to token $j$. For each pair of utterances $(x,c)$,
we go through all spans of $x$ and $c$ and identify
a set of pairs 
of potential paraphrases $(x_{i:j},c_{i':j'})$,
which we call \emph{associations}.
(We will describe how associations are identified shortly.)
We then define features on each association;
the weighted combination of these features yields a score.
In this light, associations can be viewed as soft paraphrase rules.
\reffig{associationExample} presents examples of associations extracted
from a paraphrase pair and visualizes the learned scores.
We can see that our model
learns a positive score for associating \nl{type} with \nl{genres}, 
and a negative score for associating \nl{is} with \nl{play}.

We define associations in $x$ and $c$ primarily by looking up phrase
pairs in a phrase table constructed using the \Paralex{} corpus
\cite{fader2013paraphrase}.
\Paralex{} is a large monolingual parallel corpora,
containing 18 million pairs of question paraphrases from \texttt{wikianswers.com},
which were tagged as having the same meaning by users.
\Paralex{} is suitable for our needs
since it focuses on \emph{question} paraphrases.
For example, the phrase \nl{do for a living}
occurs mostly in questions, and we can extract associations for this phrase from \Paralex. 
Paraphrase pairs in \Paralex{} are word-aligned using standard machine translation methods.
We use the word alignments to construct a phrase table
by applying the consistent phrase pair heuristic \cite{och2004alignment}
to all 5-grams.
This results in a phrase table with
approximately 1.3 million phrase pairs.
We let $\sA$ denote this set of mined candidate associations.

\begin{table}[t]
{\footnotesize
\hfill{}
\begin{tabular}{|p{1.2cm}|p{6cm}|}
\hline
\textbf{Category} &\textbf{Description} \\
\hline
\hline
Assoc.
  & $\text{lemma}(x_{i:j}) \land  \text{lemma}(c_{i':j'})$ \\
  & $\text{pos}(x_{i:j}) \land  \text{pos}(c_{i':j'})$ \\
  & $\text{lemma}(x_{i:j}) = \text{lemma}(c_{i':j'})$?  \\
  & $\text{pos}(x_{i:j})  =  \text{pos}(c_{i':j'})$? \\
  & $\text{lemma}(x_{i:j})$ and $\text{lemma}(c_{i':j'})$ are synonyms? \\
  & $\text{lemma}(x_{i:j})$ and $\text{lemma}(c_{i':j'})$ are derivations? \\
\hline
Deletions
  & Deleted lemma and POS tag \\
\hline
\end{tabular}}
\hfill{}
\caption{\footnotesize{
  Full feature set in the association model. $x_{i:j}$ and $c_{i':j'}$ denote spans from $x$ and $c$.
  $\text{pos}(x_{i:j})$ and $\text{lemma}(x_{i:j})$ denote the POS tag and lemma sequence of $x_{i:j}$.
}}
\label{tab:features}
\end{table}

For a pair $(x,c)$, we also consider as candidate associations the set $\sB$ (represented implicitly),
which contains token pairs $(x_i,c_{i'})$ such that $x_i$ and $c_{i'}$ share the
same lemma, the same POS tag, or are linked through a \emph{derivation} link on WordNet \cite{fellbaum1998wordnet}.
This allows us to learn paraphrases for words that appear in our datasets but are not covered
by the phrase table, and to handle nominalizations for phrase pairs such as \nl{Who designed the
game of life?} and \nl{What game designer is the designer of the game of life?}.

Our model goes over all possible spans of $x$ and $c$ and constructs all possible associations from $\sA$ and $\sB$. 
This results in many poor associations (e.g., \nl{play} and \nl{the}),
but as illustrated in \reffig{associationExample},
we learn weights that discriminate good from bad associations.
\reftab{features} specifies the full set of features.
Note that unlike standard paraphrase detection
and RTE systems, we use lexicalized features,
firing approximately 400,000 features on \WebQuestionsDataset{}.
By extracting POS features, we obtain soft syntactic rules, e.g., the feature 
``\texttt{JJ N} $\land$ \texttt{N}" indicates that omitting adjectives before nouns is possible. Once associations are constructed, we mark tokens in $x$ and $c$ that were
not part of any association, and extract deletion features for their lemmas and POS tags. Thus, we learn that 
deleting pronouns is acceptable, while deleting nouns is not.

To summarize, the association model links phrases of two utterances in multiple overlapping ways.
During training, the model learns which associations are characteristic of
paraphrases and which are not.

\subsection{Vector space model} \label{subsec:vsm}

The association model relies on having a good set of candidate associations,
%imposes constraints on which phrases that can be paired with one another,
%which can lead to coverage issues.
but mining associations suffers from coverage issues.
We now introduce a vector space (VS) model,
which assigns a vector representation for each utterance, and learns a scoring
function that ranks paraphrase candidates.

% words, phrases
We start by constructing vector representations of words.
We run the \textsc{word2vec} tool \cite{mikolov2013efficient} on lower-cased Wikipedia text
(1.59 billion tokens), using the CBOW model with a window of 5 and hierarchical softmax.
We also experiment with publicly released word embeddings
\cite{huang2012wordrep}, which were trained using both local and global
context.  Both result in $k$-dimensional vectors ($k=50$).
Next, we construct a vector $v_x \in \R^k$ for each utterance
$x$ by simply averaging the vectors of all content words (nouns,
verbs, and adjectives) in $x$.

%Assume the utterances $x$ and $c$ are represented by vectors $v_x \in \R^k$ and $v_c \in \R^k$, respectively.
We can now estimate a paraphrase score for
two utterances $x$ and $c$ via a weighted combination of the components of the vector representations:
\begin{align*}
v_x^\top W v_c = \sum_{i,j=1}^k w_{ij} v_{x,i} v_{c,j} % = \phi_\text{vs}(x,c)^\top \theta_\text{vs},
\end{align*}
where $W \in \R^{k \times k}$ is a parameter matrix.
In terms of our earlier notation,
we have $\theta_\text{vs} = \text{vec}(W)$ and $\phi_{\text{vs}}(x, c) = \text{vec}(v_x v_c^\top)$,
where $\text{vec}(\cdot)$ unrolls a matrix into a vector.
In \refsec{experiments}, we experiment with $W$ equal to the identity matrix,
constraining $W$ to be diagonal, and learning a full $W$ matrix.
%with $k^2$ parameters.

%848 vs. 918
The VS model can identify correct paraphrases in cases where it is hard to directly associate phrases from $x$ and $c$. For example, the answer to \nl{Where is made Kia car?} (from \WebQuestionsDataset{}), is given
by the canonical utterance \nl{What city is Kia motors a headquarters of?}. The association model does not associate \nl{made}
and \nl{headquarters}, but the VS model is able to determine that these utterances are semantically related.
In other cases, the VS model cannot distinguish correct paraphrases from incorrect ones.
For example, the association model identifies
that the paraphrase for \nl{What type of music did Richard Wagner Play?} is \nl{What is the musical genres of Richard Wagner?}, by relating phrases such as \nl{type of music} and \nl{musical genres}.
The VS model ranks the canonical utterance \nl{What composition has Richard Wagner as lyricist?} higher, as this utterance is also in the music domain.
Thus, we combine the two models to benefit from their complementary nature.

In summary, while the association model aligns particular phrases to one
another, the vector space model provides a soft vector-based representation for
utterances. 
