\section{Introduction}

% PL: don't define semantic parsing too narrowly or we'll regret it
We consider the semantic parsing problem of mapping natural language utterances into 
logical forms to be executed on a knowledge base (KB)
\cite{zelle96geoquery,zettlemoyer05ccg,wong07synchronous,kwiatkowski10ccg}.
Scaling semantic parsers to large knowledge bases 
has attracted substantial attention recently
\cite{cai2013large,berant2013freebase,kwiatkowski2013scaling},
since it drives applications such as question answering (QA) and information
extraction (IE).

\FigTop{figures.slides/paraphraseSchema}{0.3}{paraphraseSchema}{
\footnotesize{
Semantic parsing via paraphrasing:
For each candidate logical form (in red), we generate canonical utterances (in purple).
The model is trained to 
paraphrase the input utterance (in green) into the
canonical utterances associated with the correct denotation (in blue).
}
}

% Potential of text-only
Semantic parsers need to somehow associate natural language phrases with logical
predicates, e.g., they must learn that the constructions
\nl{What does X do for a living?}, \nl{What is X's profession?}, and
\nl{Who is X?}, should all map to the logical predicate \wl{Profession}.
To learn these mappings, 
traditional semantic parsers use data
which pairs natural language with the KB.
%where
%each of these constructions is independently paired with the KB, 
However, this leaves untapped a vast amount of text not related to the KB.
For instance, the utterances 
\nl{Where is ACL in 2014?} and \nl{What is the location of ACL 2014?} cannot
be used in traditional semantic parsing methods, since the KB does not contain
an entity \wl{ACL2014}, but this pair clearly
contains valuable linguistic information.
As another reference point, out of 500,000 relations
extracted by the ReVerb Open IE system \cite{fader11reverb}, only
about 10,000 can be aligned to
Freebase \cite{berant2013freebase}.

%Traditionally, semantic parsers map natural language phrases (\nl{do for a living}) 
%directly to knowledge base (KB) predicates (\wl{Profession}), and 
%then combine the logical predicates
%to form a full logical form. Consequently, 
%the parser needs to handle the various ways in which a certain predicate may be
%expressed. For example, the parser must identify that the questions 
%\nl{What does Whitney 
%Houston do for a living?}, \nl{Who is Whitney Houston?} and \nl{What is Whitney Houston's profession?}
%all express the KB predicate \wl{Profession} in different ways.

% Our proposed method
In this paper, we present a novel approach for semantic parsing based on
paraphrasing
%that focuses on modeling such language variability by
that can exploit large amounts of text not
covered by the KB (\reffig{paraphraseSchema}).
Our approach targets factoid questions with a modest amount of
compositionality.
Given an input utterance,
we first use a simple deterministic procedure
to construct a manageable set of candidate logical forms 
(ideally, we would generate canonical utterances
for all possible logical forms, but this is intractable).
Next, we heuristically generate \emph{canonical utterances} for each logical
form based on the text descriptions of predicates from the KB.
%and requires very little manual effort.
%Effectively, this reduces the problem from
%converting text to logical forms to a text-to-text problem.
Finally, we choose the canonical utterance that best paraphrases the input
utterance, and thereby the logical form that generated it.
We use two complementary
paraphrase models: an \emph{association model} based on
aligned phrase pairs extracted from a monolingual parallel corpus, and a
\emph{vector space model}, which represents each utterance as a vector and
learns a similarity score between them. The entire system is trained jointly
from question-answer pairs only.


\FigTop{figures.slides/textFreebaseMapping}{0.35}{textFreebaseMapping}{
\footnotesize{
The main challenge in semantic parsing is coping with the \emph{mismatch} between
language and the KB.
(a) Traditionally, semantic parsing maps utterances directly to logical forms.
(b) \newcite{kwiatkowski2013scaling} map the utterance to an underspecified logical form,
and perform ontology matching to handle the mismatch.
(c) We approach the problem in the other direction, generating canonical utterances
for logical forms, and use paraphrase models to handle the mismatch.
}
}

% Related work: intermediate logical form, paraphrase for QA
Our work relates to recent lines of research in semantic parsing and question answering.
\newcite{kwiatkowski2013scaling} first maps
utterances to a domain-independent
\emph{intermediate logical form}, and then performs ontology matching to produce
the final logical form.
In some sense, we approach the problem from the opposite end,
using an \emph{intermediate utterance},
which allows us to employ paraphrasing methods (\reffig{textFreebaseMapping}).
\newcite{fader2013paraphrase} presented a QA system
that maps questions onto simple queries against Open IE extractions,
by learning paraphrases from a large monolingual parallel corpus, and
performing a single paraphrasing step.
We adopt the idea of using paraphrasing
for QA, but suggest a more general paraphrase model and work against a formal
KB (Freebase).

% Empirical results
We apply our semantic parser on two datasets: \WebQuestionsDataset{} 
\cite{berant2013freebase}, which contains
5,810 question-answer pairs with common questions
asked by web users; and \CaiYatesDataset{} \cite{cai2013large},
which has 917 questions manually authored by annotators.
On \WebQuestionsDataset{}, we obtain a relative improvement of 12\% in accuracy over
the state-of-the-art, and on \CaiYatesDataset{}
we match the current best performing system.
The source code of our system \textsc{ParaSempre} is released at 
\url{http://www-nlp.stanford.edu/software/sempre/}.
