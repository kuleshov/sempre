// For node.js
require('./sfig/internal/sfig.js');
require('./sfig/internal/Graph.js');
require('./sfig/internal/RootedTree.js');
require('./sfig/internal/metapost.js');
require('./sfig/internal/seedrandom.js');
require('./utils.js');

Text.defaults.setProperty('font', 'Times New Roman');  // For the paper

prez = sfig.presentation();
prez.addSlide(textFreebaseMapping().id('textFreebaseMapping'));
prez.addSlide(paraphraseSchema().id('paraphraseSchema'));
prez.addSlide(associationExample().id('associationExample'));
prez.addSlide(associationExample2().id('associationExample2'));
prez.addSlide(vsm().id('vsm'));

// For node.js: output PDF (you must have Metapost installed).
prez.writePdf({outPrefix: 'figures', combine: false});
