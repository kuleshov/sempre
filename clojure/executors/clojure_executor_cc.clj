(defn execute []
  (reduce 

    ;; to simplify denotations produced, assume exchange rate of 0.1 points -> dollars
    #(+ %1 (if (= (:currency %2) :$) (:amount %2) (* 0.1 (:amount %2))))
    0
    (let [events [{:time 1 :currency :$ :amount -10 :type :purchase :location nil}
                  {:time 2 :currency :$ :amount -10 :type :purchase :location :gas}
                  {:time 3 :currency :$ :amount -10 :type :purchase :location :restaurant}]]
      (loop [es events t 0]
        (if (< t 5) (recur (concat es "(var body)") (inc t)) es)))))
    
