#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""(For Trivia Question task)
Count the number of completed sentences.

Used to be useful when not all assignments were finished
before the expiration date, and I wanted to publish 
new HITs (with new web page design) only for the remaining assignments.

But now since the web interface is now stable, I just have to
extend the expiration date of the old HIT batch.
This script is not used anymore.
"""

import sys, os, shutil, re, argparse, json
from codecs import open
from itertools import izip
from collections import defaultdict

def process(counts, data, batch_index=None):
    for datum in data:
        if batch_index:
            assert int(datum['answers']['batchIndex']) == batch_index
        key = int(datum['answers']['dataIndex'])
        counts[key] += sum(x[0] == 'a' and x[1].isdigit() for x in datum['answers'])

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('infiles', nargs='+')
    parser.add_argument('-s', '--subtract', type=int,
                        help='return SUBTRACT - count instead')
    parser.add_argument('-d', '--divide', type=int,
                        help='divide the result by DIVIDE (round up)')
    parser.add_argument('-b', '--batch-index', type=int,
                        help='check batch index')
    parser.add_argument('-i', '--index', action='store_true',
                        help='also print index')
    parser.add_argument('-r', '--range', nargs=2, type=int,
                        help='range of data indices to print (inclusive)')
    args = parser.parse_args()

    counts = defaultdict(int)
    for infile in args.infiles:
        with open(infile, 'r', 'utf8') as fin:
            process(counts, json.load(fin), batch_index=args.batch_index)

    if args.range:
        target = xrange(args.range[0], args.range[1] + 1)
    else:
        target = sorted(counts)

    for key in target:
        value = counts[key]
        if args.subtract:
            value = max(0, args.subtract - value)
        if args.divide:
            value = (value + args.divide - 1) / args.divide
        if args.index:
            print '%d\t%d' % (key, value)
        else:
            print '%d' % value

if __name__ == '__main__':
    main()

