#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Get random samples from the specified tasks."""

import sys, os, shutil, re, argparse, json, random
from codecs import open
from itertools import izip
from collections import defaultdict, OrderedDict

def get_only_approved(results):
    return [x for x in results if x['metadata']['AssignmentStatus'] == 'Approved']

def get_trivia(results):
    for x in results:
        i = 1
        while ('a%d' % i) in x['answers']:
            yield OrderedDict([
                    ('url', x['answers']['url']),
                    ('title', x['answers']['title']),
                    ('tableid', x['answers']['tableid']),
                    ('batchIndex', x['answers']['batchIndex']),
                    ('dataIndex', x['answers']['dataIndex']),
                    ('question', x['answers']['a%d' % i]),
                    ])
            i += 1

TASKS = {'trivia': get_trivia,
         }

def dump_json(stuff, out):
    json.dump(stuff, out, ensure_ascii=False, indent=2, separators=(',', ': '))

def dump_tsv(stuff, out):
    KEYS = stuff[0].keys()
    print >> out, '\t'.join(KEYS)
    for x in stuff:
        print >> out, '\t'.join(x[key] for key in KEYS)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--task-type', default='trivia',
                        help='task type')
    parser.add_argument('-j', '--json', action='store_true',
                        help='print out json instead of tsv')
    parser.add_argument('-s', '--seed', type=int, default=42,
                        help='seed for randomization')
    parser.add_argument('-a', '--amount', type=int, default=100,
                        help='amount of answers to output')
    parser.add_argument('-o', '--output',
                        help='output file')
    parser.add_argument('batches', nargs='+')
    args = parser.parse_args()
    
    assert args.task_type in TASKS
    stuff = []
    for batch in args.batches:
        filename = os.path.join('data', batch, batch + '.results')
        with open(filename, 'r', 'utf8') as fin:
            results = get_only_approved(json.load(fin))
            stuff.extend(get_trivia(results))
    print >> sys.stderr, 'Read %d answers' % len(stuff)
    random.seed(args.seed)
    stuff = random.sample(stuff, min(args.amount, len(stuff)))
    print >> sys.stderr, 'Sampled %d answers' % len(stuff)
    dumper = dump_json if args.json else dump_tsv
    if args.output:
        with open(args.output, 'w', 'utf8') as fout:
            dumper(stuff, fout)
    else:
        dumper(stuff, sys.stdout)

if __name__ == '__main__':
    main()

