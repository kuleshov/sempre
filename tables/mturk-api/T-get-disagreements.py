#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Dump disagreement in JSON files."""

import sys, os, shutil, re, argparse, json
from codecs import open
from itertools import izip
from collections import defaultdict

def get_key(answers, metadata, i):
    return (answers['batchIndex'], answers['dataIndex'], i)

def get_qa(answers, metadata, i):
    return {
            'q': answers['q%d' % i],
            'a': answers['a%d' % i],
            'c': answers['c%d' % i],
            'batchIndex': answers['batchIndex'],
            'dataIndex': answers['dataIndex'],
            'i': i,
            'hash': answers['hash'],
            'pageid': answers['pageid'],
            'tableid': answers['tableid'],
            'title': answers['title'],
            'url': answers['url'],
            'srcBatchIndex': answers['srcBatchIndex'],
            'srcDataIndex': answers['srcDataIndex'],
            'workerid': metadata['WorkerId'],
            }

def normalize(x):
    return re.sub('[^a-z0-9]', '', x.lower())

def is_disagreement(question):
    answers = [normalize(x['a']) for x in question]
    return len(set(answers)) == len(answers)

def dump_json(data, fout):
    json.dump(data, fout,
              ensure_ascii=False, indent=2, separators=(',', ': '))
    fout.write('\n')

def question_to_hit(question):
    q = question[0]
    return {
            'srcBatchIndex': q['srcBatchIndex'],
            'srcDataIndex': q['srcDataIndex'],
            'oldAnsBatchIndex': q['batchIndex'],
            'oldAnsDataIndex': q['dataIndex'],
            'prompt': q['c'],
            'question': q['q'],
            'title': q['title'],
            'url': q['url'],
            'id': q['pageid'],
            'tableIndex': q['tableid'],
            'hashcode': q['hash'],
            }

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('infiles', nargs='+')
    parser.add_argument('-o', '--outfile')
    parser.add_argument('-O', '--outdir')
    parser.add_argument('-m', '--num-per-hit', type=int, default=4)
    args = parser.parse_args()

    questions = defaultdict(list)
    for filename in args.infiles:
        print >> sys.stderr, 'Reading', filename
        with open(filename, 'r', 'utf8') as fin:
            data = json.load(fin)
            for datum in data:
                metadata = datum['metadata']
                answers = datum['answers']
                if metadata['AssignmentStatus'] != 'Approved':
                    continue
                for i in (0, 1, 2, 3, 4, 5):
                    if 'a%d' % i not in answers:
                        continue
                    key = get_key(answers, metadata, i)
                    questions[key].append(get_qa(answers, metadata, i))
    print >> sys.stderr, 'Read', len(questions), 'questions'

    # Find disagreement
    disagreements = []
    for question in questions.values():
        if is_disagreement(question):
            disagreements.append(question)
    print >> sys.stderr, 'Found', len(disagreements), 'disagreements'

    # Dump
    if args.outfile:
        with open(args.outfile, 'w', 'utf8') as fout:
            dump_json(disagreements, fout)
    if args.outdir:
        assert not os.path.exists(args.outdir)
        os.makedirs(args.outdir)
        count = 0
        import random
        random.seed(42)
        random.shuffle(disagreements)
        for i in xrange(0, len(disagreements), args.num_per_hit):
            hit = []
            for question in disagreements[i:i+args.num_per_hit]:
                hit.append(question_to_hit(question))
            filename = '%d.json' % count
            with open(os.path.join(args.outdir, filename), 'w', 'utf8') as fout:
                dump_json(hit, fout)
            count += 1
        print >> sys.stderr, 'Dumped', count, 'JSONs'

if __name__ == '__main__':
    main()

