#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Separate data into 5 chunks:

Normal tables:
- normal-pristine-unseen-tables: U% of the tables
- normal-pristine-seen-tables: S% of the remaining data
- normal-training: the rest

Exemplar tables:
- exemplar-pristine: S% of the data (mixed tables)
- exemplar-training: the rest
"""

import sys, os, shutil, re, argparse, json, random
from codecs import open
from itertools import izip
from collections import defaultdict
from hashlib import sha1

def dump(data, filename):
    with open(filename, 'w', 'utf8') as fout:
        json.dump(data, fout,
            ensure_ascii=False, indent=2,
            separators=(',', ': '), sort_keys=True)
        fout.write('\n')
    print >> sys.stderr, 'Wrote', len(data), 'records to', filename

def get_signature(datum):
    """Return a unique signature of each data example."""
    return (
        (datum['srcBatchIndex'], datum['srcDataIndex']),
        sha1(datum['question'].encode('utf8')).hexdigest(),
        )

def get_num_tables(data):
    return len(set(signature[0] for signature, datum in data))

def encode(data, prefix):
    # Put the signature in the example dict instead of outside
    encoded = []
    for i, (signature, datum) in enumerate(data):
        datum = dict(datum)
        datum['index'] = '%s-%d' % (prefix, i)
        datum['signature'] = '%d-%d-%s' % (signature[0][0], signature[0][1], signature[1])
        encoded.append(datum)
    return encoded

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('infile')
    parser.add_argument('outprefix')
    parser.add_argument('-u', '--unseen-percent', type=float, default=20,
                        help="percent of data to be used in pristine-unseen-tables")
    parser.add_argument('-s', '--seen-percent', type=float, default=20,
                        help="percent of remaining data to be used in pristine-seen-tables")
    args = parser.parse_args()

    with open(args.infile, 'r', 'utf8') as fin:
        data = json.load(fin)
    data = [(get_signature(datum), datum) for datum in data]
    print >> sys.stderr, 'Read', len(data), 'examples'

    random.seed(42)
    
    counts = defaultdict(int)
    for signature, datum in data:
        counts[signature[0]] += 1
    exemplars = set(x for x in counts if counts[x] >= 40)
    assert all(x[0] == 300 for x in exemplars) and len(exemplars) == 15
    non_exemplars = set(x for x in counts if counts[x] < 40)

    ################################################
    # Normal (non-exemplar) tables
    print >> sys.stderr, 'Number of normal tables:', len(non_exemplars)
    
    # normal-pristine-unseen-tables
    num_unseen_tables = args.unseen_percent * len(non_exemplars) / 100
    unseen_table_ids = set(random.sample(non_exemplars, num_unseen_tables))
    unseen_normal, rest_normal, rest_exemplar = [], [], []
    for signature, datum in data:
        if signature[0] in unseen_table_ids:
            unseen_normal.append((signature, datum))
        elif signature[0] in non_exemplars:
            rest_normal.append((signature, datum))
        else:
            rest_exemplar.append((signature, datum))
    print >> sys.stderr, 'normal-pristine-unseen-tables:'
    print >> sys.stderr, '    number of examples:', len(unseen_normal)
    print >> sys.stderr, '    number of tables:', get_num_tables(unseen_normal)

    # normal-pristine-seen-tables / normal-training
    random.shuffle(rest_normal)
    num_seen_examples = args.seen_percent * len(rest_normal) / 100
    training_normal, pristine_normal = rest_normal[:-num_seen_examples], rest_normal[-num_seen_examples:]
    print >> sys.stderr, 'normal-pristine-seen-tables:'
    print >> sys.stderr, '    number of examples:', len(pristine_normal)
    print >> sys.stderr, '    number of tables:', get_num_tables(pristine_normal)
    print >> sys.stderr, 'normal-training:'
    print >> sys.stderr, '    number of examples:', len(training_normal)
    print >> sys.stderr, '    number of tables:', get_num_tables(training_normal)

    # exemplar-pristine / exemplar-training
    random.shuffle(rest_exemplar)
    num_seen_examples = args.seen_percent * len(rest_exemplar) / 100
    training_exemplar, pristine_exemplar = rest_exemplar[:-num_seen_examples], rest_exemplar[-num_seen_examples:]
    print >> sys.stderr, 'exemplar-pristine:'
    print >> sys.stderr, '    number of examples:', len(pristine_exemplar)
    print >> sys.stderr, '    number of tables:', get_num_tables(pristine_exemplar)
    print >> sys.stderr, 'exemplar-training:'
    print >> sys.stderr, '    number of examples:', len(training_exemplar)
    print >> sys.stderr, '    number of tables:', get_num_tables(training_exemplar)


    # Dump to files
    dump(encode(unseen_normal, 'nu'), args.outprefix + '-normal-pristine-unseen-tables.json')
    dump(encode(pristine_normal, 'ns'), args.outprefix + '-normal-pristine-seen-tables.json')
    dump(encode(training_normal, 'nt'), args.outprefix + '-normal-training.json')
    dump(encode(pristine_exemplar, 'xs'), args.outprefix + '-exemplar-pristine.json')
    dump(encode(training_exemplar, 'xt'), args.outprefix + '-exemplar-training.json')
if __name__ == '__main__':
    main()

