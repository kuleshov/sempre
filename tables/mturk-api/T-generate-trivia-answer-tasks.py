#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, re, argparse, json, random
from codecs import open
from collections import defaultdict

ID_REGEX = re.compile('curid=([0-9]+)&oldid=([0-9]+)$')

def make_mapping(data):
    """Make maps from (srcBatchIndex, srcDataIndex)
    to metadata (dict) and questions (list of strings)"""
    metadata = dict()
    question_map = dict()
    for datum in data:
        srcBatchIndex = int(datum['answers']['batchIndex'])
        srcDataIndex = int(datum['answers']['dataIndex'])
        key = (srcBatchIndex, srcDataIndex)
        if key not in metadata:
            id_, revision = ID_REGEX.search(datum['answers']['url']).group(1,2)
            metadata[key] = {
                'title': datum['answers']['title'],
                'url': datum['answers']['url'],
                'tableIndex': int(datum['answers']['tableid']),
                'hashcode': datum['answers']['hash'],
                'id': int(id_),
                'revision': int(revision),
                'srcBatchIndex': srcBatchIndex,
                'srcDataIndex': srcDataIndex
            }
            question_map[key] = []
        questions = []
        for a in ['a0', 'a1', 'a2', 'a3', 'a4', 'a5']:
            try:
                c = a.replace('a', 'c')
                questions.append((datum['answers'][a], datum['answers'].get(c, '(none)')))
            except LookupError:
                pass
        if len(set(x[0] for x in questions)) > 2:   # Avoid edge cases
            question_map[key].extend(questions)
    return metadata, question_map
    

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--outdir')
    parser.add_argument('-n', type=int, default=3,
                        help='number of questions per json file')
    parser.add_argument('-s', '--start-index', type=int, default=0)
    parser.add_argument('-m', '--minus', nargs='+',
                        help='do not include tasks from these directories')
    parser.add_argument('batchid', nargs='+')
    args = parser.parse_args()

    data = []
    for x in args.batchid:
        with open(os.path.join('data', x, x + '.results'), 'r', 'utf8') as fin:
            for datum in json.load(fin):
                if datum['metadata']['AssignmentStatus'] == 'Approved':
                    data.append(datum)
    print len(data), 'entries loaded'
    
    metadata, question_map = make_mapping(data)

    # Print statistics
    print len(metadata), 'uniqued entries'
    print sum(len(x) for x in question_map.itervalues()), 'total questions'
    counts = defaultdict(int)
    for key, x in question_map.iteritems():
        counts[key[0], len(x)] += 1
        if len(x) == 11:
            print x
    for (batch_id, length), count in sorted(counts.items()):
        print 'batch %d has %d entries with %d questions' % (batch_id, count, length)

    # Do not include tasks from these directories
    if args.minus:
        for dirname in args.minus:
            for x in os.listdir(dirname):
                with open(os.path.join(dirname, x), 'r', 'utf8') as fin:
                    old_data = json.load(fin)
                key = (old_data['srcBatchIndex'], old_data['srcDataIndex'])
                if key not in question_map:
                    print >> sys.stderr, 'Warning: key (%d, %d) not found' % key
                    continue
                for y in old_data['questions']:
                    assert any(z[0] == y for z in question_map[key])
                    question_map[key] = [z for z in question_map[key] if z[0] != y]
        print len(metadata), 'uniqued entries (after minus)'
        print sum(len(x) for x in question_map.itervalues()), 'total questions (after minus)'

    if args.outdir:
        if os.path.exists(args.outdir):
            print >> sys.stderr, 'ERROR: %s exists' % args.outdir
            exit(1)
        os.mkdir(args.outdir)
        index = args.start_index
        for key in metadata:
            stuff_to_dump = metadata[key]
            questions = list(question_map[key])
            remainder = len(questions) % args.n
            if remainder != 0:
                questions.extend(questions[:args.n - remainder])
            for i in xrange(0, len(questions), args.n):
                stuff_to_dump['questions'] = [x[0] for x in questions[i: i + args.n]]
                stuff_to_dump['prompts'] = [x[1] for x in questions[i: i + args.n]]
                filename = os.path.join(args.outdir, str(index) + '.json')
                with open(filename, 'w', 'utf8') as fout:
                    json.dump(stuff_to_dump, fout)
                index += 1
        print 'Wrote', index - args.start_index, 'files to', args.outdir

if __name__ == '__main__':
    main()
