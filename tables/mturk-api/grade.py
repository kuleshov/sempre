#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Utilities for processing MTurk results.
Assume 1 question per HIT.
For batch 1 (5 questions per HIT), use -f flag.

Usage: ./grade-single.py [RESULTS_FILE]
where RESULTS_FILE is usually data/[BATCHNAME]/[BATCHNAME].results
(data/[BATCHNAME]/ also works)

Optional: add -o [OUTPUT_FILE] to specify the output file
(The script will ask for a filename later if you don't use -o)
"""

import sys, os, re, argparse, json, copy, urlparse
from collections import defaultdict, OrderedDict
from codecs import open

def dump_json(data, fout):
    json.dump(data, fout,
              ensure_ascii=False, indent=2, separators=(',', ': '))
    fout.write('\n')

def normalize_query(searchquery):
    '''Return the unary without "list of ..." or "... list"'''
    if searchquery.startswith('list of '):
        return re.sub('^list of ', '', searchquery)
    if searchquery.endswith(' list'):
        return re.sub(' list$', '', searchquery)
    return searchquery

def normalize_entity(entity):
    '''Return the normalized entity.
    Return None for no answer ("n/a").
    '''
    answer = re.sub(r'\s+', ' ', entity.strip())
    if not answer or answer.lower() == 'n/a':
        return None
    # Try to fix encoding
    if not all(32 <= ord(x) < 127 for x in answer):
        try:
            answer = answer.encode('windows-1252').decode('utf-8')
        except ValueError:
            if set(ord(x) for x in answer if ord(x) > 127) == set([0xE2]):
                # â could be a punctuation (en dash, smart quotes, apostrophe)
                # Use heuristics
                fixed = answer
                # En Dash:
                fixed = re.sub(ur' â ', ur' – ', fixed, re.U)
                # Apostrophe:
                fixed = re.sub(ur'(\S)â(\S)', ur'\1’\2', fixed, re.U)
                # Quote:
                fixed = re.sub(ur'^â(.*)â$', ur'“\1”', fixed, re.U)
                fixed = re.sub(ur'(^|\s)â(\S.*\S)â(\s|$)', ur'\1“\2”\3', fixed, re.U)
                # Apostrophe Again:
                fixed = re.sub(ur'(\S)â(\s|$)', ur'\1’\2', fixed, re.U)
                answer = fixed
            # Bullet
            answer = re.sub(ur'â¢', '', answer, re.U)
    # Other normalizations should go here
    return answer

def normalize_answer(answer):
    '''Return the normalized answer for consistency checking.'''
    answer = re.sub('\([^)]*\)', '', answer)
    answer = re.sub('[^-.a-z0-9 ]', '', answer.lower())
    return answer.strip()

def percent(x, n):
    return '%5d/%5d (%8.3f%%)' % (x, n, x * 100.0 / (n + 1e-20))

def argmax(liz):
    """Return (max count, list of (index, elements) with that count)."""
    liz = list(liz)
    if not liz:
        return 0, []
    max_count = max(liz.count(x) for x in liz)
    pairs = [(i,x) for (i,x) in enumerate(liz) if liz.count(x) == max_count]
    return max_count, pairs

################################################################

ASSIGNMENT_FIELDS = (
    'AssignmentId', 'WorkerId', 'HITId',
    'AssignmentStatus', # Submitted / Approved / Rejected
    'AcceptTime', 'SubmitTime', 'AutoApprovalTime',
    'ApprovalTime', 'RejectionTime',
    )

class Assignment(object):
    AssignmentId = None
    WorkerId = None
    HITId = None
    AssignmentStatus = None
    AcceptTime = None
    SubmitTime = None
    AutoApprovalTime = None
    ApprovalTime = None
    RejectionTime = None
    answers = None
    raw = None

    def __init__(self, raw):
        self.raw = raw
        for key, value in raw['metadata'].items():
            setattr(self, key, value)
        self.answers = raw['answers']

    @staticmethod
    def parse_five(raw):
        '''Parse a batch with 5 questions per HIT (Batch #01)'''
        for i in xrange(5):
            i = str(i)
            new_raw = copy.deepcopy(raw)
            # Transform the keys / values
            new_raw['metadata']['AssignmentId'] += '_' + i
            new_raw['metadata']['HITId'] += '_' + i
            # aiN -> aN / aij -> a(j-1) / {hash|query|url}i -> {hash|query|url}
            old_answers = new_raw['answers']
            new_raw['answers'] = {
                'aN': old_answers['a' + i + 'N'],
                'a0': old_answers['a' + i + '1'],
                'a1': old_answers['a' + i + '2'],
                'a2': old_answers['a' + i + '3'],
                'a3': old_answers['a' + i + '4'],
                'a4': old_answers['a' + i + '5'],
                'hash': old_answers['hash' + i],
                'query': old_answers['query' + i],
                'url': old_answers['url' + i],
                'dataIndex': old_answers['dataIndex'],
                }
            yield Assignment(new_raw)

    def summary(self):
        '''Get a summary string.'''
        mark = '(%s)' % {'Submitted': ' ',
                         'Approved': 'O',
                         'Rejected': 'X'}[self.AssignmentStatus]
        assignment_id = '[%s]' % self.AssignmentId
        return mark + assignment_id + \
            '|'.join((self.answers[x].strip().replace('\t', ' ')
                      .replace('\n', '\\n').replace('\r', '\\r'))
                     for x in sorted(self.answers))

class AssignmentBatch(object):
    assignments = None
    by_HITId = None
    by_WorkerId = None
    by_AssignmentStatus = None
    is_five = False
    
    def __init__(self, raw=None, filename=None, is_five=False):
        if filename:
            assert not raw, 'Only 1 argument should be specified'
            with open(filename, 'r', 'utf8') as fin:
                raw = json.load(fin)
        assert raw, 'At least 1 argument should be specified'
        if is_five:
            self.is_five = True
            self.assignments = []
            for x in raw:
                self.assignments.extend(Assignment.parse_five(x))
        else:
            self.assignments = [Assignment(x) for x in raw]
        self.by_HITId = defaultdict(list)
        self.by_WorkerId = defaultdict(list)
        self.by_AssignmentStatus = defaultdict(list)
        for assignment in self.assignments:
            self.by_HITId[assignment.HITId].append(assignment)
            self.by_WorkerId[assignment.WorkerId].append(assignment)
            self.by_AssignmentStatus[assignment.AssignmentStatus].append(assignment)

    def extend(self, batch):
        """Add everything from another batch to this batch."""
        self.assignments.extend(batch.assignments)
        for key, value in batch.by_HITId.iteritems():
            self.by_HITId[key].extend(value)
        for key, value in batch.by_WorkerId.iteritems():
            self.by_WorkerId[key].extend(value)
        for key, value in batch.by_AssignmentStatus.iteritems():
            self.by_AssignmentStatus[key].extend(value)
        self.is_five = False    # Something will break but whatever
            
    def dump_dataset(self, fout, strict=False, batch_id='[batchid]'):
        """Dump a dataset and return the number of records.
        An assignment is valid if it is Approved
        and the answer is not 'Z' (0 answers)
        """
        data = []
        for hit_id in self.by_HITId:
            usable_assignments = [
                assignment for assignment in self.by_HITId[hit_id]
                if assignment.AssignmentStatus == 'Approved']
            if not usable_assignments:
                continue
            datum = self.get_datum(usable_assignments, strict=strict)
            if datum:
                data.append(datum)
        options = {'useHashcode': True,
                   'cacheDirectory': 'frozen.cache/' + batch_id,
                   'detailed': True,
                   'strict': strict}
        dataset = {'options': options, 'data': data}
        dump_json(dataset, fout)
        return len(data)

    def get_datum(self, assignments, strict=False):
        """Return a dict representing an example (a datum).
        Keys:
        - query
        - searchquery (usually "List of " + query)
        - url
        - hashcode
        - rawanswers
        - entities = distinct answers
        - criteria = {'first': ..., 'second': ..., 'last': ...}
        """
        datum = {'hashcode': assignments[0].answers['hash'],
                 'searchquery': assignments[0].answers['query'],
                 'url': assignments[0].answers['url']}
        datum['query'] = normalize_query(datum['searchquery'])
        # Get normalized answers
        raw_answers = []
        entities = set()
        for assignment in assignments:
            turker_answers = []
            answer_type = assignment.answers.get('aN', 'Z')
            if answer_type == 'L' or answer_type == 'H':
                for i in xrange(5):
                    answer = assignment.answers['a%d' % i].strip()
                    answer = normalize_entity(answer)
                    if not answer:
                        continue
                    turker_answers.append(answer)
            entities.update(turker_answers)
            raw_answers.append({'type': answer_type,
                                'answers': turker_answers})
        datum['rawanswers'] = raw_answers
        datum['entities'] = sorted(entities)
        # Get agreed answers
        criteria = self.get_criteria(raw_answers, strict=strict)
        if not criteria:
            return None
        datum['criteria'] = criteria
        return datum

    CKEYS = ((0, 'first'), (1, 'second'), (-1, 'last'))

    def get_criteria(self, raw_answers, strict=False):
        """Return the agreed {"first": ..., "second": ..., "last": ...}"""
        # Get non-zero answers
        answers = [x['answers'] for x in raw_answers if len(x['answers']) >= 2]
        if not answers or (strict and len(answers) == 1):
            return None
        criteria = OrderedDict()
        usable_indices = set(xrange(len(answers)))
        for i, key in self.CKEYS:
            count, index_element_pairs = argmax(x[i] for x in answers)
            if strict:
                if count < 2:
                    return None
                usable_indices &= set([x[0] for x in index_element_pairs])
            else:
                criteria[key] = index_element_pairs[0][1]
        if strict:
            if not usable_indices:
                return None
            agreed = answers[usable_indices.pop()]
            for i, key in self.CKEYS:
                criteria[key] = agreed[i]
        return criteria

    def count_statuses(self, assignments):
        '''Return a map from assignmentStatus to count.'''
        counts = defaultdict(int)
        for assignment in assignments:
            counts[assignment.AssignmentStatus] += 1
        return counts

    def dump_grouped_assignments(self, groups, fout):
        '''Dump grouped assignments to fout.
        
        Argument:
        groups -- a dict mapping each group name to a list of assignments
        '''
        num_assignments = num_turkers = 0
        for name, assignments in groups.iteritems():
            if any(x.AssignmentStatus == 'Submitted' for x in assignments):
                num_turkers += 1
                print >> fout, '=' * 20, name, '=' * 20
                counts = self.count_statuses(assignments)
                print >> fout, ('[ Approved = %d | Rejected = %d ]' %
                                (counts['Approved'], counts['Rejected']))
                print >> fout, '--- NEW (%d) ---' % counts['Submitted']
                for assignment in assignments:
                    if assignment.AssignmentStatus == 'Submitted':
                        num_assignments += 1
                        print >> fout, assignment.summary()
                print >> fout
        print '(dumped %d submitted assignments from %d Turkers)' \
            % (num_assignments, num_turkers)

    def dump_by_worker_id(self, fout, worker_id):
        """Dump all HITs done by this worker id.
        Put the work by this worker id at the top of each HIT.
        Return the number of HITs.
        """
        assignments = self.by_WorkerId[worker_id]
        print >> fout, '=' * 20, worker_id, '=' * 20
        counts = self.count_statuses(assignments)
        print >> fout, ('[ Approved = %d | Rejected = %d ]' %
                        (counts['Approved'], counts['Rejected']))
        for assignment in assignments:
            print >> fout, '#' * 20, assignment.HITId, '#' * 20
            print >> fout, assignment.summary()
            for x in self.by_HITId[assignment.HITId]:
                if x.WorkerId != assignment.WorkerId:
                    print >> fout, x.summary()
        print >> fout, '#' * 20, 'SUMMARY', '#' * 20
        for assignment in assignments:
            print >> fout, assignment.AssignmentId
        return len(assignments)

################################################################

def get_filename(default=None, clobber=False):
    if default:
        filename = default
    else:
        filename = raw_input('Output filename: ').strip() or 'untitled.json'
        filename = os.path.join('temp', filename)
    if os.path.exists(filename) and not clobber:
        r = raw_input('%s exists. Overwrite? (Yes/No/Rename) ' % filename)
        response = r.lower()[:1]
        if response == 'y':
            return filename
        elif response == 'r':
            return get_filename(clobber=clobber)
        else:
            return None
    return filename

def rage_quit(*args, **kwargs):
    '''[x/q] Exit'''
    exit(0)

def write_dataset(batch, output_filename=None, results_filename=None, 
                  strict=False, clobber=False, *args, **kwargs):
    '''[d] Create a dataset JSON file.'''
    batch_id = re.search('/([^/]+).results$', results_filename).group(1)
    filename = get_filename(output_filename, clobber=clobber)
    if filename:
        with open(filename, 'w', 'utf8') as fout:
            n = batch.dump_dataset(fout, strict=strict, batch_id=batch_id)
            print 'Wrote %d records to %s' % (n, filename)

def check_consistencies_across_assignments(batch, hit_type=None, *args, **kwargs):
    '''Check the consistencies across assignments in the same HIT.'''
    if not hit_type:
        # Default: the first-second-last entity list task
        # NOTE TO SELF: some people put 'L' answers out of order
        c_1_turker = c_differ_numanswer = \
            c_differ_firstanswer = c_agree_firstanswer = 0
        for hit_id, assignments in batch.by_HITId.iteritems():
            if len(assignments) == 1:
                c_1_turker += 1
            else:
                if len(set(x.answers.get('aN') for x in assignments)) != 1:
                    c_differ_numanswer += 1
                elif len(set(x.answers.get('a0') for x in assignments)) != 1:
                    c_differ_firstanswer += 1
                else:
                    c_agree_firstanswer += 1
        n = len(batch.by_HITId)
        print '            1 Turker:', percent(c_1_turker, n)
        print '  Disagreed #answers:', percent(c_differ_numanswer, n)
        print 'Disagreed 1st answer:', percent(c_differ_firstanswer, n)
        print '   Agreed 1st answer:', percent(c_agree_firstanswer, n)
    elif hit_type == 'a':
        # Trivia answer task
        counts = defaultdict(int)
        KEYS = ('a0', 'a1', 'a2', 'a3')
        for hit_id, assignments in batch.by_HITId.iteritems():
            print '=' * 30, assignments[0].answers['title'], '|',
            print assignments[0].answers['dataIndex'], '|',
            print assignments[0].HITId, '=' * 30
            print [(a.WorkerId, a.AssignmentId) for a in assignments]
            for key in KEYS:
                if key not in assignments[0].answers:
                    continue
                print assignments[0].answers[key.replace('a', 'q')]
                print '|'.join(str([a.answers[key]]) for a in assignments)
                answers = [normalize_answer(a.answers[key]) for a in assignments]
                counts['%d/%d' % (len(set(answers)), len(answers))] += 1
                if answers.count('na') >= 2:
                    counts['na'] += 1
                elif answers.count('longlist') >= 2:
                    counts['longlist'] += 3
                elif answers.count('long-calculation') >= 2:
                    counts['long-calculation'] += 3
        print counts
    else:
        raise ValueError('Unrecognized HIT type')

def classify_hits(batch, results_filename=None, *args, **kwargs):
    '''Classify HITs.'''
    # Get all possible HIT IDs
    success_filename = re.sub(r'\.results$', '.success', results_filename)
    with open(success_filename) as fin:
        hit_ids = [x.split()[0] for x in fin.readlines()[1:]]
    if batch.is_five:
        hit_ids = sum(([x + '_' + i for i in '01234'] for x in hit_ids), [])
    # Group into number of Turkers
    groups = defaultdict(list)
    for hit_id in hit_ids:
        groups[len(batch.by_HITId.get(hit_id, []))].append(hit_id)
    for i in (0,1,2,3):
        print i, ':', percent(len(groups[i]), len(hit_ids))

def classify_queries_and_urls(batch, *args, **kwargs):
    '''Classify queries and URLs.'''
    query_bag = defaultdict(int)
    url_bag = defaultdict(int)
    for hit_id, assignments in batch.by_HITId.iteritems():
        answers = [x.answers.get('aN', 'Z') for x in assignments]
        if all(x == 'Z' for x in answers):
            a = assignments[0].answers
            for x in a['query'].split():
                query_bag[x] += 1
            for x in a['url'].replace('http://', '').split('/'):
                url_bag[x] += 1
    query_bag = sorted(query_bag.items(), key=lambda x: -x[1])
    url_bag= sorted(url_bag.items(), key=lambda x: -x[1])
    for x in query_bag[:10]:
        print '\t'.join(unicode(y) for y in x)
    for x in url_bag[:10]:
        print '\t'.join(unicode(y) for y in x)

def classify_workers(batch, output_filename=None, results_filename=None, 
                     clobber=False, *args, **kwargs):
    '''Classify workers.'''
    # Print TSV:
    # WorkerID SampleAssignmentID BatchId Approved Rejected Total
    batch_id = os.path.basename(results_filename).replace('.results', '')
    filename = get_filename(output_filename, clobber=clobber)
    if filename:
        with open(filename, 'w', 'utf8') as fout:
            for worker_id, assignments in batch.by_WorkerId.iteritems():
                sample_assignment_id = assignments[0].AssignmentId
                counts = batch.count_statuses(assignments)
                print >> fout, '\t'.join((worker_id, sample_assignment_id, batch_id,
                                          str(counts['Approved']), str(counts['Rejected']),
                                          str(counts['Approved'] + counts['Rejected'])))

def group_by_worker(batch, output_filename=None, clobber=False,
                    *args, **kwargs):
    '''[g] Group by worker ID to catch spammers.'''
    filename = get_filename(output_filename, clobber=clobber)
    if filename:
        with open(filename, 'w', 'utf8') as fout:
            batch.dump_grouped_assignments(batch.by_WorkerId, fout)
        print 'Dumped stuff to', filename

def heuristic(batch, hit_type=None, *args, **kwargs):
    '''[h] Heuristically catch spammers'''
    if not hit_type:
        # Default: the first-second-last entity list task
        for worker_id, assignments in batch.by_WorkerId.iteritems():
            n = len(assignments)
            m = len([x for x in assignments if x.answers.get('aN', 'Z') == 'Z'])
            if n > 5 and m > n * 0.5:
                print '( %20s )' % worker_id, percent(m, n)
    elif hit_type == 'a':
        # Trivia answer task
        KEYS = ('a0', 'a1', 'a2', 'a3')
        worker_agreement = defaultdict(int)   # Add 1 point for each agreement
        worker_all = defaultdict(int)         # Don't count HITs with 1 submission
        worker_submitted = defaultdict(int)   # Count HITs with 1 submission too
        worker_lifetime_agreement = defaultdict(int)   # Also include Approved and Rejected work
        worker_lifetime_all = defaultdict(int)
        worker_lifetime_submitted = defaultdict(int)
        for hit_id, assignments in batch.by_HITId.iteritems():
            for key in KEYS:
                if key not in assignments[0].answers:
                    continue
                for a in assignments:
                    worker_lifetime_submitted[a.WorkerId] += 1
                    if a.AssignmentStatus == 'Submitted':
                        worker_submitted[a.WorkerId] += 1
                if len(assignments) == 1:
                    # Cannot do anything with only 1 submission ...
                    continue
                for a in assignments:
                    worker_lifetime_all[a.WorkerId] += 1
                    if a.AssignmentStatus == 'Submitted':
                        worker_all[a.WorkerId] += 1
                answers = [normalize_answer(a.answers[key]) for a in assignments]
                # Find the majority
                max_count, pairs = argmax(answers)
                # One person is not enough!
                if max_count == 1:
                    continue
                # Award points to all people matching the majority
                for idx, answer in pairs:
                    worker_lifetime_agreement[assignments[idx].WorkerId] += 1
                    if assignments[idx].AssignmentStatus == 'Submitted':
                        worker_agreement[assignments[idx].WorkerId] += 1
        for worker_id in sorted(worker_submitted,
                key=lambda x: -worker_agreement[x] * 1. / (worker_all[x] + 1e-10)):
            print '%25s :' % worker_id,
            print percent(worker_agreement[worker_id], worker_all[worker_id]),
            print '%5d' % worker_submitted[worker_id],
            print percent(worker_lifetime_agreement[worker_id], worker_lifetime_all[worker_id]),
            print '%5d' % worker_lifetime_submitted[worker_id]
    else:
        raise ValueError('Unrecognized HIT type')

def single_worker(batch, output_filename=None, clobber=False, *args, **kwargs):
    '''Dump the work of a single worker.'''
    worker_id = raw_input("Worker ID: ")
    if worker_id not in batch.by_WorkerId:
        print "Worker ID %s not found" % worker_id
        return
    filename = get_filename(output_filename, clobber=clobber)
    if filename:
        with open(filename, 'w', 'utf8') as fout:
            batch.dump_by_worker_id(fout, worker_id)

def generate_approve(batch, results_filename=None, *args, **kwargs):
    '''[a] Read ___.reject and dump (results - reject) to ___.approve'''
    for status, assignments in batch.by_AssignmentStatus.iteritems():
        print '%15s: %6d' % (status, len(assignments))
    reject_filename = re.sub(r'\.results$', '.reject', results_filename)
    if os.path.exists(reject_filename):
        with open(reject_filename, 'r', 'utf8') as fin:
            rejected = [x.strip() for x in fin if x.strip() and x[0] != '#']
    else:
        if raw_input("Reject file not found. "
                     "Approve everything? (Yes/No) ").lower()[:1] != 'y':
            exit(1)
        rejected = []
    approved = set(x.AssignmentId for x in batch.assignments
                   if x.AssignmentStatus == 'Submitted')
    approved -= set(rejected)
    print '%15s: %6d' % ('New Reject', len(rejected))
    print '%15s: %6d' % ('New Approve', len(approved))
    approve_filename = get_filename(
        re.sub(r'\.results$', '.approve', results_filename))
    if approve_filename:
        with open(approve_filename, 'w', 'utf8') as fout:
            for assignment_id in approved:
                print >> fout, assignment_id
        print ('Printed %d assignment ids to %s' %
               (len(approved), approve_filename))

def print_comments(batch, *args, **kwargs):
    '''Print comments. Prepare to be hurted.'''
    comments = set(a.answers.get('comment', '') for a in batch.assignments)
    comments.remove('')
    for i, comment in enumerate(comments):
        print '%d.' % i, comment.replace('\n', ' ').replace('\r', ' ')

FUNCTIONS = [rage_quit,
             group_by_worker,
             heuristic,
             single_worker,
             generate_approve,
             write_dataset,
             check_consistencies_across_assignments,
             classify_hits,
             classify_queries_and_urls,
             classify_workers,
             print_comments,
             ]
SHORTCUTS = {'g': group_by_worker,
             'h': heuristic,
             'a': generate_approve,
             'd': write_dataset,
             'q': rage_quit,
             'x': rage_quit,
             'c': check_consistencies_across_assignments,
             }
def interactive(action=None, *args, **kwargs):
    print '=' * 40
    exit_after = bool(action)
    fn = None
    try:
        if not action:
            for i, fn in enumerate(FUNCTIONS):
                print '  %2d)' % (i), fn.__doc__
            action = raw_input("Choose an action: ")
        if action.isdigit():
            fn = FUNCTIONS[int(action)]
        else:
            fn = SHORTCUTS[action]
    except (EOFError, KeyboardInterrupt):
        print
        exit(0)
    except (LookupError, ValueError):
        print 'ERROR: Invalid action %s' % action
    if fn:
        fn(*args, **kwargs)
    if exit_after:
        exit(0)

################################################################

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('result', help="Result File")
    parser.add_argument('-o', '--output', help="Output File")
    parser.add_argument('-f', '--five', action="store_true",
                        help="Five questions per HIT")
    parser.add_argument('-s', '--strict', action="store_true",
                        help="Be more strict when generating datasets")
    parser.add_argument('-a', '--action',
                        help="Use the action and don't ask anything")
    parser.add_argument('-t', '--hit-type',
                        help="HIT type (may change some behaviors)")
    args = parser.parse_args()

    if not args.result.endswith('.results'):
        batch_id = [x for x in args.result.split('/') if x][-1]
        args.result = os.path.join(args.result, batch_id + '.results')
        print 'Using', args.result

    batch = AssignmentBatch(filename=args.result, is_five=args.five)

    print 'Read %d assignments (%d HITs)' % \
        (len(batch.assignments), len(batch.by_HITId))
    while True:
        interactive(batch=batch, results_filename=args.result,
                    output_filename=args.output, strict=args.strict,
                    action=args.action, clobber=bool(args.action),
                    hit_type=args.hit_type)
