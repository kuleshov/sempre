#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, shutil, re, argparse, json, random
from codecs import open
from itertools import izip
from collections import defaultdict, Counter

'''Example entry:
  {
    "answers": [
      "3",
      "3",
      "3"
    ],
    "batchIndex": 1203,
    "dataIndex": 3963,
    "hash": "2ea70ff7cd61bb0aea89dbe6238be042b3587ddd",
    "pageid": 15269572,
    "prompt": "\"How many ...\"",
    "question": "How many Members had businessman listed as their profession?",
    "srcBatchIndex": 204,
    "srcDataIndex": 68,
    "tableid": 0,
    "title": "Oklahoma State Regents for Higher Education",
    "url": "http://en.wikipedia.org/wiki?action=render&curid=15269572&oldid=583254230"
  }
'''

################################################
# Utils

def memoized(fn):
    cache = {}
    def wrapped(*args):
        if args not in cache:
            cache[args] = fn(*args)
        return cache[args]
    return wrapped

def print_histogram(counter, pattern='{0:10} : {1:5d}'):
    for x in sorted(counter):
        print pattern.format(x, counter[x])

def normalize(x):
    if isinstance(x, list):
        return [normalize(y) for y in x]
    x = re.sub(r'\s+', ' ', x).strip()
    return re.sub(r'[^[-]a-z0-9 ]', '', x.lower())

def get_agreement(x):
    answer = max((x.count(y), y) for y in x)
    if answer[0] == 1:
        return None
    return answer[1]

def is_punt(x):
    return x == 'n/a' or (x.startswith('[[') and x.endswith(']]'))

def is_list(x):
    return '\n' in x

def are_words_in(x, y):
    # check if some words in x are in y
    x = re.sub(r'[^a-z]', ' ', x).strip()
    y = normalize(y)
    return any(word in y for word in x.split())

################################################
# Aggregators

class QuestionPromptAggregator(object):
    def __init__(self):
        self.num_questions = 0
        self.num_u_agreements = 0
        self.num_n_agreements = 0
        self.num_raw_punts = defaultdict(int)
        self.num_agreed_punts = defaultdict(int)
        self.num_u_nonpunt_agreements = 0
        self.num_n_nonpunt_agreements = 0
        self.num_list_agreements = 0
        self.num_prompt_in_question = 0

    def add(self, datum):
        self.num_questions += 1
        if are_words_in(datum['prompt'], datum['question']):
            self.num_prompt_in_question += 1
        u_answers = datum['answers']
        n_answers = normalize(u_answers)
        for x in u_answers:
            if is_punt(x):
                self.num_raw_punts[x] += 1
        agree_u_answers = get_agreement(u_answers)
        agree_n_answers = get_agreement(n_answers)
        if agree_u_answers:
            self.num_u_agreements += 1
            if not is_punt(agree_u_answers):
                self.num_u_nonpunt_agreements += 1
            else:
                self.num_agreed_punts[agree_u_answers] += 1
            if is_list(agree_u_answers):
                self.num_list_agreements += 1
        if agree_n_answers:
            self.num_n_agreements += 1
            if not is_punt(agree_n_answers):
                self.num_n_nonpunt_agreements += 1
        # Return True if it is a non-punt agreement
        if agree_u_answers and not is_punt(agree_u_answers):
            datum['agreedAnswer'] = agree_u_answers
            return True
        return False

    def summarize(self):
        print '(raw = raw text, normalized = strip non-alpha-numeric or spaces)'
        print '(punt = [[n/a]], [[long-calculation]], etc.)'
        print '(agreement = at least 2 people agree)'
        print '(list = answer is a list of things)'
        print json.dumps({
            'questions': self.num_questions,
            'rawAgreements': self.num_u_agreements,
            'normalizedAgreements': self.num_n_agreements,
            'rawPunts': dict(self.num_raw_punts),
            'rawAgreedPunts': dict(self.num_agreed_punts),
            'rawNonPuntAgreements': self.num_u_nonpunt_agreements,
            'rawNonPuntAgreementsPercent':
                self.num_u_nonpunt_agreements * 1. / self.num_questions,
            'normalizedNonPuntAgreements': self.num_n_nonpunt_agreements,
            'normalizedNonPuntAgreementsPercent':
                self.num_n_nonpunt_agreements * 1. / self.num_questions,
            'listAgreements': self.num_list_agreements,
            'exactPromptIsInQuestion': self.num_prompt_in_question,
            'exactPromptIsInQuestionPercent':
                self.num_prompt_in_question * 1. / self.num_questions,
            }, indent=2, separators=(',', ': '), sort_keys=True)

    @staticmethod
    def summarize_mini_title():
        print '(n = num questions, R = raw, N = normalized, L = list, good = agreed and not punt)'
        print '(prmpt = use exact wording from prompt (question template) in question)'
        print '  '.join([
            '%50s' % 'name',
            '%8s' % 'goodN%',
            '%6s' % 'n',
            '%6s' % 'agreeR',
            '%6s' % 'agreeN',
            '%6s' % 'goodR',
            '%6s' % 'goodN',
            '%6s' % 'agreeL',
            '%6s' % 'prmpt',
            '%6s' % 'prmpt%',
            ])

    def summarize_mini(self, name):
        print '  '.join([
            '%50s' % name,
            '%8.2f' % (self.num_n_nonpunt_agreements * 100. / self.num_questions),
            '%6d' % self.num_questions,
            '%6d' % self.num_u_agreements,
            '%6d' % self.num_n_agreements,
            '%6d' % self.num_u_nonpunt_agreements,
            '%6d' % self.num_n_nonpunt_agreements,
            '%6d' % self.num_list_agreements,
            '%6d' % self.num_prompt_in_question,
            '%6.2f' % (self.num_prompt_in_question * 100. / self.num_questions),
            ])

class Aggregator(object):
    def __init__(self):
        # Per question prompt
        self.question_prompt_stats = defaultdict(QuestionPromptAggregator)
        self.title_table_to_counts = defaultdict(int)
        self.num_tokens_to_counts = defaultdict(int)
        self.good_datum = []

    def add(self, datum):
        if self.question_prompt_stats[datum['prompt']].add(datum):
            self.good_datum.append(datum)
        self.question_prompt_stats['TOTAL'].add(datum)
        self.title_table_to_counts[datum['title'], datum['tableid']] += 1
        # Count on space split (pretty crude):
        self.num_tokens_to_counts[len(datum['question'].strip().split())] += 1

    def summarize(self):
        print '===== Agreements ====='
        self.question_prompt_stats['TOTAL'].summarize()
        print '===== Breakdown per Question Prompt ===='
        aggregators = [(self.question_prompt_stats[p], p)
                        for p in self.question_prompt_stats if p != 'TOTAL']
        aggregators.sort(key=lambda x: -x[0].num_n_nonpunt_agreements * 1. / x[0].num_questions)
        QuestionPromptAggregator.summarize_mini_title()
        for aggregator, name in aggregators:
            aggregator.summarize_mini(name)
        print '===== Questions per Table (histogram) ====='
        print_histogram(Counter(self.title_table_to_counts.values()),
                        '{1:5d} tables with {0:5d} questions')
        print '===== Question Lengths (#words) ====='
        print_histogram(self.num_tokens_to_counts,
                        '{1:5d} questions with {0:5d} words')

    def dump(self, fout):
        random.seed(42)
        random.shuffle(self.good_datum)
        json.dump(self.good_datum, fout,
              ensure_ascii=False, indent=2,
              separators=(',', ': '), sort_keys=True)
        fout.write('\n')
        print >> sys.stderr, 'Wrote', len(self.good_datum), 'records'

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', help='JSON file produced by summarize-trivia.py')
    parser.add_argument('-o', '--outfile',
                        help='print non-punt agreements into this file')
    args = parser.parse_args()

    with open(args.infile, 'r', 'utf8') as fin:
        data = json.load(fin)
    print >> sys.stderr, 'Read', len(data), 'records'
    aggregator = Aggregator()
    for datum in data:
        aggregator.add(datum)
    aggregator.summarize()
    if args.outfile:
        with open(args.outfile, 'w', 'utf8') as fout:
            aggregator.dump(fout)

if __name__ == '__main__':
    main()

