#!/usr/bin/env bash
# Utilities for running Turk tasks.
# Don't run this directly.
exit 1

# Get data and dump to file
for x in {1203d,}; do ./run.py data/$x get --real && ./grade.py -a g -o temp/$x data/$x || break; done
# Grade assignments
for x in {1203d,}; do ./grade.py -a a data/$x && ./run.py grade data/$x --real && ./run.py get data/$x --real || break; done
# Random samples
./T-get-random-samples.py 200 202 203 203a 203b 300 300a 300b -o temp/sampled.tsv
./T-get-random-samples.py 200 202 203 203a 203b 300 300a 300b -o temp/sampled.json -j

# Check consistency for the Trivia Answer task
for x in {1203d,}; do ./grade.py data/$x -t a -a c | tee temp/$x-consistency && ./grade.py data/$x -t a -a h | tee temp/$x-heuristics || break; done
./T-get-disagreements.py data/{1200a,1201,1202a,1202b,1202c,1202d,1203a,1203b,1203c,1203d}/*.results -o temp/disagreements -O temp/1300-json

# Dump trivia-answer
./data/summarize-trivia.py trivia-answer -o compiled/trivia-answer.json
./"T-print-statistics.py" compiled/trivia-answer.json -o compiled/good-trivia-answer.json > compiled/stats
jq -S '.[:100] | .[] | {url: .url, answer: .agreedAnswer, question: .question, logicalForm: [""], tableid: .tableid}' compiled/good-trivia-answer.json > compiled/sampled-raw.json
./"T-separate-pristine-data.py" compiled/good-trivia-answer.json compiled/23K
# Clean up the sampled json (need https://github.com/phadej/relaxed-json)
rjson compiled/sampled.json

################################################
# HIT IDs:
# 203: 37R8JAFGQ02CDPJW1AIBDC6ULXCJD7
# 300: 3KTTL66PWGK2G0OS4VFK7LWM8CJHV6
#
# Answer tasks:
# - 200, 202, 203, 300 --> 1200a and 1201 (3 questions per HIT)
#   All 694 HITs (2082 questions) got answered.
# - 203a, 203b, 300a, 300b --> ...
#   
