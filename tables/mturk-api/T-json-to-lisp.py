#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, shutil, re, argparse, json
from codecs import open
from itertools import izip
from collections import defaultdict

import locale
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

################################################
# LispTree

def lisp_tree_to_string(tree, is_root=False):
    answer = []
    if isinstance(tree, (list, tuple)):
        answer.append('(')
        for x in tree:
            answer.append(lisp_tree_to_string(x))
            if is_root:
                answer.append('\n')
                answer.append('  ')
            else:
                answer.append(' ')
        answer.pop()    # Pop the last space
        answer.append(')')
        return ''.join(answer)
    else:
        return unicode(tree)

def quote(x):
    return '"' + x.replace('"', r'\"') + '"'

CLASSNAME = 'tables.TableKnowledgeGraph'

def get_context(batch_id, data_id):
    return "csv/%d-csv/%d.csv" % (batch_id, data_id)

def get_target_values(rawstring):
    stuff = rawstring.split('\r\n')
    answer = ['list']
    for x in stuff:
        try:
            x = locale.atoi(x)
            answer.append(['number', x])
        except ValueError:
            try:
                x = locale.atof(x)
                answer.append(['number', x])
            except ValueError:
                answer.append(['description', quote(x)])
    return answer

################################################
# Read Dataset

def get_data(filename, limit=0, offset=0):
    with open(filename, 'r', 'utf8') as fin:
        raw = json.load(fin)[offset:]
    data = []
    for i, x in enumerate(raw):
        if limit and i >= limit:
            break
        # Convert into lisp tree
        utterance = ["utterance", quote(x['question'].lower())]
        context = ["context", ["graph", CLASSNAME, get_context(x['srcBatchIndex'], x['srcDataIndex'])]]
        targetValue = ["targetValue", get_target_values(x['agreedAnswer'])]
        data.append(["example", utterance, context, targetValue])
    print >> sys.stderr, 'Read', len(data), 'examples'
    return data

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('infile')
    parser.add_argument('outfile')
    parser.add_argument('-l', '--limit', type=int,
                        help='limit the number of examples to print')
    parser.add_argument('-o', '--offset', type=int, default=0,
                        help='example id offset')
    parser.add_argument('-i', '--indent', action='store_true',
                        help='add indent to lisp trees')
    args = parser.parse_args()

    data = get_data(args.infile, args.limit, args.offset)
    with open(args.outfile, 'w', 'utf8') as fout:
        for i, datum in enumerate(data):
            if args.indent:
                print >> fout, '#' * 30, 'ex', (args.offset + i), '#' * 30
            print >> fout, lisp_tree_to_string(datum, is_root=args.indent)

if __name__ == '__main__':
    main()

