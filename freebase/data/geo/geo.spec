# This file specifies the mapping from predicates in the old Geobase to new Freebase.
# http://www.cs.utexas.edu/~ml/wasp/geo-funql.html
# t  - bool
# c  - city
# co - country
# r  - river
# s  - state
# i  - int
# n  - string
# NOTE Some of the comments are actually used by a converter to turn these
# into part of the geoquery grammar

# unary_predicates

city (fb:type.object.type fb:location.citytown)
town (fb:type.object.type fb:location.citytown)
state (fb:type.object.type fb:location.us_state)
river (fb:type.object.type fb:geography.river)
mountain (fb:type.object.type fb:geography.mountain)
lake (fb:type.object.type fb:geography.lake)
place (fb:type.object.type fb:location.location)

#major_city ((lambda x (!fb:measurement_unit.dated_integer.number (argmax 1 1 (!fb:location.statistical_region.population (var x)) fb:measurement_unit.dated_integer.year))) (>= 1000000))
major_city (fb:location.location.area (>= 200))
major_river (fb:geography.river.length (>= 2000))
major_lake (fb:geography.body_of_water.surface_area (>= 2000))

capital:t (!fb:location.us_state.capital (fb:type.object.type fb:location.us_state))

# binary_predicates

#named (lambda x (fb:type.object.name (STRSTARTS (var x))))
named fb:type.object.name
loc fb:location.location.containedby
river_loc (lambda x (or (fb:location.location.containedby (var x)) (fb:location.location.partially_contained_by (var x))))
next_to (lambda x (fb:location.location.adjoin_s (fb:location.adjoining_relationship.adjoins (var x))))

# FIXME All assume us_state
capital !fb:location.us_state.capital
# FIXME Hacky
is_capital fb:location.us_state.capital

#population (lambda x (!fb:measurement_unit.dated_integer.number (argmax 1 1 (!fb:location.statistical_region.population (var x)) fb:measurement_unit.dated_integer.year)))
population (lambda x (!fb:location.geocode.latitude (!fb:location.location.geolocation (var x))))
# NOTE Think states missing this property
#density (lambda x (!fb:measurement_unit.dated_integer.number (argmax 1 1 (!fb:location.statistical_region.arithmetic_population_density (var x)) fb:measurement_unit.dated_integer.year)))
density (lambda x (!fb:location.geocode.longitude (!fb:location.location.geolocation (var x))))
# FIXME argument specific (elevation of valleys for example)
elevation !fb:geography.mountain.elevation
len !fb:geography.river.length
area !fb:location.location.area
high_point (lambda x (argmax 1 1 (fb:location.location.containedby (var x)) fb:geography.mountain.elevation))
equals =

# Built-in
< <
> >
<= <=
>= >=
= =

# Require some specialization
#size_city fb:location.location.area
#size_state fb:location.location.area
#size_river fb:geography.river.length
# FIXME Currently just modified some of the logical forms instead of having predicates depend on the argument type
size !fb:location.location.area

# Countries
usa fb:en.united_states_of_america

# States
alabama fb:en.alabama
alaska fb:en.alaska
arizona fb:en.arizona
arkansas fb:en.arkansas
california fb:en.california
colorado fb:en.colorado
connecticut fb:en.connecticut
delaware fb:en.delaware
florida fb:en.florida
georgia fb:en.georgia
hawaii fb:en.hawaii
idaho fb:en.idaho
illinois fb:en.illinois
indiana fb:en.indiana
iowa fb:en.iowa
kansas fb:en.kansas
kentucky fb:en.kentucky
louisiana fb:en.louisiana
maine fb:en.maine
maryland fb:en.maryland
massachusetts fb:en.massachusetts
michigan fb:en.michigan
minnesota fb:en.minnesota
mississippi fb:en.mississippi
missouri fb:en.missouri
montana fb:en.montana
nebraska fb:en.nebraska
nevada fb:en.nevada
new_hampshire fb:en.new_hampshire
new_jersey fb:en.new_jersey
new_mexico fb:en.new_mexico
new_york_state fb:en.new_york_state
north_carolina fb:en.north_carolina
north_dakota fb:en.north_dakota
ohio fb:en.ohio
oklahoma fb:en.oklahoma
oregon fb:en.oregon
pennsylvania fb:en.pennsylvania
rhode_island fb:en.rhode_island
south_carolina fb:en.south_carolina
south_dakota fb:en.south_dakota
tennessee fb:en.tennessee
texas fb:en.texas
utah fb:en.utah
vermont fb:en.vermont
virginia fb:en.virginia
washington fb:en.washington
west_virginia fb:en.west_virginia
wisconsin fb:en.wisconsin
wyoming fb:en.wyoming

# Cities
albany fb:en.albany
albany_ny fb:en.albany
atlanta_ga fb:en.atlanta
austin_tx fb:en.austin
baton_rouge_la fb:en.baton_rouge
boston_ma fb:en.boston
boulder_co fb:en.boulder
chicago_il fb:en.chicago
columbus_oh fb:en.columbus
dallas_tx fb:en.dallas
denver_co fb:en.denver
des_moines_ia fb:en.des_moines
dover_de fb:en.dover
flint_mi fb:en.flint
fort_wayne_in fb:en.fort_wayne
houston_tx fb:en.houston
indianapolis_in fb:en.indianapolis
kalamazoo_mi fb:en.kalamazoo
montgomery_al fb:en.montgomery
new_orleans_la fb:en.new_orleans
new_york_ny fb:en.new_york
pittsburgh_pa fb:en.pittsburgh
portland_me fb:en.portland
riverside_ca fb:en.riverside
rochester_ny fb:en.rochester_new_york
sacramento_ca fb:en.sacramento
salem_or fb:en.salem_oregon
san_diego_ca fb:en.san_diego
san_francisco_ca fb:en.san_francisco
san_jose_ca fb:en.san_jose
scotts_valley_ca fb:en.scotts_valley
seattle_wa fb:en.seattle
spokane_wa fb:en.spokane
springfield_il fb:en.springfield_illinois
springfield_mo fb:en.springfield_missouri
springfield_sd fb:en.springfield_south_dakota
washington_dc fb:en.washington

# Landforms
death_valley fb:en.death_valley
guadalupe_peak fb:en.guadalupe_peak
mount_mckinley fb:en.mount_mckinley
mount_whitney fb:en.mount_whitney

# Rivers
colorado_river fb:en.colorado_river
missouri_river fb:en.missouri_river
ohio_river fb:en.ohio_river
potomac_river fb:en.potomac_river
red_river fb:en.red_river_wisconsin
rio_grande_river fb:en.rio_grande
mississippi_river fb:en.mississippi_river
