#!/usr/bin/env python
import os
import sys
import re
from collections import defaultdict
from build_geo_lexicon import preprocess_mappings, add_manual_mappings,\
        SPEC_FILE, SEMPARSE_DIR, CATEGORIES, read_spec_mappings, preprocess_key


# Some utility functions


def print_mappings(mapping_dict):
    keys = sorted(mapping_dict.keys())
    for key in keys:
        print '%15s -> %s' % (key, mapping_dict[key])


def build_rule(key, val, rule_type='unary', ent_type='fb:en.entity'):
    return '(rule $%s (%s) (ConstantFn %s %s))' % (rule_type, key, val, ent_type)


def car(strpair):
    strpair = strpair.strip()[1:-1]  # Remove parens
    return strpair.split()[0]


def cdr(strpair):
    strpair = strpair.strip()[1:-1]  # Remove parens
    return strpair.split()[1]


if __name__ == '__main__':

    BASE_GRAMMAR_FILE = os.path.join(SEMPARSE_DIR, 'data/geo/base.grammar')  # PARAM
    OUT_FILE = sys.argv[1]

    mappings = read_spec_mappings(SPEC_FILE)
    #preprocess_mappings(mappings)
    add_manual_mappings(mappings)

    rules = list()

    '''
    Form rules from spec file
    '''

    for cat in CATEGORIES:
        if cat == 'binary_predicates':
            #rule_type = 'Binary'
            continue
        elif cat == 'unary_predicates':
            #rule_type = 'Unary'
            continue
        else:
            rule_type = 'NamedEntity'

        rules.append('# %s' % cat)

        ent_type = None
        if cat == 'cities':
            ent_type = 'fb:location.citytown'
        elif cat == 'states':
            ent_type = 'fb:location.us_state'
        elif cat == 'landforms':
            ent_type = 'fb:location.location'  # FIXME
        elif cat == 'rivers':
            ent_type = 'fb:geography.river'
        elif cat == 'countries':
            ent_type = 'fb:location.country'

        for key, val in mappings[cat].iteritems():
            if cat == 'cities':
                key = key.rsplit('_', 1)[0]
            if key == 'new_york_state':
                key = 'new_york'
            key = preprocess_key(key)

            if ent_type is None:
                print key, val
                raise
            rules.append(build_rule(key, val, rule_type=rule_type, ent_type=ent_type))

            # FIXME Hacks for rivers and in, move these elsewhere
            if key != 'river' and key.endswith('river'):
                rules.append(build_rule(key.rsplit(' ', 1)[0], val, rule_type=rule_type, ent_type=ent_type))
            elif key == 'river in':
                rules.append(build_rule('run through', val, rule_type='Binary', ent_type=ent_type))

    '''
    Finally, write the grammar file
    '''

    with open(BASE_GRAMMAR_FILE, 'r') as f:
        main_grammar = f.read()
        # FIXME Current reverses on some predicates cause issue
        #main_grammar = main_grammar.replace('unaryCanBeArg0 ', '')

        # FIXME Remove lines with LexiconFn
        main_grammar_lines = main_grammar.split('\n')
        main_grammar = ''
        for l in main_grammar_lines:
            main_grammar += l + '\n'
        main_grammar = main_grammar.strip()

    with open(OUT_FILE, 'w') as f:
        f.write('\n'.join(rules) + '\n\n' + main_grammar)
