./run @mode=train \
         @domain=geo \
         @sparqlserver=localhost:3093 \
         @cacheserver=local \
         -Dataset.inPaths test:$1 \
         -Learner.maxTrainIters 0
