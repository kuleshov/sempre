#!/usr/bin/env python
import sys
from fabric.colors import blue

'''
Compare parameters between two runs
'''

execs = [int(sys.argv[1]), int(sys.argv[2])]
option_keys = list()
options_map = list()

# Build option map dict for each run

for k in range(len(execs)):
    options_file = 'state/execs/%d.exec/options.map' % execs[k]
    with open(options_file, 'r') as f:
        keys, vals = zip(*[l.split('\t') for l in f.read().strip().split('\n')])
    option_keys.append(keys)
    options_map.append(dict(zip(keys, vals)))

# Print out options in one map but not the other

only_in_0 = sorted(set(option_keys[0]).difference(set(option_keys[1])))
print blue('Only in %d' % execs[0])
print '\n'.join(only_in_0)
only_in_1 = sorted(set(option_keys[1]).difference(set(option_keys[0])))
print blue('Only in %d' % execs[1])
print '\n'.join(only_in_1)

# Print out options with different values

print blue('Differences')
for key in sorted(set(option_keys[0]) & set(option_keys[1])):
    val0 = options_map[0][key]
    val1 = options_map[1][key]
    if val0 != val1:
        print '{key}: {val0} | {val1}'.format(key=key, val0=val0, val1=val1)
