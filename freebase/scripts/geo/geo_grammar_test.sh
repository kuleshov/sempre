rlwrap java edu.stanford.nlp.sempre.Main\
    -executor SparqlExecutor\
    -endpointUrl http://localhost:3093/sparql\
    -Grammar.inPaths data/geo/geo.grammar\
    -Grammar.tags bridge join hardcode\
    -LexiconFn.lexiconClassName edu.stanford.nlp.sempre.fbalignment.lexicons.Lexicon\
    -SparqlExecutor.cachePath SparqlExecutor.cache\
    -interactive
