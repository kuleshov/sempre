#!/usr/bin/env python
import sys

'''
Look at differences in correct/mistakes between runs
'''

execs = [int(sys.argv[1]), int(sys.argv[2])]
execs_lines = list()
for k in range(len(execs)):
    execs_lines.append(open('state/execs/%d.exec/learner.events' % execs[k], 'r').read().strip().split('\n'))

execs_results = list()

for lines in execs_lines:
    results = dict()
    last_dev = False
    for l in lines[:-1]:  # Last line is summary
        if 'iter=6\tgroup=dev' in l:
            last_dev = True
        if not last_dev:
            continue
        d = dict()
        kvs = l.split('\t')
        for kv in kvs:
            #print kv
            k, v = kv.split('=', 1)
            d[k] = v
        results[d['utterance']] = d
    execs_results.append(results)

for k in execs_results[0]:
    if k not in execs_results[1]:
        print 'NO KEY:', k
        continue
    oracle1 = execs_results[0][k]['oracle']
    oracle2 = execs_results[1][k]['oracle']
    if oracle1 != oracle2:
        print k, oracle1, oracle2
        print '\t', execs_results[0][k]['targetValue']
