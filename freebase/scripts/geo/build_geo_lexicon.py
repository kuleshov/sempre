#!/usr/bin/env python
import os
import sys
import json
import re
from collections import defaultdict

SEMPARSE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SPEC_FILE = os.path.join(SEMPARSE_DIR, 'data/geo/geo.spec')  # PARAM
CATEGORIES = ('unary_predicates', 'binary_predicates', 'countries', 'states',
        'cities', 'landforms', 'rivers')  # PARAM


class LexiconEntry(object):

    def __init__(self, lexeme, formula, source='STRING_MATCH', features={}):
        # NOTE The binary lexicon entries seem to all have features
        self.lexeme = lexeme
        self.formula = formula
        self.source = source
        self.features = features

    def toJson(self):
        return json.dumps({
            'lexeme': self.lexeme,
            'formula': self.formula,
            'source': self.source,
            'features': self.features
        })


def preprocess_mappings(mappings):
    # Countries and states are fine, but cities have state appended to them
    city_mappings = mappings['cities']
    new_city_mappings = dict()
    for key in city_mappings:
        val = city_mappings[key]
        new_key = key.rsplit('_', 1)[0]
        new_city_mappings[new_key] = val
    mappings['cities'] = new_city_mappings


def add_manual_mappings(mappings):
    mappings['countries']['united states'] = 'fb:en.united_states_of_america'


def preprocess_key(key):
    # Not going to be in natural language
    # TODO Good amount more to do here I think
    if key in ['<', '>', '=', '<=', '>=']:
        return None
    # Some predicates till have geo tag to differentiate between two
    # predicate mappings
    key = key.split(':')[0]
    # Replace underscores in the key if any
    key = key.replace('_', ' ')
    # FIXME Hack for specific keys
    key = re.sub('loc$', 'in', key)
    key = re.sub('len$', 'length', key)
    # FIXME Currently FBFormulaInfo.hasOpposite <- BinaryLexicon.validBinaryFormula throws NullPointerException for "high point" formula
    if key == 'high point':
        return None
    return key


def read_spec_mappings(spec_file):
    mappings = defaultdict(dict)

    with open(SPEC_FILE, 'r') as f:
        lines = f.read().split('\n')

    for line in lines:
        if line.strip() == '':
            continue
        if line.startswith('#'):
            comment = line.split(' ')[1].lower()
            if comment in CATEGORIES:
                category = comment
                #print comment
            continue
        try:
            #print line
            key, val = line.split(' ', 1)
        except:
            print line
            raise
        mappings[category][key] = val

    return mappings


if __name__ == '__main__':
    OUT_DIR = sys.argv[1]

    UNARY_FILE = os.path.join(OUT_DIR, 'unaryLexicon.txt')
    BINARY_FILE = os.path.join(OUT_DIR, 'binaryLexicon.txt')

    # Read in spec file and build mappings dict

    mappings = read_spec_mappings(SPEC_FILE)
    #preprocess_mappings(mappings)
    add_manual_mappings(mappings)

    # Build the unary and binary lexicons

    unary_lexicon = list()
    binary_lexicon = list()

    for cat in CATEGORIES:
        if cat == 'binary_predicates':
            lexicon_type = 'binary'
        elif cat == 'unary_predicates':
            lexicon_type = 'unary'
            continue
        else:
            # Entities
            continue

        lexicon_entry_list = list()
        for key, val in mappings[cat].iteritems():
            key = preprocess_key(key)
            if key is None:
                continue
            # FIXME
            if key in ['next to', 'population', 'density', 'capital', 'river in'] or key.startswith('major'):
                continue

            lexicon_entry = LexiconEntry(key, val)
            lexicon_entry_list.append(lexicon_entry)

        if lexicon_type == 'unary':
            # Add to unary lexicon
            unary_lexicon.extend(lexicon_entry_list)
        else:
            # Add to binary lexicon
            binary_lexicon.extend(lexicon_entry_list)

    # Finally write to file
    with open(UNARY_FILE, 'w') as fout:
        fout.write('\n'.join([entry.toJson() for entry in unary_lexicon]))
    with open(BINARY_FILE, 'w') as fout:
        fout.write('\n'.join([entry.toJson() for entry in binary_lexicon]))
