./run @mode=train \
         @domain=geo \
         @sparqlserver=localhost:3093 \
         @cacheserver=local \
         -Learner.maxTrainIters 6
