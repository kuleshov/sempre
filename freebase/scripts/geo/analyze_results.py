#!/usr/bin/env python
import pprint
from collections import defaultdict
import sys

'''
Examine results of learner events, specifically whether
- parsed
- correct
'''

# Assumes want to look at last exec
with open('state/lastExec', 'r') as f:
    last_exec = int(f.read().strip())
if len(sys.argv) < 2:
    exec_to_use = last_exec
else:
    exec_to_use = int(sys.argv[1])

with open('state/execs/%d.exec/learner.events' % exec_to_use, 'r') as f:
    lines = f.read().strip().split('\n')

results = defaultdict(list)
for l in lines:
    d = dict()
    kvs = l.split('\t')
    for kv in kvs:
        #print kv
        k, v = kv.split('=', 1)
        d[k] = v
    eps = 0.5 if d['group'] == 'dev' else 0
    results[int(d['iter']) + eps].append(d)

# FIXME handle train vs dev
last_iter = max(results.keys())
results = results[last_iter]

# Check out ones that are correct
'''
print '-' * 80
print 'CORRECT'
print '-' * 80
pp = pprint.PrettyPrinter(indent=2)
for r in results[:-1]:
    if r['oracle'] == '1':
        #pp.pprint(r)
        print r['utterance']
        print '\t', r['targetValue']
'''

# Check out out ones that are wrong
print '# WRONG'
NUM_WRONG_TO_PRINT = float('inf')
count = 0
for r in results[:-1]:
    #if r['correct'] == '0':
    if r['oracle'] == '0':
        print '*' + r['utterance'] + '*'
        if r['parsed'] == '1':
            print '\tpred val:', r['predValue']
            print '\ttarg val:', r['targetValue']
            print '\tfallBeam:', r['fallOffBeam']
        else:
            print '\tfailed parse'
        count += 1
        if count >= NUM_WRONG_TO_PRINT:
            break

'''
# Check out ones that didn't parse
print '-' * 80
print 'FAILED PARSE'
print '-' * 80
for r in results[:-1]:
    if r['parsed'] == '0':
        print r['utterance']
'''

# Last line is summary
#pp.pprint(results[-1])
