import os,sys,re, operator, random
from andrewutil import normalizeName, getDirName, normalizeAnswer
import numpy as np
from sklearn import datasets
from sklearn import svm
from sklearn.linear_model import LogisticRegression

def getSeedWords(q):
  tmp = q.split()
  w1 = tmp[0]
  w2 = tmp[1]
  return w1, w2
def getFeatures(q, k, l, answers, gtent):
  #TODO use all the features
  invpos = 0
  if k in answers:
    pos = answers.index(k)
    #if pos==0:
    invpos = 1.0/(pos+1)
  #TODO could combine with the end features
  if q.startswith('what '):
    pass
  elif q.startswith('who '):
    pass
  elif q.startswith('when '):
    pass
  l = [[invpos]+list(elem) for elem in l]
  l = [elem[:NFEAT] for elem in l]
  x = np.array(l)
  m = np.mean(x, axis=0)
  m = np.append(m, [len(l), 1]) # add the number of instances and a 1
  m = np.tile(m, [len(l), 1])
  # add the means, etc to each example
  x = np.append(x, m, axis=1)
  if k == gtent:
    y = np.ones(shape = len(l))
  else:
    y = np.zeros(shape = len(l))
  return x,y
def containsGt(s, gtent):
  for k in s:
    if k == gtent:
      return True
  return False
def aggregateFeatures(q, featfilename, gtent, answers, useall):
  X = np.zeros(shape=(0,2*NFEAT+2))
  Y = np.zeros(shape=(0))
  ents = []
  with open(featfilename) as featf:
    featf.readline()
    line = featf.readline()
    s = eval(line)
    if gtent == None or containsGt(s, gtent):
      # only use 1 negative example
      keys = [k for k in s if k != gtent]
      negkey = None
      if len(keys):
        negkey = random.choice(keys)
      for k,l in s.items():
        if useall or k == gtent or k == negkey:
          x,y = getFeatures(q, k, l, answers, gtent)
          # add to the overall training stuctures
          # TODO do this more efficiently
          X = np.append(X, x, axis=0)
          Y = np.append(Y, y, axis=0)
          ents += len(l)*[k]
  return X,Y,ents
def extractQuestion(line):
  # ent1 is the ent in the question.
  # gtent is the/a correct answer.
  q,ent1,gtent,answers = line.split('>>>')
  answers = eval(answers)
  answers = [x[0] for x in answers]
  #print answers
  w1,w2 = getSeedWords(q)
  featname = 'answers/features/'+getDirName(q,w1,w2)+'/'+normalizeName(q)
  return q,ent1,gtent,answers,featname
def trainModel(train):
  X = np.zeros(shape=(0,2*NFEAT+2))
  Y = np.zeros(shape=(0))
  nTrainingPairs = 0
  for line in train:
    q,ent1,gtent,answers,featname = extractQuestion(line)
    if not os.path.exists(featname):
      continue
    _X,_Y,ents = aggregateFeatures(q, featname, gtent, answers, True)
    if X.size > 0:
      nTrainingPairs += 1
    # TODO do this more efficiently
    X = np.append(X, _X, axis=0)
    Y = np.append(Y, _Y, axis=0)
  model = LogisticRegression().fit(X, Y)
  #model = LogisticRegression(class_weight = 'auto').fit(X, Y)
  return model
def testModel(test, model):
  right = 0
  wrong = 0
  for line in test:
    q,ent1,gtent,answers,featname = extractQuestion(line)
    if not os.path.exists(featname):
      continue
    X,Y,ents = aggregateFeatures(q, featname, None, answers, True)
    #print X.shape
    #print len(ents)
    #print len(answers)
    if X.size == 0:
      wrong += 1
      continue
    probs = model.predict_proba(X)
    answer_index = np.argmax(probs[:,1])
    pred_ent = ents[answer_index]
    #print pred_ent
    # TODO do more sophisticated tp,fp,tn,fn analysis
    #corr = np.array([(e==pred_ent and y) or (e!=pred_ent and not y) for e,y in zip(ents,Y)])
    if pred_ent == gtent:
      #print pred_ent
      right += 1
    else:
      wrong += 1
  return right, wrong

########################################################################
if __name__ == "__main__":
  np.random.seed(0)
  random.seed(0)
  NFEAT = 4
  lines = []
  with open('data/labelledqs.txt') as inf:
    for line in inf:
      line = line.strip()
      q,ent1,gtent,answers,featname = extractQuestion(line)
      if not os.path.exists(featname):
        print q
        continue
      lines.append(line)
  random.shuffle(lines)
  print len(lines)

  nanswers = []
  pcts0 = []
  pcts1 = []
  pcts2 = []
  trainpcts2 = []
  for i in range(len(lines)):
    train = lines[:i] + lines[i+1:]
    test = [lines[i]]
    ############################ BASELINE 0: ORACLE ###########################
    right = 0
    wrong = 0
    for line in test:
      q,ent1,gtent,answers,featname = extractQuestion(line)
      if gtent in answers:
        #print gtent
        right += 1
      else:
        print '###############################################################'
        print q
        print gtent
        print answers
        print '###############################################################'
        wrong += 1
    pct0 = float(right)/(right+wrong)
    pcts0.append(pct0)
    nanswers.append(len(answers))
    ############################ BASELINE 1: 1st answer ###########################
    right = 0
    wrong = 0
    for line in test:
      q,ent1,gtent,answers,featname = extractQuestion(line)
      pred_ent = answers[0]
      #print pred_ent, gtent
      # TODO do more sophisticated tp,fp,tn,fn analysis
      if pred_ent == gtent:
        #print pred_ent
        right += 1
      else:
        wrong += 1
    pct1 = float(right)/(right+wrong)
    pcts1.append(pct1)

    ############################ BASELINE 2: Classifier ###########################
    model = trainModel(train)
    right,wrong = testModel(train, model)
    trainpcts2.append(float(right)/(right+wrong))
    right,wrong = testModel(test, model)
    pct2 = float(right)/(right+wrong)
    pcts2.append(pct2)
  nanswers = np.array(nanswers)
  print 'Mean number of answers:', np.mean(nanswers)
  print 'Std Dev number of answers:', np.std(nanswers)
  print 'Overall Percentage for Baseline 0 (Oracle):', np.mean(np.array(pcts0))
  print 'Overall Percentage for Baseline 1    (1st):', np.mean(np.array(pcts1))
  print '  Train Percentage for Baseline 2 (LogReg):', np.mean(np.array(trainpcts2))
  print 'Overall Percentage for Baseline 2 (LogReg):', np.mean(np.array(pcts2))
