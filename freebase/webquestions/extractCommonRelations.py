import os,sys,re
from collections import defaultdict as ddict
import operator

def removeNERtags(s):
  s = s.replace('<LOCATION>','').replace('</LOCATION>','')
  s = s.replace('<PERSON>','').replace('</PERSON>','')
  s = s.replace('<ORGANIZATION>','').replace('</ORGANIZATION>','')
  return s

NSPLITS = 1000
# types of NEs
locs = ddict(int)
ppl = ddict(int)
orgs = ddict(int)
rels = ddict(int)

types = ['person', 'place', 'org']
p1all = ['who', 'what', 'when', 'where'] #, 'which']
v1all = ['did', 'was']#, 'were', 'are', 'is', 'do', 'does'] # 'will'
for t1 in types:
  for p1 in p1all:
    for v1 in v1all:
      pref = p1+' '+v1+' '
      with open('output/ner_relations_'+t1+'_'+p1+'_'+v1+'.txt') as inf:
        for line in inf:
          li = line.find('<LOCATION>')
          pi = line.find('<PERSON>')
          oi = line.find('<ORGANIZATION>')
          print li, pi, oi, line
          if li>=0 and (pi<0 or li<pi) and (oi<0 or li<oi):
            m = re.search(pref+r'<LOCATION>(.*?)</LOCATION>(.*)', line)
            if m:
              rel = ('L', pref.lower(), removeNERtags( m.group(2)).lower())
              rels[rel] += 1
              locs[m.group(1).lower()] += 1
          elif pi>=0 and (oi<0 or pi<oi):
            m = re.search(pref+r'<PERSON>(.*?)</PERSON>(.*)', line)
            if m:
              rel = ('P', pref.lower(), removeNERtags( m.group(2)).lower())
              rels[rel] += 1
              ppl[m.group(1).lower()] += 1
          elif oi>=0:
            m = re.search(pref+r'<ORGANIZATION>(.*?)</ORGANIZATION>(.*)', line)
            if m:
              rel = ('O', pref.lower(), removeNERtags( m.group(2)).lower())
              rels[rel] += 1
              orgs[m.group(1).lower()] += 1
      #break
    #break
sorted_rels = sorted(rels.iteritems(), key=operator.itemgetter(1), reverse=True)
sorted_locs = sorted(locs.iteritems(), key=operator.itemgetter(1), reverse=True)
sorted_ppl = sorted(ppl.iteritems(), key=operator.itemgetter(1), reverse=True)
sorted_orgs = sorted(orgs.iteritems(), key=operator.itemgetter(1), reverse=True)
print len(sorted_rels)
print len(sorted_locs)
print len(sorted_ppl)
print len(sorted_orgs)
div = ' ### '
with open('output/all_common_relations.txt', 'w') as outf:
  for ent,num in sorted_rels:
    if num==1:
      break
    if len(ent[2]) >= 2:
      outf.write(ent[0]+div+str(num)+div+ent[1]+div+ent[2]+'\n')

