from andrewutil import getOutnameAndMakeDirQOnly
import StringIO, zlib, os, urllib2, gzip

def decode (page):
  encoding = page.info().get("Content-Encoding")
  if encoding in ('gzip', 'x-gzip', 'deflate'):
    content = page.read()
    if encoding == 'deflate':
      data = StringIO.StringIO(zlib.decompress(content))
    else:
      data = gzip.GzipFile('', 'rb', 9, StringIO.StringIO(content))
    page = data.read()
  return page
def gzipFile(outname):
  gzcmd = 'gzip '+outname
  os.system(gzcmd)
def getLynxDump(outname):
  cmdstr = "lynx -dump %s" % outname
  print cmdstr
  cmd = os.popen(cmdstr)
  output = cmd.read()
  cmd.close()
  return output
def saveUrlForEnt(ent, url, outdir):
  name = getOutnameAndMakeDirQOnly(outdir, ent, True) + '.html'
  print name
  if not os.path.exists(name) or os.stat(name)[6] == 0:
    print url
    headers = {
      'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16',
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'Accept-Language': 'en-us,en;q=0.5',
      'Accept-Encoding': 'gzip',
      'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.7',
      'Keep-Alive': '115',
      'Connection': 'keep-alive'}
    req = urllib2.Request(url);
    for h,v in headers.items():
      req.add_header(h,v)
    response = urllib2.urlopen(req)
    html = decode(response)
    outf = open(name, 'w')
    outf.write(html)
    outf.close()
