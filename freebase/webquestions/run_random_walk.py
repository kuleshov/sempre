import os, sys, time
if __name__ == "__main__":
  # jaclyn is not actually special but it has ipython which is good for debugging
  special = ['nlp', 'jamie', 'jacob', 'janet', 'jay', 'jaclyn']
  allnodes = ['jack', 'jackfruit', 'jackknife', 'jacko', 'jackson', 'jaclyn', 'jagupard1', 'jagupard2', 'jagupard3', 'jen', 'jeremy', 'jerome', 'jess', 'john0', 'john1', 'john2', 'john3', 'john4', 'john5', 'jude0', 'jude1', 'jude10', 'jude11', 'jude12', 'jude13', 'jude14', 'jude15', 'jude16', 'jude17', 'jude18', 'jude19', 'jude2', 'jude20', 'jude21', 'jude22', 'jude23', 'jude24', 'jude25', 'jude26', 'jude27', 'jude28', 'jude29', 'jude3', 'jude4', 'jude5', 'jude6', 'jude7', 'jude8', 'jude9']
  allnodes = [x for x in set(allnodes)]
  allnodes.sort()
  print allnodes
  free = ['jack', 'jackfruit', 'jackknife', 'jaclyn', 'jagupard1', 'jagupard3', 'janet', 'jeremy', 'john2', 'john5', 'jude10', 'jude20', 'jude21', 'jude22', 'jude23', 'jude27', 'jude29', 'jude3', 'jude5', 'jude6', 'jude7', 'jude8']
  free = [x for x in free if x not in special]
  free.sort()
  print free
  terminate = 0
  if len(sys.argv) >= 2:
    terminate = int(sys.argv[1])
  N = len(allnodes)
  num = 11 #TODO should be 0
  for node in free:
    assert(node in allnodes)
    i = allnodes.index(node)
    cmd = 'qsub -v index='+str(num)
    cmd += ' run_random_walk.script -l nodes='+node+'.stanford.edu'
    print cmd
    ret = os.system(cmd)
    print ret
    if ret != 0:
      continue
    time.sleep(60)
    num += 1
    if num == 18:
      break
