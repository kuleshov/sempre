import urllib, urllib2, urlparse
import string, time, calendar, sys, os
import gzip, StringIO, random
import filterOutput
import socket
import types, re
from andrewutil import normalizeName, getDirName, Error, getOutnameAndMakeDir,\
    t2q, getOutnameAndMakeDirQOnly, getDirNameQOnly
from common import *
from ioUtils import readLinesFromFile
from htmlUtils import gzipFile, getLynxDump
#import httplib2
sys.path.append('BeautifulSoup-3.2.1/')
from BeautifulSoup import BeautifulSoup, SoupStrainer

def fixurl(url):
  if url.find(';') >= 0:
    url = re.sub(r"&#39;", "'", url)
    url = re.sub(r"&#699;", "'", url)
  # turn string into unicode
  if not isinstance(url,unicode):
    url = url.decode('utf8')
  # parse it
  parsed = urlparse.urlsplit(url)
  parsed = parsed[2]+parsed[4]
  # encode each component
  path = '/'.join(  # could be encoded slashes!
    urllib.quote(urllib.unquote(pce).encode('utf8'),'')
    for pce in parsed.split('/')
  )
  # put it back together
  url = urlparse.urlunsplit(('','',path,'',''))
  if url.find('ndash') >= 0:
    url = re.sub(r"%26ndash%3B", "%E2%80%93", url)
  return url
def getMidToFbIdMap():
  print 'Getting mID To FbId Mapping...'
  mIdsToFbIds = {}
  pref = 'http://en.wikipedia.org'
  prefLen = len(pref)
  i = 0
  # 27933717 lines
  if (not os.path.exists(MID_TO_FBID_MAP_CACHE)) or os.stat(MID_TO_FBID_MAP_CACHE)[6] == 0:
    outf = open(MID_TO_FBID_MAP_CACHE, 'w')
    inf = open(MID_TO_FBID_MAP)
    for line in inf:
      mid,fbid = line.split()
      if not fbid.startswith('fb:en.'):
        continue
      assert(mid.startswith('fb:'))
      mid = mid[3:]
      fbid = fbid[6:]
      mIdsToFbIds[mid] = fbid
      outf.write(mid + ' ' + fbid + '\n')
      i += 1
      if i%1000000==0:
        print i
    inf.close()
    outf.close()
  else:
    inf = open(MID_TO_FBID_MAP_CACHE)
    for line in inf:
      mid,fbid = line.split()
      mIdsToFbIds[mid] = fbid
      i += 1
      if i%1000000==0:
        print i
    inf.close()
  return mIdsToFbIds
def getUrlToMidMap():
  print 'Getting url To mID Mapping...'
  urlsToMids = {}
  pref = 'http://en.wikipedia.org'
  prefLen = len(pref)
  i = 0
  # 3834877 lines
  if (not os.path.exists(WIKIPEDIA_TO_MID_MAP_CACHE)) or os.stat(WIKIPEDIA_TO_MID_MAP_CACHE)[6] == 0:
    outf = open(WIKIPEDIA_TO_MID_MAP_CACHE, 'w')
    inf = open(WIKIPEDIA_TO_MID_MAP)
    for line in inf:
      mid,rel,url = line.split()
      url = url[1:-2]
      assert(url.startswith(pref))
      url = url[prefLen:]
      if rel != 'ns:common.topic.topic_equivalent_webpage':
        continue
      #print mid
      #print url
      url2 = fixurl(url)
      #print url2
      urlsToMids[url2] = mid
      outf.write(mid + ' ' + url2 + '\n')
      i += 1
      if i%1000000==0:
        print i
    inf.close()
    outf.close()
  else:
    inf = open(WIKIPEDIA_TO_MID_MAP_CACHE)
    for line in inf:
      mid,url = line.split()
      mid = mid.strip()
      url = url.strip()
      urlsToMids[url] = mid
      i += 1
      if i%1000000==0:
        print i
    inf.close()
  return urlsToMids
def getGreenSitesOnGoogleSearch(soup):
  cites = soup.findAll('cite')
  if len(cites) == 0:
    green = soup.findAll('font', {'color':'green'})
    cites = [g.text for g in green]
    cites = [re.sub(r'\s*-\s*[0-9]+[a-z]$', '', x) for x in cites]
    cites = [unicode(x) for x in cites]
  else:
    cites = [x.text for x in cites]
  citesFinal = []
  for cite in cites:
    if not cite.startswith('en.wikipedia.org'):
      continue
    if cite.startswith('en.wikipedia.org/wiki/File:'):
      continue
    if cite.startswith('en.wikipedia.org/wiki/File_talk:'):
      continue
    if cite.startswith('en.wikipedia.org/wiki/User:'):
      continue
    citesFinal.append(cite)
  return citesFinal
def getTopHits(n, overwrite, ents, urlsToMids, seed, extension = 'Bfs'):
  print 'Running getTopHits'
  malformed = [x for x in urlsToMids.iteritems() if not x[0].startswith('/wiki')]
  assert(len(malformed) == 0)

  outdir = REPRO_OUT + 'enturls'+extension+'/'
  os.system('mkdir -p '+outdir)
  outfile = outdir + '_'.join(['seed', str(seed), 'n', str(n)])
  prelimoutfile = outdir + '_'.join(['prelim', 'seed', str(seed), 'n', str(n)])
  pref = 'site:en.wikipedia.org'
  prefLen = len('en.wikipedia.org')
  print 'outfile:', outfile
  print 'prelimoutfile:', prelimoutfile

  if (not os.path.exists(outfile)) or os.stat(outfile)[6] == 0 or overwrite:
    tophits = {}
    if (not os.path.exists(prelimoutfile)) or os.stat(prelimoutfile)[6] == 0:
      i=0
      nNoCitesInARow = 0
      for ent in ents:
        print i, 'of', len(ents), 'in ents.'
        i+=1
        assert(ent != '')
        print ent
        q = ent+' '+pref
        inname = getOutnameAndMakeDirQOnly(GOOGLE_CUSTOM_SEARCH_PICKLES, q, True) + '.pickle'
        if (not os.path.exists(inname)) or os.stat(inname)[6] == 0:
          print q
          print inname
          Error('Make sure to run randomwalkCustomEntSearch.py')
        results = pickle.load(open(inname))
        items = results['items']
        for item in items:
          url = item['formattedUrl']
          if url not in urlsToMids:
            print 'url not in urlsToMids:'
            print url
            sys.exit()
          tophits[ent] = url
          break
      outf = open(prelimoutfile, 'w')
      for e,url in tophits.iteritems():
        outf.write(e + '###' + url +'\n')
      outf.close()
    else:
      inf = open(prelimoutfile)
      for line in inf:
        line = line.strip().split('###');
        e = line[0]
        url = line[1]
        tophits[ent] = url
      inf.close()

    outf = open(outfile, 'w')
    i = 0
    for e,url in tophits.iteritems():
      print i, 'of', len(tophits), 'in tophits.'
      i+=1
      req = urllib2.Request('http://'+url, headers={'User-Agent' : "Magic Browser"})
      try:
        con = urllib2.urlopen( req )
      except Exception, ex:
        print ex
        print 'Couldnt reach a URL:'
        print 'Ent:', e, 'en.wikipedia.org'
        print 'url:', url
        print 'This needs to be debugged, but continuing for now.'
        continue
        outf.close()
        Error('This needs to be debugged.')
      #except urllib2.HTTPError, er:
      #except urllib2.URLError:
      outf.write(e + '###' + url +'\n')
    outf.close()
  else:
    tophits = {}
    inf = open(outfile)
    for line in inf:
      if len(line.split('###')) != 2:
        print ent
        print url
        print line
        print outfile
      ent,url = [l.strip() for l in line.split('###')]
      if not url.startswith('en.wikipedia.org'):
        print e, url
        print outfile
        Error('bad url')
      tophits[ent] = url
    inf.close()
  return tophits
def old_getTopHits(n, overwrite, ents, urlsToMids, seed, extension = 'Bfs'):
  print 'Running getTopHits'
  malformed = [x for x in urlsToMids.iteritems() if not x[0].startswith('/wiki')]
  assert(len(malformed) == 0)

  outdir = REPRO_OUT + 'enturls'+extension+'/'
  os.system('mkdir -p '+outdir)
  outfile = outdir + '_'.join(['seed', str(seed), 'n', str(n)])
  prelimoutfile = outdir + '_'.join(['prelim', 'seed', str(seed), 'n', str(n)])
  pref = 'site:en.wikipedia.org'
  prefLen = len('en.wikipedia.org')
  print 'outfile:', outfile
  print 'prelimoutfile:', prelimoutfile

  if (not os.path.exists(outfile)) or os.stat(outfile)[6] == 0 or overwrite:
    tophits = {}
    if (not os.path.exists(prelimoutfile)) or os.stat(prelimoutfile)[6] == 0:
      i=0
      nNoCitesInARow = 0
      for ent in ents:
        print i, 'of', len(ents), 'in ents.'
        i+=1
        assert(ent != '')
        print ent
        q = ent+' '+pref
        inname = getOutnameAndMakeDirQOnly(GOOGLE_RESULTS_HTML, q, True) + '.html'
        if (not os.path.exists(inname)) or os.stat(inname)[6] == 0:
          print q
          print inname
          Error('Make sure to run randomwalkEntSearch.py')
        url = 'file://' + os.getcwd() + '/' + inname
        #print url
        soup = BeautifulSoup(urllib.urlopen(url))
        #cites = soup.findAll('cite')
        # scrape the green urls on google
        #green = soup.findAll('font', {'color':'green'})
        #TODO
        cites = getGreenSitesOnGoogleSearch(soup)
        if len(cites) == 0:
          nNoCitesInARow += 1
          print 'WARNING NO SITES FOR:'
          print soup
          print q
          print ent
          print url
          print inname
          print 'nNoCitesInARow:', nNoCitesInARow
          if nNoCitesInARow >= 10:
            Error('Too many no-cites in a row. You should check that there was no rate limiting.')
        else:
          nNoCitesInARow = 0
        for cite in cites:
          #cite = fixurl(cite)
          #cite = unicode(cite)
          #cite = re.sub(r'<cite>(.*)</cite>', '\\1', cite)
          #cite = re.sub(r'<b>(.*?)</b>', '\\1', cite)
          print repr(cite)
          url = fixurl(cite)
          url2 = url[prefLen:]
          url3 = re.sub('^/\.\.\.', '/wiki', url2)
          if url3 not in urlsToMids:
            print 'Not in mapping from urls to mIDs'
            print 'XXXXXXXXXXXXXXXXXXXXXXX', ent
            print 'XXXXXXXXXXXXXXXXXXXXXXX', url
            print 'XXXXXXXXXXXXXXXXXXXXXXX', url2
            print 'XXXXXXXXXXXXXXXXXXXXXXX', url3
            continue
          else:
            print 'AAAAAAA', url3
          tophits[ent] = url
          break
      outf = open(prelimoutfile, 'w')
      for e,url in tophits.iteritems():
        outf.write(e + '###' + url +'\n')
      outf.close()
    else:
      inf = open(prelimoutfile)
      for line in inf:
        line = line.strip().split('###');
        e = line[0]
        url = line[1]
        tophits[ent] = url
      inf.close()

    outf = open(outfile, 'w')
    i = 0
    for e,url in tophits.iteritems():
      print i, 'of', len(tophits), 'in tophits.'
      i+=1
      req = urllib2.Request('http://'+url, headers={'User-Agent' : "Magic Browser"})
      try:
        con = urllib2.urlopen( req )
      except Exception, ex:
        print ex
        print 'Couldnt reach a URL:'
        print 'Ent:', e, 'en.wikipedia.org'
        print 'url:', url
        print 'This needs to be debugged, but continuing for now.'
        continue
        outf.close()
        Error('This needs to be debugged.')
      #except urllib2.HTTPError, er:
      #except urllib2.URLError:
      outf.write(e + '###' + url +'\n')
    outf.close()
  else:
    tophits = {}
    inf = open(outfile)
    for line in inf:
      if len(line.split('###')) != 2:
        print ent
        print url
        print line
        print outfile
      ent,url = [l.strip() for l in line.split('###')]
      if not url.startswith('en.wikipedia.org'):
        print e, url
        print outfile
        Error('bad url')
      tophits[ent] = url
    inf.close()
  return tophits
def getEntToFbIdMap(tophits, urlsToMids, mIdsToFbIds):
  pref = 'en.wikipedia.org'
  prefLen = len(pref)
  entToFbIdMap = {}
  num = 0
  for e,url in tophits.iteritems():
    assert(url.startswith(pref))
    url = url[prefLen:]
    if url in urlsToMids:
      mid = urlsToMids[url][3:]
      if mid in mIdsToFbIds:
        fbid = mIdsToFbIds[mid]
        #print e, url, mid, fbid
        entToFbIdMap[e] = fbid
        num += 1
      else:
        print e, url, mid
    else:
      print e, url
  print num, 'of', len(tophits), 'found.'
  return entToFbIdMap

#######################################################
if __name__ == "__main__":
  if len(sys.argv) not in [2,3]:
    Error('Usage: getTopGoogleResultsForEnt.py <n> <overwrite - optional>')
  n = int(sys.argv[1])
  overwrite = False
  if len(sys.argv) == 3:
    overwrite = bool(sys.argv[2])
  skipLynx = False
  ents = set()
  from wikianswersRandomWalkBfs import loadRandomWalkBfsQs
  for seed in range(3):
    qs, outbase, newqoutbase, qsAndTemplates = loadRandomWalkBfsQs(False, n, seed)
    for q,t in qsAndTemplates:
      t = t.split(' _')
      assert(len(t) == 3)
    ents.update([t.split(' _')[1].strip() for q,t in qsAndTemplates])
    break
  print len(ents)
  #################
  urlsToMids = getUrlToMidMap()
  tophits = getTopHits(n, overwrite, ents, urlsToMids, 'all')
  mIdsToFbIds = getMidToFbIdMap()
  ####
  entToFbIdMap = getEntToFbIdMap(tophits, urlsToMids, mIdsToFbIds)
  print 'DONE'
  sys.exit()
