import os, sys, random, re
from andrewutil import normalizeName, getDirName, Error, t2q
from saveAmtAnswers import isNegativeAnswer
from createAmtData2 import getAmtResultsName, getAmtNameWithSuffix
from aggregateRandomWalkDfs import readTemplates
from randomwalkEntSearch import getGoodSeedNums
from common import *
from createSemanticParserInputAmtCsv import getAmtAnswers3, writeAnswers

####################################################################
'''
0('where was', 'barack obama', 'born'), \
1('what is the captial of', 'california', ''), \
2('who was', 'google', "'s ceo"), \
3('what is the tallest mountain in', 'africa', ''), \
6('where was the', 'titanic', 'built'), \
8('what movies has', 'oprah', 'produced'), \
9('what countries border', 'india', ''), \
10('what country is', 'heathrow', 'in'), \
11('where was', 'einstein', 'a professor'), \
14('what bridges are in', 'new york', ''), \
19('what towers are in', 'dubai', ''), \
20('where is', 'andreessen horowitz', 'located'), \

4('who wrote', 'the hobbit', ''), \
7('where does', 'manchester united', 'play'), \
13('who founded', 'nike', ''), \
16('what shows has', 'david letterman', 'hosted'), \
17('who endorses', 'smart water', ''), \
21('what city is', 'flo rida', 'from'), \
22('what was', 'kelly clarkson', "'s first album"), \
'''
########################################################################
if __name__ == "__main__":
  random.seed(0)
  if len(sys.argv) != 2:
    Error('Usage createSemanticParserInputFromAmt.py <bfs trial>')
  trial = int(sys.argv[1])
  ######################
  seeds = getGoodSeedNums()
  #pplLocOrgSeeds = [1,2,3,4,7,10,11,14,20,21]
  pplLocOrgSeeds = [0,1,2,3,6,8,9,10,11,14,19,20]
  pplLocOrgTmps = set()
  for seed in pplLocOrgSeeds:
    tmp, qs = readTemplates(50000, seed, RANDOM_WALK_BFS_DIR)
    pplLocOrgTmps.update(tmp)
  pplLocOrgTmps = [t2q(' _ '.join(list(t))) for t in pplLocOrgTmps]
  #sportEntertainmentSeeds = [6,8,9,13,16,17,19,22,23]
  sportEntertainmentSeeds = [4,7,13,16,17,21,22]
  sportEntertainmentTmps = set()
  for seed in sportEntertainmentSeeds:
    tmp, qs = readTemplates(50000, seed, RANDOM_WALK_BFS_DIR)
    sportEntertainmentTmps.update(tmp)
  sportEntertainmentTmps = [t2q(' _ '.join(list(t))) for t in sportEntertainmentTmps]

  ######################
  good,bad,entsOnly,entsAndDates = getAmtAnswers3(trial)
  n1 = 0
  n2 = 0
  both = 0
  neither = 0
  qs1 = []
  qs2 = []
  for q,u,a in entsAndDates:
    if q in pplLocOrgTmps and q in sportEntertainmentTmps:
      n1 += 1
      n2 += 1
      both += 1
    elif q in pplLocOrgTmps:
      n1 += 1
      qs1.append((q,u,a))
    elif q in sportEntertainmentTmps:
      n2 += 1
      qs2.append((q,u,a))
    else:
      neither += 1
      print 'XXX'+q+'XXX'
  print n1, n2, both, neither
  #1608 1315 951 639
  writeAnswers(getAmtNameWithSuffix(trial, '_pplLocOrgSeeds.examples'), qs1)
  writeAnswers(getAmtNameWithSuffix(trial, '_sportEntertainmentSeeds.examples'), qs2)
  sys.exit()
  # q,u,a

  writeAnswers(getAmtNameWithSuffix(trial, '_entsAndDates.examples'), entsAndDates)
  #sys.exit()
  writeAnswers(getAmtNameWithSuffix(trial, '_good.examples'), good)
  writeAnswers(getAmtNameWithSuffix(trial, '_entsOnly.examples'), entsOnly)
  writeAnswers(getAmtNameWithSuffix(trial, '_bad.examples'), bad)
