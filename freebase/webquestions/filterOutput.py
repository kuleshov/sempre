def filterOutputLynx(o):
  import re
  o=o.split('\n')
  o2 = []
  header=True
  start=False
  for line in o:
    #print header, start
    #print line
    if header:
      #if line.startswith('   Web Results 1 -'):
      if re.match(r'^\s+Web\s+Results\s+1\s+-', line)!=None or line.startswith('Search Options'):
        header= False
    elif not start:
      if line=='':
        start= True
    elif re.match(r'^\s+Searches related to', line)!=None:
      break
    elif line=='References':
      o2 = o2[:-9]
      while re.match(r'^\s*[0-9]+\s*\[[0-9]*\]$', o2[-1])!=None:
        o2.pop()
      break
    else:
      o2.append(line)
  o2='\n'.join(o2)
  o2=o2.split('\n\n')
  o3=[]
  for line in o2:
    line = re.sub(r'\s\s+',' ',line.replace('\n',''))
    line = re.split(r'[^\/]\.\.\.[^\/]', line)
    line = '\n'.join(line)
    o3.append(line)
  return o3
def filterOutput(o):
  import re
  o=o.split('\n')
  o2 = []
  header=True
  start=False
  adcountpart = 0
  for line in o:
    #print header, start, adcountpart
    #print line
    if header:
      #if line.find('Show search tools')>=0:
      if line.startswith('Search Options'):
        header= False
    elif not start:
      if line=='':
        start= True
    elif line.find('Searches related to')>=0:
      break
    elif line=='References':
      break
    elif line.find('Show search tools')>=0:
      continue
    elif line=='Ad':
      adcountpart = 1
    elif adcountpart>0:
      if line=='':
        adcountpart += 1
      if adcountpart==3:
        adcountpart = 0
    else:
      o2.append(line)
  o2='\n'.join(o2)
  o2=o2.split('\n\n')
  o3=[]
  for line in o2:
    line = re.sub(r'\s\s+',' ',line.replace('\n',''))
    line = re.split(r'[^\/]\.\.\.[^\/]', line)
    line = '\n'.join(line)
    o3.append(line)
  return o3
