import re, sys
def Error(s):
  print s
  sys.exit(1)
def normalizeAnswer(x):
  x = x.lower()
  x = x.replace('.','')
  x = re.sub('\s+',' ', x)
  x = re.sub(r'(^|\s)([a-z]) ([a-z]) ', r'\1\2\3 ', x)
  x = x .strip()
  return x
def normalizeName(n):
  n = n.replace(' ','_').replace("'","").replace("\\","").replace("/","").replace('?','_').replace(')','_').replace('(','_').replace("'", "-").replace('$','-').replace('`','-').replace('*','-')
  return n

def getDirNameQOnly(q):
  q = normalizeName(q)
  while len(q) < 3:
    q += '_'
  return q[:3]
def getOutnameAndMakeDirQOnly(outbase, q, useEnts):
  outname = outbase
  if useEnts:
    outname+= 'ents/'
  outname+= getDirNameQOnly(q)
  import os
  os.system('mkdir -p '+outname)
  outname+= '/'+normalizeName(q)
  return outname
def getDirName(q, w1, w2):
  w1 = normalizeName(w1)
  w2 = normalizeName(w2)
  q = normalizeName(q)
  return q[len(w1)+len(w2)+2:len(w1)+len(w2)+5].replace(' ','_')

def getOutnameAndMakeDir(outbase, q):
  outname = outbase
  tmp = q.split()
  w1 = tmp[0]
  w2 = tmp[1]
  outname+= w1+'_'+w2+'/'
  outname+= getDirName(q, w1, w2)
  import os
  os.system('mkdir -p '+outname)
  outname+= '/'+normalizeName(q)
  return outname, w1, w2

def getSuggestOutName(outbase, rel):
  outname = outbase+'suggest_'+normalizeName(rel[0])+'___'
  outname += normalizeName(rel[1])+'.txt'
  return outname

def normalizeUnicode(s):
  if isinstance(type(s), unicode):
    import unicodedata
    s = unicodedata.normalize('NFKD', s).encode('ascii','ignore')
    return str(s)
  return s

# t is a template
def getRandomWalkSuggestOutName(outbase, t, index):
  assert(outbase[-1] == '/')
  t = [normalizeName(x.strip()) for x in t]
  t[index] = ''
  outfile = 'suggest_'+'___'.join(t)
  outdir = re.sub(r'_','', outfile)[-3:] + '/'
  import os
  os.system('mkdir -p '+outbase+outdir)
  outname = outbase+outdir+outfile
  return outname

# converts template to q
def t2q(t):
  return t.strip('? ').replace(' _', '').replace(" '","'")+'?'
def tlist2tstr(tl):
  assert(len(tl) == 3)
  ts = tl[0].strip() + ' ' + tl[1].strip()
  if len(tl[2]) > 0 and (not tl[2][0] == "'") and (not tl[2][0] == "?"):
    ts += ' '
  ts += tl[2].strip()
  return ts
