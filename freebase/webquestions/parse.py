import string, time, calendar, sys, re
def getTree(treestr):
  treestr = treestr.replace("'","^")
  t=eval(str(treestr.replace('(',' [').replace(')','] ').split()).replace("'[","['").replace("]'","']").replace(", ''","").replace("^","\\'"))
  return t[0]
def getPOS(tree, pos):
  nodes = [tree]
  index = -1
  while len(nodes)>0:
    index+=1
    val = nodes.pop(0)
    #print type(val), len(val), val
    if type(val) is list and len(val)>1:
      if val[0]==pos:
        return val[1:], index
      nodes += val[1:]
  return None, index
def getStr(tree, exc=None):
  if type(tree)==str:
    return tree
  strs = []
  if type(tree[0])==list:
    strs.append(getStr(tree[0], exc))
  elif exc and type(tree[0])==str and tree[0]==exc:
    return ''
  for n in tree[1:]:
    strs.append(getStr(n, exc))
  return ' '.join(strs).strip().replace(" '","'")

i=0
Num = 6000000
trees = []
#with open('output/parses_when.txt') as inf:
with open('output/parses_all.txt') as inf:
  treestr=''
  intree= True
  for line in inf:
    line=line.strip()
    if intree:
      treestr += line
    if line=='':
      if intree:
        trees.append(getTree(treestr))
        #print treestr
        i+=1
        if i%10000==0:
          print 'Parsing', i
        if i==Num:
          break
        treestr = ''
      intree= not intree
#sys.exit()
#print trees
vps = {}
nps = {}
i=0
for tree in trees:
  i+=1
  if i%10000==0:
    print 'Validating', i
  vpl,vi = getPOS(tree, 'VP')
  npl,ni = getPOS(tree, 'NP')
  if vpl!=None and npl!=None and ni<vi and ni>=7:
    vp = getStr(vpl)
    np = getStr(npl, 'VP')
    print ni, vi, 'NP:', np, '       VP:', vp
    line = getStr(tree)
    line = line.split()
    prefix = ' '.join(line[:2])
    if prefix not in nps:
      vps[prefix] = set()
      nps[prefix] = set()
    vps[prefix].add(vp)
    nps[prefix].add(np)
for p in nps:
  nps[p] = [np for np in nps[p]]
  nps[p].sort()
  vps[p] = [vp for vp in vps[p]]
  vps[p].sort()
print nps
print vps

w1 = ['who', 'what', 'when', 'where', 'which']
w2 = ['did', 'was', 'were', 'are', 'is', 'will', 'do', 'does']
for k in nps:
  kl = k.split()
  if (kl[0] not in w1) or (kl[1] not in w2):
    continue
  kstr = k.replace(' ','_')
  with open('output/parsed_objects_'+kstr+'.txt', 'w') as of:
    for v in nps[k]:
      of.write(v+'\n')
  with open('output/parsed_relations_'+kstr+'.txt', 'w') as of:
    for v in vps[k]:
      of.write(v+'\n')

