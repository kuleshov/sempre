import json, urllib, urllib2 # contacting websites
import string, calendar, sys, os, re
import gzip, StringIO, random, socket, types
import filterOutput
from math import ceil, log
from andrewutil import normalizeName, getDirName, Error, getOutnameAndMakeDir,\
    t2q, getOutnameAndMakeDirQOnly, getDirNameQOnly, tlist2tstr
from common import *
from ioUtils import readLinesFromFile
from htmlUtils import gzipFile, getLynxDump
from randomwalkRw import getRandomWalkDir
import cPickle as pickle
from randomwalkEntSearch import loadRwTemplates, loadBfsTemplates, getGoodSeedNums
from randomwalkCustomEntSearch import doCustomGoogleSearch, checkResultsForErrors

def readQs(name, allas):
  qs = []
  ans = []
  inf = open(name)
  for line in inf:
    line = line.strip()
    m = re.match(r'\(utterance [\'"](.*)[\'"]', line)
    if m:
      q = m.group(1).strip('?')
      qs.append(q)
    else:
      m = re.match(r'\(targetValues \(description [\'"](.*)[\'"]\)\)', line)
      if m:
        a = m.group(1)
        ans.append(a)
  inf.close()
  assert(len(qs) == len(ans))
  for q,a in zip(qs, ans):
    allas[q] = a
  return qs
def cleanUpCustomGoogleSearchPickles(qs, resultsdir):
  for q in qs:
    outname = getOutnameAndMakeDirQOnly(resultsdir, q, False)
    outname = outname + '.pickle'
    print outname
    if os.path.exists(outname) and os.stat(outname)[6] > 0:
      results = pickle.load(open(outname))
      error = checkResultsForErrors(results)
      if error:
        print 'Error in doCustomGoogleSearch'
        cmd = 'rm -f '+outname
        print cmd
        os.system(cmd)
#######################################################
if __name__ == "__main__":
  random.seed(0)
  if len(sys.argv) not in [2]:
    Error('Usage: google_baseline.py <dataset>')
  datasetnum = int(sys.argv[1])
  if datasetnum == 6:
    allas = {}
    trainqs = readQs('data/dataset_6/bfs_6_entsAndDates_checked2x.examples', allas)
    testqs = readQs('data/dataset_6/bfs_6_entsAndDates_checked2x.examples.fb2', allas)
    searchResultsFile = 'data/dataset_6/search_results.pickle'
  elif datasetnum == 8:
    Error('datasetnum is bad')
  else:
    Error('datasetnum is bad')
  allqs = trainqs + testqs
  if os.path.exists(searchResultsFile):
    allresults = pickle.load(open(searchResultsFile))
  else:
    outbase = GOOGLE_CUSTOM_SEARCH_PICKLES
    #cleanUpCustomGoogleSearchPickles(allqs, outbase)
    allresults = {}
    i=0
    for q in allqs:
      print i, 'of', len(allqs), ':', q
      i+=1
      outname = getOutnameAndMakeDirQOnly(outbase, q, False)
      if os.path.exists(outname) and os.stat(outname)[6] > 0:
        continue
      #continue
      try:
        results = doCustomGoogleSearch(q, outbase, True)
        allresults[q] = results
        #sys.exit()
      except Exception, inst:
        print inst
        print 'Outer retry'
        sys.exit()
      print outname
    print 'DONE'
    print i
    pickle.dump(allresults, open(searchResultsFile, 'w'))
  allresultsList = [(q,r) for q,r in allresults.iteritems()]
  input = GOOGLE_NER + 'input' + str(datasetnum)
  output = GOOGLE_NER + 'output' + str(datasetnum)
  ANSWER_FLAG = 'ANSWER: '
  if not os.path.exists(output):
    os.system('mkdir -p '+GOOGLE_NER)
    outf = open(input, 'w')
    i = 0
    for q,r in allresultsList:
      outf.write(ANSWER_FLAG + allas[q] + '\n')
      items = r['items']
      for item in items:
        snippet = item['snippet']
        if isinstance(snippet, unicode):
          snippet = snippet.encode('utf-8')
        assert(not snippet.startswith(ANSWER_FLAG))
        outf.write(snippet+'\n')
        i += 1
    outf.close()
    print i
    cwd = os.getcwd()
    os.chdir(SCRIPTS_DIR)
    os.system(NER_EXE)
    os.chdir(cwd)
  inf = open(output)
  for q,r in allresultsList:
    items = r['items']
    for i,item in enumerate(items):
      snippet = inf.readline().strip()
      ents = inf.readline().strip().split('###')
      cor = [float(x) for x in inf.readline().strip().split('###') if x != ''] #handles the empty line case
      tokens = inf.readline().strip().split('###')
      postags = inf.readline().strip().split('###')
      nertags = inf.readline().strip().split('###')
      #TODO add features here
  inf.close()
  print 'TODO fix below here'
  sys.exit()
