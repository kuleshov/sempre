import os, sys, time
from andrewutil import t2q, Error, normalizeName, getOutnameAndMakeDirQOnly, \
    tlist2tstr
from common import *
import random, urllib, urllib2
from getTopGoogleResultsForEnt import getUrlToMidMap, getTopHits, \
    getMidToFbIdMap, getEntToFbIdMap
from randomwalkEntSearch import loadRwTemplates, getGoodSeedNums
from createAmtData3NewFb import getFbAnswersNew
from createAmtData2 import getPreviouslyTurkedQs
from htmlUtils import saveUrlForEnt
from amtCommon import getAmtInputDir, getAmtInputName
from ioUtils import writeQsToFile, readLinesFromFile
import cPickle as pickle

def loadCheckedEnts():
  # Loading checked ents
  checkedEnts = {} # map from ent -> (answers, mIds)
  os.system('mkdir -p ' + FREEBASE_ANSWER_CACHE)
  for f in os.listdir(FREEBASE_ANSWER_CACHE):
    if f.startswith('checkedEnts'):
      cachename = FREEBASE_ANSWER_CACHE + f
      print 'Loading from', cachename
      checkedEnts_ = pickle.load(open(cachename, 'rb'))
      for k,v in checkedEnts_.iteritems():
        if k in checkedEnts:
          assert(checkedEnts[k] == v)
        else:
          checkedEnts[k] = v
  return checkedEnts
#######################################################
if __name__ == "__main__":
  random.seed(0)
  print sys.argv
  if len(sys.argv) != 7:
    Error('createAmtData4.py <seed or all> <nTemplates> <AMT trial #> <n HITs> <do-ent-search> <rseed>')
  seedarg = sys.argv[1].lower()
  if seedarg == 'all':
    seeds = getGoodSeedNums()
  else:
    seeds = [int(seedarg)]
  nTemplates = int(sys.argv[2])
  amtTrialNum = int(sys.argv[3])
  targetNumHits = int(sys.argv[4])
  doEntSearch = bool(int(sys.argv[5]))
  rseed = int(sys.argv[6])
  amtInputFile = getAmtInputName(amtTrialNum)
  if os.path.exists(amtInputFile):
    print amtInputFile, 'exists.'
    sys.exit()

  #### Need to search for the ents
  if doEntSearch:
    cmd = 'python randomwalkEntSearch.py rw '+str(nTemplates)+' 0 1 0 0 0 1'
    print cmd
    ret = os.system(cmd)
    if ret != 0:
      Error('Something wrong in randomwalkEntSearch.py')
  #sys.exit()
  random.seed(0)
  ####
  os.system('mkdir -p '+getAmtInputDir(amtTrialNum))
  prevqs = getPreviouslyTurkedQs(amtTrialNum)
  os.system('mkdir -p ' + FREEBASE_ENT_HTML)
  #
  templates = loadRwTemplates(seeds, rseed, nTemplates)
  templates = list(templates)
  templates.sort()
  random.shuffle(templates)
  #assert(nTemplates == 1000000) # 1M
  #templatesForJonathan = templates[200000:]
  #templates = templates[:200000]
  print len(templates) #, len(templatesForJonathan)
  #qFile200k = getAmtInputDir(amtTrialNum) + 'qs200k'
  #if not os.path.exists(qFile200k):
  #  writeQsToFile(qFile200k, [tlist2tstr(t)+'?' for t in templates], True)
  #qFile800k = getAmtInputDir(amtTrialNum) + 'qs800k'
  #if not os.path.exists(qFile800k):
  #  writeQsToFile(qFile800k, [tlist2tstr(t)+'?' for t in templatesForJonathan], True)
  #sys.exit()#TODO
  readme = getAmtInputDir(amtTrialNum) + 'README'
  outf = open(readme, 'w')
  outf.write('seeds: ' + ', '.join([str(s) for s in seeds]) + '\n')
  outf.write('search n: ' + str(nTemplates) +'\n')
  outf.write('n HITs: ' + str(targetNumHits) +'\n')
  outf.write('Note that this excludes any questions sent to AMT from previous trials.\n')
  outf.write('See grammar file for the grammar used.')
  outf.close()
  os.system('cp /user/akchou/scr/semparse/data/induce-patterns.grammar '+getAmtInputDir(amtTrialNum))
  ##############
  urlsToMids = getUrlToMidMap()
  ents = set([t[1].strip() for t in templates])
  tophits = getTopHits(nTemplates, False, ents, urlsToMids, seedarg, 'Rw')
  mIdsToFbIds = getMidToFbIdMap()
  entToFbIdMap = getEntToFbIdMap(tophits, urlsToMids, mIdsToFbIds)
  ####
  outf = open(amtInputFile, 'w')
  #outf.write('freebasesearch,question,answers,mids,headers,properties\n')
  outf.write('freebasesearch,question,answers\n')
  ##################
  checkedEnts = loadCheckedEnts()
  #checkedEnts = {} #TODO
  ##################

  badEnts = set()
  badUrls = set()
  noAnswerEnts = set()
  i = 0
  for template in templates:
    if i >= targetNumHits:
      break
    q = t2q(tlist2tstr(template))
    # TODO
    # remove things like "\\\\" and
    # map u0301 to '
    # map u0026 to and
    # Preferably do it before NER/fitlering by the grammar.
    if q in prevqs:
      continue
    assert(len(template) == 3)
    ent = template[1].strip()
    if ent not in entToFbIdMap:
      print 'No Freebase page for:', repr(ent)
      continue
    if ent in badEnts:
      print 'Known bad ent:', repr(ent)
      continue
    fbid = entToFbIdMap[ent]
    url = 'http://www.freebase.com/view/en/' + fbid
    if ent in checkedEnts:
      answers,mIds,headings,props = checkedEnts[ent]
    else:
      MAX_NUM_TRIES = 1
      numTries = 0
      while numTries < MAX_NUM_TRIES:
        try:
          print 'Try:', numTries
          saveUrlForEnt(ent, url, FREEBASE_ENT_HTML)
          break
        except Exception, e:
          print e
          print 'Error saving url for:', repr(ent), 'with url:', repr(url)
          numTries += 1
          if numTries == MAX_NUM_TRIES:
            badEnts.add(ent)
            badUrls.add(url)
      if ent in badEnts:
        continue
      fbHtmlName = getOutnameAndMakeDirQOnly(FREEBASE_ENT_HTML, ent, True) + '.html'
      print fbHtmlName
      answers,mIds,headings,props = getFbAnswersNew(fbHtmlName)
      answers = answers.encode('ascii', 'xmlcharrefreplace')
      mIds = mIds.encode('ascii', 'xmlcharrefreplace')
      headings = headings.encode('ascii', 'xmlcharrefreplace')
      props = props.encode('ascii', 'xmlcharrefreplace')
      checkedEnts[ent] = (answers,mIds,headings,props)
    if len(answers) == 0:
      print 'ERROR FOR ENT:', repr(ent)
      print 'PLEASE CHECK URL:', repr(url)
      noAnswerEnts.add(ent)
      continue
    if q.startswith('how '):
      print 'Skipping "how" question:', q
      continue
    print 'Saving', i, 'of', targetNumHits
    assert(url.find(',') == -1)
    assert(q.find(',') == -1)
    # AMT only supports lines up to lenth 2^16 so
    #outf.write(','.join([url,q,answers,mIds,headings,props])+'\n')
    line = ','.join([url,q,answers])+'\n'
    assert(len(line) < 65536)
    outf.write(line)
    i += 1
  outf.close()
  print 'Num Bad Ents:', len(badEnts)
  noAndrewFbids = [entToFbIdMap[x] for x in noAnswerEnts]
  print 'Num No Answer Ents:', len(noAnswerEnts)
