import sys, os, socket, time, types, copy
from subprocess import Popen, PIPE, STDOUT
from andrewutil import Error, getRandomWalkSuggestOutName, t2q, tlist2tstr
from common import *
import random, string, copy
from aggregateRandomWalkDfs import readTemplatesWrapper
from randomwalkEntSearch import getGoodSeedNums
from randomwalkDfs import readLinesFromFile
import scipy
from scipy.sparse import lil_matrix
from scipy.linalg import eig, eigvals, eigvalsh

def getSumAndInv(l, w):
  sumw = 0.0
  suminvw = 0.0
  for v in l:
    sumw += w[v]
    suminvw += 1.0/w[v]
  return sumw, suminvw
def getCounts(l):
  counts = {}
  for x in l:
    if x not in counts:
      counts[x] = 1
    else:
      counts[x] += 1
  return counts
def inducedEdges2(l1, l2, w1, w2, neighbors1, verbose = True):
  counts1 = getCounts(l1)
  counts2 = getCounts(l2)
  sumw1,suminvw1 = getSumAndInv(l1, w1)
  sumw2,suminvw2 = getSumAndInv(l2, w2)
  numer = sumw1*suminvw2 + sumw2*suminvw1
  numer2 = 0.0
  n1 = len(l1)
  n2 = len(l2)
  for x1 in l1:
    for x2 in l2:
      numer2 += float(w1[x1])/w2[x2] + float(w2[x2])/w1[x1]
  print numer2, numer

  denom = 0.0
  for x1 in set(l1):
    c1 = counts1[x1]
    for x2 in neighbors1[x1]:
      if x2 in counts2:
        c2 = counts2[x2]
        denom += c1*c2* (1.0/w1[x1] + 1.0/w2[x2])
  if verbose:
    estimate = 1 + float(numer)/denom
    print 'Induced Edges Safety Margin Estimate 2:', estimate
  return numer, denom
# m:margin
def inducedEdges(l, w, neighbors, m, verbose = True):
  assert(m >= 0)
  n = len(l)
  #nInduced = 0
  numer = 0.0
  denomCounts = (max(w.values())+1) * [0]
  for i1 in range(n):
    x1 = l[i1]
    neighb = neighbors[x1]
    for i2 in range(i1+m+1,n):
      x2 = l[i2]
      numer += float(w[x1])/w[x2] + float(w[x2])/w[x1]
      if x2 in neighb:
        denomCounts[w[x1]] += 1
        denomCounts[w[x2]] += 1
        #denom += 1.0/w[x1] + 1.0/w[x2]
        #nInduced += 1
  denom = 0.0
  assert(denomCounts[0] == 0)
  for i in range(1,len(denomCounts)):
    denom += float(denomCounts[i]) / i
  if verbose:
    estimate = 1 + float(numer)/denom
    print 'Induced Edges Safety Margin Estimate (m = '+str(m)+'):', estimate
  return numer, denom

# l: list
# w: weights
def nodeCollisionW(l, w, verbose = True):
  n = len(l)
  ncol = 0
  for i1 in range(n):
    for i2 in range(i1+1,n):
      if l[i1] == l[i2]:
        ncol += 1
  sumw = 0.0
  suminvw = 0.0
  for v in l:
    sumw += w[v]
    suminvw += 1.0/w[v]
  numerator = sumw * suminvw
  if verbose:
    estimate = -1
    if ncol:
      estimate = numerator / ncol
    print 'Weighted Node Collision Estimate:', estimate
  return numerator, ncol
def nodeCollision(l, verbose = True):
  n = len(l)
  ncol = 0
  for i1 in range(n):
    for i2 in range(i1+1,n):
      if l[i1] == l[i2]:
        ncol += 1
  if verbose:
    estimate = -1
    if ncol:
      estimate = n*n / ncol
    print 'Unweighted Node Collision Estimate:', estimate
  return n*n, ncol
def shiftedThinning(l, w, k, method):
  numers = []
  denoms = []
  estimates = []
  for i in range(k):
    curList = l[i::k] # every kth elem starting at i
    if method == 'nodeCol':
      numer,denom = nodeCollision(curList, False)
    elif method == 'nodeColW':
      numer,denom = nodeCollisionW(curList, w, False)
    else:
      Error('Invalid method: '+method)
    numers.append(numer)
    denoms.append(denom)
  estimate = sum(numers) / sum(denoms)
  print 'Shifted Thinning Estimate (k = '+str(k)+', '+method+'):', estimate
  return estimate
def makeNeighborsSymmetric(oldn):
  n = copy.copy(oldn)
  for k,v in oldn.iteritems():
    for x in v:
      if x in n:
        n[x] += k
  n2 = {}
  for k in n:
    n2[k] = set(n[k])
  return n2
def readQsAndNeighbors(n, seed, rseed, dir, neighbors, allQs, neighborDir, prevQs):
  print 'N:', n
  sequenceOfTemplates,qs = readTemplatesWrapper(n, seed, dir, 'sequenceOfTemplates')
  qs = [t2q(q) for q in qs]
  cachename = neighborDir+'_'.join(['seed', str(seed), 'rseed', str(rseed),\
      'n', str(n)])
  print cachename
  if os.path.exists(cachename):
    inf = open(cachename)
    for line in inf:
      line = line.strip().split('###')
      q = line[0]
      others = line[1:]
      if q not in neighbors:
        neighbors[q] = []
      neighbors[q] += others
    inf.close()
  else:
    #Error('dont run this')
    i = 0
    s = set(sequenceOfTemplates)
    s = [t for t in s if t2q(tlist2tstr(t)) not in neighbors]
    for t in s:
      i += 1
      if i%100==0:
        time.sleep(.1)
      print i, 'OF', len(s)
      q = t2q(tlist2tstr(t))
      if q in neighbors:
        print q
        print 'skipping'
        continue
      neighbors[q] = []
      for index in [0,1,2]:
        name = getRandomWalkSuggestOutName(REPRO_SEARCH_QUEUE, t, index)
        try:
          newQs = readLinesFromFile(name)
          newQs2 = [x+'?' for x in newQs if x+'?' in allQs]
          neighbors[q] += newQs2
        except:
          pass
    outf = open(cachename, 'w')
    for k,v in neighbors.iteritems():
      outf.write('###'.join([k] + v) + '\n')
    outf.close()
  newn = {}
  for k,v in neighbors.iteritems():
    if k in allQs:
      newn[k] = [x for x in v if x in allQs]
  #TODO
  #assert(len(neighbors) == len(set(qs)))
  return qs, newn
def setDegree(qs, neighbors, degree):
  for q in qs:
    if q not in neighbors:#TODO
      neighbors[q] = set()
    assert(q in neighbors)
  for q,v in neighbors.iteritems():
    degree[q] = max(1, len(v))
  return degree

###############################
if __name__ == "__main__":
  if len(sys.argv) != 2:
    Error('calculateGlobalGraphStats.py <rseed>')
  rseed = int(sys.argv[1])
  if rseed == 0:
    dir = 'random_walk_rw_templates0/'
  else:
    dir = 'random_walk_rw_templates1/'
  templates = []
  seed = 0
  neighborDir = 'neighbors/'
  os.system('mkdir -p '+neighborDir)
  degree = {}
  neighbors = {}
  maxn = 200000
  allTemplates,dummy = readTemplatesWrapper(maxn, seed, dir, 'oneEntTemplates')
  allQs = set([t2q(tlist2tstr(t)) for t in allTemplates])
  prevQs = set()
  for n in range(maxn+1)[20000::20000]:
    #n=200000
    qs,neighbors = readQsAndNeighbors(n, seed, rseed, dir, neighbors, allQs, neighborDir, prevQs)
    #neighbors = makeNeighborsSymmetric(neighbors)
    N = len(neighbors)
    M = lil_matrix((N,N))
    qset = set(qs)
    i = 0
    indexMap = {}
    for q in neighbors:
      indexMap[q] = i
      i += 1
    with open('sparse3.txt', 'w') as outf:
      for k,v in neighbors.iteritems():
        i1 = indexMap[k]
        for x in v:
          if x not in neighbors:
            continue
          assert(x in allQs)
          i2 = indexMap[x]
          outf.write(str(i1) +' '+ str(i2) +'\n')
          M[i1,i2] = 1
          M[i2,i1] = 1

    #eig = eigvals(M)
    print M.shape

    sys.exit()
    continue
    #prevQs = set(qs1 + qs2)
    print 'Computing degrees:'
    neighbors1 = makeNeighborsSymmetric(neighbors1)
    degree1 = setDegree(qs1, neighbors1, degree1)
    neighbors2 = makeNeighborsSymmetric(neighbors2)
    degree2 = setDegree(qs2, neighbors2, degree2)
    k = 100
    #shiftedThinning(qs, degree, k, 'nodeCol')
    #shiftedThinning(qs, degree, k, 'nodeColW')
    print 'Running induced edges'
    for m in [10000]:
      inducedEdges2(qs1, qs2, degree1, degree2, neighbors1)
      sys.exit()
    print 'Running nodeCollisionW'
    nodeCollisionW(qs, degree)
    #continue
    sys.exit()

    #sequenceOfTemplates2,qs2 = readTemplatesWrapper(n, seed, dir2, 'sequenceOfTemplates')
    #nodeCollision(sequenceOfTemplates1)
    #for k in range(101)[10::10]:
  sys.exit()
