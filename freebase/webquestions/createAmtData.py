import os, sys
from aggregateRandomWalkBfs import readLinesFromFile
from andrewutil import t2q, Error, normalizeName
from common import *
from repro_search import decode
import random, urllib, urllib2

def getVersionsOfEnt(ent):
  ent = ent.lower()
  v = []
  v.append(ent.lower())
  v.append(ent.upper())
  parts = ent.split()
  for i in range(len(parts)):
    pt = parts[i][0].upper() + parts[i][1:]
    tmp = parts
    tmp[i] = pt
    v.append(' '.join(tmp))
  parts = [p[0].upper()+p[1:] for p in parts]
  v.append(' '.join(parts))
  return v
def getW3mText(inname):
  cmdstr = "w3m -dump %s" % inname
  print cmdstr
  cmd = os.popen(cmdstr)
  output = cmd.read()
  cmd.close()
  output = output.split('\n')
  output = [x.strip() for x in output]
  return output
def isValidFreebaseSearchResult(text):
  for line in text:
    if line.startswith('No search results for'):
      return False
  return True
def getValidEnt(ent, outdir):
  vents = getVersionsOfEnt(ent)
  print vents
  validEnt = None
  for e in vents:
    name = normalizeName(e)
    while len(name) < 3:
      name += '_'
    outdir2 = outdir + name[:3] + '/'
    os.system('mkdir -p '+outdir2)
    name = outdir2 + name + '.html'
    print name
    if not os.path.exists(name) or os.stat(name)[6] == 0:
      query = urllib.urlencode({'query': e})
      url = 'http://www.freebase.com/search?limit=30&start=0&'+query
      print url
      headers = {
        'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language': 'en-us,en;q=0.5',
        'Accept-Encoding': 'gzip',
        'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.7',
        'Keep-Alive': '115',
        'Connection': 'keep-alive'}
      req = urllib2.Request(url);
      for h,v in headers.items():
        req.add_header(h,v)
      response = urllib2.urlopen(req)
      html = decode(response)
      outf = open(name, 'w')
      outf.write(html)
      outf.close()
    text = getW3mText(name)
    if isValidFreebaseSearchResult(text):
      validEnt = e
      break
  print validEnt
  return validEnt

if __name__ == "__main__":
  random.seed(0)
  print sys.argv
  if len(sys.argv) != 4:
    Error('createAmtData.py <nTemplates> <rfs seed> <AMT rfs trial #>')
  os.system('mkdir -p mturk')
  nTemplates = int(sys.argv[1])
  seed = int(sys.argv[2])
  amtTrialNum = int(sys.argv[3])
  if False:
    os.system('python randomwalkRfs.py ' + str(nTemplates))
  templateFile = RANDOM_WALK_RFS_DIR + '_'.join(['oneEntTemplates', 'n', str(nTemplates), 'seed', str(seed)])
  amtInputFile = 'mturk/rfs_' + str(amtTrialNum) + '.input'
  if os.path.exists(amtInputFile):
    print amtInputFile, 'exists.'
    sys.exit()
  # cache freebase search results
  outdir = REPRO_OUT + 'freebaseSearchHtml/'
  os.system('mkdir -p '+outdir)
  #
  templates = readLinesFromFile(templateFile)
  random.shuffle(templates)
  #templates = templates[:1000] #TODO
  outf = open(amtInputFile, 'w')
  header = '\t'.join(['freebasesearch', 'entity', 'question'])
  outf.write(header + '\n')
  invalid = set()
  targetNumEnts = 100000 #TODO
  targetNumEnts = 1000 #TODO
  templates = templates[1000:] #TODO
  i = 0
  for template in templates:
    t = template.split(' _')
    assert(len(t) == 3)
    ent = t[1].strip()
    validEnt = getValidEnt(ent, outdir)
    if validEnt == None:
      print ent
      print 'No valid ents for:', ent
      invalid.add(ent)
      continue
    url = 'http://www.freebase.com/search?limit=30&start=0&'
    # MUST be uppercased since Frebase search works better that way.
    url += urllib.urlencode({'query': validEnt})
    q = t2q(template)
    assert(url.find('\t') == -1)
    assert(q.find('\t') == -1)
    line = '\t'.join([url, ent, q])
    outf.write(line + '\n')
    i += 1
    print 'done with', i, 'of', len(templates)
    if i == targetNumEnts:
      break
  outf.close()
  outf = open('mturk/invalidEnts.txt', 'w')
  for e in invalid:
    outf.write(e+'\n')
  outf.close()
