import os, sys, random, re
from andrewutil import normalizeName, getDirName, Error
from saveAmtAnswers import isNegativeAnswer
from createAmtData2 import getAmtResultsName, getAmtNameWithSuffix
from createSemanticParserInputAmtCsv import writeAnswers, isNumberQuestion

'''
def writeAnswers(outname, s):
  #write = not os.path.exists(outname) or os.stat(outname)[6]==0
  print outname
  outf = open(outname, 'w')
  for q,url,a in s:
    #print q, '###', a
    #print url
    writeExample(outf, q, a, url)
  outf.close()
'''
def getDummyAnswers(trial):
  inf = open('data/dataset_'+str(trial)+'/bfs_'+str(trial)+'.input')
  lines = [l for l in inf][1:]
  inf.close()
  all = []
  for line in lines:
    line = line.strip().split(',')
    assert(len(line) == 2)
    url = line[0]
    q = line[1]
    a = '' #dummy
    all.append((q,url,a))
  entsAndDates = [[q,u,a] for q,u,a in all if not isNumberQuestion(q, a)]
  return entsAndDates
########################################################################
if __name__ == "__main__":
  random.seed(0)
  if len(sys.argv) != 2:
    Error('Usage createSemanticParserDummyInput.py <bfs trial>')
  trial = int(sys.argv[1])
  ######################
  ######################
  entsAndDates = getDummyAnswers(trial)
  print len(entsAndDates)
  for i in range(10):
    start = 2000*i
    end = min(2000*(i+1), len(entsAndDates))
    print start, end
    writeAnswers(getAmtNameWithSuffix(trial, '_dummyEntsAndDates_'+str(i)+'.examples'), \
        entsAndDates[start:end])
