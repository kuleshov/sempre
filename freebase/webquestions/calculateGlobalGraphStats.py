import sys, os, socket, time, types, copy
from subprocess import Popen, PIPE, STDOUT
from andrewutil import Error, getRandomWalkSuggestOutName, t2q, tlist2tstr
from common import *
import random, string, copy
from aggregateRandomWalkDfs import readTemplatesWrapper
from randomwalkEntSearch import getGoodSeedNums
from util import readLinesFromFile
import cPickle as pickle

def getSumAndInv(l, w):
  sumw = 0.0
  suminvw = 0.0
  for v in l:
    sumw += w[v]
    suminvw += 1.0/w[v]
  return sumw, suminvw
def getCounts(l):
  counts = {}
  for x in l:
    if x not in counts:
      counts[x] = 1
    else:
      counts[x] += 1
  return counts
def node2(l1, l2, w1, w2, neighbors1, verbose = True):
  counts1 = getCounts(l1)
  counts2 = getCounts(l2)
  sumw1,suminvw1 = getSumAndInv(l1, w1)
  sumw2,suminvw2 = getSumAndInv(l2, w2)
  numer = sumw1*suminvw2 + sumw2*suminvw1

  denom = 0.0
  for x1 in set(l1):
    c1 = counts1[x1]
    for x2 in neighbors1[x1]:
      if x2 in counts2:
        c2 = counts2[x2]
        denom += 2*c1*c2

  if verbose:
    estimate = 1 + float(numer)/denom
    print 'Node Safety Margin Estimate 2:', estimate
  return numer, denom
def inducedEdges2(l1, l2, w1, w2, neighbors1, verbose = True):
  counts1 = getCounts(l1)
  counts2 = getCounts(l2)
  sumw1,suminvw1 = getSumAndInv(l1, w1)
  sumw2,suminvw2 = getSumAndInv(l2, w2)
  numer = sumw1*suminvw2 + sumw2*suminvw1

  denom = 0.0
  for x1 in set(l1):
    c1 = counts1[x1]
    for x2 in neighbors1[x1]:
      if x2 in counts2:
        c2 = counts2[x2]
        denom += c1*c2* (1.0/w1[x1] + 1.0/w2[x2])

  if verbose:
    estimate = 1 + float(numer)/denom
    print 'Induced Edges Safety Margin Estimate 2:', estimate
  return numer, denom
# m:margin
def inducedEdges(l, w, neighbors, m, verbose = True):
  assert(m >= 0)
  n = len(l)
  #nInduced = 0
  numer = 0.0
  denomCounts = (max(w.values())+1) * [0]
  for i1 in range(n):
    x1 = l[i1]
    neighb = neighbors[x1]
    for i2 in range(i1+m+1,n):
      x2 = l[i2]
      numer += float(w[x1])/w[x2] + float(w[x2])/w[x1]
      if x2 in neighb:
        denomCounts[w[x1]] += 1
        denomCounts[w[x2]] += 1
        #denom += 1.0/w[x1] + 1.0/w[x2]
        #nInduced += 1
  denom = 0.0
  assert(denomCounts[0] == 0)
  for i in range(1,len(denomCounts)):
    denom += float(denomCounts[i]) / i
  if verbose:
    if denom == 0:
      print 'denom is 0. numer is', numer
    else:
      estimate = 1 + float(numer)/denom
      print 'Induced Edges Safety Margin Estimate (m = '+str(m)+'):', estimate
  return numer, denom

# l: list
# w: weights
def nodeCollisionW(l, w, verbose = True):
  n = len(l)
  counts = getCounts(l)
  ncol = 0
  for c in counts.values():
    ncol += c*(c-1)/2
  sumw = 0.0
  suminvw = 0.0
  for v in l:
    sumw += w[v]
    suminvw += 1.0/w[v]
  numerator = sumw * suminvw
  if verbose:
    estimate = -1
    if ncol:
      estimate = numerator / ncol
    print 'Weighted Node Collision Estimate:', estimate
  return numerator, ncol
def nodeCollision(l, verbose = True):
  n = len(l)
  ncol = 0
  for i1 in range(n):
    for i2 in range(i1+1,n):
      if l[i1] == l[i2]:
        ncol += 1
  if verbose:
    estimate = -1
    if ncol:
      estimate = n*n / ncol
    print 'Unweighted Node Collision Estimate:', estimate
  return n*n, ncol
def shiftedThinning(l, w, k, method, neighbors = None):
  numers = []
  denoms = []
  estimates = []
  for i in range(k):
    curList = l[i::k] # every kth elem starting at i
    if method == 'nodeCol':
      numer,denom = nodeCollision(curList, False)
    elif method == 'nodeColW':
      numer,denom = nodeCollisionW(curList, w, False)
    elif method == 'inducedEdges':
      numer,denom = inducedEdges(curList, w, neighbors, 5, False)
    else:
      Error('Invalid method: '+method)
    numers.append(numer)
    denoms.append(denom)
  estimate = sum(numers) / sum(denoms)
  print 'Shifted Thinning Estimate (k = '+str(k)+', '+method+'):', estimate
  return estimate
def reverseNeighbors(oldn):
  n = {}
  for k,v in oldn.iteritems():
    n[k] = [] # so there are no key errors later
    for x in v:
      if x not in n:
        n[x] = [k]
      else:
        n[x] += k
  n2 = {}
  for k in n:
    n2[k] = set(n[k])
  return n2
def makeNeighborsSymmetric(oldn):
  n = copy.copy(oldn)
  for k,v in oldn.iteritems():
    for x in v:
      if x in n:
        n[x] += k
  n2 = {}
  for k in n:
    n2[k] = set(n[k])
  return n2
# nUsedToGenerate is the n used to load allQs
def readQsAndNeighbors(n, seed, rseed, dir, neighbors, allQs, nUsedToGenerate, neighborDir, prevQs):
  print 'N:', n
  sequenceOfTemplates,qs = readTemplatesWrapper(n, seed, dir, 'sequenceOfTemplates')
  qs = [t2q(q) for q in qs]
  tmpn = n
  while tmpn > 0:
    cachename = neighborDir+'_'.join(['seed', str(seed), 'rseed', str(rseed),\
        'n', str(tmpn)]) + '.p'
    print cachename
    if os.path.exists(cachename):
      print 'Loading from pickle...'
      neighbors_,nUsedToGenerate_ = pickle.load(open(cachename, 'rb'))
      if nUsedToGenerate_ >= n:
        neighbors = neighbors_
        break
    else:
      tmpn -= 10000
  if tmpn < n:
    #Error('dont run this')
    i = 0
    s = set(sequenceOfTemplates)
    s = [t for t in s if t2q(tlist2tstr(t)) not in neighbors]
    for t in s:
      i += 1
      if i%100==0:
        time.sleep(.1)
      print i, 'OF', len(s)
      q = t2q(tlist2tstr(t))
      if q in neighbors:
        print q
        print 'skipping'
        continue
      neighbors[q] = []
      for index in [0,1,2]:
        name = getRandomWalkSuggestOutName(REPRO_SEARCH_QUEUE, t, index)
        try:
          newQs = readLinesFromFile(name)
          newQs2 = [x+'?' for x in newQs if x+'?' in allQs]
          neighbors[q] += newQs2
        except:
          # This failure will occur if some indices arent chosen for suggestion.
          print 'Failed to load:', (index, t)
          print name
    allGood,allBad,entsOnly,entsAndDates,invalidResults = getAmtAnswers3(trial)
    print 'Saving to pickle...'
    pickle.dump((neighbors,nUsedToGenerate), open(cachename, 'wb'))
  #TODO
  #assert(len(neighbors) == len(set(qs)))
  return qs, neighbors
def setDegree(qs, neighbors, degree):
  for q in qs:
    if q not in neighbors:#TODO
      neighbors[q] = set()
    assert(q in neighbors)
  for q,v in neighbors.iteritems():
    degree[q] = max(1, len(v))
  return degree

###############################
if __name__ == "__main__":
  if len(sys.argv) != 2:
    Error('calculateGlobalGraphStats.py <oneWalk>')
  oneWalk = bool(int(sys.argv[1]))
  rseed = 0
  dir = 'repro_out/random_walk_rw_templates'+str(rseed)+'/'
  dir1 = 'repro_out/random_walk_rw_templates0/'
  dir2 = 'repro_out/random_walk_rw_templates1/'
  #path1 = dir1 + 'sequenceOfTemplates_n_10000_seed_0'
  #path2 = dir2 + 'sequenceOfTemplates_n_10000_seed_0'
  templates = []
  neighborDir = 'neighbors/'
  os.system('mkdir -p '+neighborDir)
  degree = {}
  degree1 = {}
  degree2 = {}
  neighbors = {}
  neighbors1 = {}
  neighbors2 = {}
  outneighbors1 = {}
  outneighbors2 = {}
  maxn1 = 860000
  maxn2 = 300000
  n1 = 20000
  n2 = n1
  seed1 = 0
  seed2 = 0
  rseed1 = 0
  rseed2 = 1
  assert(dir1.strip('/').endswith(str(rseed1)))
  assert(dir2.strip('/').endswith(str(rseed2)))
  if oneWalk:
    allTemplates,dummy = readTemplatesWrapper(maxn1, seed1, dir, 'oneEntTemplates')
    allQs = [t2q(tlist2tstr(t)) for t in allTemplates]
  else:
    allTemplates1,dummy = readTemplatesWrapper(maxn1, seed1, dir1, 'oneEntTemplates')
    allQs1 = [t2q(tlist2tstr(t)) for t in allTemplates1]
    allTemplates2,dummy = readTemplatesWrapper(maxn2, seed2, dir2, 'oneEntTemplates')
    allQs2 = [t2q(tlist2tstr(t)) for t in allTemplates2]
    allQs = set(allQs1 + allQs2)
  prevQs = set()
  stepSize = 10000
  for n in range(stepSize, min(maxn1,maxn2)+1, stepSize):
    if oneWalk:
      #qs,neighbors = readQsAndNeighbors(n, seed, rseed, dir, neighbors,
      #    allQs, maxn1, neighborDir, prevQs)
      assert(maxn1 >= n)
      qs1,outneighbors1 = readQsAndNeighbors(n, seed1, rseed1, dir1, \
          outneighbors1, allQs, maxn1, neighborDir, prevQs)
    else:
      assert(maxn1 >= n and maxn2 >= n)
      qs1,outneighbors1 = readQsAndNeighbors(n, seed1, rseed1, dir1, \
          outneighbors1, allQs, maxn1, neighborDir, prevQs)
      qs2,outneighbors2 = readQsAndNeighbors(n, seed2, rseed2, dir2, \
          outneighbors2, allQs, maxn2, neighborDir, prevQs)
    #continue
    #prevQs = set(qs1 + qs2)
    print 'Computing degrees:'
    #inneighbors1 = reverseNeighbors(outneighbors1)
    neighbors1 = makeNeighborsSymmetric(outneighbors1)
    #degree1 = setDegree(qs1, inneighbors1, degree1)
    degree1 = setDegree(qs1, neighbors1, degree1)
    if not oneWalk:
      #inneighbors2 = reverseNeighbors(outneighbors2)
      neighbors2 = makeNeighborsSymmetric(outneighbors2)
      #degree2 = setDegree(qs2, inneighbors2, degree2)
      degree2 = setDegree(qs2, neighbors2, degree2)
    k = 100
    shiftedThinning(qs1, degree1, 10, 'inducedEdges', neighbors1)
    inducedEdges(qs1, degree1, neighbors1, 5000)
    #continue
    shiftedThinning(qs1, degree1, k, 'inducedEdges', neighbors1)
    shiftedThinning(qs1, degree1, k, 'nodeCol')
    shiftedThinning(qs1, degree1, k, 'nodeColW')
    print 'Running induced edges'
    #for m in [10000]:
    if not oneWalk:
      inducedEdges2(qs1, qs2, degree1, degree2, neighbors1)
      node2(qs1, qs2, degree1, degree2, neighbors1)
      #sys.exit()
    print 'Running nodeCollisionW'
    nodeCollisionW(qs1, degree1)
    continue
    sys.exit()

    #sequenceOfTemplates2,qs2 = readTemplatesWrapper(n, seed, dir2, 'sequenceOfTemplates')
    #nodeCollision(sequenceOfTemplates1)
    #for k in range(101)[10::10]:
  sys.exit()
