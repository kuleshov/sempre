import urllib, urllib2 # contacting websites
import string, time, calendar, sys, os
import gzip, StringIO, random
import filterOutput, socket, re
from andrewutil import Error, normalizeName, getDirName, normalizeUnicode, getOutnameAndMakeDir, t2q
from common import *
from wikianswersRandomWalkBfs import loadRandomWalkBfsQs
from randomwalkBfsSearch import readTestTs
from aggregateRandomWalk import readTemplates, readLinesFromFile

if __name__ == "__main__":
  ts = readTestTs()#TODO remove
  #qs, outbase, newqoutbase, qsAndTemplates = loadRandomWalkBfsQs(False)
  #templates = [t for q,t in qsAndTemplates]
  badt = readLinesFromFile('repro_out/nonFreebaseTsBfs.txt')
  badt = set([t for t in badt if t])
  goodt = readLinesFromFile('repro_out/freebaseTsBfs.txt')
  goodt = set([t for t in goodt if t])

  goodTestTs = readLinesFromFile('tsFb.txt')
  badTestTs = readLinesFromFile('tsNonFb.txt')
  tp = 0
  tn = 0
  print 'Positive:'
  for t in goodTestTs:
    print t
    assert((t in badt) or (t in goodt))
    if t in goodt:
      tp += 1
  print 'Negative:'
  for t in badTestTs:
    print t
    assert((t in badt) or (t in goodt))
    if t in badt:
      tn += 1
  print 'TP:', tp, 'FN:', len(goodTestTs)-tp
  print 'FP:', len(badTestTs)-tn, 'TN:', tn,
  sys.exit()
