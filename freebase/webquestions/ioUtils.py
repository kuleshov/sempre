def readLinesFromFile(inname):
  inf = open(inname)
  lines = [line.strip() for line in inf]
  inf.close()
  return lines
def writeQsToFile(outname, qs, keepQMark = False):
  outf = open(outname, 'w')
  for q in qs:
    q=q.strip('"')
    if not keepQMark:
      q = q.strip('?')
    outf.write(q + '\n')
  outf.close()

