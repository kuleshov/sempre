import os, sys, time
from util import readLinesFromFile
from andrewutil import t2q, Error, normalizeName, getOutnameAndMakeDirQOnly, \
    tlist2tstr
from common import *
from repro_search import decode
import urllib, urllib2
from getTopGoogleResultsForEnt import getUrlToMidMap, getTopHits, \
    getMidToFbIdMap, getEntToFbIdMap
from randomwalkEntSearch import loadBfsTemplates, getGoodSeedNums
from createAmtData3NewFb import getFbAnswersNew
from createAmtData2 import saveUrlForEnt, getAmtInputDir, getAmtInputName, \
    getPreviouslyTurkedQs
from aggregateRandomWalkRfs import writeQsToFile
import cPickle as pickle

def loadCheckedEnts():
  # Loading checked ents
  checkedEnts = {} # map from ent -> (answers, mIds)
  cachedir = REPRO_OUT + 'fbAnswers/'
  os.system('mkdir -p ' + cachedir)
  for f in os.listdir(cachedir):
    if f.startswith('checkedEnts'):
      cachename = cachedir + f
      print 'Loading from', cachename
      checkedEnts_ = pickle.load(open(cachename, 'rb'))
      for k,v in checkedEnts_.iteritems():
        if k in checkedEnts:
          assert(checkedEnts[k] == v)
        else:
          checkedEnts[k] = v
  return checkedEnts
#######################################################
if __name__ == "__main__":

  #### Need to search for the ents
  ####
  prevqs = set()
  outdir = REPRO_OUT + 'freebaseEntHtml/'
  #
  templates = loadBfsTemplates(getGoodSeedNums(), 100000)
  templates = list(templates)
  qsAndTs_ = [(t2q(tlist2tstr(t)),t) for t in templates]
  qsAndTs = {}
  for q,t in qsAndTs_:
    qsAndTs[q] = t
  qsAndTs['what was the owls name in harry potter?'] = ['what was the owls name in', 'harry potter', '']
  qsAndTs['what religion was prophet muhammad before islam?'] = ['what religion was prophet', 'muhammad', 'before islam']
  #sys.exit()#TODO
  nTemplates = 100000
  urlsToMids = getUrlToMidMap()
  ents = set([t[1].strip() for t in templates])
  tophits = getTopHits(nTemplates, False, ents, urlsToMids, 'all')
  mIdsToFbIds = getMidToFbIdMap()
  entToFbIdMap = getEntToFbIdMap(tophits, urlsToMids, mIdsToFbIds)
  ####
  outf = open('data/dataset_6/bfs_6.input.fb2', 'w')
  #outf.write('freebasesearch,question,answers,mids,headers,properties\n')
  outf.write('freebasesearch,question,answers\n')
  ##################
  checkedEnts = loadCheckedEnts()
  #checkedEnts = {} #TODO
  inf = open('data/dataset_6/bfs_6_good.examples.qs1')
  qs1 = set([l.strip() for l in inf])
  inf.close()
  inf = open('data/dataset_6/bfs_6_good.examples.qs2')
  qs2 = set([l.strip() for l in inf])
  inf.close()
  #qs = [q for q in qs2 if q not in qs1]
  inf = open('data/dataset_6/bfs_6.input')
  qs = set([l.split(',')[1].strip() for l in inf])
  inf.close()
  ##################

  badEnts = set()
  badUrls = set()
  noAnswerEnts = set()
  i = 0
  for q in qs:
    if (q in qs1) or (q not in qs2):
      continue
    template = qsAndTs[q]
    assert(len(template) == 3)
    ent = template[1].strip()
    if ent not in entToFbIdMap:
      print 'No Freebase page for:', repr(ent)
      continue
    if ent in badEnts:
      print 'Known bad ent:', repr(ent)
      continue
    fbid = entToFbIdMap[ent]
    url = 'http://www.freebase.com/view/en/' + fbid
    if ent in checkedEnts:
      answers,mIds,headings,props = checkedEnts[ent]
    else:
      try:
        saveUrlForEnt(ent, url, outdir)
      except Exception, e:
        print e
        print 'Error saving url for:', repr(ent), 'with url:', repr(url)
        badEnts.add(ent)
        badUrls.add(url)
        continue
      fbHtmlName = getOutnameAndMakeDirQOnly(outdir, ent, True) + '.html'
      print fbHtmlName
      answers,mIds,headings,props = getFbAnswersNew(fbHtmlName)
      answers = answers.encode('ascii', 'xmlcharrefreplace')
      mIds = mIds.encode('ascii', 'xmlcharrefreplace')
      headings = headings.encode('ascii', 'xmlcharrefreplace')
      props = props.encode('ascii', 'xmlcharrefreplace')
      checkedEnts[ent] = (answers,mIds,headings,props)
    if len(answers) == 0:
      print 'ERROR FOR ENT:', repr(ent)
      print 'PLEASE CHECK URL:', repr(url)
      noAnswerEnts.add(ent)
      continue
    if q.startswith('how '):
      print 'Skipping "how" question:', q
      continue
    print 'Saving', i, 'of 1599'
    assert(url.find(',') == -1)
    assert(q.find(',') == -1)
    # AMT only supports lines up to lenth 2^16 so
    #outf.write(','.join([url,q,answers,mIds,headings,props])+'\n')
    line = ','.join([url,q,answers])+'\n'
    assert(len(line) < 65536)
    outf.write(line)
    i += 1
  outf.close()
  print 'Num Bad Ents:', len(badEnts)
  noAndrewFbids = [entToFbIdMap[x] for x in noAnswerEnts]
  print 'Num No Answer Ents:', len(noAnswerEnts)
