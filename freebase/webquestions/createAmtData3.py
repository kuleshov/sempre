import os, sys
from util import readLinesFromFile
from andrewutil import t2q, Error, normalizeName, getOutnameAndMakeDirQOnly, \
    tlist2tstr
from common import *
from repro_search import decode
import random, urllib, urllib2, re
from getTopGoogleResultsForEnt import getUrlToMidMap, getTopHits, getMidToFbIdMap, getEntToFbIdMap
from randomwalkEntSearch import loadBfsTemplates, getGoodSeedNums
from createAmtData2 import getAmtInputName
sys.path.append('BeautifulSoup-3.2.1/')
from BeautifulSoup import BeautifulSoup

def getFbAnswers(htmlName):
  classes = ['property-list', 'property-list property-list-inline',\
      'property-list ']
  f = open(htmlName)
  soup = BeautifulSoup(f)
  f.close()
  li = soup.findAll('li', attrs={'class':'value'})
  print len(li)
  #vals = [''.join([re.sub(r'\s+', ' ', a.lstrip()) for a in v.findAll(text=True)]).replace(' )', ')') for v in li]
  ul = []
  for c in classes:
    ul += soup.findAll('ul', attrs={'class':c})
  print len(ul)
  vals2 = [[''.join([re.sub(r'\s+', ' ', a.lstrip()) for a in v.findAll(text=True)]).replace(' )', ')') for v in u.findAll('li')] for u in ul]
  #print vals2
  vals3 = [';;;'.join([x.strip(' ,') for x in v if x.strip(' ,')]) for v in vals2]
  for v in vals3:
    print v
    if v.find(';;;;;;') >= 0:
      Error('Empty field!')
    if v.find('###') >= 0:
      Error('Cant have ### in answers!')
  print 'DONE'
  print len(vals3)
  return '###'.join(vals3).replace(',', '^')
#######################################################
if __name__ == "__main__":
  random.seed(0)
  print sys.argv
  if len(sys.argv) != 2:
    Error('createAmtData3.py <AMT bfs trial #>')
  amtTrialNum = int(sys.argv[1])
  amtInputFile = getAmtInputName(amtTrialNum)
  if not os.path.exists(amtInputFile):
    print amtInputFile, 'doesnt exist.'
    sys.exit()

  ##############
  #urlsToMids = getUrlToMidMap()
  #ents = set([t[1].strip() for t in templates])
  #tophits = getTopHits(nTemplates, False, ents, urlsToMids, seedarg)
  #mIdsToFbIds = getMidToFbIdMap()
  #entToFbIdMap = getEntToFbIdMap(tophits, urlsToMids, mIdsToFbIds)
  ####
  outdir = REPRO_OUT + 'freebaseEntHtml/'
  nTemplates = 100000
  seeds = getGoodSeedNums()
  templates = loadBfsTemplates(seeds, nTemplates)
  templates = list(templates)
  qsAndTs_ = [(t2q(tlist2tstr(t)),t) for t in templates]
  qsAndTs = {}
  for q,t in qsAndTs_:
    qsAndTs[q] = t
  qsAndTs['what was the owls name in harry potter?'] = ['what was the owls name in', 'harry potter', '']
  qsAndTs['what religion was prophet muhammad before islam?'] = ['what religion was prophet', 'muhammad', 'before islam']
  amtFbAnswersInputFile = amtInputFile + '.fb'
  outf = open(amtFbAnswersInputFile, 'w')
  inf = open(amtInputFile)
  inf.readline()
  #TODO write mIds
  outf.write('freebasesearch,question,answers\n')
  for line in inf:
    url,q = line.strip().split(',')
    print url, q
    t = qsAndTs[q]
    ent = t[1].strip()
    #fbid = entToFbIdMap[ent]
    #url = 'http://www.freebase.com/view/en/' + fbid
    #saveUrlForEnt(ent, url, outdir)
    fbHtmlName = getOutnameAndMakeDirQOnly(outdir, ent, True) + '.html'
    print fbHtmlName
    answers = getFbAnswers(fbHtmlName)
    if answers.find('^') >= 0:
      Error('Cant have carats (^) in the answers since we use them as delimiters.')
    answers = answers.encode('ascii', 'xmlcharrefreplace')
    #TODO write mIds
    outf.write(','.join([url,q,answers])+'\n')
  inf.close()
  outf.close()
  print 'Wrote to:', amtFbAnswersInputFile
