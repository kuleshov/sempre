import urllib, urllib2 # contacting websites
import string, time, calendar, sys, os
import gzip, StringIO, random
import filterOutput, socket, re
from andrewutil import Error, normalizeName, getDirName, normalizeUnicode, getOutnameAndMakeDir, normalizeAnswer
from common import *
from aggregateRandomWalk import readTemplates, writeQsToFile
sys.path.append('BeautifulSoup-3.2.1/')
from BeautifulSoup import BeautifulSoup, NavigableString
from wikianswersRandomWalk import getWikiAnswersOutFiles
from wikianswersRandomWalkBfs import loadRandomWalkBfsQs
from randomwalkBfsSearch import readTestQs
from createListOfRandomWalkBfsOutputFiles import getOutputFilesAndTemplates

def readLines(name):
  inf = open(name)
  lines = [line.strip() for line in inf]
  inf.close()
  return lines
# This function would be much easier with defaultdict,
# but that's new in python2.5, which the cluster doesnt have.
def convertToCountedAndSortedList(l):
  d = {}
  for x in l:
    if x not in d:
      d[x] = 1
    else:
      d[x] += 1
  d = [x for x in d.iteritems()]
  d.sort(key=lambda x: -x[1])
  return d
def filterSearchSites(a):
  search_sites = ['wikipedia', 'yahoo', 'answerscom', 'new york times', \
      'bbc news', 'youtube', 'bbc', 'abc news', 'usmagazinecom']
  return [x for x in a if x[0] not in search_sites]
def removeEnt(ent, answers):
  if ent:
    tmp = ent.split()
    first = tmp[0]
    last = tmp[-1]
    answers = [x for x in answers if x!=first and x!=last and x!=ent]
  return answers
def getAnswers(name, ent):
  ans = readLines(name)
  ans = [normalizeAnswer(a) for a in ans]
  ans = convertToCountedAndSortedList(ans)
  ans = filterSearchSites(ans)
  ans = removeEnt(ent, ans)
  return ans

#######################################################
if __name__ == "__main__":
  # read args
  if len(sys.argv) != 1:
    Error('Usage: createListOfRandomWalkBfsOutputFiles.py')
  ## Output google suggest files that we are testing
  wikiAnswersInBase = 'repro_out/wa_results_ner/'
  googleInBase = 'repro_out/google_results_ner/'
  qs = readTestQs()#TODO remove
  qsAndDummy = [(q,q) for q in qs]
  names, qs = getOutputFilesAndTemplates(qsAndDummy, googleInBase, 'dummy/')
  for name,q in zip(names,qs):
    print '#################################'
    print q
    googleAnswers = getAnswers(googleInBase + name, None)
    wikiAnswers = getAnswers(wikiAnswersInBase + name, None)
    print
    print googleAnswers
    print
    print wikiAnswers
    print


  sys.exit()
  ####################
  qs, outbase, newqoutbase, qsAndTemplates = loadRandomWalkBfsQs()
  print outbase
  print newqoutbase
  sys.exit()
  outnames, outTemplates = getOutputFilesAndTemplates(qsAndTemplates, outbase, newqoutbase)
  print 'DONE'
  writeQsToFile(REPRO_OUT + 'listOfRandomWalkBfsOutputQs.txt', outTemplates)
  writeQsToFile(REPRO_OUT + 'listOfRandomWalkBfsOutputFiles.txt', outnames)
  sys.exit()
