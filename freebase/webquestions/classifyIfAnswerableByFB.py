import os,sys,re, operator, random
from andrewutil import normalizeName, getDirName, normalizeAnswer
import numpy as np
import sklearn
from sklearn import datasets, svm
from sklearn.linear_model import LogisticRegression
from createSemanticParserInputAmtCsv import getAmtAnswers3
import cPickle as pickle
from math import ceil, log
import pylab
from createAmtData2 import getAmtInputName

def getWordsIndices(allExamples, qs2Tags):
  word2IndexAndCount = {}
  wi = 0
  for q,url,a in allExamples:
    q = q.strip('?')
    qwords = q.split()
    for w in qwords:
      if w in word2IndexAndCount:
        word2IndexAndCount[w][1] += 1
      else:
        word2IndexAndCount[w] = [wi, 1]
        wi += 1
  tag2IndexAndCount = {}
  ti = 0
  for q,tags in qs2Tags.iteritems():
    for tag in tags:
      if tag in tag2IndexAndCount:
        tag2IndexAndCount[tag][1] += 1
      else:
        tag2IndexAndCount[tag] = [ti, 1]
        ti += 1
    for i in range(len(tags)-1):
      tag = '_'.join(tags[i:i+2])
      #print tag
      if tag in tag2IndexAndCount:
        tag2IndexAndCount[tag][1] += 1
      else:
        tag2IndexAndCount[tag] = [ti, 1]
        ti += 1
    '''
    for i in range(len(tags)-2):
      tag = '_'.join(tags[i:i+3])
      #print tag
      if tag in tag2IndexAndCount:
        tag2IndexAndCount[tag][1] += 1
      else:
        tag2IndexAndCount[tag] = [ti, 1]
        ti += 1
    '''
  print wi,ti
  return wi,ti,word2IndexAndCount,tag2IndexAndCount
def getExampleMatrix(allExamples, word2IndexAndCount, tag2IndexAndCount, qs2Tags, N_UNIQUE_WORDS, N_UNIQUE_TAGS):
  N = len(allExamples)

  NFEAT = N_UNIQUE_WORDS + N_UNIQUE_TAGS
  X = np.zeros((N,NFEAT))
  i = 0
  for q,url,a in allExamples:
    q = q.strip('?')
    qwords = q.split()
    for w in qwords:
      wi,count = word2IndexAndCount[w]
      if count > 1:
        X[i,wi] = 1 # TODO This can be = or +=
      #X[i,wi] = 1.0/log(float(N)/count)
    tags = qs2Tags[q+'?']
    for tag in tags:
      ti,count = tag2IndexAndCount[tag]
      X[i,N_UNIQUE_WORDS+ti] += 1
    for j in range(len(tags)-1):
      tag = '_'.join(tags[j:j+2])
      ti,count = tag2IndexAndCount[tag]
      #print N_UNIQUE_WORDS, NFEAT, tag, ti, count
      X[j,N_UNIQUE_WORDS+ti] += 1
    '''
    for j in range(len(tags)-2):
      tag = '_'.join(tags[j:j+3])
      ti,count = tag2IndexAndCount[tag]
      #print N_UNIQUE_WORDS, NFEAT, tag, ti, count
      X[j,N_UNIQUE_WORDS+ti] += 1
    '''
    #firstTag = tags[0]
    #fti,count = tag2IndexAndCount[firstTag]
    #X[i,N_UNIQUE_WORDS+1*N_UNIQUE_TAGS+fti] = 1
    #lastTag = tags[-1]
    #lti,count = tag2IndexAndCount[lastTag]
    #X[i,N_UNIQUE_WORDS+2*N_UNIQUE_TAGS+lti] = 1

    i += 1
  return X
def getPrecisionRecallCurve(pY, corVal, visualize):
  cor = [v == corVal for p,v in pY]
  N = len(pY)
  cumCor = np.zeros(N)
  i = 0
  for c in cor:
    prev = 0
    if i > 0:
      prev = cumCor[i-1]
    inc = 0
    if c:
      inc = 1
    cumCor[i] = prev + inc
    i += 1
  for i in range(N):
    cumCor[i] /= float(i+1)
  # Make sure it's strictly decreasing
  maxSoFar = 0.0
  for i in range(N-1, -1, -1):
    cumCor[i] = max(maxSoFar, cumCor[i])
    maxSoFar = cumCor[i]
  nInClass = len([x for x in pY if x[1] == corVal])
  inc = 1.0 / nInClass
  rec = np.arange(0, 1.0, inc)
  prec = np.zeros(nInClass)
  i = 0
  for v,c in zip(cumCor, cor):
    if c:
      prec[i] = v
      i += 1

  if visualize:
    pylab.plot(rec, prec)
    pylab.show()
  return prec
# TODO want precision,recall curve
def evaluatePreds(Y, pneg, threshPrec, visualize):
  for y in Y:
    assert(y == 1.0 or y == 0.0)
  pNegY = zip(pneg,Y)
  pNegY.sort(reverse=True) # highest prob of neg first
  negPrecRec = getPrecisionRecallCurve(pNegY, 0.0, visualize)
  pPosY = pNegY
  pPosY.sort() # highest prob of pos first
  posPrecRec = getPrecisionRecallCurve(pNegY, 1.0, False)
  goodi = 0
  for i,v in zip(range(len(negPrecRec)), negPrecRec):
    if v < threshPrec:
      break
    else:
      goodi = i
  # at threshPrec for the negatives we have recall
  recNeg = float(goodi) / len(negPrecRec)
  nNeg = len(negPrecRec)
  nPos = len(posPrecRec)
  recPos = recNeg*nNeg * (1.0-threshPrec) / threshPrec / nPos
  return negPrecRec, posPrecRec, recNeg, recPos
def getQsFromTrial(trial):
  inf = open(getAmtInputName(trial))
  inf.readline()
  qs = [l.strip().split(',')[1] for l in inf]
  inf.close()
  return qs

########################################################################
if __name__ == "__main__":
  np.random.seed(0)
  random.seed(0)
  if len(sys.argv) != 3:
    Error('Usage classifyIfAnswerableByFB.py <bfs trial> <use svm>')
  trial = int(sys.argv[1])
  USE_SVM = bool(sys.argv[2])
  ######################
  cacheDir = 'cache/'
  os.system('mkdir -p '+cacheDir)
  cacheFile = cacheDir + 'classifyIfAnswerableByFB.p'
  if os.path.exists(cacheFile):
    print 'Loading from pickle...'
    allGood,allBad,entsOnly,entsAndDates,invalidResults = \
        pickle.load(open(cacheFile, 'rb'))
  else:
    allGood,allBad,entsOnly,entsAndDates,invalidResults = getAmtAnswers3(trial)
    print 'Saving to pickle...'
    pickle.dump((allGood,allBad,entsOnly,entsAndDates,invalidResults), \
        open(cacheFile, 'wb'))
  # Do POS tagging
  print 'Computing POS tags...'
  qs = []
  for trial in [6]: #TODO add more data
    qs_ = getQsFromTrial(trial)
    qs += qs_
  posFile = cacheDir + 'classifyIfAnswerableByFB.pos'
  if not os.path.exists(posFile):
    outf = open(cacheDir + 'classifyIfAnswerableByFB_qs.txt', 'w')
    for q in qs:
      outf.write(q+'\n')
    outf.close()
    cwd = os.getcwd()
    os.chdir('../stanford-postagger-2013-04-04/')
    cmd = './stanford-postagger.sh english-caseless-left3words-distsim.tagger ' + cwd + '/cache/classifyIfAnswerableByFB_qs.txt > ' + cwd + '/' + posFile
    os.system(cmd)
    os.chdir(cwd)
  inf = open(posFile)
  posTags = []
  for line in inf:
    line = [x.split('_')[1] for x in line.split()]
    posTags.append(line[:-1])
  inf.close()
  qs2Tags = {}
  for q,tags in zip(qs, posTags):
    qs2Tags[q] = tags
  #os.system
  ######################

  random.shuffle(allGood)
  random.shuffle(allBad)
  print 'Percent Negative:', float(len(allBad)) / (len(allBad) + len(allGood))
  if USE_SVM:
    threshPrec = 0.915
  else:
    threshPrec = 0.92
  print 'Running cross validation with precision threshold:', threshPrec
  N_SPLITS = 10
  N_ALL = len(allGood) + len(allBad)
  nPosPerSplit = int(ceil(float(len(allGood)) / N_SPLITS))
  nNegPerSplit = int(ceil(float(len(allBad)) / N_SPLITS))
  recNegs = []
  recPoss = []
  for crossValIter in range(N_SPLITS):
    goodi1 = crossValIter * nPosPerSplit
    goodi2 = min((crossValIter+1) * nPosPerSplit, len(allGood))
    badi1 = crossValIter * nNegPerSplit
    badi2 = min((crossValIter+1) * nNegPerSplit, len(allBad))
    # organize into test and train
    good_train = allGood[:goodi1] + allGood[goodi2:]
    bad_train = allBad[:badi1] + allBad[badi2:]
    good_test = allGood[goodi1:goodi2]
    bad_test = allBad[badi1:badi2]
    train = good_train + bad_train
    test = good_test + bad_test
    allExamples = train + test
    NPOS = len(good_train)
    # Assign word indices and counts
    N_UNIQUE_WORDS,N_UNIQUE_TAGS,word2IndexAndCount,tag2IndexAndCount = \
        getWordsIndices(allExamples, qs2Tags)

    Y_train = np.hstack((np.ones(len(good_train)), np.zeros(len(bad_train))))
    X_train = getExampleMatrix(train, word2IndexAndCount, tag2IndexAndCount, \
        qs2Tags, N_UNIQUE_WORDS, N_UNIQUE_TAGS)
    if USE_SVM:
      svmModel = sklearn.svm.SVC()
      svmModel.fit(X_train, Y_train)
    else:
      model = LogisticRegression().fit(X_train, Y_train)
    Y_test = np.hstack((np.ones(len(good_test)), np.zeros(len(bad_test))))
    X_test = getExampleMatrix(test, word2IndexAndCount, tag2IndexAndCount, \
        qs2Tags, N_UNIQUE_WORDS, N_UNIQUE_TAGS)

    if USE_SVM:
      preds = svmModel.decision_function(X_train)
      pneg = [-x[0] for x in preds] # these are not actually probabilities
    else:
      probs = model.predict_proba(X_train)
      pneg = [x[0] for x in probs]

    negPrecRec, posPrecRec, recNeg, recPos = evaluatePreds(Y_train, pneg, threshPrec, False)
    print 'Train: Throwing out', recNeg, 'negatives and', recPos, 'positives.'
    if USE_SVM:
      preds = svmModel.decision_function(X_test)
      pneg = [-x[0] for x in preds] # these are not actually probabilities
    else:
      probs = model.predict_proba(X_test)
      pneg = [x[0] for x in probs]
    negPrecRec, posPrecRec, recNeg, recPos = evaluatePreds(Y_test, pneg, threshPrec, False)
    print 'Test:  Throwing out', recNeg, 'negatives and', recPos, 'positives.'
    recNegs.append(recNeg)
    recPoss.append(recPos)
    break
  print np.mean(recNegs)
  print np.mean(recPoss)
