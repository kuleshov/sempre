import urllib, urllib2 # contacting websites
import string, time, calendar, sys, os
import gzip, StringIO, random
import filterOutput, socket, re
from andrewutil import Error, normalizeName, getDirName, normalizeUnicode
from common import *
from aggregateRandomWalk import readTemplates
sys.path.append('../BeautifulSoup-3.2.1/')
from BeautifulSoup import BeautifulSoup, NavigableString
from wikianswersRandomWalk import getWikiAnswersOutFiles

#######################################################
if __name__ == "__main__":
  # read args
  if len(sys.argv) != 1:
    Error('Usage: createListOfRandomWalkOutputFiles.py')
  # setup dirs
  outbase = REPRO_OUT + 'wa_results/'
  newqoutbase = REPRO_OUT + 'wa_newq/' # used for new qs suggested by wiki answers
  ####################
  nSeeds = 18
  qs = set()
  for seed in range(nSeeds):
    if seed in [5]:
      continue #TODO
    #TODO 20
    _dummy, someQs = readTemplates(20, seed, RANDOM_WALK_DIR)
    qs.update(someQs)
    break #TODO
  qs = [q.replace(' _', '')+'?' for q in qs]
  bad = [q for q in qs if q.find('_')>=0]
  assert(len(bad) == 0)
  qs.sort()
  ####################
  outnames = []
  for q in qs:
    if not q:
      continue
    print q
    outname,newqoutname,w1,w2 = getWikiAnswersOutFiles(q, outbase, newqoutbase)
    if not os.path.exists(outname) and not os.path.exists(newqoutname):
      print '####################'
      print outname
      print newqoutname
      print 'Please make sure that wikianswersRandomWalk.py is done.'
      continue
      Error('Please make sure that wikianswersRandomWalk.py is done.')
    if os.path.exists(outname):
      name = outname.split('/')
      dirname = 'repro_out/wa_results_ner/'+'/'.join(name[2:-1])
      print dirname
      os.system('mkdir -p '+dirname)
      outnames.append('/'.join(name[2:]))
  print 'DONE'
  outfile = REPRO_OUT + 'listOfRandomWalkOutputFiles.txt'
  outf = open(outfile, 'w')
  for outname in outnames:
    outf.write(outname + '\n')
  outf.close()
  sys.exit()
