import json, urllib, urllib2 # contacting websites
import string, calendar, sys, os
import gzip, StringIO, random, socket, types
import filterOutput
from math import ceil, log
from andrewutil import normalizeName, getDirName, Error, getOutnameAndMakeDir,\
    t2q, getOutnameAndMakeDirQOnly, getDirNameQOnly, tlist2tstr
from common import *
from ioUtils import readLinesFromFile
from htmlUtils import gzipFile, getLynxDump
from randomwalkRw import getRandomWalkDir
import cPickle as pickle
from randomwalkEntSearch import loadRwTemplates, loadBfsTemplates, getGoodSeedNums

def checkResultsForErrors(results):
  if 'error' in results:
    print results
    return True
  return False
# returns True or some string output if Google was hit
# returns False or None if Google wasnt used.
def doCustomGoogleSearch(q, resultsdir, searchWholeQ):
  outname = getOutnameAndMakeDirQOnly(resultsdir, q, not searchWholeQ)
  outname = outname + '.pickle'
  print outname
  if not os.path.exists(outname) or os.stat(outname)[6] == 0:
    print 'Using custom google search...'
    print q
    url = CUSTOM_GOOGLE_SEARCH_URL % (GOOGLE_APIKEY, GOOGLE_CX, q)
    search_response = urllib.urlopen(url)
    search_results = search_response.read()
    results = json.loads(search_results)
    error = checkResultsForErrors(results)
    if error:
      print 'Error in doCustomGoogleSearch'
      sys.exit()
    pickle.dump(results, open(outname, 'w'))
    if os.stat(outname)[6]==0:
      print 'No more space!'
      sys.exit()
  else:
    results = pickle.load(open(outname))
    error = checkResultsForErrors(results)
    if error:
      print 'Error in doCustomGoogleSearch'
      cmd = 'rm -f '+outname
      print cmd
      os.system(cmd)
      sys.exit()
  return results

#######################################################
if __name__ == "__main__":
  random.seed(0)
  if len(sys.argv) not in [8]:
    hostname = socket.gethostname()
    if hostname.startswith('corn') and len(sys.argv) in [6,7]:
      cornNum = int(hostname.split('.')[0][4:])
    else:
      Error('Usage: randomwalkEntSearch.py <rw|bfs> <n> <seed> <search whole q> <rseed> <part> <of>')
  useBfs = (sys.argv[1].lower() == 'bfs')
  n = int(sys.argv[2])
  if sys.argv[3].lower() == 'all':
    seeds = getGoodSeedNums()
  else:
    seeds = [int(sys.argv[3])]
  searchWholeQ = bool(int(sys.argv[4]))
  rseed = int(sys.argv[5])
  arglist = sys.argv
  if len(sys.argv) > 6:
    index = int(sys.argv[6])
  else:
    indices =    [0, 1, 1, 3, 1, 3, 5, 7, 1, 3, 5, 7, 9, 11, 13, 15, 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31]
    index = indices[cornNum-1]
    arglist.append(index)
  if len(sys.argv) > 7:
    nParts = int(sys.argv[7])
  else:
    nPartsList = [1, 2, 4, 4, 8, 8, 8, 8,16,16,16,16,16, 16, 16, 16,32,32,32,32,32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32]
    nParts = nPartsList[cornNum-1]
    arglist.append(nParts)
  assert(nParts > 0)
  if useBfs:
    print 'Using BFS.'
    templates = loadBfsTemplates(seeds, n)
  else:
    print 'Using RW.'
    templates = loadRwTemplates(seeds, rseed, n)
  outbase = GOOGLE_CUSTOM_SEARCH_PICKLES
  for t in templates:
    assert(len(t) == 3)
  if searchWholeQ:
    qsAndEnts = set([(tlist2tstr(t),t[1]) for t in templates])
  else:
    #Error('Must search Whole Q right now')
    qsAndEnts = set([(None,t[1]) for t in templates])
  print len(qsAndEnts)
  os.system('mkdir -p '+outbase)
  print outbase
  qsAndEnts = list(qsAndEnts)
  nInEachPart = ceil(float(len(qsAndEnts)) / nParts)
  start = int(index * nInEachPart)
  end = int(min((index+1) * nInEachPart, len(qsAndEnts)))
  print start, end, len(qsAndEnts)
  qsAndEnts = qsAndEnts[start:end]

  i=0
  for q,ent in qsAndEnts:
    assert(ent != '')
    print i, 'of', len(qsAndEnts), ':', ent, ':', q
    i+=1
    if searchWholeQ:
      queries = [q+' site:en.wikipedia.org']
    else:
      queries = [ent+' site:en.wikipedia.org']
    assert(not searchWholeQ)
    print searchWholeQ, sys.argv[5]
    print arglist
    print queries
    for query in queries:
      outname = getOutnameAndMakeDirQOnly(outbase, query, not searchWholeQ)
      if os.path.exists(outname) and os.stat(outname)[6] > 0:
        continue
      #continue
      retry= True
      while retry:
        retry=False
        try:
          searchoutput = doCustomGoogleSearch(query, outbase, searchWholeQ)
          #sys.exit()
        except Exception, inst:
          print inst
          print 'Outer retry'
          retry= True
          sys.exit()
      print outname
      # wait at least a minute
  print 'DONE'
  print i
  sys.exit()
