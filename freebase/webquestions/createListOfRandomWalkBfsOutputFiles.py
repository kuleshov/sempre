import urllib, urllib2 # contacting websites
import string, time, calendar, sys, os
import gzip, StringIO, random
import filterOutput, socket, re
from andrewutil import Error, normalizeName, getDirName, normalizeUnicode, getOutnameAndMakeDir, t2q
from common import *
from aggregateRandomWalk import readTemplates, writeQsToFile
sys.path.append('BeautifulSoup-3.2.1/')
from BeautifulSoup import BeautifulSoup, NavigableString
from wikianswersRandomWalk import getWikiAnswersOutFiles
from wikianswersRandomWalkBfs import loadRandomWalkBfsQs
from randomwalkBfsSearch import readTestTs
def getOutputFilesAndTemplates(qsAndTemplates, outbase, newqoutbase):
  outnames = []
  outTemplates = []
  for q,template in qsAndTemplates:
    assert(q)
    print q
    outname,newqoutname,w1,w2 = getWikiAnswersOutFiles(q, outbase, newqoutbase)
    if not os.path.exists(outname) and not os.path.exists(newqoutname):
      print '####################'
      print outname
      print newqoutname
      print 'Please make sure that wikianswersRandomWalk.py is done.'
      continue
      Error('Please make sure that wikianswersRandomWalk.py is done.')
    if os.path.exists(outname):
      name = outname.split('/')
      nerbase = outbase
      while nerbase.endswith('/'):
        nerbase = nerbase[:-1]
      nerbase += '_ner/'
      dirname = nerbase+'/'.join(name[2:-1])
      print dirname
      os.system('mkdir -p '+dirname)
      outnames.append('/'.join(name[2:]))
      outTemplates.append(template)
  return outnames, outTemplates

#######################################################
if __name__ == "__main__":
  # read args
  if len(sys.argv) not in [1,2]:
    Error('Usage: createListOfRandomWalkBfsOutputFiles.py <run on small test set - optional>')
  runOnTestSet = False
  if len(sys.argv) == 2:
    runOnTestSet = bool(sys.argv[1])
  ## Output google suggest files that we are testing
  if runOnTestSet:
    outbase = 'repro_out/google_results/'
    newqoutbase = 'dummy/'
    ts = readTestTs()#TODO remove
    qsAndDummy = [(t2q(t),None) for t in ts]
    outnames, dummy = getOutputFilesAndTemplates(qsAndDummy, outbase, newqoutbase)
    writeQsToFile(REPRO_OUT + 'listOfTestOutputFiles.txt', outnames)
    sys.exit()
  ####################
  qs, outbase, newqoutbase, qsAndTemplates = loadRandomWalkBfsQs()
  print outbase
  print newqoutbase
  sys.exit()
  outnames, outTemplates = getOutputFilesAndTemplates(qsAndTemplates, outbase, newqoutbase)
  print 'DONE'
  writeQsToFile(REPRO_OUT + 'listOfRandomWalkBfsOutputTs.txt', outTemplates)
  writeQsToFile(REPRO_OUT + 'listOfRandomWalkBfsOutputFiles.txt', outnames)
  sys.exit()
