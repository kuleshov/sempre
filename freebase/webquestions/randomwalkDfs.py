import sys, os, socket, time, types, copy
from subprocess import Popen, PIPE, STDOUT
from andrewutil import Error, getRandomWalkSuggestOutName
from common import *
import random, string
from ioUtils import readLinesFromFile, writeQsToFile

def writeTemplatesWrapper(n, seed, outdir, outfile, templates, putInOrder = True, depth = -1):
  outname = outdir + '_'.join([outfile, 'n', str(n), 'seed', str(seed)])
  if depth >= 0:
    outname += '_'.join(['', 'depth', str(depth)])
  print outname
  templates = list(templates)
  if putInOrder:
    templates.sort()
  outf = open(outname, 'w')
  for template in templates:
    outf.write(' _ '.join(list(template)) + '\n')
  outf.close()
def writeTemplates(n, seed, outdir, allTemplates, \
    multiEntTemplates, sequenceOfTemplates = None):
  writeTemplatesWrapper(n, seed, outdir, 'oneEntTemplates', allTemplates)
  writeTemplatesWrapper(n, seed, outdir, 'multiEntTemplates', multiEntTemplates)
  if sequenceOfTemplates != None:
    writeTemplatesWrapper(n, seed, outdir, 'sequenceOfTemplates', sequenceOfTemplates, False)
'''
def readSeedTemplates(popen):
  template = ('where was', 'barack obama', 'born')
  return [template]
  #TODO

  import urllib2
  from xml.dom.minidom import parseString
  file = urllib2.urlopen('file:///user/akchou/scr/questionsdataset2/data/randomwalk/dbpedia-train.xml')
  data = file.read()
  file.close()
  dom = parseString(data)
  questions = []
  questionsXml = dom.getElementsByTagName('question')
  for q in questionsXml:
    s = q.getElementsByTagName('string')
    assert(len(s) == 1)
    q = str(s[0].toxml().split('\n')[1])
    questions.append(q)
  badTemplates = []
  templates = []
  for q in questions:
    oneEntTemplates,multiEntTemplates = runSemanticParser(popen, q)
    templates += oneEntTemplates
    badTemplates += multiEntTemplates
  print len(templates)
  return templates
'''

###############################
if __name__ == "__main__":
  random.seed(0)
  print sys.argv
  if len(sys.argv) != 3:
    Error('randomwalkDfs.py <n> <seed index>')
  n = int(sys.argv[1])
  seed = int(sys.argv[2])
  os.system('mkdir -p ' + RANDOM_WALK_DFS_DIR)
  ################################
  popen = loadGibbsSampler()
  ################################
  allTemplates = set()
  multiEntTemplates = set()
  from randomwalkBfs import getSeedTemplates
  seedTemplates = getSeedTemplates()
  template = seedTemplates[seed]
  allTemplates.add(template)
  usedTemplates = set()
  usedTemplates.add(template)
  sequenceOfTemplates = [template]
  #numberOfIterBeforeReset = 100
  numSuggests = 0
  i = 0
  while len(allTemplates) < n:
    print 'ITER:', i
    print len(allTemplates), 'OF', n, 'TEMPLATES'
    index = random.randint(0, len(template) - 1)
    outname = getRandomWalkSuggestOutName(REPRO_SEARCH_QUEUE, template, index)
    print outname
    if os.path.exists(outname):
      newQs = readLinesFromFile(outname)
    else:
      newQs = None
      while newQs == None:
        newQs = getQuestions(template, index)
        #break
        if newQs == None:
          print 'Sleeping during', template
          time.sleep(1200) # 20 mins
      newQs.sort()
      writeQsToFile(outname, newQs)
      numSuggests += 1
    newQs.sort()
    #print newQs
    newTemplates = []
    for q in newQs:
      oneEntTemplates,newMultiEntTemplates = runSemanticParser(popen, q)
      newTemplates += oneEntTemplates
      multiEntTemplates.update(newMultiEntTemplates)
    allTemplates.update(newTemplates)
    i += 1
    newTemplates = [t for t in newTemplates if t not in usedTemplates]
    if len(newTemplates) == 0:# or i % numberOfIterBeforeReset == 0:
      #if len(allTemplates) == 1:
      #  Error('No suggestions for seed: ' + ' '.join(list(template)))
      template = random.choice(list(allTemplates))
      #template = seedTemplates[seed]
    else:
      template = random.choice(newTemplates)
    print 'New template:', template
    sequenceOfTemplates.append(template)
    usedTemplates.add(template)
  writeTemplates(n, seed, RANDOM_WALK_DFS_DIR, allTemplates, \
      multiEntTemplates, sequenceOfTemplates)
  closeGibbsSampler(popen)
  print 'allTemplates:', len(allTemplates)
  ents = set([x[1] for x in allTemplates])
  print 'ents:', len(ents)
  prefixSuffix = set([(x[0], x[2]) for x in allTemplates])
  print 'prefixSuffix:', len(prefixSuffix)
  print 'DONE with randomwalkDfs.py!'
  print 'numSuggests:', numSuggests
  sys.exit()
