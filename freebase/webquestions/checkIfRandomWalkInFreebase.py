import urllib, urllib2 # contacting websites
import string, time, calendar, sys, os
from subprocess import Popen, PIPE, STDOUT
import gzip, StringIO, random
import filterOutput, socket, re
from andrewutil import Error, normalizeName, getDirName, normalizeUnicode
from common import *
from aggregateRandomWalk import readTemplates
sys.path.append('BeautifulSoup-3.2.1/')
from BeautifulSoup import BeautifulSoup, NavigableString
from wikianswersRandomWalk import getWikiAnswersOutFiles
from wikianswersRandomWalkBfs import loadRandomWalkBfsQs

def loadFreebaseChecker():
  cwd = os.getcwd()
  semparsedir = '/user/akchou/scr/semparse/'
  os.chdir(semparsedir)
  cmd = [semparsedir+'possiblyInFreebaseChecker']
  popen = Popen(cmd, shell=True, stdin=PIPE, stderr=PIPE, close_fds=True)
  for line in iter(popen.stderr.readline, ""):
    print line.strip('\n')
    if line == 'AAAAAAAAAAAAAAAAAAAAAAAAAA\n':
      break
  os.chdir(cwd)
  return popen
def closeFreebaseChecker(popen):
  popen.stdin.write('\n')
  for line in iter(popen.stderr.readline, ""):
    pass
  popen.wait()

def checkFreebase(popen, template, ent, otherEnts):
  ent = ent.strip()
  otherEnts = [e.strip() for e in otherEnts]
  print 'Running Freebase Checker...'
  retry = True
  while retry:
    retry = False
    dummyinput = 'TTT'.join([template, ent, ' _ '.join(otherEnts)]) + ' \n'
    print dummyinput
    input = '\t'.join([template, ent, ' _ '.join(otherEnts)]) + ' \n'
    print input
    assert(popen.poll() == None)
    popen.stdin.write(input)
    last = False
    for line in iter(popen.stderr.readline, ""):
      line = line.strip('\n')
      print 'Line:XXX'+line+'XXX'
      if line.startswith('Invalid command:'):
        print 'RETRYING!'
        retry = True
        break
      if last:
        if line == 'SUCCESS!':
          print ent, 'and', otherEnts, 'match.'
          return True
        elif line == 'FAILURE!':
          pass
        else:
          print line
          Error('Bad line')
        break
      elif not line:
        last = True
  return False

#######################################################
if __name__ == "__main__":
  # read args
  if len(sys.argv) != 2 or sys.argv[1] not in ['bfs', 'dfs']:
    Error('Usage: checkIfRandomWalkInFreebase.py <dfs or bfs>')
  if sys.argv[1].lower() == 'bfs':
    bfs = True
    listOfRandomWalkFiles = 'repro_out/listOfRandomWalkBfsOutputFiles.txt'
    freebaseTsFile = 'freebaseTsBfs.txt'
    nonFreebaseTsFile = 'nonFreebaseTsBfs.txt'
  elif sys.argv[1].lower() == 'dfs':
    bfs = False
    freebaseTsFile = 'freebaseTs.txt'
    nonFreebaseTsFile = 'nonFreebaseTs.txt'
    listOfRandomWalkFiles = 'repro_out/listOfRandomWalkOutputFiles.txt'
  else:
    Error(sys.argv[1] + ' is not a valid option.')
  # setup dirs
  outbase = REPRO_OUT + 'wa_results/'
  newqoutbase = REPRO_OUT + 'wa_newq/' # used for new qs suggested by wiki answers
  fbchecker = loadFreebaseChecker()
  ####################
  nSeeds = 18
  if bfs:
    qs, outbase, newqoutbase, qsAndTemplates = loadRandomWalkBfsQs()
    templates = [t for q,t in qsAndTemplates]
  else:
    templates = set()
    for seed in range(nSeeds):
      if seed in [5]:
        continue #TODO
      #TODO 20
      _dummy, someQs = readTemplates(20, seed, RANDOM_WALK_DIR)
      templates.update(someQs)
      break #TODO
  qsAndEnts = [(t.strip(), t.strip(), t.split('_')[1].strip()) for t in templates]
  qsAndEnts = [(t, q.replace(' _', '')+'?', e) for t,q,e in qsAndEnts]
  bad = [q for t,q,e in qsAndEnts if q.find('_')>=0]
  assert(len(bad) == 0)
  #qsAndEnts.sort()
  ####################
  freebaseTs = []
  nonFreebaseTs = []
  inf = open(listOfRandomWalkFiles)
  validfiles = set(['repro_out/wa_results/'+line.strip() for line in inf])
  inf.close()
  for t,q,ent in qsAndEnts:
    print 'TEMPLATE:', t
    print 'QUESTION:', q
    print 'ENT:', ent
    outname,newqoutname,w1,w2 = getWikiAnswersOutFiles(q, outbase, newqoutbase)
    if outname not in validfiles:
      continue
    if not os.path.exists(outname) and not os.path.exists(newqoutname):
      print '####################'
      print outname
      print newqoutname
      if bfs:
        Error('Please make sure that wikianswersRandomWalkBfs.py is done.')
      else:
        Error('Please make sure that wikianswersRandomWalk.py is done.')
    if os.path.exists(outname):
      print 'Checking Freebase for:', q
      name = outname.split('/')
      nername = 'repro_out/wa_results_ner/'+'/'.join(name[2:])
      print nername
      assert(os.path.exists(nername))
      inf = open(nername)
      ents = [line.strip() for line in inf]
      inf.close()
      ents = [other for other in ents if other not in ent]
      ents = [other for other in ents if ent not in other]
      ents = list(set(ents))
      ents.sort()
      print 'ENT:', ent
      print 'OTHER ENTS:', ents
      if checkFreebase(fbchecker, t, ent, ents):
        freebaseTs.append(t)
        #Error('SUCCESS!')
      else:
        print 'NO MATCH!'
        nonFreebaseTs.append(t)
        #Error('FAILURE!')
  outf = open(REPRO_OUT + freebaseTsFile, 'w')
  for t in freebaseTs:
    outf.write(t+'\n')
  outf.close()
  outf = open(REPRO_OUT + nonFreebaseTsFile, 'w')
  for t in nonFreebaseTs:
    outf.write(t+'\n')
  outf.close()
  print 'total Qs:', len(qsAndEnts)
  print 'Qs with WikiAnswers results:', len(freebaseTs) + len(nonFreebaseTs)
  print 'freebaseTs:', len(freebaseTs)
  print 'nonFreebaseTs:', len(nonFreebaseTs)
  print 'DONE'
  closeFreebaseChecker(fbchecker)
  # Move the
  if bfs:
    extension = '_bfs.txt'
  else:
    extension = '.txt'
  cmd = 'mv /user/akchou/scr/semparse/fb_good.txt '+REPRO_OUT+'fb_good'+extension
  os.system(cmd)
  cmd = 'mv /user/akchou/scr/semparse/fb_bad.txt '+REPRO_OUT+'fb_bad'+extension
  os.system(cmd)
  sys.exit()
