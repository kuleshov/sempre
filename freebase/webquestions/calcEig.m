f = fopen('sparse2.txt');
C = textscan(f, '%f %f\n');
fclose(f);
n = 200012;
n = 8120;
n = 119611;
rows = [C{1};C{2}]+1;
cols = [C{2};C{1}]+1;
M = sparse( rows, cols, 1, n, n, 2*length(C{1}));
I = sparse(1:n,1:n,1, n, n, n);
disp('normalizing cols')
for i=1:n
  i
  s = sum(M(:,i));
  if s > 0
    M(:,i) = M(:,i)/(2*s);
    M(i,i) = 0.5;
  end
end
disp('computing eigs')
%v = eigs(M,I,2)
v = eigs(M,I)
v-1
