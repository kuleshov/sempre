import os, time
from common import *
from subprocess import Popen, PIPE, STDOUT

def loadGibbsSampler():
  cwd = os.getcwd()
  os.chdir(SCRIPTS_DIR)
  cmd = [GIBBS_EXE]
  #popen = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, close_fds=True)
  popen = Popen(cmd, shell=True, stdin=PIPE, stderr=PIPE, close_fds=True)
  for line in iter(popen.stderr.readline, ""):
    print line.strip('\n')
    if line == 'AAAAAAAAAAAAAAAAAAAAAAAAAA\n':
      break
  os.chdir(cwd)
  return popen
def closeGibbsSampler(popen):
  popen.stdin.write('\n')
  for line in iter(popen.stderr.readline, ""):
    pass
  popen.wait()
def runSemanticParser(popen, q):
  import select
  assert(popen.poll() == None)
  q = q.strip('? ').strip() + '?'
  assert(q.endswith('?'))
  print 'Running Semantic Parser on:'
  print q
  #TODO this shouldn't happen, but it does sometimes
  # Probably because the cache is shared with other things.
  if q.find('_') >= 0:
    return [], []
  retry = True
  while retry:
    retry = False
    assert(popen.poll() == None)
    time.sleep(0.001)
    popen.stdin.write(q+'\n')
    templates = []
    nTemplates = -1
    last = False
    assert(popen.poll() == None)
    while True:
      # 30 second timeout
      i, o, e = select.select( [popen.stderr], [], [], 30 )
      if i:
        line = popen.stderr.readline()
        line = line.strip('\n')
        print 'Line:XXX'+line+'XXX'
        if line.startswith('Invalid command:'):
          print 'RETRYING!'
          retry = True
          break
        if last:
          if nTemplates >= 0:
            assert(nTemplates > 0)
            templates.append(line)
            nTemplates -= 1
          else:
            try:
              nTemplates = int(line)
            except:
              retry = True
              break
        elif not line:
          last = True
        # break if there are no more
        if nTemplates == 0:
          break
        print last, nTemplates
      else:
        print 'runSemanticParser timed out. Retrying...'
        retry = True
        break
  return splitTemplates(templates)

def splitTemplates(templates):
  newTemplates = []
  multiEntTemplates = []
  for template in templates:
    template = template.strip('"?').strip().split('_')
    template = [x.strip() for x in template]
    if len(template) == 2: # no prefix
      print 'Template had length 2:', template
      template = [''] + template
    template = tuple(template)
    if len(template) != 3:
      print 'Template had problem:', template
    assert(len(template) == 3)
    if len(templates) > 1:
      multiEntTemplates.append(template)
    else:
      newTemplates.append(template)
  return newTemplates, multiEntTemplates
