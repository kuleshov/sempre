REPRO_OUT = 'repro_out/'
SEMANTIC_PARSER_DIR = '../'
SCRIPTS_DIR = SEMANTIC_PARSER_DIR + 'scripts/dataset/'
GIBBS_EXE = './gibbs'
NER_EXE = './gibbs'
REPRO_SUGGEST_QUEUE = REPRO_OUT + 'reprosuggestqueue/'
REPRO_SEARCH_QUEUE = REPRO_OUT + 'reprosearchqueue/'
REPRO_PARSED_DIR = REPRO_OUT + 'repro_parsed/'
FREEBASE_ANSWER_CACHE = REPRO_OUT + 'fbAnswers/'
FREEBASE_ENT_HTML = REPRO_OUT + 'freebaseEntHtml/'
GOOGLE_RESULTS_HTML = REPRO_OUT + 'google_resultshtml/'
GOOGLE_CUSTOM_SEARCH_PICKLES = REPRO_OUT + 'google_custom_search_pickles/'
GOOGLE_NER = REPRO_OUT + 'google_ner/'
NUM_FILE_NAME = 'NUM.txt'

RANDOM_WALK_RW_DIR = REPRO_OUT + 'random_walk_rw_templates/'
RANDOM_WALK_DFS_DIR = REPRO_OUT + 'random_walk_dfs_templates/'
RANDOM_WALK_BFS_DIR = REPRO_OUT + 'random_walk_bfs_templates/'
RANDOM_WALK_RFS_DIR = REPRO_OUT + 'random_walk_rfs_templates/'

WIKIPEDIA_TO_MID_MAP = '/jackson/scr4/psl/kb/freebase-wikipedia.ttl'
WIKIPEDIA_TO_MID_MAP_CACHE = 'data/freebase-en.wikipedia.org'
MID_TO_FBID_MAP = '/jackson/scr4/psl/kb/state/execs/1.exec/mid-to-id.tsv'
MID_TO_FBID_MAP_CACHE = 'data/mId-fbId.txt'

# Custom Google Search Stuff from the percyliangresearch@gmail.com account.
GOOGLE_APIKEY = 'AIzaSyAXFT6ABaApvBuvqinXvZ012uQaVyMV9sM'
GOOGLE_CX = '009657280970127345998:u8_i7q_by2m'
CUSTOM_GOOGLE_SEARCH_URL = 'https://www.googleapis.com/customsearch/v1?key=%s&cx=%s&alt=json&q=%s'
