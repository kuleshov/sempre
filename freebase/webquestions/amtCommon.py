def getAmtInputDir(trial):
  name = 'data/dataset_'+str(trial)+'/'
  return name
def getAmtName(trial):
  return getAmtInputDir(trial)+'amt_' + str(trial)
def getAmtNameWithSuffix(trial, suffix):
  return getAmtName(trial) + suffix
def getAmtInputName(trial):
  return getAmtName(trial) + '.input'
def getAmtResultsName(trial):
  return getAmtName(trial) + '.results'

