import os, sys
import socket
from andrewutil import normalizeName
sys.path.append('../BeautifulSoup-3.2.1/')
from BeautifulSoup import BeautifulSoup, NavigableString
import urllib2, random

############################
if __name__ == "__main__":
  random.seed(0)
  inf = open('repro_out/listOfRandomWalkBfsOutputTs.txt')
  qs = [line.strip() for line in inf]
  inf.close()
  qs.sort()
  random.shuffle(qs)
  outfile = 'tsShuffled.txt'
  outf = open(outfile, 'w')
  for q in qs:
    outf.write(q + '\n')
  outf.close()

  sys.exit()
