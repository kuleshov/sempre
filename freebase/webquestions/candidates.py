#!/usr/bin/python
from collections import defaultdict as ddict
import json, re, operator
import urllib, urllib2, sys, time, os, urlparse, httplib
import unicodedata
sys.path.append('beautifulsoup4-4.1.3')
from bs4 import BeautifulSoup

search_sites = ['wikipedia', 'yahoo', 'answerscom', 'new york times', 'bbc news']
search_sites = set(search_sites)

def filterOutput(o):
  o=o.split('\n')
  o2 = []
  header=True
  start=False
  for line in o:
    if header:
      if line.startswith('   Web Results 1 -'):
        header= False
    elif not start:
      if line=='':
        start= True
    elif line.startswith('   Searches related to:'):
      break
    elif line=='References':
      o2 = o2[:-9]
      while re.match(r'^\s*[0-9]+\s*\[[0-9]*\]$', o2[-1])!=None:
        o2.pop()
      break
    else:
      o2.append(line)
      #print line
  '''
  '''
  o2='\n'.join(o2)
  o2=o2.split('\n\n')
  o3=[]
  for line in o2:
    line = re.sub(r'\s\s+',' ',line.replace('\n',''))
    line = re.split(r'[^\/]\.\.\.[^\/]', line)
    line = '\n'.join(line)
    o3.append(line)

  return o3
def doSearch(q):
  filename = 'http://www.google.com/search?' + urllib.urlencode({'q': q})
  cmd = os.popen("lynx -dump %s" % filename)
  output = cmd.read()
  cmd.close()
  return output

q = ' '.join(sys.argv[1:])
print q
#continue
retry= True
while retry:
  retry=False
  try:
    output = doSearch(q)
  except Exception as inst:
    print inst
    print 'Outer retry'
    retry= True
output = filterOutput(output)
if (not output) or (len(output)==1 and output[0]==''):
  print 'Quota hit'
  sys.exit()
#sys.exit()
outname = 'output/tmp'
print outname
with open(outname, 'w') as outf:
  outf.write('\n\n'.join(output))
os.system('stanford-ner-2012-11-11/ner.sh output/tmp > output/tmp2')

def normalize(x):
  x = x.replace('.','')
  x = re.sub('\s+',' ', x)
  x = re.sub(r'(^|\s)([a-z]) ([a-z]) ', r'\1\2\3 ', x)
  x = x .strip()
  return x

with open( 'output/tmp3', 'w') as out:
  qs = []
  inname = 'output/tmp2'
  print inname
  allppl = ddict(int)
  alllocs = ddict(int)
  allorgs = ddict(int)
  with open(inname) as inf:
    for line in inf:
      ppl = re.findall(r'(<PERSON>(.*?)</PERSON>.*?)+.*?', line)
      ppl = [normalize(x[1].lower()) for x in ppl]
      for x in ppl:
        allppl[x] += 1
      locs = re.findall(r'(<LOCATION>(.*?)</LOCATION>.*?)+.*?', line)
      locs = [normalize(x[1].lower()) for x in locs]
      for x in locs:
        alllocs[x] += 1
      orgs = re.findall(r'(<ORGANIZATION>(.*?)</ORGANIZATION>.*?)+.*?', line)
      orgs = [normalize(x[1].lower()) for x in orgs]
      for x in orgs:
        allorgs[x] += 1
  allppl = sorted(allppl.iteritems(), key=operator.itemgetter(1), reverse=True)
  alllocs = sorted(alllocs.iteritems(), key=operator.itemgetter(1), reverse=True)
  allorgs = sorted(allorgs.iteritems(), key=operator.itemgetter(1), reverse=True)
  if q.startswith('who '):
    answers = allppl+allorgs+alllocs
  #elif q.startswith('what is the largest city in '):
  else:
    answers = alllocs+allorgs+allppl
  # filter out common search sites
  answers = [(x,y) for x,y in answers if x not in search_sites]
  # TODO filter out the entity in the question
  #print answers
  for a,n in answers:
    print n, a

