import os, sys, random
from andrewutil import normalizeName, getDirName
import numpy as np

def writeExample(outf, q, answers, gtent = None, pred_ent = None,
    label = 'targetValues'):
  assert(len(label.split()) == 1)
  outf.write('(example\n')
  outf.write('  (utterance "'+str(q)+'")\n')
  outf.write('  (' + label)
  for a in answers:
    if a.find('"') >= 0:
      print a
      Error('bad character')
    outf.write(' "'+a+'"')
  outf.write(')\n')
  if gtent != None:
    assert(pred_ent != None)
    outf.write('  (groundTruth "'+str(gtent)+'")\n')
    outf.write('  (predicted "'+str(pred_ent)+'")\n')
    if gtent == pred_ent:
      outf.write('  (difficulty "CORRECT")\n')
    elif gtent in answers:
      outf.write('  (difficulty "POSSIBLE")\n')
    else:
      outf.write('  (difficulty "HOPELESS")\n')
  outf.write(')\n')

########################################################################3
if __name__ == "__main__":
  from sklearn.linear_model import LogisticRegression
  from baselines import *
  np.random.seed(0)
  random.seed(0)
  lines = []
  inf = open('data/labelledqs.txt')
  outname = 'output2/trial2.examples'
  write = not os.path.exists(outname) or os.stat(outname)[6]==0
  if write:
    outf = open(outname, 'w')
  for line in inf:
    line = line.strip()
    q,ent1,gtent,answers,featname = extractQuestion(line)
    if not os.path.exists(featname):
      continue
    lines.append(line)

    if write:
      print q
      #print answers
      print gtent
      writeExample(outf, q, answers)
  if write:
    outf.close()
  inf.close()
  #random.shuffle(lines)
  lines.sort()

  outname = 'output2/trial2.examples.extra'
  if os.path.exists(outname) and os.stat(outname)[6] > 0:
    sys.exit()
  outf = open(outname, 'w')
  for i in range(len(lines)):
    train = lines[:i] + lines[i+1:]
    test = [lines[i]]
    ############################ BASELINE 2: Classifier ###########################
    model = trainModel(train)

    for line in test:
      q,ent1,gtent,answers,featname = extractQuestion(line)
      #TODO remove
      #gtent = 'alvin martin'
      #featname = 'answers/features/who/who_did_whoopi_goldberg_married_'
      #print featname
      if not os.path.exists(featname):
        continue
      X,Y,ents = aggregateFeatures(q, featname, None, answers, True)
      if X.size == 0:
        #there are no correct ents yet.
        correct = False
        pred_ent = answers[0]
      else:
        probs = model.predict_proba(X)
        answer_index = np.argmax(probs[:,1])
        pred_ent = ents[answer_index]
        #print pred_ent
        # TODO do more sophisticated tp,fp,tn,fn analysis
        #corr = np.array([(e==pred_ent and y) or (e!=pred_ent and not y) for e,y in zip(ents,Y)])
        if pred_ent == gtent:
          #print pred_ent
          correct = True
        else:
          correct = False
        print q
        print gtent
        print pred_ent
      writeExample(outf, q, answers, gtent, pred_ent)
  outf.close()
