import os, sys, random, re
from andrewutil import normalizeName, getDirName, Error
from amtCommon import getAmtNameWithSuffix
from createSemanticParserInputAmtCsv import writeAnswers, \
    fixUpAnswer, isNumberQuestion, isHardQuestion
import HTMLParser

def isNegativeAnswer2(a):
  negAnswers = ['MISSING_ANSWER', 'NOT_ON_PAGE', 'AMBIGUOUS', 'IRRELEVANT']
  return a in negAnswers
def parseResult2(line):
  assert(line)
  #field = r'(?:,"(?:[a-zA-Z\s.,\$0-9:;%()/_?+\'\n\r!-\\]|[^,]"")*")'
  #default = re.compile(start+field+'{25}'+answers, flags)
  field = r'(?:,"(?:[a-zA-Z\s.,\$0-9:;%()/_?+\'\n\r!-]|[^,]""|""[^,]|[^"])*")'
  flags = re.MULTILINE|re.DOTALL
  id = r'"[A-Z0-9]{30}"'
  start = r'(^'+id+','+id
  answers = r',")(.*)","(.*)","(.*)","(.*)"'
  default = re.compile(start+field+'{14},"(Approved|Rejected|Submitted)"'+field+'{10}'+answers, flags)
  m = default.findall(line)
  assert(len(m) < 2)
  if len(m) == 1:
    status = m[0][1]
    url = m[0][2]
    q = m[0][3]
    answers = m[0][4]
    a = m[0][5]
    #print 'STATUS:', status
    #print 'URL:', url
    #print 'QUESTION:', q
    #print 'ANSWERS:', answers
    #print 'ANSWER:', a
    assert(status in ['Approved', 'Rejected', 'Submitted'])
    return (status, url, q, fixUpAnswer(a), answers)
  else:
    print line
    print m
    Error('BAD')
  Error('BAD2')
def collapseAnswer(a):
  a = re.sub(r'\.+','.', re.sub(r'\s|\^','', a.lower()))
  a = re.sub(r"['|\"]+", '"', a)
  return a
def getAmtAnswers4(trial, resultFileSuffix = ''):
  #inf = open('data/dataset_6/duplicate_qs.results')
  inf = open(getAmtResultsName(trial)+resultFileSuffix)
  lines = [l for l in inf][1:]
  inf.close()
  results = []
  newline = True
  prev = ""
  for line in lines:
    if re.findall(r'^"[A-Z0-9]{30}"', line):
      if prev:
        #print 'PREV:'
        #print prev
        results.append(parseResult2(prev))
      prev = line
    else:
      prev += line
  results.append(parseResult2(prev))
  statusIndex = 0
  urlIndex = 1
  qIndex = 2
  answerIndex = 3
  statusesIveChecked = ['Approved', 'Rejected', 'Submitted', 'NotReviewed', '']
  validStatuses = ['Approved', 'Submitted', 'NotReviewed', '']
  needToCheck = [r for r in results if r[statusIndex] not in statusesIveChecked]
  if len(needToCheck) > 0:
    print set([r[statusIndex] for r in needToCheck])
    Error('Review some statuses to decide what to do with them.')
  validResults = [r for r in results if r[statusIndex] in validStatuses]
  #print validAnswers
  # TODO use the correct url
  negAnswers = ['MISSING_ANSWER', 'NOT_ON_PAGE', 'AMBIGUOUS', 'IRRELEVANT']
  normalized = []
  for status,url,q,a,answers in validResults:
    if a in negAnswers:
      normalized.append([q,url,'NO_ANSWER'])
      continue
    a = a.decode('utf-8')
    a = re.sub(r';', '', a)
    a = re.sub(r"['|\"]+", '"', a)
    answers = HTMLParser.HTMLParser().unescape(answers)
    answers = answers.replace('^',',')
    answers = answers.replace(';;;',',').split('###')
    realas = [x for x in answers if collapseAnswer(x) == a]
    if len(realas) == 0:
      print ''
      print len(answers)
      print answers
      print realas
      print repr(a)
      return answers,a,q,None,normalized
    reala = realas[0]
    reala = re.sub(r'([^\s]),([^\s])', r'\1, \2', reala)
    if isinstance(reala, str):
      reala = unicode(reala)
    normalized.append([q,url,reala])
  # count how many times each question was answered
  counts = {}
  for q,u,a in normalized:
    if q not in counts:
      counts[q] = [a]
    else:
      counts[q].append(a)
      assert(len(counts[q]) <= 2)
  oneAnswer = [(q,u,a) for q,u,a in normalized if len(counts[q]) == 1]
  mismatchList = [(q,u,a) for q,u,a in normalized if len(counts[q]) == 2 and counts[q][0] != counts[q][1]]
  matchList = [(q,u,a) for q,u,a in normalized if len(counts[q]) == 2 and counts[q][0] == counts[q][1]]
  match = {}
  for q,u,a in matchList:
    if q not in match:
      match[q] = (u,a)
  mismatch = {}
  for q,u,a in mismatchList:
    if q not in mismatch:
      mismatch[q] = [(u,a)]
    else:
      mismatch[q].append((u,a))
  good = {}
  bad = {}
  for q,v in match.iteritems():
    if v[1] == 'NO_ANSWER':
      bad[q] = v
    else:
      good[q] = v
  return good,bad,mismatch,oneAnswer,normalized
########################################################################
if __name__ == "__main__":
  random.seed(0)
  if len(sys.argv) != 2:
    Error('Usage createSemanticParserInputFromAmt2.py <bfs trial>')
  trial = int(sys.argv[1])
  ######################
  ######################
  #good,mismatch,oneAnswer,normalized = getAmtAnswers4(trial, '.newfb.short')
  #answers,a,q,dummy = getAmtAnswers4(trial, '.newfb')
  #sys.exit()
  good,bad,mismatch,oneAnswer,normalized = getAmtAnswers4(trial, '.newfb')
  #good,bad,mismatch,oneAnswer,normalized = getAmtAnswers4(trial, '.1000qs.allFields')
  print 'MATCHING:'
  print '  GOOD:', len(good)
  print '  BAD:', len(bad)
  print 'MISMATCH:', len(mismatch)
  print 'ONE ANSWER:', len(oneAnswer)
  print 'ALL:', len(normalized)
  assert(2*(len(good)+len(bad)+len(mismatch)) == len(normalized))

  goodList = []
  for q,v in good.iteritems():
    url = v[0]
    a = v[1]
    goodList.append((q,url,a))
  sys.exit()

  #writeAnswers(getAmtNameWithSuffix(trial, '_entsAndDates.examples'), entsAndDates)
  writeAnswers(getAmtNameWithSuffix(trial, '_good_checked2x.examples'), goodList)
  #writeAnswers(getAmtNameWithSuffix(trial, '_entsOnly.examples'), entsOnly)
  #writeAnswers(getAmtNameWithSuffix(trial, '_bad.examples'), bad)
