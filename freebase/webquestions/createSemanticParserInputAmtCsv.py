import os, sys, random, re, codecs
from andrewutil import normalizeName, getDirName, Error
from saveAmtAnswers import isNegativeAnswer
from createAmtData2 import getAmtResultsName, getAmtNameWithSuffix

#TODO make writeChild recursive rather than having this writeAnswerChild as a special case
def writeAnswerChild(outf, name, c):
  outf.write('  (' + name)
  if c == None:
    text = ' NO_ANSWER'
  else:
    c = c.replace('"', '\\"')
    if re.search(r'[^\\]"', c):
      print repr(c)
      Error('bad character')
    text = ' (description "' + c +'")'
  try:
    outf.write(text)
  except UnicodeEncodeError:
    print repr(text)
    outf.close()
    print 'Problem in createSemanticParserInputAmtCsv.py writeAnswerChild.'
    sys.exit()
  outf.write(')\n')
def writeChild(outf, name, c):
  outf.write('  (' + name)
  c = c.replace('"', '\\"')
  #if c.find('"') >= 0:
  if re.search(r'[^\\]"', c):
    print repr(c)
    Error('bad character in writeChild')
  outf.write(' "'+c+'"')
  outf.write(')\n')
def writeExample(outf, q, a, url):
  # header
  outf.write('(example\n')
  outf.write('  (utterance "'+str(q)+'")\n')
  # children
  writeAnswerChild(outf, 'targetValues', a)
  writeChild(outf, 'url', url)
  # footer
  outf.write(')\n')
def writeAnswers(outname, s):
  #write = not os.path.exists(outname) or os.stat(outname)[6]==0
  print outname
  UTF8Writer = codecs.getwriter('utf8')
  outf = UTF8Writer(open(outname, 'w'))
  for q,url,a in s:
    #print repr(q), '###', repr(a)
    #print repr(url)
    writeExample(outf, q, a, url)
  outf.close()
def fixUpAnswer(a):
  a = a.replace('"', "'").replace('\r\n', '. ')
  a = re.sub(r'\.+', '.', a)
  return a
def parseResult(line):
  assert(line)
  #field = r'(?:,"(?:[a-zA-Z\s.,\$0-9:;%()/_?+\'\n\r!-\\]|[^,]"")*")'
  #default = re.compile(start+field+'{25}'+answers, flags)
  field = r'(?:,"(?:[a-zA-Z\s.,\$0-9:;%()/_?+\'\n\r!-]|[^,]""|""[^,]|[^"])*")'
  flags = re.MULTILINE|re.DOTALL
  id = r'"[A-Z0-9]{30}"'
  start = r'(^'+id+','+id
  answers = r',")(.*)","(.*)","(.*)"'
  default = re.compile(start+field+'{14},"(Approved|Rejected|Submitted)"'+field+'{10}'+answers, flags)
  m = default.findall(line)
  #print m
  assert(len(m) < 2)
  if len(m) == 1:
    status = m[0][1]
    url = m[0][2]
    q = m[0][3]
    a = m[0][4]
    print 'STATUS:', status
    print 'URL:', url
    print 'QUESTION:', q
    print 'ANSWER:', a
    assert(status in ['Approved', 'Rejected', 'Submitted'])
    return (status, url, q, fixUpAnswer(a))
  else:
    print line
    print m
    Error('BAD')
  Error('BAD2')
def isHardQuestion(q, a):
  a = a.lower()
  if a.find('longitude')>=0 and a.find('latitude')>=0:
    return True
  if a.find('lon')>=0 and a.find('lat')>=0 and re.findall(r'[0-9]', a):
    return True
  if re.findall(r'[0-9]', q):
    return True
  return False
def isNumberQuestion(q, a):
  if q.startswith('how '):
    return True
  # Filters too many things like "what year did bruce lee die?"
  #if not q.startswith('when ') and not re.match("^[^0-9]*$", a):
  #  return True
  return False
def getAmtAnswers3(trial, resultFileSuffix = ''):
  #inf = open('data/dataset_6/duplicate_qs.results')
  inf = open(getAmtResultsName(trial)+resultFileSuffix)
  lines = [l for l in inf][1:]
  inf.close()
  results = []
  newline = True
  prev = ""
  for line in lines:
    if re.findall(r'^"[A-Z0-9]{30}"', line):
      if prev:
        print 'PREV:'
        print prev
        results.append(parseResult(prev))
      prev = line
    else:
      prev += line
  results.append(parseResult(prev))
  statusIndex = 0
  urlIndex = 1
  qIndex = 2
  answerIndex = 3
  statusesIveChecked = ['Approved', 'Rejected', 'Submitted', 'NotReviewed', '']
  validStatuses = ['Approved', 'Submitted', 'NotReviewed', '']
  needToCheck = [r for r in results if r[statusIndex] not in statusesIveChecked]
  if len(needToCheck) > 0:
    print set([r[statusIndex] for r in needToCheck])
    Error('Review some statuses to decide what to do with them.')
  validResults = [r for r in results if r[statusIndex] in validStatuses]
  invalidResults = [r for r in results if r[statusIndex] not in validStatuses]
  #print validAnswers
  # TODO use the correct url
  good = [[r[qIndex], r[urlIndex], fixUpAnswer(r[answerIndex])] for r in validResults if not isNegativeAnswer(r[answerIndex])]
  bad = [[r[qIndex], r[urlIndex], fixUpAnswer(r[answerIndex])] for r in validResults if isNegativeAnswer(r[answerIndex])]
  print 'Num valid questions:', len(good), 'of', len(validResults)
  print 'GOOD'
  for q,url,a in good:
    print ' ### '.join([q,a])
    if a.find('"') >= 0:
      print 'BAD A:'
      print a
      sys.exit()
  print 'BAD'
  i=0
  for q,url,a in bad:
    if a.lower() != 'no':
      print ' ### '.join([q,a])
      i+=1
    if a.find('"') >= 0:
      print 'BAD A:'
      print a
      sys.exit()
  print i
  entsOnly = [[q,u,a] for q,u,a in good if re.match("^[^0-9]*$", a)]
  entsAndDates = [[q,u,a] for q,u,a in good if (not isNumberQuestion(q,a)) and (not isHardQuestion(q,a))]
  return good,bad,entsOnly,entsAndDates,invalidResults
def getAmtAnswers2(trial):
  inf = open(getAmtResultsName(trial))
  lines = [l for l in inf][1:]
  inf.close()
  results = []
  newline = True
  for line in lines:
    line = re.sub(r'([^,])""', "\\1'", line)
    line2 = line.split('","')
    #print len(line2)
    #print line2
    if newline:
      #print 'newline'
      if len(line2) != 30:
        #print line2
        #print len(line2)
        #print line2[16]
        pass
      if line2[16] != 'Rejected':
        assert(len(line2) == 30)
      if line2[-1].endswith('"\r\n'):
        line2[0] = line2[0].strip('"')
        line2[-1] = line2[-1][:-3].strip()
        if line2[16] != 'Rejected':
          results.append(line2)
        #sys.exit()
        newline = True
      else:
        line2[-1] = line2[-1].strip()
        prev = line2
        newline = False
      #print prev
    else:
      #print 'continued line'
      if len(line2) != 1:
        #print line2
        pass
      #assert(line2[0].endswith('\r\n'))
      if line2[0].endswith('"\r\n'):
        line2[0] = line2[0][:-3]
        finish = True
        newline = True
      else:
        line2[0] = line2[0].strip('"\r\n')
        finish = False
      #print line2
      if not prev[-1].endswith('.') and not prev[-1].endswith(','):
        prev[-1] += '.'
      prev[-1] += ' '
      prev[-1] += line2[0].strip()
      prev += line2[1:]
      if finish:
        if prev[16] != 'Rejected':
          results.append(prev)
        #sys.exit()
  idIndex = 15
  statusIndex = 16
  urlIndex = 27
  qIndex = 28
  answerIndex = 29
  #ids = [r[idIndex] for r in results if r[statusIndex]]
  statusesIveChecked = ['Approved', 'Rejected', 'Submitted', 'NotReviewed', '']
  validStatuses = ['Approved', 'Submitted', 'NotReviewed', '']
  needToCheck = [r for r in results if r[statusIndex] not in statusesIveChecked]
  if len(needToCheck) > 0:
    print set([r[statusIndex] for r in needToCheck])
    Error('Review some statuses to decide what to do with them.')
  validResults = [r for r in results if r[statusIndex] in validStatuses]
  urls = [r[-3] for r in validResults]
  allAnswers = [r[answerIndex] for r in validResults]
  feedback = [r[-1] for r in validResults]
  validAnswers = [a for a in allAnswers if a.lower() != 'no']
  #print validAnswers
  print 'Num valid questions:', len(validAnswers), 'of', len(allAnswers)
  # TODO use the correct url
  good = [[r[qIndex], r[urlIndex], r[answerIndex]] for r in validResults if not isNegativeAnswer(r[answerIndex])]
  bad = [[r[qIndex], r[urlIndex], None] for r in validResults if isNegativeAnswer(r[answerIndex])]
  bad = [[r[qIndex], r[urlIndex], r[answerIndex]] for r in validResults if isNegativeAnswer(r[answerIndex])]
  print 'GOOD'
  for q,url,a in good:
    print ' ### '.join([q,a])
  print 'BAD'
  i=0
  for q,url,a in bad:
    if a.lower() != 'no':
      print ' ### '.join([q,a])
      i+=1
    if a.find('"') >= 0:
      print 'BAD A:'
      print a
      sys.exit()
  print i
  nonum = [[q,u,a] for q,u,a in good if re.match("^[^0-9]*$", a)]
  return good,bad,nonum
########################################################################
if __name__ == "__main__":
  random.seed(0)
  if len(sys.argv) != 2:
    Error('Usage createSemanticParserInputFromAmt.py <bfs trial>')
  trial = int(sys.argv[1])
  ######################
  ######################
  good,bad,entsOnly,entsAndDates,invalidResults = getAmtAnswers3(trial)
  print len(good)
  print len(bad)
  print len(entsOnly)
  print len(entsAndDates)
  print len(invalidResults)
  #sys.exit()
  '''
  outf = open('data/dataset_6/duplicate_qs.txt', 'w')
  header = ','.join(['freebasesearch', 'question'])
  outf.write(header + '\n')
  for q,u,a in entsAndDates:
    line = ','.join([u, q])
    outf.write(line + '\n')
  outf.close()
  sys.exit() #TODO
  '''
  entsAndDates2 = list(entsAndDates)
  entsAndDates2.sort()
  random.shuffle(entsAndDates2)
  entsAndDates2 = entsAndDates2[:2000]

  writeAnswers(getAmtNameWithSuffix(trial, '_entsAndDates2000.examples'), entsAndDates2)
  writeAnswers(getAmtNameWithSuffix(trial, '_entsAndDates.examples'), entsAndDates)
  writeAnswers(getAmtNameWithSuffix(trial, '_good.examples'), good)
  writeAnswers(getAmtNameWithSuffix(trial, '_entsOnly.examples'), entsOnly)
  writeAnswers(getAmtNameWithSuffix(trial, '_bad.examples'), bad)
