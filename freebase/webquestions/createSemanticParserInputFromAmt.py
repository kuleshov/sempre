import os, sys, random
from andrewutil import normalizeName, getDirName, Error
from saveAmtAnswers import getAmtAnswers

#TODO make writeChild recursive rather than having this writeAnswerChild as a special case
def writeAnswerChild(outf, name, c):
  outf.write('  (' + name)
  if c == None:
    text = ' NO_ANSWER'
  else:
    if c.find('"') >= 0:
      print c
      Error('bad character')
    text = ' (description "' + c +'")'
  outf.write(text)
  outf.write(')\n')
def writeChild(outf, name, c):
  outf.write('  (' + name)
  if c.find('"') >= 0:
    print c
    Error('bad character')
  outf.write(' "'+c+'"')
  outf.write(')\n')
def writeExample(outf, q, a, ent, url):
  # header
  outf.write('(example\n')
  outf.write('  (utterance "'+str(q)+'")\n')
  # children
  writeAnswerChild(outf, 'targetValues', a)
  writeChild(outf, 'entity', ent)
  writeChild(outf, 'url', url)
  # footer
  outf.write(')\n')
def writeAnswers(outname, s):
  #write = not os.path.exists(outname) or os.stat(outname)[6]==0
  print outname
  outf = open(outname, 'w')
  for ent,q,url,a in s:
    print q, '###', a
    print url
    writeExample(outf, q, a, ent, url)
  outf.close()
########################################################################
if __name__ == "__main__":
  random.seed(0)
  if len(sys.argv) != 2:
    Error('Usage createSemanticParserInputFromAmt.py <name>')
  name = sys.argv[1]
  good,bad = getAmtAnswers(name)
  writeAnswers('data/rfs_10000_subset_good.examples', good)
  writeAnswers('data/rfs_10000_subset_bad.examples', bad)
