import os, sys
from andrewutil import t2q, Error, normalizeName, getOutnameAndMakeDirQOnly, \
    tlist2tstr
from common import *
from htmlUtils import decode
import random, urllib, urllib2
from getTopGoogleResultsForEnt import getUrlToMidMap, getTopHits, getMidToFbIdMap, getEntToFbIdMap
from randomwalkEntSearch import loadBfsTemplates, getGoodSeedNums
from amtCommon import getAmtInputDir, getAmtInputName
from htmlUtils import saveUrlForEnt

def getPreviouslyTurkedQs(amtTrialNum):
  qs = set()
  for trial in range(amtTrialNum):
    name = getAmtInputName(trial)
    if not os.path.exists(name):
      print name, 'doesnt exist!'
      if trial == 7:
        continue
      else:
        sys.exit()
        return None
    inf = open(name)
    inf.readline() #header
    for line in inf:
      data = [x.strip() for x in line.split(',')]
      #url,q = [x.strip() for x in line.split(',')]
      q = data[1]
      qs.add(q)
    inf.close()
  return qs
#######################################################
if __name__ == "__main__":
  random.seed(0)
  print sys.argv
  if len(sys.argv) != 5:
    Error('createAmtData2.py <seed or all> <nTemplates> <AMT bfs trial #> <n HITs>')
  seedarg = sys.argv[1].lower()
  if seedarg == 'all':
    seeds = getGoodSeedNums()
  else:
    seeds = [int(seedarg)]
  nTemplates = int(sys.argv[2])
  amtTrialNum = int(sys.argv[3])
  targetNumHits = int(sys.argv[4])
  amtInputFile = getAmtInputName(amtTrialNum)
  if os.path.exists(amtInputFile):
    print amtInputFile, 'exists.'
    sys.exit()

  #### Need to search for the ents
  # TODO need to update this function call
  cmd = 'python randomwalkEntSearch.py bfs '+str(nTemplates)+' all 1 0'
  print cmd
  ret = os.system(cmd)
  #ret = 0 #TODO
  if ret != 0:
    Error('Something wrong in randomwalkEntSearch.py')
  sys.exit()
  random.seed(0)
  ####
  os.system('mkdir -p '+getAmtInputDir(amtTrialNum))
  prevqs = getPreviouslyTurkedQs(amtTrialNum)
  outdir = REPRO_OUT + 'freebaseEntHtml/'
  os.system('mkdir -p '+outdir)
  #
  templates = loadBfsTemplates(seeds, nTemplates)
  templates = list(templates)
  templates.sort()
  random.shuffle(templates)
  sys.exit()#TODO
  readme = getAmtInputDir(amtTrialNum) + 'README'
  outf = open(readme, 'w')
  outf.write('seeds: ' + ', '.join([str(s) for s in seeds]) + '\n')
  outf.write('search n: ' + str(nTemplates) +'\n')
  outf.write('n HITs: ' + str(targetNumHits) +'\n')
  outf.write('Note that this excludes any questions sent to AMT from previous trials.\n')
  outf.write('See grammar file for the grammar used.')
  outf.close()
  os.system('cp /user/akchou/scr/semparse/data/induce-patterns.grammar '+getAmtInputDir(amtTrialNum))
  ##############
  urlsToMids = getUrlToMidMap()
  ents = set([t[1].strip() for t in templates])
  tophits = getTopHits(nTemplates, False, ents, urlsToMids, seedarg)
  mIdsToFbIds = getMidToFbIdMap()
  entToFbIdMap = getEntToFbIdMap(tophits, urlsToMids, mIdsToFbIds)
  ####
  outf = open(amtInputFile, 'w')
  header = ','.join(['freebasesearch', 'question'])
  outf.write(header + '\n')
  checkedEnts = set()
  badEnts = set()
  i = 0
  for template in templates:
    if i >= targetNumHits:
      break
    q = t2q(tlist2tstr(template))
    # TODO
    # remove things like "\\\\" and
    # map u0301 to '
    # map u0026 to and
    # Preferably do it before NER/fitlering by the grammar.
    if q in prevqs:
      continue
    assert(len(template) == 3)
    ent = template[1].strip()
    if ent not in entToFbIdMap:
      print 'No Freebase page for:', repr(ent)
      continue
    if ent in badEnts:
      print 'Known bad ent:', repr(ent)
      continue
    fbid = entToFbIdMap[ent]
    url = 'http://www.freebase.com/view/en/' + fbid
    if ent not in checkedEnts:
      try:
        saveUrlForEnt(ent, url, outdir)
        checkedEnts.add(ent)
      except:
        print 'Error saving url for:', repr(ent), 'with fbid:', repr(fbid)
        badEnts.add(ent)
        continue
    print 'Saving', i, 'of', targetNumHits
    assert(url.find(',') == -1)
    assert(q.find(',') == -1)
    line = ','.join([url, q])
    outf.write(line + '\n')
    i += 1
  outf.close()
