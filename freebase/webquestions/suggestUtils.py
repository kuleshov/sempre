import urllib2, string
# template: (prefix, ent, suffix)
def getQuestions(template, index):
  assert(len(template) == 3)
  template = [x.strip() for x in template]
  # the index of the template to be replaced
  assert(index >= 0 and index <= 2)
  template[index] = ''
  if index == 0:
    cp = 0
    q = ' ' + template[1]
    if len(template[2]) == 0 or template[2][0] != "'":
      q += ' '
    q += template[2]
  elif index == 1:
    q = template[0] + ' '
    cp = len(q)
    if len(template[2]) == 0 or template[2][0] != "'":
      q += ' '
    q += template[2]
  elif index == 2:
    q = template[0] + ' ' + template[1] + ' '
    # continue position
    cp = len(q)
  print q+'XXX'
  #print '012345678901234567890'
  #print cp
  #print template
  res = makeQuery(q, cp)
  if res==False:
    return False
  if res==True:
    return [] # TODO
  res = [r.strip('"') for r in res]
  res = [filter(lambda x: x in string.printable, r) for r in res]
  print res
  s = [r for r in res if r]
  return s

# q: the query
# index: the spot to suggest from
# returns: a list of the results of the query
def makeQuery(q, index):
  print q, ':', index
  q = q.replace(' ', '%20')
  #q = urllib.urlencode({'q': q})
  try:
    query = 'http://suggestqueries.google.com/complete/search?client=chrome'
    query += '&cp='+str(index)
    query += '&q='+q
    print query
    resp = urllib2.urlopen(query)
  except urllib2.HTTPError, e:
    print e
    if e.code == 400:
      return True
    return False
  html = resp.read()
  html = html.split('[')
  html = html[2]
  html = html.strip('],')
  html = html.split(',')
  return html
