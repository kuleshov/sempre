import urllib, urllib2 # contacting websites
import string, time, calendar, sys, os
import gzip, StringIO, random, socket, types
import filterOutput
from math import ceil, log
from andrewutil import normalizeName, getDirName, Error, getOutnameAndMakeDir,\
    t2q, getOutnameAndMakeDirQOnly, getDirNameQOnly, tlist2tstr
from common import *
from ioUtils import readLinesFromFile
from htmlUtils import gzipFile, getLynxDump
from randomwalkRw import getRandomWalkDir

# returns True or some string output if Google was hit
# returns False or None if Google wasnt used.
def doW3mGoogleSearch(q, htmldir, skipLynx, searchWholeQ):
  assert(htmldir.endswith('/'))
  outname = getOutnameAndMakeDirQOnly(htmldir, q, not searchWholeQ)
  outname = outname + '.html'
  print outname
  outnamegz = outname+'.gz'
  existshtml = os.path.exists(outname)
  existsgz = os.path.exists(outnamegz)
  usedGoogle = False
  if ((not existshtml) and (not existsgz)) or \
      (existshtml and os.stat(outname)[6]==0):
    print existshtml, existsgz, (existshtml and os.stat(outname)[6]==0)
    print outname
    print outnamegz
    print q
    q = urllib.urlencode({'q': q})
    url = 'http://www.google.com/search?'+q+'&hl=en&biw=&bih=&gbv=2&gs_l=heirloom-hp.3..0l10.1536.2085.0.2179.6.6.0.0.0.0.56.290.6.6.0...0.0...1c.1.wRBDM49rJVE&o'+q

    url = 'http://www.google.com/search?'+q

    print url
    print outnamegz
    #os.system('lynx -dump -source '+url+' > '+outname)
    #cmd = 'w3m -dump_source -o accept_encoding=gzip '+url+' > '+outnamegz
    #cmd = 'lynx -dump -source '+url+' > '+outname #TODO
    hostname = socket.gethostname()
    if hostname.startswith('corn') or hostname.startswith('myth'):
      cmd = 'lynx -dump -source '+url+' > '+outname
    else:
      cmd = 'w3m -dump_source '+url+' > '+outname
    print cmd
    #Error('TODO')
    os.system(cmd)
    cmdstr = 'grep CAPTCHA '+outname
    cmd = os.popen(cmdstr)
    grepout = cmd.read()
    cmd.close()
    if grepout:
      print 'CAPTCHA FOUND 1'
      print grepout
      print 'CAPTCHA FOUND 2'
      sys.exit()
    usedGoogle = True
    #retval = os.system('gunzip -f '+outnamegz)
    #if retval != 0:
    #  return usedGoogle
    if os.stat(outname)[6]==0:
      print 'Quota hit!'
      sys.exit()
      time.sleep(60)
  if os.path.exists(outnamegz):
    cmd = 'gunzip -f '+outnamegz.replace("'", "\\'")
    print cmd
    retval = os.system(cmd)
    if retval != 0:
      return usedGoogle
  if skipLynx:
    return usedGoogle
  if os.path.exists(outname):
    output = getLynxDump(outname)
    gzipFile(outname)
    return output
  return usedGoogle
def loadRwTemplates(seeds, rseed, n):
  templates = set()
  for seed in seeds:
    templateFile = getRandomWalkDir(rseed) + '_'.join(['oneEntTemplates', 'n', str(n), 'seed', str(seed)])
    _templates = readLinesFromFile(templateFile)
    templates.update(_templates)
  templates = [[x.strip() for x in t.split('_')] for t in templates]
  return templates
def loadBfsTemplates(seeds, n):
  templates = set()
  if n == 0: #infinity
    n = 100000
    success = True
    while success: # while any are found
      success = False
      for seed in seeds:
        templateFile = RANDOM_WALK_BFS_DIR + '_'.join(['oneEntTemplates', 'n', str(n), 'seed', str(seed)])
        print 'Loading', templateFile
        try:
          _templates = readLinesFromFile(templateFile)
          templates.update(_templates)
          success = True
        except Exception:
          print templateFile, 'not found.'
      n += 10000
  else:
    for seed in seeds:
      templateFile = RANDOM_WALK_BFS_DIR + '_'.join(['oneEntTemplates', 'n', str(n), 'seed', str(seed)])
      _templates = readLinesFromFile(templateFile)
      templates.update(_templates)
  templates = [[x.strip() for x in t.split('_')] for t in templates]
  return templates
def getGoodSeedNums():
  seedsThatEndedEarly = [5, 12, 15, 18]
  seeds = range(23)
  return [s for s in seeds if s not in seedsThatEndedEarly]

#######################################################
if __name__ == "__main__":
  random.seed(0)
  if len(sys.argv) not in [9]:
    hostname = socket.gethostname()
    if hostname.startswith('corn') and len(sys.argv) in [7,8]:
      cornNum = int(hostname.split('.')[0][4:])
    else:
      Error('Usage: randomwalkEntSearch.py <rw|bfs> <n> <seed> <skip lynx> <search whole q> <rseed> <part> <of>')
  useBfs = (sys.argv[1].lower() == 'bfs')
  n = int(sys.argv[2])
  if sys.argv[3].lower() == 'all':
    seeds = getGoodSeedNums()
  else:
    seeds = [int(sys.argv[3])]
  skipLynx = bool(int(sys.argv[4]))
  searchWholeQ = bool(int(sys.argv[5]))
  rseed = int(sys.argv[6])
  arglist = sys.argv
  if len(sys.argv) > 7:
    index = int(sys.argv[7])
  else:
    indices =    [0, 1, 1, 3, 1, 3, 5, 7, 1, 3, 5, 7, 9, 11, 13, 15, 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31]
    index = indices[cornNum-1]
    arglist.append(index)
  if len(sys.argv) > 8:
    nParts = int(sys.argv[8])
  else:
    nPartsList = [1, 2, 4, 4, 8, 8, 8, 8,16,16,16,16,16, 16, 16, 16,32,32,32,32,32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32]
    nParts = nPartsList[cornNum-1]
    arglist.append(nParts)
  assert(nParts > 0)
  if useBfs:
    print 'Using BFS.'
    templates = loadBfsTemplates(seeds, n)
  else:
    print 'Using RW.'
    templates = loadRwTemplates(seeds, rseed, n)
  outbase = REPRO_OUT + 'google_results/'
  for t in templates:
    assert(len(t) == 3)
  if searchWholeQ:
    qsAndEnts = set([(tlist2tstr(t),t[1]) for t in templates])
  else:
    #Error('Must search Whole Q right now')
    qsAndEnts = set([(None,t[1]) for t in templates])
  print len(qsAndEnts)
  os.system('mkdir -p '+outbase)
  outbasehtml = outbase
  while outbasehtml.endswith('/'):
    outbasehtml = outbasehtml[:-1]
  outbasehtml += 'html/'
  os.system('mkdir -p '+outbasehtml)
  print outbase
  print outbasehtml
  qsAndEnts = list(qsAndEnts)
  nInEachPart = ceil(float(len(qsAndEnts)) / nParts)
  start = int(index * nInEachPart)
  end = int(min((index+1) * nInEachPart, len(qsAndEnts)))
  print start, end, len(qsAndEnts)
  qsAndEnts = qsAndEnts[start:end]


  i=0
  for q,ent in qsAndEnts:
    assert(ent != '')
    print i, 'of', len(qsAndEnts), ':', ent, ':', q
    i+=1
    if searchWholeQ:
      queries = [q+' site:en.wikipedia.org']
    else:
      queries = [ent+' site:en.wikipedia.org']
    print searchWholeQ, sys.argv[5]
    print arglist
    print queries
    for query in queries:
      outname = getOutnameAndMakeDirQOnly(outbase, query, not searchWholeQ)
      if os.path.exists(outname) and os.stat(outname)[6] > 0:
        continue
      #continue
      retry= True
      expbackoff = 300 # default to 5 min sleep
      while retry:
        retry=False
        try:
          searchoutput = doW3mGoogleSearch(query, outbasehtml, skipLynx, searchWholeQ)
          #sys.exit()
        except Exception, inst:
          print inst
          print 'Outer retry'
          retry= True
          sys.exit()
        if type(searchoutput) == types.BooleanType:
          output = None
          break
        output = filterOutput.filterOutputLynx(searchoutput)
        print output
        if (not output) or (len(output)==1 and output[0]==''):
          retry= True
          print 'Quota hit after', i, '... sleeping for', expbackoff, 'seconds.'
          sys.exit()
      rtime = 30.176+5*random.random()
      #rtime = 20.176+5*random.random()
      #rtime = 10.176+5*random.random()
      #rtime = 5.176+5*random.random()
      #rtime = 3.176+5*random.random()
      #rtime = 2.176+2*random.random()
      #rtime = 1.176+2*random.random()
      #rtime = 1.176+1.5*random.random()
      #rtime = 1.076+1.01*random.random()
      if output==None:
        if searchoutput:
          print 'resting for', rtime, 'so google doesnt get suspicious.'
          time.sleep(rtime)
        continue
      print outname
      outf = open(outname, 'w')
      outf.write('\n\n'.join(output))
      outf.close()
      # wait at least a minute
      print 'resting for', rtime, 'so google doesnt get suspicious'
      time.sleep(rtime)
  print 'DONE'
  print i
  sys.exit()
