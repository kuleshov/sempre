import os, sys
import cPickle as pickle
from ioUtils import readLinesFromFile
from andrewutil import t2q, Error, normalizeName, getOutnameAndMakeDirQOnly, \
    tlist2tstr
from common import *
from htmlUtils import decode
import random, urllib, urllib2, time
from getTopGoogleResultsForEnt import getUrlToMidMap, getTopHits, getMidToFbIdMap, getEntToFbIdMap
from randomwalkEntSearch import loadRwTemplates, getGoodSeedNums
from createAmtData2 import saveUrlForEnt, getAmtInputDir, getAmtInputName, getPreviouslyTurkedQs
from createAmtData3NewFb import getFbAnswersNew
from createAmtData4NewFb import loadCheckedEnts

#######################################################
if __name__ == "__main__":
  random.seed(0)
  print sys.argv
  #print 'Sleeping for 6 hours'
  #time.sleep(6*3600)
  if len(sys.argv) not in [4,5,6]:
    Error('scrapeFreebasePages.py <seed or all> <nTemplates> <rseed> <start-optional> <end-optional>')
  seedarg = sys.argv[1].lower()
  if seedarg == 'all':
    seeds = getGoodSeedNums()
  else:
    seeds = [int(seedarg)]
  nTemplates = int(sys.argv[2])
  rseed = int(sys.argv[3])
  start = 0
  if len(sys.argv) >= 5:
    start = int(sys.argv[4])
  end = 100000000 # big number
  if len(sys.argv) >= 6:
    end = int(sys.argv[5])

  #### Need to search for the ents
  cmd = 'python randomwalkEntSearch.py rw '+str(nTemplates)+' '+seedarg+' 1 0 0 1'
  print cmd
  ret = 0 #TODO
  #ret = os.system(cmd)
  if ret != 0:
    Error('Something wrong in randomwalkEntSearch.py')
  random.seed(0)
  ####
  outdir = REPRO_OUT + 'freebaseEntHtml/'
  os.system('mkdir -p '+outdir)
  #
  templates = loadRwTemplates(seeds, rseed, nTemplates)
  templates = list(templates)
  templates.sort()
  random.shuffle(templates)
  ##############
  urlsToMids = getUrlToMidMap()
  ents = set([t[1].strip() for t in templates])
  tophits = getTopHits(nTemplates, False, ents, urlsToMids, seedarg, 'Rw')
  assert(len(tophits) > 0)
  mIdsToFbIds = getMidToFbIdMap()
  entToFbIdMap = getEntToFbIdMap(tophits, urlsToMids, mIdsToFbIds)
  #for fbid in
  ####
  badEnts = set()
  entsAndFbids = [(ent,fbid) for ent,fbid in entToFbIdMap.iteritems()]
  end = min(end, len(entsAndFbids))
  entsAndFbids = entsAndFbids[start:end]
  # map from ent -> (answers, mIds)
  cachename = REPRO_OUT + 'fbAnswers/' + '_'.join(['checkedEnts', str(start), str(end)]) + '.p'
  print cachename
  if os.path.exists(cachename):
    Error('Cache already exists. Exiting.')
  checkedEnts = loadCheckedEnts()
  noAnswerEnts = set()
  i = 0
  for ent,fbid in entsAndFbids:
    if ent in checkedEnts:
      i += 1
      continue
    url = 'http://www.freebase.com/view/en/' + fbid
    try:
      saveUrlForEnt(ent, url, outdir)
    except Exception, e:
      print 'Error saving url for:', repr(ent), 'with fbid:', repr(fbid)
      print e
      badEnts.add(ent)
      print 'Now have', len(badEnts), 'badEnts.'
      i += 1
      continue
    fbHtmlName = getOutnameAndMakeDirQOnly(outdir, ent, True) + '.html'
    print fbHtmlName
    answers,mIds,headings,props = getFbAnswersNew(fbHtmlName)
    answers = answers.encode('ascii', 'xmlcharrefreplace')
    mIds = mIds.encode('ascii', 'xmlcharrefreplace')
    headings = headings.encode('ascii', 'xmlcharrefreplace')
    props = props.encode('ascii', 'xmlcharrefreplace')
    checkedEnts[ent] = (answers,mIds,headings,props)
    if len(answers) == 0:
      print 'ERROR FOR ENT:', repr(ent)
      print 'PLEASE CHECK URL:', repr(url)
      noAnswerEnts.add(ent)
      i += 1
      continue
    print 'Done with', i, 'of', len(entsAndFbids), '(', start, ',', end, ',', len(entToFbIdMap), ')'
    i += 1
    if i%10 == 0:
      time.sleep(0.01)
    assert(url.find(',') == -1)
  print cachename
  pickle.dump(checkedEnts, open(cachename, 'wb'))
