import os, sys, random, re
from math import sqrt
from andrewutil import Error
from createAmtData2 import getAmtNameWithSuffix, getAmtInputName
from createSemanticParserInputAmtCsv import writeAnswers, \
    fixUpAnswer, isNumberQuestion, isHardQuestion
from createSemanticParserInputAmtCsv2 import isNegativeAnswer2
from createAmtData2 import getAmtResultsName

def parseResult3(line, verbose = False):
  assert(line)
  if verbose:
    print line
  #field = r'(?:,"(?:[a-zA-Z\s.,\$0-9:;%()/_?+\'\n\r!-\\]|[^,]"")*")'
  #default = re.compile(start+field+'{25}'+answers, flags)
  field = r'(?:,"(?:[a-zA-Z\s.,\$0-9:;%()/_?+\'\n\r!-]|[^,]""|""[^,]|[^"])*")'
  flags = re.MULTILINE|re.DOTALL
  id = r'"[A-Z0-9]{30}"'
  start = r'(^'+id+','+id
  #answers = r',")(.*)","(.*)","(.*)","(.*)","(.*)","(.*)","(.*)"' for answers,mids,headers,props
  answers = r',")(.*)","(.*)","(.*)","(.*)"'
  #regex = start+field+'{14},"(Approved|Rejected|Submitted)"'+field+'{10}'+answers
  regex = start+field+'{13}'+r',"(.*)"'+',"(Approved|Rejected|Submitted)"'+field+'{10}'+answers
  #print regex
  #sys.exit()
  default = re.compile(regex, flags)
  m = default.findall(line)
  assert(len(m) < 2)
  if len(m) == 1:
    #return m[0] #TODO
    m = m[0]
    tid = m[1] #turker id
    status = m[2]
    url = m[3]
    q = m[4]
    answers = m[5]
    a = m[6]
    #mids = m[6]
    #headers = m[7]
    #properties = m[8]
    #print 'STATUS:', status
    #print 'URL:', url
    #print 'QUESTION:', q
    #print 'ANSWERS:', answers
    #print 'ANSWER:', a
    assert(status in ['Approved', 'Rejected', 'Submitted'])
    #return [status, tid, url, q, answers, mids, headers, properties, fixUpAnswer(a)]
    return [status, tid, url, q, answers, fixUpAnswer(a)]
  else:
    print line
    print m
    Error('BAD')
  Error('BAD2')
def getAllLines(pathname):
  lines = set()
  prefix = pathname.split('/')[-1]
  dirname = '/'.join(pathname.split('/')[:-1])+'/'
  for f in os.listdir(dirname):
    if not f.startswith(prefix):
      continue
    f = dirname+f
    print 'Reading from:', f
    inf = open(f)
    lines_ = [l for l in inf][1:]
    inf.close()
    lines.update(lines_)
  return lines

def getAmtAnswers5(trial, resultFileSuffix = ''):
  #inf = open('data/dataset_6/duplicate_qs.results')
  lines = getAllLines(getAmtResultsName(trial)+resultFileSuffix)
  results = []
  newline = True
  prev = ""
  print len(lines)
  i = 0
  for line in lines:
    if re.findall(r'^"[A-Z0-9]{30}"', line):
      if prev:
        #print 'PREV:'
        #print prev
        print i
        results.append(parseResult3(prev, i==3879))
        i += 1
      prev = line
    else:
      prev += line
  results.append(parseResult3(prev))
  #return results #TODO
  statusIndex = 0
  urlIndex = 2
  qIndex = 3
  answerIndex = 5
  statusesIveChecked = ['Approved', 'Rejected', 'Submitted', 'NotReviewed', '']
  validStatuses = ['Approved', 'Submitted', 'NotReviewed', '']
  validStatuses = ['Approved', 'Submitted', 'NotReviewed', '', 'Rejected'] #TODO
  needToCheck = [r for r in results if r[statusIndex] not in statusesIveChecked]
  if len(needToCheck) > 0:
    print set([r[statusIndex] for r in needToCheck])
    Error('Review some statuses to decide what to do with them.')
  validResults = [r for r in results if r[statusIndex] in validStatuses]
  #print validAnswers
  negAnswers = ['MISSING_ANSWER', 'NOT_ON_PAGE', 'AMBIGUOUS', 'IRRELEVANT']
  normalized = []
  #for status,tid,url,q,answers,mids,headers,properties,a in validResults:
  for r in validResults:
    a = r[answerIndex]
    if a in negAnswers:
      index = -1
      a = 'NO_ANSWER'
    else:
      # get rid of the
      m = re.match(r'([0-9])+: (.*)', a)
      index = m.group(1)
      a = m.group(2)
      # TODO also do something with the paths
      a = a.decode('utf-8')
      a = re.sub(r';', '', a)
      a = re.sub(r"['|\"]+", '"', a)
      if isinstance(a, str):
        a = unicode(a)
    #Error('TODO') # check this
    r[answerIndex] = (index,a)
    #normalized.append([q,url,a,tid])
    normalized.append(tuple(r))
  #return normalized #TODO
  # count how many times each question was answered
  counts = {}
  #for q,u,a,tid in normalized:
  for r in normalized:
    q = r[qIndex]
    a = r[answerIndex]
    if q not in counts:
      counts[q] = [a]
    else:
      counts[q].append(a)
      if len(counts[q]) > 2:
        #print q
        print counts[q]
        assert(len(counts[q]) <= 2)
  oneAnswer = []
  oneAnswerGood = []
  mismatchList = []
  matchList = []
  for r in normalized:
    q = r[qIndex]
    a = r[answerIndex]
    if len(counts[q]) == 1:
      oneAnswer.append(r)
    if len(counts[q]) == 1 and a[1] != 'NO_ANSWER':
      oneAnswerGood.append(r)
    if len(counts[q]) == 2 and counts[q][0] != counts[q][1]:
      mismatchList.append(r)
    if len(counts[q]) == 2 and counts[q][0] == counts[q][1]:
      matchList.append(r)
  match = {}
  for s,t,u,q,answers,a in matchList:
    if q in match:
      match[q][2].append(t)
    else:
      match[q] = (u,a,[t])
  mismatch = {}
  for s,t,u,q,answers,a in mismatchList:
    if q not in mismatch:
      mismatch[q] = [(u,a,t)]
    else:
      mismatch[q].append((u,a,t))
  #Error('TODO')
  good = {}
  bad = {}
  for q,v in match.iteritems():
    if v[1][1] == 'NO_ANSWER':
      bad[q] = v
    else:
      good[q] = v
  return good,bad,mismatch,oneAnswer,oneAnswerGood,normalized
def getTurkerStats(good,bad,mismatch,oneAnswer,oneAnswerGood,normalized):
  # map from turkerid -> #goodmatch, #goodmismatch, #badmatch, #badmismatch
  # where "good" means they chose a real answer (not necessarily correct)
  # and "bad" means they chose something like "not on page"
  goodMatchIndex = 0
  goodMisMatchIndex = 1
  badMatchIndex = 2
  badMisMatchIndex = 3
  goodIndex = 4
  badIndex = 5
  nFields = 6
  stats = {}
  #goodmatch
  for q,v in good.iteritems():
    tids = v[2]
    for tid in tids:
      if tid not in stats:
        stats[tid] = nFields*[0]
      stats[tid][goodMatchIndex] += 1
  #badmatch
  for q,v in bad.iteritems():
    tids = v[2]
    for tid in tids:
      if tid not in stats:
        stats[tid] = nFields*[0]
      stats[tid][badMatchIndex] += 1
  #goodmismatch and badmismatch
  for q,v in mismatch.iteritems():
    for u,a,tid in v:
      if tid not in stats:
        stats[tid] = nFields*[0]
      if a == 'NO_ANSWER':
        stats[tid][badMisMatchIndex] += 1
      else:
        stats[tid][goodMisMatchIndex] += 1
  # total good and bad
  # Note: this is not necessarily equal to the sum of the previous parts,
  #       because maybe not all question pairs have finished turking.
  for s,tid,u,q,answers,a in normalized:
    if tid not in stats:
      stats[tid] = nFields*[0]
    if a[1] == 'NO_ANSWER':
      assert(a[0] == -1)
      stats[tid][badIndex] += 1
    else:
      assert(a[0] >= 0)
      stats[tid][goodIndex] += 1
  return stats
def findSuspiciousTurkers(stats, nstddevs = 2.0):
  goodMatchIndex = 0
  goodMisMatchIndex = 1
  badMatchIndex = 2
  badMisMatchIndex = 3
  goodIndex = 4
  badIndex = 5
  nFields = 6
  # compute totals
  incompetent_old_lady = ['AC5LU9TF9CTK9']
  badtids = ['A2BIWCFCBRN1TK', 'ADER510RQCBFN', 'ASC8BJ2QFM3FJ','A3PRF4IIM2IQN2', \
          'A1Z5H7VBJHAT79','A23A3VLVRE3GDU', 'A3MHDNEZKB5XUY']
  # A14A8J5H98KLO6 is really good
  goodtids = ['A3IHLWMZNBLUR4', 'A1GGOZPHYU0OC0', 'A14A8J5H98KLO6', 'ANMGOZ451W9ZN', 'APH3VBFEHD020', 'A37VB2HA91E811', 'A3F4XU9JPE277V', 'A39LGUN7NFAZF5']
  othertids = ['AC5LU9TF9CTK9'] # bad but paid and asked not to work anymore
  totals = nFields*[0]
  for tid,v in stats.iteritems():
    if tid in badtids:
      continue
    for i in range(nFields):
      totals[i] += v[i]
  print totals

  for tid,v in stats.iteritems():
    if tid in badtids:
      continue
    if tid in goodtids:
      continue
    if tid in othertids:
      continue
    diff = nFields*[0]
    for i in range(nFields):
      #subtracting current turker and smoothing
      diff[i] = float(totals[i] - v[i] + 1)
    g = diff[goodIndex]
    b = diff[badIndex]
    n = g + b
    p = g / (g+b)
    var = n*p*(1-p)
    std = sqrt(var)
    ncur = v[goodIndex] + v[badIndex]
    if ncur*p + nstddevs*std < v[goodIndex] or ncur*p - nstddevs*std > v[goodIndex]:
      print 'Bad turker!'
      print tid
      print 'Num Std Dev away:', (v[goodIndex] - ncur*p) / std
      print v
      print ncur*p - nstddevs*std, ncur*p, ncur*p + nstddevs*std, v[goodIndex]
def hasGoodAnswer(line, goodQs):
  line = line.split(',')
  q = line[1]
  return q in goodQs
def saveGoodAnswersForReTurking(trial, goodQs):
  print 'Saving good answers for returking!'
  #inf = open('data/dataset_6/duplicate_qs.results')
  inf = open(getAmtInputName(trial))
  lines = [l for l in inf]
  inf.close()
  outf = open(getAmtInputName(trial)+'.good_for_returking', 'w')
  outf.write(lines[0]) #header
  for line in lines[1:]:
    if hasGoodAnswer(line, goodQs):
      outf.write(line)
  outf.close()

########################################################################
if __name__ == "__main__":
  random.seed(0)
  if len(sys.argv) != 2:
    Error('Usage createSemanticParserInputFromAmt2.py <bfs trial>')
  trial = int(sys.argv[1])
  ######################
  ######################
  #tmp = getAmtAnswers5(trial, '.1000qs.allFields')
  good,bad,mismatch,oneAnswer,oneAnswerGood,normalized = getAmtAnswers5(trial, '.qs')
  print 'MATCHING:'
  print '  GOOD:', len(good)
  print '  BAD:', len(bad)
  print 'MISMATCH:', len(mismatch)
  print 'ONE ANSWER:', len(oneAnswer)
  print 'ALL:', len(normalized)
  tstats = getTurkerStats(good,bad,mismatch,oneAnswer,oneAnswerGood,normalized)
  findSuspiciousTurkers(tstats, 0.3)
  good2 = [(x[2],x[3],x[5]) for x in oneAnswerGood]
  goodQs = set([x[3] for x in normalized if x[-1][1] != 'NO_ANSWER'])
  saveGoodAnswersForReTurking(trial, goodQs)
  sys.exit()

  tmp = getAmtAnswers5(trial, '.small')
  print len(tmp)
  t = tmp[0]
  #good,bad,mismatch,oneAnswer,normalized = getAmtAnswers5(trial, '.1000qs.allFields')
  assert(2*(len(good)+len(bad)+len(mismatch)) == len(normalized))

  goodList = []
  for q,v in good.iteritems():
    url = v[0]
    a = v[1]
    goodList.append((q,url,a))
  sys.exit()

  #writeAnswers(getAmtNameWithSuffix(trial, '_entsAndDates.examples'), entsAndDates)
  writeAnswers(getAmtNameWithSuffix(trial, '_good_checked2x.examples'), goodList)
  #writeAnswers(getAmtNameWithSuffix(trial, '_entsOnly.examples'), entsOnly)
  #writeAnswers(getAmtNameWithSuffix(trial, '_bad.examples'), bad)
