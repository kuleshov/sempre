import os, sys
import socket
from andrewutil import normalizeName

for base in os.listdir('nerresults/'):
  base = 'nerresults/'+base+'/'
  for dname in os.listdir(base):
    if os.path.isfile(dname) or dname.endswith('.tar'):
      print 'skipping'
      continue
    for f in os.listdir(base+dname):
      forig = base+dname+'/'+f.replace("'","\\'").replace("?","\\?")
      f = base+dname+'/'+normalizeName(f)
      if f==forig:
        continue
      print forig, f
      #fosin = f.replace("'", "\\'")
      cmd = 'mv '+forig+' '+f
      #cmd = 'tar -zcvf '+forig+'.tar '+f
      print cmd
      ret = os.system(cmd)
      print ret
      if ret!=0:
        sys.exit()
