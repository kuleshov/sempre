import sys, os, socket, time, types, copy
from subprocess import Popen, PIPE, STDOUT
from andrewutil import Error, getRandomWalkSuggestOutName
from common import *
import random, string
from ioUtils import readLinesFromFile, writeQsToFile
from suggestUtils import getQuestions
from semanticParser import loadGibbsSampler, closeGibbsSampler, \
    runSemanticParser
from randomwalkDfs import writeTemplatesWrapper, writeTemplates
from seedTemplates import getSeedTemplates

def getRandomWalkDir(rseed):
  return RANDOM_WALK_RW_DIR.strip('/') + str(rseed) + '/'
def randomwalkRwMain(args):
  print args
  if len(args) != 4:
    Error('randomwalkRw.py <n> <seed index> <random seed>')
  n = int(args[1])
  seed = int(args[2])
  rseed = int(args[3])
  random.seed(rseed)
  outdir = getRandomWalkDir(rseed)
  os.system('mkdir -p ' + outdir)
  ################################
  popen = loadGibbsSampler()
  ################################
  allTemplates = set()
  multiEntTemplates = set()
  seedTemplates = getSeedTemplates()
  template = seedTemplates[seed]
  allTemplates.add(template)
  usedTemplatesAndIndex = {}
  sequenceOfTemplates = [template]
  #numberOfIterBeforeReset = 100
  numSuggests = 0
  nextNToWrite = 10000
  mustBeNewIndex = False
  numSinceNew = 0
  i = 0
  while len(allTemplates) < n:
    print 'ITER:', i
    print len(allTemplates), 'OF', n, 'TEMPLATES'
    index = random.randint(0, len(template) - 1)
    if mustBeNewIndex:
      assert(template not in usedTemplatesAndIndex or len(usedTemplatesAndIndex[template]) < 3)
      while template in usedTemplatesAndIndex and index in usedTemplatesAndIndex[template]:
        index = random.randint(0, len(template) - 1)
      assert(template not in usedTemplatesAndIndex or index not in usedTemplatesAndIndex[template])
    if template not in usedTemplatesAndIndex:
      usedTemplatesAndIndex[template] = set()
    usedTemplatesAndIndex[template].add(index)
    outname = getRandomWalkSuggestOutName(REPRO_SEARCH_QUEUE, template, index)
    print outname
    if os.path.exists(outname):
      newQs = readLinesFromFile(outname)
    else:
      newQs = None
      while newQs == None:
        newQs = getQuestions(template, index)
        #break
        if newQs in [None, True, False]:
          print 'Sleeping during', template
          time.sleep(1200) # 20 mins
      newQs.sort()
      writeQsToFile(outname, newQs)
      numSuggests += 1
    newQs.sort()
    #print newQs
    newTemplates = []
    for q in newQs:
      oneEntTemplates,newMultiEntTemplates = runSemanticParser(popen, q)
      newTemplates += oneEntTemplates
      multiEntTemplates.update(newMultiEntTemplates)
    oldsize = len(allTemplates)
    allTemplates.update(newTemplates)
    newsize = len(allTemplates)
    if newsize > oldsize:
      numSinceNew = 0
    else:
      numSinceNew += 1
    i += 1
    #newTemplates = [t for t in newTemplates if t not in usedTemplatesAndIndex]
    newTemplates = [t for t in newTemplates if t != template]
    if len(newTemplates) == 0 or numSinceNew > 100:
      #if len(allTemplates) == 1:
      #  Error('No suggestions for seed: ' + ' '.join(list(template)))
      choices = [x for x in allTemplates if x not in usedTemplatesAndIndex or len(usedTemplatesAndIndex[x]) < 3]
      template = random.choice(choices)
      mustBeNewIndex = True
      #template = seedTemplates[seed]
    else:
      template = random.choice(newTemplates)
      mustBeNewIndex = False
    print 'New template:', template
    sequenceOfTemplates.append(template)
    if len(allTemplates) >= nextNToWrite:
      writeTemplates(nextNToWrite, seed, outdir, allTemplates, \
          multiEntTemplates, sequenceOfTemplates)
      nextNToWrite += 10000
  closeGibbsSampler(popen)
  print 'allTemplates:', len(allTemplates)
  ents = set([x[1] for x in allTemplates])
  print 'ents:', len(ents)
  prefixSuffix = set([(x[0], x[2]) for x in allTemplates])
  print 'prefixSuffix:', len(prefixSuffix)
  print 'DONE with randomwalkRw.py!'
  print 'numSuggests:', numSuggests
  sys.exit()

###############################
if __name__ == "__main__":
  randomwalkRwMain(sys.argv)
