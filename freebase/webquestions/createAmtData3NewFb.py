import os, sys, time, types
from andrewutil import t2q, Error, normalizeName, getOutnameAndMakeDirQOnly, \
    tlist2tstr, normalizeUnicode
from common import *
import random, urllib, urllib2, re
from getTopGoogleResultsForEnt import getUrlToMidMap, getTopHits, getMidToFbIdMap, getEntToFbIdMap
from randomwalkEntSearch import loadBfsTemplates, getGoodSeedNums
from createAmtData2 import getAmtInputName, saveUrlForEnt
sys.path.append('BeautifulSoup-3.2.1/')
from BeautifulSoup import BeautifulSoup

def extractTextFromListOfDatalist(elist, tag):
  ulForTag = [x for x in elist if len(x.findAll(tag, attrs={'class':'property-value'})) > 0]
  good = []
  for e in ulForTag:
    vals = e.findAll(tag, attrs={'class':'property-value'})
    vals2 = []
    for v in vals:
      try:
        mid = v['data-id']
      except KeyError:
        continue
      answer = ''.join([re.sub(r'\s+', ' ', x.lstrip()) for x in v.findAll(text=True)]).replace(' )', ')')
      vals2.append((answer,mid))
    if vals2:
      good.append(vals2)
  return good
def assertNotMissing(v):
  if v.find(';;;;;;') >= 0:
    print repr(v)
    Error('Empty field!')
  if v.find('###') >= 0:
    print repr(v)
    Error('Cant have ### in answers!')
# allVals is a list of lists.
# Each sublist contains (answer, mId) tuples.
def getAllValues(allVals, headers):
  DEFAULT_HEADER = 'HEAD'
  good = []
  for i in range(len(allVals)):
    vals = allVals[i]
    assert(type(vals) == types.ListType)
    if vals:
      if i < len(headers):
        good += [(x,y,headers[i]) for x,y in vals]
      else:
        good += [(x,y,DEFAULT_HEADER) for x,y in vals]
  vals2 = []
  for v in allVals:
    answer = ';;;'.join([x[0].strip(' ,') for x in v if x[0].strip(' ,')])
    mIds = ';;;'.join([x[1].strip() for x in v])
    vals2.append((answer, mIds))
  for answer,mIds in vals2:
    assertNotMissing(answer)
    assertNotMissing(mIds)
  good += [(x,y,DEFAULT_HEADER) for x,y in vals2]
  return good
def getFbAnswersNew(htmlName):
  f = open(htmlName)
  soup = BeautifulSoup(f)
  f.close()

  # Removing sections
  invalidPairs = [('id', 'page-header'), \
                  ('class', 'column nav'), \
                  ('data-id', '/type/object'), \
                  ('data-id', '/common'), \
                  ('data-id', '/user'), \
                  ('data-id', '/base'), \
                  ('style', 'display:none;'), \
                  ('data-id', '/common/topic/description'), \
                  ('data-id', '/common/topic/article')]
  # remove unwanted sections
  for attr,val in invalidPairs:
    for div in soup.findAll('div', attrs={attr: val}):
      print 'found:', repr(attr), repr(val)
      div.extract()
  # remove hidden sections
  for e in soup.findAll('div'):
    d = e.__dict__['attrs']
    styles = [x[1] for x in d if x[0] == 'style']
    assert(len(styles) <= 1)
    if styles:
      style = styles[0]
      style = re.sub(r'\s', '', style).lower()
      if style.find('display:none') >= 0:
        e.extract()
        print repr(style)
  # Save this edited soup
  outbase = REPRO_OUT + 'freebaseEntHtmlClean/ents/'
  fname = htmlName.split('/')
  name = outbase + fname[-2]+'/'
  os.system('mkdir -p '+name)
  name += fname[-1]
  outf = open(name, 'w')
  outf.write(str(soup))
  outf.close()

  # get each section separately so we can store the property value
  # ex: /government/governmental_jurisdiction/government_positions
  propSections = soup.findAll('div', attrs={'class':'property-section'})
  good = []
  for section in propSections:
    prop = section['data-id']
    good += [(text,mId,heading,prop) for text,mId,heading in getFbAnswerForSection(section)]

  good = list(set(good))
  good.sort()
  #good = [normalizeUnicode(x).encode('utf-8') for x in good]
  allAs = {}
  for a,mIds,heading,prop in good:
    #Error('Cant have carats (^) in the answers since we use them as delimiters.')
    a = a.replace('^','')
    mIds = mIds.replace('^','')
    heading = heading.replace('^','')
    prop = prop.replace('^','')
    if a not in allAs:
      # mIds, heading, prop
      allAs[a] = ([],[],[])
    allAs[a][0].append(mIds)
    allAs[a][1].append(heading)
    allAs[a][2].append(prop)
  allMids = []
  allHeadings = []
  allProps = []
  aList = [a for a in allAs] # need a list to keep the answers ordered
  aList.sort()
  for a in aList:
    v = allAs[a]
    allMids.append('|||'.join(v[0]))
    allHeadings.append('|||'.join(v[1]))
    allProps.append('|||'.join(v[2]))
  return '###'.join(aList).replace(',', '^'), \
      '###'.join(allMids).replace(',', '^'), \
      '###'.join(allHeadings).replace(',', '^'), \
      '###'.join(allProps).replace(',', '^')
def getFbAnswerForSection(section):
  # Gettings values from tables
  tableVals = []
  tables = section.findAll('table', attrs={'class':'data-table'})
  for table in tables:
    # remove header and footer
    # also store the column titles
    headers = table.findAll('thead')
    assert(len(headers) == 1)
    thead = headers[0]
    headers = [x.text for x in thead.findAll('th')]
    #print headers
    thead.extract()
    for tfoot in section.findAll('tfoot'):
      tfoot.extract()
    # Get the rows
    rows = table.findAll('tr', attrs={'class':'kbs data-row hover-row'})
    vals = [] # init one list for each column
    ncols = 0
    if len(rows) > 0:
      ncols = len(headers)
      for i in range(ncols):
        vals.append([])
    for row in rows:
      cols = row.findAll('td')
      assert(len(cols) == ncols) # check that all rows have same number of cols
      for col,i in zip(cols, range(len(cols))):
        colEnts = extractTextFromListOfDatalist([col], 'a')
        colDates = extractTextFromListOfDatalist([col], 'span')
        colAll = colEnts + colDates
        assert(len(colAll) <= 1)
        if len(colAll) > 0:
          vals[i] += colAll[0]
          vals.append(colAll[0])
    tableVals += getAllValues(vals, headers)
    if len(tableVals) > 0: #TODO remove
      break
  tableVals = list(set(tableVals))
  #print tableVals
  #sys.exit()

  # Getting other values
  ul = section.findAll('ul', attrs={'class':'data-list'})
  gooda = extractTextFromListOfDatalist(ul, 'a')
  goods = extractTextFromListOfDatalist(ul, 'span')

  good = getAllValues(gooda, [])
  good += getAllValues(goods, [])
  #print len(good)
  good = list(set(good))

  good += tableVals
  return [x for x in good if x[0] and x[1]]

#######################################################
if __name__ == "__main__":
  random.seed(0)
  print sys.argv
  if len(sys.argv) != 2:
    Error('createAmtData3.py <AMT bfs trial #>')
  amtTrialNum = int(sys.argv[1])
  amtInputFile = getAmtInputName(amtTrialNum)
  print amtInputFile
  if not os.path.exists(amtInputFile):
    print amtInputFile, 'doesnt exist.'
    sys.exit()

  # Freebase changed so we had to re-scrape
  #outdir = REPRO_OUT + 'freebaseEntHtml/'
  outdir = REPRO_OUT + 'freebaseNewEntHtml/'
  nTemplates = 100000
  seeds = getGoodSeedNums()
  templates = loadBfsTemplates(seeds, nTemplates)
  templates = list(templates)
  qsAndTs_ = [(t2q(tlist2tstr(t)),t) for t in templates]
  qsAndTs = {}
  for q,t in qsAndTs_:
    qsAndTs[q] = t
  qsAndTs['what was the owls name in harry potter?'] = ['what was the owls name in', 'harry potter', '']
  qsAndTs['what religion was prophet muhammad before islam?'] = ['what religion was prophet', 'muhammad', 'before islam']
  amtFbAnswersInputFile = amtInputFile + '.newfb'
  ##############
  urlsToMids = getUrlToMidMap()
  ents = set([t[1].strip() for t in templates])
  tophits = getTopHits(nTemplates, False, ents, urlsToMids, 'all')
  mIdsToFbIds = getMidToFbIdMap()
  entToFbIdMap = getEntToFbIdMap(tophits, urlsToMids, mIdsToFbIds)
  ####
  inf = open('data/dataset_'+str(amtTrialNum)+'/bfs_'+str(amtTrialNum)+'_good.examples.qs')
  goodQs = [x.strip() for x in inf]

  inf.close()
  goodQs = set(goodQs)
  ####
  inf = open(amtInputFile)
  inf.readline()
  outf = open(amtFbAnswersInputFile, 'w')
  outf.write('freebasesearch,question,answers,mids\n')
  checkedEnts = {} # map from ent -> (answers, mIds)
  badEnts = []
  badUrls = []
  ####
  #q2aMap = {}
  #partf = open('data/dataset_6/bfs_6.input.newfb.partial')
  #for line in partf:
  #  url,q,a = line.strip().split(',')
  #  q2aMap[q] = a
  #partf.close()
  ####
  i = 0
  for line in inf:
    print 'LINE:', i
    i += 1
    url,q = line.strip().split(',')
    print repr(url), repr(q)
    if q not in goodQs:
      print 'BAD Q'
      continue
    t = qsAndTs[q]
    ent = t[1].strip()
    #if q in q2aMap:
    #  answers = q2aMap[q]
    #  checkedEnts[ent] = answers
    if ent in checkedEnts:
      answers,mIds = checkedEnts[ent]
    else:
      try:
        saveUrlForEnt(ent, url, outdir)
      except Exception, e:
        print e
        print 'Error saving url for:', repr(ent), 'with url:', repr(url)
        badEnts.append(ent)
        badUrls.append(url)
        continue
      fbHtmlName = getOutnameAndMakeDirQOnly(outdir, ent, True) + '.html'
      print repr(fbHtmlName)
      answers,mIds,headings,props = getFbAnswersNew(fbHtmlName)
      answers = answers.encode('ascii', 'xmlcharrefreplace')
      checkedEnts[ent] = (answers,mIds)
    outf.write(','.join([url,q,answers,mIds])+'\n')
  inf.close()
  outf.close()
  print 'Wrote to:', amtFbAnswersInputFile
  print 'Number of bad ents:', len(badEnts)
