This document describes the full details of SEMPRE.

[TODO: insert high-level overview]

# Logical forms

The [lambda DCS document](http://arxiv.org/pdf/1309.4408.pdf) describes the
language via a set of examples.  This section simply connects the examples in
the document to code.

Recall from the tutorial that logical forms are represented in the code using a LISP-like formalism.
Concretely, we have logical forms like the following:

    (call + (number 3) (number 4))

This logical form has a denotation of 7, i.e. it says that we are taking the sum of 3 and 4.
Do not worry about the details of this logical form, they will be explained in the sub-sections below.

The key thing to note is how the logical form is composed/written using a LISP-like parentheses-based representation.
Note also that such a LISP-like representation can be interpreted as a tree, 
where the scopes (i.e., extents) of the parenthesis-pairs correspond to sub-trees.
This document (and the code) will use this tree-intuition often, referring to sub-trees and leaves of a formula.

It is important realize that the interpretation/denotation of a logical form depends on the `Executor` that is being used
(specified via the `-Builder.Executor` option). 
`Executor`s (i.e., classes that extend `Executor`) define how logical forms are mapped to actual concrete values. 
The most basic `Executor` is `JavaExecutor`, which interprets the logical forms using simple Java code.
A more complicated `Executor` is the `SparqlExecutor` which interfaces with Freebase to give denotations.
Certain logical formulas are currently only supported by certain executors and this will be noted.
Also note that whenever we discuss entities and relations of the form `fb:<something>` in this documentation,
we are implicitly assuming that the `SparqlExecutor` is being used.
Finally, note that is possible for you to define your own `Executor` in order to meet your own specific needs (e.g., by extending `JavaExecutor).

## Primitive

Primitive formulas can be thought of as representing the atomic components of a logical formula.
Alternatively, if you take the LISPy parenthesis-tree mindset, then you can think of primitive formulas as corresponding to leaves of these trees.

### ValueFormula (Date, Name, etc.)

`ValueFormula`s are the most basic primitives and are basically SEMPRE's analogue of a programming language's primitive types (int, String, etc.).
Concretely, a value formula is simply a wrapper around a `Value` object.
The basic value types (i.e., instantiations of `Value`) supported in SEMPRE (and their corresponding logical forms) are listed below:

- `BooleanValue (boolean <true/false>)`: Standard boolean value.
- `NumberValue  (number <value>)`: Single numeric values that are internally stored as Java doubles.
- `StringValue  (string <value>)`: Atomic strings (stored as Java Strings).
- `DateValue  (date <year> <month> <day>)`: Dates represented in a native SEMPRE format. Fields set as -1 indicate no information for that field. 
- `NameValue (name <value>)`: Logical predicates (e.g., composed from freebase predicates or a lexicon).
- `ListValue  (list <value1> <value2> ... <valuen>`: List of values, which can be of any of the types listed above (i.e., list of objects that extend `Value` class). 

[TODO: Do we want to talk about special use things such as URIValues and TableValues?]

### VariableFormula

Variable formulas primitives that do not correspond to concrete values.
Instead, they represent variables in lambda expressions.
An example `VariableFormula` would simply be 

    (var x)

which simply represents a variable with the name `x`.
On their own, these formulas are rather uninteresting.
However, they are integral subcomponents of `LambdaFormula`s which are described below.

## Composition

These formula-types are used to compose or build up large formulas.
In the simplest case, these formulas represent a composition of primitive formulas,
but they can be recursively composed as well to create more complex formulas.
From a tree-based perspective, these formulas are roots of sub-trees.

Note that some of the explanations and examples in this section will have strong parallels with the section
on compositional `SemanticFn`s below.

### JoinFormula

`JoinFormula`s are used to represent the application of a binary relation/predicate to unary/predicates.
In other words, a `JoinFormula` has the form

    (<relation> <unary>)

which denotes that we are applying (or joining) the `<relation>` to the `<unary>`.

So supposing that we have the entity/unary `fb:en.barack_obama` and the relation `fb:people.person.place_of_birth`, 
which is map from people to their birthplace, then we can join these in the formula:

    (fb:people.person.place_of_birth fb:en.barack_obama)

And this formula will denote the singleton set containing the entity corresponding to where Barack Obama was born (i.e., `fb:en.honolulu`).
Note that this is called a `JoinFormula` since it can be viewed as a sort of "database join" on our knowledge base/graph.

Finally, note that in some `JoinFormula`s the relation component will be prefixed with a `!`.
This means that the relation map is being reversed.
For example, we might have

    (!fb:people.person.place_of_birth fb:en.honolulu)

which would denote the set of people who were born in Honolulu.
This reversal is a special case of the function reversal (see `ReversalFormula` below for the general case).


## LambdaFormula

`LambdaFormula`s are used to represent lambda functions, i.e. anonymous functions.
That is, they add lambda-abstraction to the lambda DCS logical language representation in SEMPRE.
They are also the primary place in which `VariableFormula`s get used.
An example lambda expression would be the following:

    (lambda x (fb:people.person.place_of_birth (var x))))) 

This expression simply says that we have defined a function (i.e., relation) that,
when given an argument, substitutes this argument for the `(var x)`.
So, if we applied this function (via beta-reduction, see the documentation for `JoinFn`) to the entity `fb:en.barack_obama` then we would get

    (fb:people.person.place_of_birth fb:en.barack_obama)

That is, this function simply provided an abstraction over the relation `fb:people.person.place_of_birth`.

In that example, the abstraction was not particularly useful.
However, suppose that we have two relations: `fb:people.person.place_lived`, which returns a set of "place_lived" entities,
and `fb:people.place_lived.location`, which returns a regular "location" entity given a "place_lived" entity.
In this case, we could create the `LambdaFormula` below:

    (lambda x (fb:place_lived.location (fb:people.person.place_lived (var x)))

And this would define an anonymous function that takes a person and returns the places where they lived (as "location" entities).
Note that the lambda-abstraction allowed for the "hiding" of the intermediate "place_lived" entities.

ReverseFormula

`ReversalFormula`s are used to represent general function reversal.
In the `JoinFormula` section above the `!` symbol was used to denote that a relation was reversed.
However, in general, we may want to represent reversed versions of more general functions, such as those defined via `LambdaFormula`s (not just simple relations).
In order to do this, we simply wrap formula defining the function we want to reverse in a `ReverseFormula`.
For example, we could reverse the `LambdaFormula` we introduced in the previous section as follows:

    (reverse (lambda x (fb:place_lived.location (fb:people.person.place_lived (var x))))

And this function will now take a location and return all the people that have lived there.
In other words, the above formula would reduce to (i.e., be equivalent to)

    (lambda x (!fb:people.person.place_lived (!fb:place_lived.location (var x))))

### MergeFormula

`MergeFormula`s represent the logical `or` or logical `and` of two other formulas.
Concretely, we might have the following `MergeFormula`:

    (and <formula1> <formula2>)

which denotes that we are taking the logical `and` of the two formulas.
In the lambda DCS framework, this corresponds to set intersection.
In other words, both `<formula1>` and `<formula1>` are logical formulas that have sets as denotations,
and the above `MergeFormula` then returns the set of elements that are shared between these two sets.

Alternatively, we could have an `or` `MergeFormula` such as

    (or <formula1> <formula2>)

which would denote the union of the sets denoted by `<formula1>` and `<formula2>`.

As a concrete example (building upon the example formulas introduced in the previous sections), we might have

    (and (!fb:people.person.place_lived (!fb:place_lived.location fb:en.honolulu)) (!fb:people.person.place_lived (!fb:place_lived.location fb:en.washington_dc)))

which denotes the set of people who have both lived in Honolulu and Washington D.C. (e.g., Obama).


### AggregateFormula (SparqlExecutor only)

`AggregateFormula`s are used for expressing basic aggregation relations and take the following form:

    (<mode> <formula>)

In this expression `<mode>` defines the type of aggregation to be performed while `<formula>` denotes/defines the set that we are aggregating over.
The possible modes (with their corresponding semantics) are listed below:

- `count`: Returns the cardinality of a set.
- `sum` : Returns the sum of the elements in the set. 
- `avg` : Returns the average/mean of the elements in the set.
- `min` : Returns the minimum element of a set.
- `max` : Returns the maximum element of a set.

Note that the latter four modes are only compatible with numeric types (e.g., entities that Freebase considers numeric).

For a concrete example, consider the following logical form:

    (count (!fb:people.person.place_of_birth fb:en.honolulu))

This would return the number of people that were born in Honolulu.

### SuperlativeFormula (SparqlExecutor only)

`SuperlativeFormula`s are similar to `AggregateFormulas` and allow one to express that you want a superlative of a function/relation map.
They take the following form:

    (<mode> <rank> <count> <unary> <binary>) 

The first three parts/entries of this logical representation have the following meanings:

- `<mode>` is either `argmin` or `argmax`. 
- `<rank>` is an integer indicating where in the ranked list of the entities you want to start collection from;
for example, setting `<rank>` to 2 in `<argmin>` mode would mean that you want the second-to-last-ranked entity.
- `<count>` specifies the cardinality of the set you want returned.

The last two parts have the following meaning:
`<binary>` defines the function/relation which you are using to rank the elements of the set defined by `<unary>`.
For example, suppose we want to know the city with the largest area, then we could write:

    (argmax 1 1 (fb:type.object.type fb:location.citytown) fb:location.location.area))

ArithmeticFormula (SparqlExecutor only)

`ArithematicFormula`s are used to express basic arithmetic in cases where the `JavaExecutor` is not being used.
However, note that if you are using the `JavaExecutor`, then you can represent such expressions in an alternative 
(and more general) manner using `CallFormula`s (see the section below).
The form of an `ArithmeticFormula` is as follows:

    (<operator> <arg1> <arg2>)

The `<operator>` entry is either `add`, `sub`, `mul`, or `div`, and the two arguments are formulas that return numeric values.
For example, we might have

    (add (number 5) (number 3))

which would denote the number 8.
Note that we could not define such formulas via joins since they are effectively ternary.

### MarkFormula

[TODO: need motivation and compelling example for this]

### NotFormula (SparqlExecutor only)

`NotFormula`s are used to represent negation/complements and have the form:

    (not <formula>)

[TODO: I am not entirely clear on when/how this is actually used.]

CallFormula (JavaExecutor only)

`CallFormula`s are very flexible formulas that are used with the `JavaExecutor` to build up complicated logical representations. 
Concretely, a `CallFormula` takes the following form:

    (call <function> <arg1> <arg2> ... <argn>)

The `<argi>` entries are simply logical forms (which must resolve to some collection of primitive formulas), while 
the `<function>` entry can be any fully specified Java function (e.g., `java.lang.Math.cos`) 
(though it must be a function that accepts n arguments and the types must match). 
Some common Java functions have shortcut names, such as the basic arithmetic functions (e.g., `+`) and String functions (e.g., `.substring`).
Some concrete examples of using `CallFormula` are given below:

    (call + (number 3) (number 4))
    (call java.lang.Math.cos (number 0))
    (call .indexOf (string "what is this?") (string is))

# Semantic functions

Perhaps the most fundamental step in applying SEMPRE to a new problem, is the specification of a grammar.
From the tutorial, we know that a grammar is simply a set of rewrite rule of the following form: 

    (rule <target-category> (<source-1> ... <source-k>) <semantic-function>)

(If you haven't seen this before, or are unclear on what a rule is, you should look over the tutorial before continuing.
This documentation assumes a modest understanding of how SEMPRE works at a high level).
Basically, these rules specify how derivations belonging to the source categories are re-written, or transformed, 
into derivations that correspond to the target category.
Semantic functions are the workhorses of these rules: 
they take in a set of source derivations and apply transformations (defined via Java code) on these derivations
in order to produce a derivation that is a member of the target category.

Since semantic functions are built from arbitrary Java code (any class extending `SemanticFn`), there is a great deal of flexibility,
and knowing how to properly use semantic functions is perhaps the most important step in working with SEMPRE.
This section defines the semantic functions that come pre-packaged with SEMPRE.
In all likelihood, these semantic functions will provide all the functionality that you need.

Some of these semantic functions will rely on other packages/classes/options being used/set in order to function properly.
For example, many semantic functions require that the Stanford CoreNLP package is loaded (via the `-languageAnalyzer corenlp.CoreNLPAnalyzer` option);
this package contains many general purpose NLP algorithms that extract linguistic information (e.g., parts of speech) from utterances.
Dependences on packages such as CoreNLP will be noted when necessary.

## Special categories:

Some special categories that you should now in order to effectively write grammars and use semantic functions:

- $TOKEN: selects a single atomic word from the utterance.
- $PHRASE: selects any subsequence of tokens/words from the utterance.
- $LEMMA_TOKEN: selects any atomic word from the utterance and lemmatizes the word. For example, "arriving" would become "arrive".
- $LEMMA_PHRASE: selects any subsequence of tokens/words and lemmatizes them.

Both of the special categories that rely on lemmatization will only function properly if the CoreNLPAnalyzer is loaded.
Otherwise, the lemmatization will simply amount to making everything lowercase.

## Primitives:

Primitives are basic semantic functions that directly map spans (i.e., subsequences) of an utterance to logical forms. 
Most grammars will rely on primitives as the "bottom" rules, i.e., the rules that have some span of the utterance as their right hand side (RHS)
and which will other rules build off of. 

### NumberFn (partially reliant on the CoreNLPAnalyzer)

`NumberFn` is the basic primitive function for parsing numbers from an utterance.
For example,

    (rule $ROOT (NumberFn))

would parse the following strings into the following logical forms:


    2.3               # (number 2)
    four              # (number 4)
    3 million         # (number 3000000)
    
The last one works only if `-languageAnalyzer corenlp.CoreNLPAnalyzer` is set.

### DateFn (reliant on the CoreNLPAnalyzer) 

`DateFn` is the basic primitive function for parsing date values.
For example,

    (rule $ROOT (DateFn))

would parse the following strings into the following logical forms:

    Thursday, December 4th        # (date -1 12 4)
    The 12th of Nov, 2012         # (date 2012 11 12)
    August                        # (date -1 8 -1) 

Note that the logical representation of dates here (defined in the `DateValue` class) is distinct from Java's date (or JodaTime etc.)
and that only dates not times are represented. 
The logical form representation of a `DateValue` is simply `(date year month day)`, with one-based indexing for the months. 
The above examples also illustrate how missing aspects of dates are treated: 
if any part of a date (day, month, or year) is left unspecified, then the `DateValue` logical representation inserts -1s in those positions.

### ConstantFn

This is a basic primitive function which allows for you to hard-code basic rules, such as 

    (rule $ROOT (barack obama) (ConstantFn fb:en.barack_obama fb:people.person))

which would parse the following utterance into its corresponding entity (i.e., logical form):

    barack obama        # (name fb:en.barack_obama)

Note the form that `ConstantFn` takes, i.e. `(ConstantFn formula type)`, and that the `type` argument is optional, meaning that

    (rule $ROOT (barack obama) (ConstantFn fb:en.barack_obama))

would also parse the above utterance. 
However, note that if types are not explicitly added, the system relies on automatic type inference (see the section on Types below).

Another example of `ConstantFn` would be the following: 

    (rule $ROOT (born in) (ConstantFn fb:people.person.place_of_birth (-> fb:location.location fb:people.person))))

which would parse the phrase `born in` to a relation/binary logical form (as opposed to an entity) as follows:

    born in         # (name fb:people.person.place_of_birth)

Lastly, note that in both these cases the parsed logical form is prefixed with `name`.
This denotes that the logical form has a `NameValue` denotation, which simply means that it represents a logical predicate/entity
and not some other primitive (e.g., a `NumberValue` or `DateValue`).

### SimpleLexiconFn (reliant on having a SimpleLexicon)

This function allows you to map phrases/tokens to logical entities in a more scalable manner than hard-coding everything with `ConstantFn` directly in your grammar.
Suppose you have a `SimpleLexicon` loaded from a JSON file containing the entries:

    {'lexeme' : 'barack obama', 'formula' : 'fb:en.barack_obama', 'type' : 'fb:people.person'} 
    {'lexeme' : 'born in', 'formula' : 'fb:people.person.place_of_birth', 'type' : '(-> fb:location.location fb:people.person)'} 

then the rule

    (rule $ROOT ($PHRASE) (SimpleLexiconFn (type fb:people.person)))

will parse the following:

    barack obama       # (name fb:en.barack_obama)

and the rule

    (rule $ROOT ($PHRASE) (SimpleLexiconFn (type (-> fb:location.location fb:people.person))))

will parse 

    born in        # (name fb:people.person.place_of_birth) 

Thus, the function of `SimpleLexiconFn` is similar to `ConstantFn`, but it facilitates more modularity
since the lexical items are contained within the `SimpleLexicon` (loaded from a JSON) instead of being hard-coded into the grammar.

In the above examples, this type annotation is not particularly important, but consider the case where we have the following lexical entries:

    {"lexeme" : "Lincoln", "formula" : "fb:en.abraham_lincoln", "type" : "fb:people.person"} 
    {"lexeme" : "Lincoln", "formula" : "fb:en.lincoln_nevada", "type" : "fb:location.location"} 

Both of these lexical entries have the same trigger phrase (i.e., lexeme).
However, they correspond to very distinct entities (one is a former president, while the other is a city in Nevada).
By leveraging the type annotation, we can specify which entity we actually want to trigger.
For example, the rule
 
    (rule $ROOT ($PHRASE) (SimpleLexiconFn (type fb:location.location)))

would ensure that we only trigger the entity that corresponds to the city.
Alternatively, if we wanted both entities to be triggered (e.g., if type-checking later on will handle the ambiguity),
then we could write:

    (rule $ROOT ($PHRASE) (SimpleLexiconFn (type fb:type.any)))

 
## For filtering what you trigger on (important for computational reasons):

Often you will find that your grammar is generating far too many candidate derivations
and that this is overwhelming your system.
Over-generation is problematic for two reasons:
it leads to long runtimes, which can be frustrating and prevent quick development cycles, 
and it leads to situations where correct derivations "fall off the beam", 
which means that your system is not getting adequate feedback during learning due to the large number of useless candidate derivations.

The semantic functions described below help to ameliorate this issue by allowing you to filter what you trigger on.
In other words, they help control how many logical forms you generate by allowing you to specify more precise situations in which rules should fire.

### FilterSpanLengthFn

This semantic function allows you to filter out the length of spans that you trigger on. For example,

    (rule $Length2Span ($PHRASE) (FilterSpanLengthFn 2)) 

produces a new category `$Length2Span` which contains only phrases of length 2, 
which is useful for limiting the number of phrases you compute on.
For instance, we could combine this with the simple lexicon function we used above and say

    (rule $ROOT ($Length2Span) (SimpleLexiconFn (type fb:type.any)))

and this would parse

    barack obama       # (name fb:en.barack_obama)
 
but would make running the grammar faster than if we replaced `$Length2Span` with `$PHRASE`, since `$PHRASE` will try all possible lengths, 
and we know that our lexicon only contains length 2 phrases.

### FilterPosTagFn (relies on CoreNLPAnalyzer)

`FilterPosTagFn` allows you to extract spans or tokens that are composed only of certain parts of speech (POS) tags.
For example,

    (rule $ProperNoun ($TOKEN) (FilterSpanPosTag token NNP NNPS)) 

produces a new category `$ProperNoun` which contains words that are proper nouns.
This would accept phrases like 

      honolulu

or

      obama

and assign them to the `$ProperNoun` category.

Note the options that are passed: `token` specifies that we want to look at single tokens and `NNP NNPS` are the POS tags for proper nouns.
If you wanted to get multi-word proper nouns then you would use
 
    (rule $ProperNounPhrase ($PHRASE) (FilterSpanPosTag span NNP NNPS))

Note that we both changed the RHS category to `$PHRASE` and changed the `token` option to `span`.
This would accept things like

    honolulu hawaii

    barack obama

but note that the entire span must have the same POS tag (that is in one of the accepted categories).
Also, it will only accept the maximal span, meaning that `barack obama` will NOT be parsed three times as `barack`, `obama`, and `barack obama`.

Note also that, unlike the previous examples for simpler semantic functions, the logical form is not written out next to the phrases.
This is because the above rule to does not rewrite to `$ROOT` (i.e., it does not have `$ROOT` as a LHS),
and thus, this rule would accept the example phrases above, but it does not parse them directly to a logical form.
Other rules which take `$ProperNounPhrase` as a RHS are necessary to complete a grammar with this rule. 
Most of the filtering functions discussed below will also have this flavor.

### FilterNerSpanFn (relies on CoreNLPAnalyzer)

This `SemanticFn` allows you to extract spans that are named entities (NEs). 
For example,

    (rule $Person ($Phrase) (FilterNerSpan PERSON))

produces a new category (`$Person`) which contains phrases that the CoreNLPAnalyzer labels as referring to people.
This would accept phrases like

    barack obama
    michelle obama

Note that unlike our earlier examples where we parsed these phrases corresponding to an entity/person, 
the LHS side of our rule in this case is a new category `$Person`.
The idea is that you would use this new category to restrict what you apply rules on further up in the derivation.
For example, you may want to change our earlier `SimpleLexiconFn` example rule to

    (rule $ROOT ($Person) (SimpleLexiconFn (type fb:people.person)))

since you know that this rule should be restricted to spans that may contain a mention of a person.

### ConcatFn

This function allows you to concatenate strings, giving you more fine-grained control over how categories are constructed over spans,
which is useful for doing things that are not possible with the other filter functions. 
Concretely, say you wanted to look for spans of the utterance that may correspond to relations (i.e., binaries),
you would probably want to look at verbs (e.g., "lived", "born").
However, triggering only on verbs is too restrictive; for example, some relations/binaries take their meaning from verb-preposition pairs.
In fact, the example of "born in" used previously is exactly such a phrase.
In order to handle this case, we could use `ConcatFn` with the following rules:

    (rule $Verb ($TOKEN) (FilterSpanPosTag token VB VBD VBN VBG VBP VBZ))
    (rule $Prep ($TOKEN) (FilterSpanPosTag token IN)) 
    (rule $VerbPrep ($Verb $Prep) (ConcatFn " "))

These rules would create a new category (`$VerbPrep`) that contains two word phrases consisting of a verb followed by a preposition. 
One way we could leverage this category would be with a rule that uses `SimpleLexiconFn`, such as the following:
 
    (rule $Relation ($VerbPrep) (SimpleLexiconFn (type (-> fb:type.any fb:type.any))))

This rule basically says that when we look for binaries/relations, we shouldn't look at any phrase, we should restrict to phrases consisting
of verbs followed by prepositions. 
Of course, there are other grammatical constructions that could give rise to a relation, and you would need to add new rules/categories for these.
Nevertheless, using filtering to narrow down to the spans which trigger certain rules is a very powerful tool.

## Composition:

Semantic functions in this category are used to compose categories as you build up derivations.
They serve as the glue of your grammar.

### IdentityFn 

This is the most basic composition function, useful for refactoring grammar categories. 
For example, suppose you have the following rule:

    (rule $Entity ($Phrase) (SimpleLexiconFn (type (-> fb:common.topic fb:common.topic))))

this rule basically uses `SimpleLexiconFn` to extract entities from spans of the utterance.
The `$Entity` category can then be used as the RHS of other rules (e.g., a relation may be applied on it, as described in `JoinFn` below).
However, you may also want to allow the whole utterance to simply be parsed as a single entity;
that is, you may want the above rule to rewrite to `$ROOT`.
The cleanest way to do this is using `IdentityFn` in the following rule:

    (rule $ROOT ($Entity) (IdentityFn)) 

Note that `IdentityFn` is not only useful for rewriting to `$ROOT`. 
It should be used anytime when a category can simply be "lifted" to a more general category.

### JoinFn

This composition function plays a number of important roles related to function application.

#### Applying binaries to unaries

The first way in which this function can be used is to apply some relation (i.e., binary) to a unary.
Moreover, the function is flexible in how exactly the relation and binary are ordered and the exact manner in which the relation is applied.
The order of arguments for a binary can be initially confusing, but should make sense once you get used to it.
Each binary (e.g., `fb:people.person.place_of_birth`) has an arg0 (return type) and arg1 (argument type).
The concrete fact is:

    arg0 fb:people.person.place_of_birth arg1

and the type of `fb:people.person.place_of_birth` is

    (-> arg1 arg0)

The way to think about a binary is not a function that returns the places of
births of people, but rather as a relation connecting people (in the return or
head position) with locations (in the argument or modifier position).

During function application, the unary can substitute for either `arg0` or `arg1`,
and either the binary can come before the unary or vice-versa.
Thus, there are 4 ways in which a binary may be applied to a unary, which are defined via the following options/flags:
a positional option, which is either `binary,unary` or `unary,binary`,
and the flags `unaryCanBeArg0` and `unaryCanBeArg1`. 

To illustrate the two most common settings for these options, 
suppose that we have the relation `fb:people.person.place_of_birth` (of type `(-> fb:location.location fb:people.person)`),
the entity `fb:en.barack_obama` (of type `fb:people.person`), and the entity `fb:en.honolulu` (of type `fb:location.location`).
And further, suppose we have rules parsing these entities into categories, such as

    (rule $Entity (barack obama) (ConstantFn fb:en.barack_obama (type fb:people.person)))
    (rule $Entity (honolulu) (ConstantFn fb:en.barack_obama (type fb:people.person)))
    (rule $Relation (born in) (ConstantFn fb:people.person.place_of_birth (type (-> fb:location.location fb:people.person))))

then we can write

    (rule $Set ($Relation $Entity) (JoinFn binary,unary unaryCanBeArg1))
    (rule $ROOT ($Set) (IdentityFn))

and these rules will parse the following utterance as follows:

    born in honolulu       # (fb:people.person.place_of_birth fb:en.honolulu)

That is, it will return all the people born in Honolulu (including `fb:en.barack_obama`).
This corresponds to standard forward application of a binary, and indeed we can equivalently write:

    (rule $Set ($Relation $Entity) (JoinFn forward))

Alternatively, we can alter the arguments and write

    (rule $Set ($Entity $Relation) (JoinFn unary,binary unaryCanBeArg1))
    (rule $ROOT ($Set) (IdentityFn))

and this will parse the following:

    barack obama born in        # (!fb:people.person.place_of_birth fb:en.barack_obama)

That is, it returns the place where Barack Obama was born, which would resolve to the singleton set `fb:en.honolulu`.
This corresponds to standard backwards application:

    (rule $Set ($Entity $Relation) (JoinFn backward))

(Note how there is a  "!" in front of the relation reverses it.

Both of the above examples have the `unaryCanBeArg1` flag set because this is far more common than the alternative flag.
For example, if we changed the second example's flag to `unaryCanBeArg0` then this would work for parsing utterances such as

    born in barack obama

which is not particularly well-formed.

#### Beta reduction

The second important way in which JoinFn is used is to apply lambda functions via beta reduction.
One thing we can do for this is simply write

    (rule $LambdaRelation (ConstantFn (lambda x (fb:people.person.place_of_birth (var x))))) 
    (rule $Set ($LambdaRelation $Entity) (JoinFn betaReduce forward))

and this will act just like the forward application example we gave above (without beta reduction).

A more interesting case would be when, we have two relations `fb:people.person.places_lived`, which returns a "place_lived" entity
and `fb:people.place_lived.location`, which returns a "location" given a "place_lived". We can write 

    (rule $LambdaRelation (lived) (ConstantFn (lambda x (fb:place_lived.location (fb:people.person.place_lived (var x)))) (-> fb:location.location fb:people.person))) 
    (rule $Set ($Entity $LambdaRelation) (JoinFn unary,binary unaryCanBeArg1))
    (rule $ROOT ($Set) (IdentityFn))

and this will parse:

    barack obama lived        

via backward application and give `fb:en.honolulu` (plus other places, e.g. `fb:en.washington_dc`). 
Note that the lambda abstraction was necessary here to hide the intermediate "placed_lived" entity.

### MergeFn

A composition function that merges two source derivations (i.e., RHS arguments)
by either taking the logical "or" or logical "and" of the derivations'
formulas.  So for example, suppose that we want to know places where both
Michelle Obama and Barack Obama lived.  That is, we want to parse an utterance
such as

      barack obama lived and michelle obama lived

Now, assuming we have the necessary lexical entries for Barack and Michelle, we can write

    (rule $Relation (lived) (ConstantFn (lambda x (fb:people.person.places_lived (fb:people.place_lived.location (var x)))) (-> fb:location.location fb:people.person))) 
    (rule $Set ($Entity $Relation) (JoinFn unary,binary unaryCanBeArg1))
    (rule $Set ($Set (and optional) $Set) (MergeFn and))
    (rule $ROOT ($Set) (IdentityFn))

and this will parse the above utterance, returning the intersection/conjunction of places where both lived.
Note that we added the `(and optional)` phrase which allows use to skip the word "and".
If we were interested in the union of places where they lived (i.e., the "or"), we would simply replace the second-to-last rule by

    (rule $Set ($Set (or optional) $Set) (MergeFn or))

### SelectFn

`SelectFn` acts as a utility composition function that aids in refactoring grammars.
More specifically, it can be used to skip over certain categories or parts of an utterance (e.g., stop words) in a controlled manner.
For example, in a question-answering setup, you may have many utterances that start with the word "what" (or "who" etc.),
but this word does not really convey any semantic content, since you know that all the utterances are questions anyways. 
Thus, in order to deal with this in a clean manner, we could have the following rules:

    (rule $What (what) (ConstantFn null))
    (rule $Set ($What $Set) (SelectFn 1)) 

This rules allow us to simply ignore the presence of "what" (assuming that we
have already built-up an `$Set` adjacent it to it via function application).

### Freebase-specific semantic functions

There are two semantic functions, `freebase.LexiconFn` and `freebase.BridgeFn`
which are used in our first sematic parsing applications, but they probably
should be avoided unless you're specifically doing Freebase QA.  Even in that
case, the main thing you should think about is:

    (rule $Entity ($PHRASE) (LexiconFn fbsearch)

which uses the Freebase Search API to look up entities.  Be aware here that the
API will generously return many candidate entities for any string you give it,
so if you are getting too many results, you should use filtering to constrain
the number of spans you look at.

### Context-specific semantic functions

`FuzzyMatchFn` is used for matching words with a context-specific graph.

#### ContextFn

`ContextFn` is used to suggest logical forms from the context.
Concretely, `Examples` may have associated `ContextValue`s, which contain, among other things, a `Formula`.
This `Formula` represents logical information that is known prior to parsing the utterance in the `Example`.
For example, when running SEMPRE in interactive mode, the `ContextValue` contains the last logical form that was parsed.
`ContextValue`s can also be provided in a dataset.

`ContextFn` can be used for tasks such as anaphora resolution.
For example, suppose you are running SEMPRE in interactive mode and input `where has barack obama lived` 
and that SEMPRE returns the logical form: `(fb:people.person.places_lived fb:en.barack_obama)`.
The `ContextValue` of the interactive system now contains this logical form.
Now, suppose that the you then say: `where was he born`. 
Parsing this utterance requires resolving the anaphoric reference `he`, and `ContextFn` facilitates this.
Specifically, we would make the following rule:

    (rule $ContextEntity (he) (ContextFn (depth 0) (type fb:people.person)))

This rule will trigger on the word `he` and retrieve an unary from the logical formula in the `ContextValue` that has the type `fb:people.person`.
The `depth` argument specifies how deep/tall of a subtree you want to retrieve.
In the above example, a depth of 0 was specified since we wanted to retrieve a single bare entity (i.e., a leaf).

# Grammar functionality

Explain binarization

macros

for loops

when

(see Grammar.java)

# Type system

TODO

# System level things

TODO: The `run` script is the main entry point.

TODO: talk about execution directories.
