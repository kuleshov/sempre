BUILD_DEPS = libsempre/sempre-core.jar \
	libsempre/sempre-cache.jar \
	libsempre/sempre-freebase.jar \
	libsempre/sempre-fbalignment.jar \
	libsempre/sempre-paraphrase.jar \
	libsempre/sempre-corenlp.jar \
	libsempre/sempre-jungle.jar

# BEGIN_HIDE
BUILD_DEPS += \
	libsempre/sempre-clojure.jar \
	libsempre/sempre-tables.jar \
	libsempre/sempre-regex.jar
# END_HIDE

default: $(BUILD_DEPS)

module-classes.txt: \
		$(shell find src/edu/stanford/nlp/sempre -name "*.java")
	scripts/extract-module-classes.rb

core: module-classes.txt libsempre/sempre-core.jar
libsempre/sempre-core.jar: \
		$(shell ls src/edu/stanford/nlp/sempre/*.java) \
		$(shell ls src/edu/stanford/nlp/sempre/test/*.java)
	cd src/edu/stanford/nlp/sempre && ant compile

cache: module-classes.txt libsempre/sempre-cache.jar
libsempre/sempre-cache.jar: \
		$(shell find src/edu/stanford/nlp/sempre/cache -name "*.java")
	cd src/edu/stanford/nlp/sempre/cache && ant compile

corenlp: module-classes.txt libsempre/sempre-corenlp.jar
libsempre/sempre-corenlp.jar: libsempre/sempre-core.jar libsempre/sempre-cache.jar \
		$(shell find src/edu/stanford/nlp/sempre/corenlp -name "*.java")
	cd src/edu/stanford/nlp/sempre/corenlp && ant compile

freebase: module-classes.txt libsempre/sempre-freebase.jar
libsempre/sempre-freebase.jar: libsempre/sempre-core.jar libsempre/sempre-cache.jar \
		$(shell find src/edu/stanford/nlp/sempre/freebase -name "*.java")
	cd src/edu/stanford/nlp/sempre/freebase && ant compile

fbalignment: module-classes.txt libsempre/sempre-fbalignment.jar
libsempre/sempre-fbalignment.jar: libsempre/sempre-freebase.jar \
		$(shell find src/edu/stanford/nlp/sempre/fbalignment -name "*.java")
	cd src/edu/stanford/nlp/sempre/fbalignment && ant compile

paraphrase: module-classes.txt libsempre/sempre-paraphrase.jar
libsempre/sempre-paraphrase.jar: libsempre/sempre-fbalignment.jar \
		$(shell find src/edu/stanford/nlp/sempre/paraphrase -name "*.java")
	cd src/edu/stanford/nlp/sempre/paraphrase && ant compile

# BEGIN_HIDE

clojure: module-classes.txt libsempre/sempre-clojure.jar
libsempre/sempre-clojure.jar: libsempre/sempre-core.jar \
		$(shell find src/edu/stanford/nlp/sempre/clojure -name "*.java")
	cd src/edu/stanford/nlp/sempre/clojure && ant compile

tables: module-classes.txt libsempre/sempre-tables.jar
libsempre/sempre-tables.jar: libsempre/sempre-core.jar \
		$(shell find src/edu/stanford/nlp/sempre/tables -name "*.java")
	cd src/edu/stanford/nlp/sempre/tables && ant compile

regex: module-classes.txt libsempre/sempre-regex.jar
libsempre/sempre-regex.jar: libsempre/sempre-core.jar \
		$(shell find src/edu/stanford/nlp/sempre/regex -name "*java")
	cd src/edu/stanford/nlp/sempre/regex && ant compile

jungle: module-classes.txt libsempre/sempre-jungle.jar
libsempre/sempre-jungle.jar: libsempre/sempre-cache.jar libsempre/sempre-core.jar \
		$(shell find src/edu/stanford/nlp/sempre/jungle -name "*.java")
	cd src/edu/stanford/nlp/sempre/jungle && ant compile

# END_HIDE

clean:
	rm -rf classes libsempre
