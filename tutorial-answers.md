This document contains the solutions to the exercises.

## Exercise 1.1

Program for computing the first word in "compositionality is key":

    (call .substring (string "compositionality is key") (number 0) (call .indexOf (string "compositionality is key") (string " ")))

## Exercise 1.2

Abstract out "compositionality is key", replacing it with a variable `(var x)`:

    (lambda x (call .substring (var x) (number 0) (call .indexOf (var x) (string " "))))

To test this program:

    (execute ((lambda x (call .substring (var x) (number 0) (call .indexOf (var x) (string " ")))) (string "compositionality is key")))

## Exercise 2.1

Here are the two rules for *length of X*:

    (rule $Function (length of) (ConstantFn (lambda x (call .length (var x)))))
    (rule $Expr ($Function $PHRASE) (JoinFn forward))

## Exercise 2.2

Add a rule for *first word in X*:

    (rule $Function (first word in) (ConstantFn (lambda x (call .substring (var x) (number 0) (call .indexOf (var x) (string " "))))))

## Exercise 2.3

When we parse *length of hello times two*, there are two possible readings
(derivations), one treating "hello times two" as a string.
