#!/usr/bin/ruby

# This script is outdated and assumes we are querying the website directly,
# rather than having our own Freebase instance.

# gem install mysql2
# mysql -u root
# create database paleo;
# source pbdb.sql;

require 'mysql2'
require 'json'

out = open('paleo.lexicon', 'w')
addLex = lambda { |phrase, lf|
  m = {"formula" => lf, "source" => "STRING_MATCH", "features" => {}, "lexeme" => phrase.downcase}
  out.puts JSON::dump(m)
}

def str(p, s); "(#{p} \"#{s.gsub(/"/, "\\\"")}\")" end

con = Mysql2::Client.new(:host => 'localhost', :username => 'root')
results = con.query("USE paleo")

puts "continent"
results = con.query("select * from continent_data")
results.each { |r| addLex.call(r['name'], str('continent', r['continent'])) }

puts "country"
results = con.query("select * from country_map")
results.each { |r| addLex.call(r['name'], str('country', r['cc'])) }

puts "interval"
results = con.query("select distinct interval_name from interval_data")
results.each { |r| addLex.call(r['interval_name'], str('interval', r['interval_name'])) }

puts "taxon"
results = con.query("select common_name, taxon_name from authorities")
results.each { |r|
  if r['common_name']
    addLex.call(r['common_name'], str('base_name', r['taxon_name']))
  end
  addLex.call(r['taxon_name'], str('base_name', r['taxon_name']))
}

out.close
