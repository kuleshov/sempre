# Representative grammar for querying PaleoBioDB.

# Convenient macros
(def @R reverse)
(def @type fb:type.object.type)
(def @any fb:type.any)

### Representative unaries

(rule $RepUnary (gastropoda) (ConstantFn fb:paleo.gastropoda))
(rule $RepUnary (occurrences) (ConstantFn (@type fb:paleo.occ)))

### Representative binaries

# generic
(rule $RepBinary (occurrences) (ConstantFn (lambda x (fb:paleo.occurrence_no.p (var x)))))
(rule $RepBinary (collections) (ConstantFn (lambda x (fb:paleo.collection_no.p (var x)))))
(rule $RepBinary (taxon name) (ConstantFn (lambda x (fb:paleo.taxon_name.p (var x))))))
(rule $RepBinary (species name) (ConstantFn (lambda x (fb:paleo.species_name.p (var x))))))

# time
(rule $RepBinary (early age) (ConstantFn (lambda x (fb:paleo.early_age.p (var x)))))
(rule $RepBinary (late age) (ConstantFn (lambda x (fb:paleo.late_age.p (var x)))))
(rule $RepBinary (early interval) (ConstantFn (lambda x (fb:paleo.early_interval.p (var x)))))
(rule $RepBinary (late interval) (ConstantFn (lambda x (fb:paleo.late_interval.p (var x)))))
(rule $RepBinary (interval) (ConstantFn (lambda x (and (fb:paleo.early_interval.p (var x)) (fb:paleo.late_interval.p (var x))))))

# space
(rule $RepBinary (latitude) (ConstantFn (lambda x (fb:paleo.lat.p (var x))))))
(rule $RepBinary (longitude) (ConstantFn (lambda x (fb:paleo.lng.p (var x))))))
(rule $RepBinary (state) (ConstantFn (lambda x (fb:paleo.state.p (var x))))))
(rule $RepBinary (county) (ConstantFn (lambda x (fb:paleo.county.p (var x))))))
(rule $RepBinary (country) (ConstantFn (lambda x (fb:paleo.cc.p (var x))))))
(rule $RepBinary (rock formation) (ConstantFn (lambda x (fb:paleo.formation.p (var x))))))

(rule $RepBinary (class) (ConstantFn (lambda x (fb:paleo.class.p (var x)))))

### Representative examples

(rule $RepSet (occurrences with class gastropoda) (ConstantFn (and (@type fb:paleo.occ) (fb:paleo.class.p fb:paleo.gastropoda))))
(rule $RepSet (country of occurrences) (ConstantFn ((@R fb:paleo.cc.p) (@type fb:paleo.occ))))
(rule $RepSet (number of occurrences) (ConstantFn (count (@type fb:paleo.occ))))

### Final

(rule $Set ($PHRASE) (SimpleLexiconFn))
(rule $Set (countries where snails have been found) (ConstantFn (!fb:paleo.cc.p (fb:paleo.class.p fb:paleo.gastropoda))))
(rule $ROOT ($Set) (IdentityFn))
