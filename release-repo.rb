#!/usr/bin/ruby

# Release the repository files (copies the necessary files to the output
# repository).  Necessary files given by a release.txt file, which contains the
# list of regex patterns which should be released.
# Usage:
#   ./release-repo.rb release

$releasePath = 'sempre-public'
$release = ARGV.index('release'); ARGV.delete_if { |x| x == 'release' }

if not File.exists?($releasePath)
  puts "#{$releasePath} doesn't exist"
  exit 1
end

def releaseCodeFile(inFileName, outFileName)
  # Remove everything that's between BEGIN_HIDE AND END_HIDE
  write = true
  outFile = File.open(outFileName, 'w')
  File.open(inFileName).each do |line|
    if line.include? "BEGIN_HIDE"
      write = false
    elsif line.include? "END_HIDE"
      write = true
    elsif write
      outFile.write(line)
    end
  end
  outFile.close()
end

def release(path)
  if File.file?(path)
    puts path
    if $release
      # Copy it to the repository
      destPath = $releasePath + '/' + path
      system "mkdir -p #{File.dirname(destPath)}" or exit 1
      system "cp -a #{path} #{destPath}" or exit 1  # To preserve permissions
      releaseCodeFile(path, destPath)
      system "cd #{File.dirname(destPath)} && git add -f #{File.basename(destPath)}" or exit 1
    end
    return
  end

  releaseFilesPath = path + '/release.files'
  if File.exists?(releaseFilesPath)
    files = IO.readlines(releaseFilesPath).map { |line|
      file = line.sub(/#.*/, '').sub(/^\s+/, '').sub(/\s+$/, '')
      # Could expand into a pattern (e.g., *.java)
      file.size > 0 ? Dir[path + '/' + file].map { |x| File.basename(x) } : []
    }.flatten
  else
    files = Dir.entries(path).delete_if { |x| x == '.' or x == '..' }
  end
  files.each { |x| release(path + '/' + x) }
end

release('.')
